/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.spi;

import java.util.Set;

import fr.lip6.msr4j.analysis.formats.DataFormat;

/**
 * Interface for analysis services, to advertise various administrative info.
 * The way each service is run is up to the developer's implementation class.
 * @author lom
 * 
 */
public interface AnalysisService {
	/**
	 * The name of a service is used as its unique id.
	 * 
	 * @return
	 */
	String getHumanReadableName();

	/**
	 * Returns a human-readable description of the service.
	 * 
	 * @return
	 */
	String getHumanReadableDescription();

	/**
	 * <p>
	 * A service will have only one provider (and at least one)
	 * </p>
	 * .
	 * <p>
	 * If a service is extended, then it must have a different name (at least
	 * extended) that makes it unique.
	 * </p>
	 * 
	 * @return
	 */
	Analyser getProvider();

	/**
	 * Sets the provider of this service.
	 * 
	 * @param p
	 */
	void setProvider(Analyser p);

	/**
	 * Used to activate/deactivate the service.
	 * 
	 * @param activation
	 * @return true if the activation state was effectively changed, false
	 *         otherwise
	 */
	boolean setActivation(boolean activation);

	/**
	 * Is this service activated?
	 * 
	 * @return
	 */
	boolean isActivated();

	/**
	 * In some situations, the activation state of the service object could not
	 * be changed (whether from activation to desactivation or the other way
	 * round), for instance when it is actively computing a service, or when the
	 * registry is register a service.
	 * 
	 * @return
	 */
	boolean couldChangeActivationState();

	/**
	 * In some situations, the service could not be activated (mainly
	 * deactivation -> activation).
	 * 
	 * @return
	 */
	boolean isActivable();

	/**
	 * What are the data formats supported by this service?
	 * <p>
	 * <strong>Note:<strong> this set is different from the one used to accept
	 * requested formats for a subsequent service invocation.
	 * </p>
	 * 
	 * @return the set of supported formats by this service.
	 */
	Set<DataFormat> getSupportedFormats();

	/**
	 * <p>
	 * Used to ask the service the data formats for which the output should be
	 * emitted.
	 * </p>
	 * <p>
	 * Any prior requested set of formats (from a previous service invocation)
	 * will be cleared before <em>ingesting</em> the provided ones as argument.
	 * </p>
	 * 
	 * @param df
	 */
	void setDataFormats(Set<DataFormat> df);

	/**
	 * <p>
	 * Returns a swallow copy of this service, so that the user can read its
	 * properties and set some config.
	 * </p>
	 * <p>
	 * Name and description are copied, along with ref to set of
	 * {@link Analyser} providers. The set of requested formats is duplicated,
	 * since another instance of the service may be asked a different (sub) set
	 * of formats for its output.
	 * </p>
	 * <p>
	 * Current activation state and state change guard ARE ALSO RETAINED in the
	 * clone.
	 * </p>
	 * <p>
	 * If you do not wish to keep the current activation state and state change
	 * guard, then use {@link #swallowCopyWithoutState()} instead
	 * <p>
	 * 
	 * @returna swallow copy of this object (ref to its provider is just copied,
	 *          provider's object is not duplicated).
	 * @see #swallowCopyWithoutState()
	 */
	AnalysisService swallowCopy();

	/**
	 * <p>
	 * Returns a swallow copy of this service, so that the user can read its
	 * properties and set some config.
	 * </p>
	 * <p>
	 * Name and description are copied, along with ref to set of
	 * {@link Analyser} providers. The set of requested formats is duplicated,
	 * since another instance of the service may be asked a different (sub) set
	 * of formats for its output.
	 * </p>
	 * <p>
	 * Current activation state and state change guard are NOT RETAINED.
	 * </p>
	 * <p>
	 * If you do wish to keep the current activation state and state change
	 * guard, then use {@link #swallowCopy()} instead.
	 * <p>
	 * 
	 * @return swallow copy of this object without internal state activation and
	 *         state change guard (ref to its provider is just copied,
	 *         provider's object is not duplicated).
	 * @see #swallowCopyWithoutState()
	 */
	AnalysisService swallowCopyWithoutState();

	/**
	 * <p>
	 * The intent of this method, if any service implementation class is willing
	 * to provide a representative instance to the registry, is to set the
	 * internal state change guard of this instance to non-changeable, upon
	 * invocation.
	 * </p>
	 * <p>
	 * Thus, a registered representative of a service is not intended to
	 * actually actively running any service. It can emit clones whose state can
	 * be changed.
	 * </p>
	 * 
	 * @param registry
	 * @see #couldChangeActivationState
	 * @see #swallowCopyWithoutState()
	 */
	void setRegistry(Object registry);

}
