/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.spi;

import java.util.Set;

/**
 * 
 * @author lom
 * @deprecated
 * @unused
 */
public interface ListServicesCommandInterface {

	/**
	 * Lists all available service providers, their services and the supported
	 * data formats for each service.
	 * <p>
	 * Outputs the result on stdout (which is suitable for remote invocations).
	 * </p>
	 * <p>
	 * Outputs <code>None</code> if there is no service resulting from
	 * processing this invocation. It is potentially followed by some stack
	 * trace or explanation, but first line in this case is always None.
	 * </p>
	 */
	void services();

	/**
	 * <p>
	 * Lists services (and the corresponding providers) supporting one of the
	 * given data formats (disjonction)
	 * </p>
	 * <p>
	 * Outputs the result on stdout (which is suitable for remote invocations).
	 * </p>
	 * <p>
	 * Outputs <code>None</code> if there is no service resulting from
	 * processing this invocation. It is potentially followed by some stack
	 * trace or explanation, but first line in this case is always None.
	 * </p>
	 * 
	 * @param dataFormats
	 */
	void services4df(Set<String> dataFormats);

	/**
	 * <p>
	 * Initial intent as for {@link #services4df(Set)} but for all and only all
	 * data formats in the given set (conjonction)
	 * </p>
	 * <p>
	 * Outputs the result on stdout (which is suitable for remote invocations).
	 * </p>
	 * <p>
	 * Outputs <code>None</code> if there is no service resulting from
	 * processing this invocation. It is potentially followed by some stack
	 * trace or explanation, but first line in this case is always None.
	 * </p>
	 * 
	 * @param dataFormats
	 * @see #services4df(Set)
	 */
	void services4dfs(Set<String> dataFormats);

	/**
	 * <p>
	 * Lists all services as {@link #services()}, but dumps the output in the
	 * local file whose path is provided. This program must have the necessary
	 * access rights to the indicated path.
	 * </p>
	 * <p>
	 * If this command is remotely invoked, beware that you won't get the file
	 * back... Use {@link #services()} instead or an additional mechanism to
	 * retrieve the content of the file.
	 * </p>
	 * <p>
	 * Primary output is in JSON, but other implementing classes may provide
	 * alternative formats.
	 * </p>
	 * <p>
	 * Output file is <code>empty</code> if there is no service resulting from
	 * processing this invocation.It is potentially followed by some stack trace
	 * or explanation, but first line in this case is always None.
	 * </p>
	 * 
	 * @param uri
	 * @see #services()
	 */
	void servicesDump(String path);

	/**
	 * <p>
	 * Same initial behavior as {@link #services4df(Set)}, but this time dumps
	 * the output in a local file, the path of which is provided. This program
	 * must have the necessary access rights to the indicated path.
	 * </p>
	 * <p>
	 * Retains the services that support at least one of the provided formats
	 * (disjonction).
	 * </p>
	 * <p>
	 * Primary output is in JSON, but other implementing classes may provide
	 * alternative formats.
	 * </p>
	 * <p>
	 * First line of output file is <code>None</code> if there is no service
	 * resulting from processing this invocation. It is potentially followed by
	 * some stack trace or explanation, but first line in this case is always
	 * None.
	 * </p>
	 * 
	 * @param dataFormat
	 * @param path
	 * @see #services4df(Set)
	 * @see #servicesDump(String)
	 */
	void services4dfDump(Set<String> dataFormats, String path);

	/**
	 * <p>
	 * Same initial behavior as {@link #services4dfs(Set)} but this time dumps
	 * the the output in a local file whose path is provided. This program must
	 * have the necessary access rights to the indicated path.
	 * </p>
	 * <p>
	 * Retains the services that support ALL the provided formats (conjonction).
	 * </p>
	 * <p>
	 * Primary output is in JSON, but other implementing classes may provide
	 * alternative formats.
	 * </p>
	 * <p>
	 * First line of output file is <code>None</code> if there is no service
	 * resulting from processing this invocation. It is potentially followed by
	 * some stack trace or explanation, but first line in this case is always
	 * None.
	 * </p>
	 * 
	 * @param dataFormats
	 * @see #services4dfs(Set)
	 * @see #services4dfDump(String, String)
	 */
	void services4dfsDump(Set<String> dataFormats, String path);

}
