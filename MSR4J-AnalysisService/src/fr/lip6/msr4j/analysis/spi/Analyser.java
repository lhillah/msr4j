/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.spi;

import java.util.Set;

import fr.lip6.msr4j.analysis.formats.DataFormat;

/**
 * This interface must be implemented by repository analysis service providers
 * (i.e repository analysers) to advertise the analysis services they propose,
 * in addition to {@link IRepositoryAnalyser}. These services will then be
 * discovered automatically.
 * 
 * @author lom
 * @see IRepositoryAnalyser
 */
public interface Analyser {

	/**
	 * Returns a human-readable name of the service provider.
	 * 
	 * @return
	 */
	String getProviderHumanReadableName();

	/**
	 * Returns a human-readable name of the service provider description.
	 * 
	 * @return
	 */
	String getProviderHumanReadableDescription();

	/**
	 * Returns a modifiable collection of modifiable copies of the provided
	 * services, so that the user can read their properties and configure them
	 * for activation/deactivation, and send them back.
	 * 
	 * @return the set of provided services, null if there is no provided
	 *         service
	 * @see #activateServices(Set)
	 * @see #activateServicesByName(Set)
	 */
	Set<AnalysisService> getProvidedServices();

	/**
	 * The (human-readable) name of a service is its Id.
	 * 
	 * @param name
	 *            id of the service to retrieve
	 * @return the reference to the service, the name of which is provided, null
	 *         if there is no such service
	 */
	AnalysisService getService(String name);

	/**
	 * Activates and run the services, the names of which are provided.
	 * 
	 * @param name
	 *            the set of services to activate
	 * @return false is at least one of them cannot be activated, true if all
	 *         can be activated and run
	 */
	boolean activateServicesByName(Set<String> name);

	/**
	 * Activate and run the set of provided service swallow copies. These copies
	 * are just used to read the configuration for the actual service objects.
	 * Names are used as keys.
	 * 
	 * @param services
	 * @return false is at least one of them cannot be activated, true if all
	 *         can be activated and run boolean
	 *         activateServices(Set<AnalysisService> services);
	 * 
	 *         /** Activates and run all services.
	 * 
	 * @return false is at least one them cannot be activated, true otherwise
	 */
	boolean activateAllServices();

	/**
	 * Best effort strategy: activates and run services that can be activated.
	 * 
	 * @param services
	 *            the services to activate and run
	 * 
	 * @return false if none could be activated. If you do not wish to support
	 *         this API, just return false.
	 */
	boolean activatePossibleServices(Set<AnalysisService> services);

	/**
	 * Best effort strategy: activates and run services that can be activated.
	 * 
	 * @param services
	 *            the services to activate and run
	 * 
	 * @return false if none could be activated. If you do not wish to support
	 *         this API, just return false.
	 */
	boolean activatePossibleServicesByName(Set<String> services);
	/**
	 * Returns services supporting one of the given formats (disjonction).
	 * @param formats
	 * @return services supporting one of the given formats (OR); empty set if there is none;
	 * <code>null</code> if something went wrong in processing the request
	 */
	Set<AnalysisService> getServicesSupportingFormats(Set<DataFormat> formats);
	
	/**
	 * Returns services supporting all formats (conjonction).
	 * @param formats
	 * @return services supporting all given formats (AND); empty set if there is none;
	 * <code>null</code> if something went wrong in processing the request
	 */
	Set<AnalysisService> getServicesSupportingAllFormats(Set<DataFormat> formats);
}
