/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.spi;

import java.util.Set;
import java.util.concurrent.Callable;

import fr.lip6.msr4j.analysis.formats.DataFormat;
import fr.lip6.msr4j.datamodel.nodes.Repository;

/**
 * Each repository analyser must implement this interface. A repository is
 * passed on by the registry before the analyser is run. A repository analyser
 * class must provide a way to clone it, so that the registered representative
 * is used by the registry to clone each repository analyser for parallel runs.
 * 
 * A repository analyser object must register itself by the
 * {@link RepoAnalyserRegistry} upon creation, in the default constructor.
 * Registry provides an API to find out if the object is already registered, to
 * avoid registration loops. It applies this procedure also internally for each registration.
 * 
 * Each analyser is assumed to return a set of java.io.Files, according to its
 * prior configuration. See for example, {@link RepositoryAnalyser}
 * 
 * @author lom
 * @see RepoAnalyserRegistry
 */
public interface IRepositoryAnalyser extends Callable<Set<java.io.File>> {

	/**
	 * Provides the repository to analyse to the implementing class.
	 * 
	 * @param r
	 */
	void setRepository(Repository r);

	/**
	 * Returns a clone of an analyser, according to the analyser's own
	 * implementation. Each repo analyser class must implement this API
	 * carefully.
	 * 
	 * @return a clone of an analyser.
	 */
	IRepositoryAnalyser cloneAnalyser();
	
	/**
	 * Each repository analyser must be aware of the data format(s) asked for the outputs,
	 * so that it can tell if it does not support that data format.
	 * @param df the data format to handle (e.g csv, json).
	 */
	void addDataFormat(DataFormat df);
}
