/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.formats;
/**
 * Possible data formats that could be currently handled,
 * for outputting analysis data.
 * 
 * @author lom
 *
 */
public enum DataFormat {
	CSV("csv"), JSON("json"), DATATABLE("datatable"), XML("xml");
	
	private DataFormat(String n) {
		this.name = n;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	private final String name;
}
