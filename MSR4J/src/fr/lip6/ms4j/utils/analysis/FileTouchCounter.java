/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.ms4j.utils.analysis;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import fr.lip6.msr4j.datamodel.nodes.File;
import fr.lip6.msr4j.datamodel.relations.TouchRelation;

/**
 * Counts the number of times a file was touched (add, modify, copy, delete) per committer.
 * @author lom
 *
 */
public final class FileTouchCounter implements Callable<Map<String, Map<String, Long>>> {
	private final File fi;

	public FileTouchCounter(File f) {
		this.fi = f;
	}

	@Override
	public Map<String, Map<String, Long>> call() throws Exception {
		Map<String, Long> committerCount = new HashMap<String, Long>();
		Iterable<TouchRelation> it = fi.getTouches();
		String svnid;
		for (TouchRelation tr : it) {
			svnid = tr.getSourceNode().getId();
			/*
			 * if (svnid == null) { svnid = "unknown committer!"; }
			 */
			// Should not happen (in principle)
			if (committerCount.get(svnid) != null) {
				committerCount.put(svnid, tr.getCount() + committerCount.get(svnid));
			} else {
				committerCount.put(svnid, tr.getCount());
			}
		}
		Map<String, Map<String, Long>> fileTouchesCount = new HashMap<String, Map<String, Long>>();
		fileTouchesCount.put(fi.getPath(), committerCount);
		return fileTouchesCount;
	}
}