/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.ms4j.utils.analysis;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * CSV Data formatter for the counts of touches on a file per committer.
 * format: file name , int,int,int...
 * @author lom
 * 
 */
public class FileTouchCSVDataFormatter implements Callable<String> {

	private final Map<String, Map<String, Long>> fileTouchesCount;
	private final Set<String> cms;
	private final Set<String> extensions;

	public FileTouchCSVDataFormatter(Map<String, Map<String, Long>> ftc, Set<String> cms, Set<String> extensions) {
		this.fileTouchesCount = ftc;
		this.cms = cms;
		this.extensions = extensions;
	}

	@Override
	public String call() throws Exception {
		StringBuilder sb = new StringBuilder();
		int sindex, eindex;
		String filename;
		for (Entry<String, Map<String, Long>> ent : fileTouchesCount.entrySet()) {
			sindex = ent.getKey().lastIndexOf("/");
			filename = ent.getKey().substring(sindex + 1);
			eindex = filename.lastIndexOf(".") != -1 ? filename.lastIndexOf(".") : 0;
			if(extensions.contains("all") || extensions.contains(filename.substring(eindex))) {
				sb.append(ent.getKey().substring(sindex + 1) + " ");
				for (String s : cms) {
					if (ent.getValue().get(s) != null) {
						sb.append("," + ent.getValue().get(s));
					} else {
						sb.append(",0");
					}
				}
			} 
		}
		return sb.toString();
	}
}
