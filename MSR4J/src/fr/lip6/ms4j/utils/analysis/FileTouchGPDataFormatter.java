/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.ms4j.utils.analysis;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

/**
 * GnuPlot Data formatter for the counts of touches on a file per committer.
 * @author lom
 *
 */
public final class FileTouchGPDataFormatter implements Callable<String> {
	private Map<String, Map<String, Long>> fileTouchesCount;

	public FileTouchGPDataFormatter(Map<String, Map<String, Long>> va) {
		this.fileTouchesCount = va;
	}

	@Override
	public String call() throws Exception {
		StringBuilder sb = new StringBuilder();
		int index;
		for (Entry<String, Map<String, Long>> ent : fileTouchesCount.entrySet()) {
			index = ent.getKey().lastIndexOf("/");
			for (Entry<String, Long> en : ent.getValue().entrySet()) {
				sb.append(ent.getKey().substring(index + 1) + " " + en.getKey() + " " + en.getValue() + RepositoryAnalyser.NL);
			}
		}
		return sb.toString();
	}
}