/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.ms4j.utils.analysis;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;


import fr.lip6.msr4j.datamodel.nodes.File;
import fr.lip6.msr4j.datamodel.nodes.Repository;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

/**
 * Provides various analysis methods on a repository.
 * Up to now, provides a heatmap data generator into csv.
 * @author lom
 *
 */
public class RepositoryAnalyser {
	public static final String NL = "\n";
	private final Logger logger = MSR4JLogger.getLogger(this.getClass().getCanonicalName());
	private Repository rep;
	/**
	 * Set of Committers found in this repository.
	 */
	private static Set<String> cms;

	public RepositoryAnalyser(Repository r) {
		this.setRepository(r);
	}

	/**
	 * @return the rep
	 */
	public Repository getRepository() {
		return rep;
	}

	/**
	 * @param rep
	 *            the rep to set
	 */
	public void setRepository(Repository rep) {
		this.rep = rep;
	}

	/**
	 * Creates data for a heatmap to be drawn by Gnuplot. For each file in the
	 * repository, we have the number of commits each committer did that touch
	 * that file.
	 * 
	 * Each row is separated by a blank line.
	 * 
	 * @param path
	 *            the filesystem path to the output file
	 * @param extensions the set of files extensions to consider for data generation, 
	 * 'all' if all files must be considered 
	 * @return the reference to the created file containing the data, null if
	 *         something went wrong (see log).
	 */
	public java.io.File createTouchHeatMapData(String path, Set<String> extensions) {
		java.io.File result = null;
		Long ostart, start, oend, end;
		int nbFiles;
		// <File path <svnid, numCommits on file>>
		final List<Callable<Map<String, Map<String, Long>>>> partition = new ArrayList<Callable<Map<String, Map<String, Long>>>>();
		Iterable<fr.lip6.msr4j.datamodel.nodes.File> it = rep.getFiles();
		nbFiles = 0;
		for (File f : it) {
			partition.add(new FileTouchCounter(f));
			nbFiles++;
		}
		logger.info("Number of files in the repository: {}", nbFiles);
		ExecutorService executorPool = ConcurrencyConfig.getFixedThreadPoolExecutor();
		logger.info("Start counting touches for each file in the repository: {}", rep.getName());
		ostart = start = System.nanoTime();
		try {
			final List<Future<Map<String, Map<String, Long>>>> values = executorPool.invokeAll(partition, ConcurrencyConfig.ALLOWED_EXEC_TIME100,
					TimeUnit.SECONDS);
			Map<String, Map<String, Long>> allFiles = new HashMap<String, Map<String,Long>>();
			Map<String, Map<String, Long>> aVal;
			Map<String, Long> cCount;
			cms = new ConcurrentSkipListSet<String>();
			long maxTouch = 0L;
			for (Future<Map<String, Map<String, Long>>> value : values) {
				aVal = value.get();
				for (Entry<String, Map<String, Long>> ent : aVal.entrySet()) {
					if(allFiles.get(ent.getKey()) == null) {
						allFiles.put(ent.getKey(), ent.getValue());
					} else {
						cCount = allFiles.get(ent.getKey());
						for (Entry<String, Long> en: ent.getValue().entrySet()) {
							if (cCount.get(en.getKey()) == null) {
								cCount.put(en.getKey(), en.getValue());
							} else {
								cCount.put(en.getKey(), en.getValue() + cCount.get(en.getKey()));
							}
							maxTouch = Math.max(maxTouch, cCount.get(en.getKey()));
							cms.add(en.getKey());
						}
					}
				}
			}
			end = System.nanoTime();
			logger.info("Finished counting touches for each file. \n\tTime taken: {}.", (end - start) / ConcurrencyConfig.NANO);
			logger.info("Max touch count in the repository: {}", maxTouch);
			final List<Callable<String>> spartition = new ArrayList<Callable<String>>();
			for (Entry<String, Map<String, Long>> value : allFiles.entrySet()) {
				aVal = new HashMap<String, Map<String,Long>>();
				aVal.put(value.getKey(), value.getValue());
				spartition.add(new FileTouchCSVDataFormatter(aVal, cms, extensions));
			}
			logger.info("Start building plot data.");
			start = System.nanoTime();
			final List<Future<String>> dataValues = executorPool.invokeAll(spartition, ConcurrencyConfig.ALLOWED_EXEC_TIME10, TimeUnit.SECONDS);
			StringBuilder content = new StringBuilder();
			// Prepare CSV header
			content.append("Name ");
			for (String s : cms) {
				content.append("," + s);
			}
			content.append(NL);
			// Append data		
			for (Future<String> d : dataValues) {
				if (!d.get().isEmpty())
					content.append(d.get() + NL);
			}
			//content.append("e"); This is for gnuplot
			end = System.nanoTime();
			logger.info("Finished building plot data. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
			byte[] buffer = content.toString().getBytes();
			logger.info("Start writing down data into destination file: {}.", path);
			start = System.nanoTime();
			RandomAccessFile raf = new RandomAccessFile(path, "rw");
			FileChannel rwChannel = raf.getChannel();
			ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, buffer.length);
			wrBuf.put(buffer);
			rwChannel.close();
			raf.close();
			end = System.nanoTime();
			logger.info("Finished writing down data into destination file. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
			result = new java.io.File(path);
		} catch (InterruptedException | ExecutionException | IOException ex) {
			logger.error(ex.getMessage());
			logger.error(Arrays.toString(ex.getStackTrace()));
		} finally {
			executorPool.shutdown();
		}
		oend = end = System.nanoTime();
		logger.info("End of heat map data generation. Total time taken: {}.", (oend - ostart) / ConcurrencyConfig.NANO);
		return result;
	}
}
