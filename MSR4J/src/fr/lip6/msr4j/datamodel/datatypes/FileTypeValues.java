/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.datatypes;

public enum FileTypeValues {
	CODE("Code"), BUILD("Build"), UI("Ui"), I18N("i18n"), DOC("Doc"), DEV_DOC(
			"Dev-Doc"), PACKAGE("Package"), IMG("Image"), MMEDIA("Multimedia"), UNKNOWN(
			"Unknown");

	private FileTypeValues(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	private final String name;

}
