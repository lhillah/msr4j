/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.datatypes;

import fr.lip6.msr4j.datamodel.relations.AddRelation;
import fr.lip6.msr4j.datamodel.relations.AttachedToRelation;
import fr.lip6.msr4j.datamodel.relations.CoCommitterRelation;
import fr.lip6.msr4j.datamodel.relations.ContainRelation;
import fr.lip6.msr4j.datamodel.relations.CopToRelation;
import fr.lip6.msr4j.datamodel.relations.CopyRelation;
import fr.lip6.msr4j.datamodel.relations.DeleteRelation;
import fr.lip6.msr4j.datamodel.relations.FileTypeRelation;
import fr.lip6.msr4j.datamodel.relations.ForkRelation;
import fr.lip6.msr4j.datamodel.relations.ForkedIntoRelation;
import fr.lip6.msr4j.datamodel.relations.HoldAdminRoleRelation;
import fr.lip6.msr4j.datamodel.relations.ImpactRelation;
import fr.lip6.msr4j.datamodel.relations.InvolveRelation;
import fr.lip6.msr4j.datamodel.relations.ModifyRelation;
import fr.lip6.msr4j.datamodel.relations.ParentRelation;
import fr.lip6.msr4j.datamodel.relations.PropagateRelation;
import fr.lip6.msr4j.datamodel.relations.RegisteredInRelation;
import fr.lip6.msr4j.datamodel.relations.RenToRelation;
import fr.lip6.msr4j.datamodel.relations.RenameRelation;
import fr.lip6.msr4j.datamodel.relations.RepByRelation;
import fr.lip6.msr4j.datamodel.relations.ReplaceRelation;
import fr.lip6.msr4j.datamodel.relations.SameProjectAsRelation;
import fr.lip6.msr4j.datamodel.relations.TopLevelProjectRelation;
import fr.lip6.msr4j.datamodel.relations.TouchRelation;

public enum RelationType {
	ADD(AddRelation.ADD), MODIFY(ModifyRelation.MODIFY), DELETE(
			DeleteRelation.DELETE), RENAME(RenameRelation.RENAME), COPY(
			CopyRelation.COPY), REPLACE(ReplaceRelation.REPLACE), IMPACT(
			ImpactRelation.IMPACT),

	REP_BY(RepByRelation.REP_BY), COP_TO(CopToRelation.COP_TO), REN_TO(
			RenToRelation.REN_TO), FILE_TYPE(FileTypeRelation.FILE_TYPE),

	CONTAIN(ContainRelation.CONTAIN), FORKED_INTO(
			ForkedIntoRelation.FORKED_INTO), ATTACHED_TO(
			AttachedToRelation.ATTACHED_TO), PARENT(ParentRelation.PARENT),

	INVOLVE(InvolveRelation.INVOLVE), TOP_LEVEL(
			TopLevelProjectRelation.TOP_LEVEL),

	PROPAGATE(PropagateRelation.PROPAGATE), TOUCH(TouchRelation.TOUCH), FORK(
			ForkRelation.FORK), CO_COMMITTER(CoCommitterRelation.CO_COMMITTER), SAME_PROJECT_AS(
			SameProjectAsRelation.SAME_PROJECT_AS), REGISTERED_IN(
			RegisteredInRelation.REGISTERED_IN), HOLD_ADMIN_ROLE(
			HoldAdminRoleRelation.HOLD_ADMIN_ROLE);

	public String getName() {
		return name;
	}

	private RelationType(String name) {
		this.name = name;
	}

	private final String name;
}
