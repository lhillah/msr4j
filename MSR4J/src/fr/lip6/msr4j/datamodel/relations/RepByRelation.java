/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.relations;

import com.tinkerpop.frames.EdgeFrame;
import com.tinkerpop.frames.InVertex;
import com.tinkerpop.frames.OutVertex;
import com.tinkerpop.frames.Property;

import fr.lip6.msr4j.datamodel.nodes.File;

/**
 * Models the REP_BY (replaced by) relationship between two File nodes.
 * 
 * @author lom
 * 
 */
public interface RepByRelation extends EdgeFrame {

	String REP_BY = "Rep_By";

	@OutVertex
	File getSourceNode();

	@InVertex
	File getTargetNode();

	@Property("replacement-revision")
	void setCopyRevision(long r);

	@Property("replacement-revision")
	long getCopyRevision();
}
