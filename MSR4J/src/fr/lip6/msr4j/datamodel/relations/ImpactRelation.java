/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.relations;

import com.tinkerpop.frames.EdgeFrame;
import com.tinkerpop.frames.InVertex;
import com.tinkerpop.frames.OutVertex;
import com.tinkerpop.frames.Property;

import fr.lip6.msr4j.datamodel.nodes.Commit;
import fr.lip6.msr4j.datamodel.nodes.Repository;

/**
 * Models the IMPACT relationship between a Commit node and a Repository node.
 * 
 * @author lom
 * 
 */
public interface ImpactRelation extends EdgeFrame {

	String IMPACT = "Impact";

	@OutVertex
	Commit getSourceNode();

	@InVertex
	Repository getTargetNode();

	@Property("is-branching")
	void setBranching(boolean b);

	@Property("is-branching")
	boolean isBranching();

	@Property("is-forking")
	int setIsForking(boolean f);

	@Property("is-forking")
	boolean isForking();

	@Property("is-tagging")
	int setIsTagging(boolean t);

	@Property("is-tagging")
	boolean isTagging();
}
