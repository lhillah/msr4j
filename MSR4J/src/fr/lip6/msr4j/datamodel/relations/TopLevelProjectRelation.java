/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.relations;

import com.tinkerpop.frames.EdgeFrame;
import com.tinkerpop.frames.InVertex;
import com.tinkerpop.frames.OutVertex;

import fr.lip6.msr4j.datamodel.nodes.Project;

/**
 * Models the TOP_LEVEL relationship between two Project nodes.
 * 
 * @author lom
 * 
 */
public interface TopLevelProjectRelation extends EdgeFrame {
	String TOP_LEVEL = "Top_Level";

	@OutVertex
	Project getSourceNode();

	@InVertex
	Project getTargetNode();
}
