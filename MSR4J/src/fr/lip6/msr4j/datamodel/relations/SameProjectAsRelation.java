/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.relations;

import com.tinkerpop.frames.EdgeFrame;
import com.tinkerpop.frames.InVertex;
import com.tinkerpop.frames.OutVertex;

import fr.lip6.msr4j.datamodel.nodes.Committer;

/**
 * Models the SAME_PROJECT_AS relationship between two Committer nodes.
 * 
 * @author lom
 * 
 */
public interface SameProjectAsRelation extends EdgeFrame {

	String SAME_PROJECT_AS = "Same_Project_As";

	@OutVertex
	Committer getSourceNode();

	@InVertex
	Committer getTargetNode();

}
