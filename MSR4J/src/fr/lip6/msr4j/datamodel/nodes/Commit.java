/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.nodes;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Adjacency;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;

import fr.lip6.msr4j.datamodel.relations.AddRelation;
import fr.lip6.msr4j.datamodel.relations.CopyRelation;
import fr.lip6.msr4j.datamodel.relations.DeleteRelation;
import fr.lip6.msr4j.datamodel.relations.ImpactRelation;
import fr.lip6.msr4j.datamodel.relations.ModifyRelation;
import fr.lip6.msr4j.datamodel.relations.PropagateRelation;
import fr.lip6.msr4j.datamodel.relations.RenameRelation;
import fr.lip6.msr4j.datamodel.relations.ReplaceRelation;

public interface Commit extends VertexFrame {
	@Property("revision")
	void setRevision(String id);

	@Property("revision")
	String getRevision();

	@Property("date")
	void setDate(long id);

	@Property("date")
	long getDate();

	@Property("message")
	void setMessage(String message);

	@Property("message")
	String getMessage();

	@Property("actual-contributor")
	void setAuthorId(String authorId);

	@Property("actual-contributor")
	String getAuthorId();

	// TODO - Adjacency to Repository
	@Adjacency(label = ImpactRelation.IMPACT, direction = Direction.OUT)
	Repository getRepository();

	@Adjacency(label = ImpactRelation.IMPACT, direction = Direction.OUT)
	void setRepository(Repository r);

	@Adjacency(label = ImpactRelation.IMPACT, direction = Direction.OUT)
	Repository addRepository();

	// TODO - Adjacency to File via Add - Hierarchy in Frames 2.4.0 (to be
	// released...) will
	// help handle these case better.
	@Adjacency(label = AddRelation.ADD, direction = Direction.OUT)
	Iterable<File> getAddedFiles();

	@Adjacency(label = AddRelation.ADD, direction = Direction.OUT)
	void addFile(File f);

	@Adjacency(label = AddRelation.ADD, direction = Direction.OUT)
	void addFiles(Iterable<File> files);

	// TODO - Adjacency to File via Modify
	@Adjacency(label = ModifyRelation.MODIFY, direction = Direction.OUT)
	Iterable<File> getModifiedFiles();

	@Adjacency(label = ModifyRelation.MODIFY, direction = Direction.OUT)
	void modifyFile(File f);

	@Adjacency(label = ModifyRelation.MODIFY, direction = Direction.OUT)
	void modifyFiles(Iterable<File> files);

	// TODO - Adjacency to File via Delete
	@Adjacency(label = DeleteRelation.DELETE, direction = Direction.OUT)
	Iterable<File> getDeletedFiles();

	@Adjacency(label = DeleteRelation.DELETE, direction = Direction.OUT)
	void deleteFile(File f);

	@Adjacency(label = DeleteRelation.DELETE, direction = Direction.OUT)
	void deletFiles(Iterable<File> files);

	// TODO - Adjacency to File via Rename
	@Adjacency(label = RenameRelation.RENAME, direction = Direction.OUT)
	Iterable<File> getRenamedFiles();

	@Adjacency(label = RenameRelation.RENAME, direction = Direction.OUT)
	void renameFile(File f);

	@Adjacency(label = RenameRelation.RENAME, direction = Direction.OUT)
	void renamedFiles(Iterable<File> files);

	// TODO - Adjacency to File via Copy
	@Adjacency(label = CopyRelation.COPY, direction = Direction.OUT)
	Iterable<File> getCopiedFiles();

	@Adjacency(label = CopyRelation.COPY, direction = Direction.OUT)
	void copyFile(File f);

	@Adjacency(label = CopyRelation.COPY, direction = Direction.OUT)
	void copyFiles(Iterable<File> files);

	// TODO - Adjacency to File via Replace
	@Adjacency(label = ReplaceRelation.REPLACE, direction = Direction.OUT)
	Iterable<File> getReplacedFiles();

	@Adjacency(label = ReplaceRelation.REPLACE, direction = Direction.OUT)
	void replaceFile(File f);

	@Adjacency(label = ReplaceRelation.REPLACE, direction = Direction.OUT)
	void replaceFiles(Iterable<File> files);

	// TODO - Adjacency from Committer
	@Adjacency(label = PropagateRelation.PROPAGATE, direction = Direction.IN)
	Committer getCommitter();

}
