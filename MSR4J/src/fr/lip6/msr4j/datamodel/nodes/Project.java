/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.nodes;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Adjacency;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;

import fr.lip6.msr4j.datamodel.relations.AttachedToRelation;
import fr.lip6.msr4j.datamodel.relations.HoldAdminRoleRelation;
import fr.lip6.msr4j.datamodel.relations.InvolveRelation;
import fr.lip6.msr4j.datamodel.relations.TopLevelProjectRelation;

public interface Project extends VertexFrame {

	@Property("name")
	void setName(String name);

	@Property("name")
	String getName();

	@Property("description")
	void setDescription(String desc);

	@Property("description")
	String getDescription();

	@Property("web-site")
	void setWebSiteUrl(String webSiteURL);

	@Property("web-site")
	String getWebSiteUrl();

	// TODO - Adjacency to Committer
	@Adjacency(label = InvolveRelation.INVOLVE, direction = Direction.OUT)
	Iterable<Committer> getCommitters();

	@Adjacency(label = InvolveRelation.INVOLVE, direction = Direction.OUT)
	void addCommitter(Committer c);

	@Adjacency(label = InvolveRelation.INVOLVE, direction = Direction.OUT)
	void addCommitters(Iterable<Committer> committers);

	// TODO - Adjacency from Repository
	@Adjacency(label = AttachedToRelation.ATTACHED_TO, direction = Direction.IN)
	Iterable<Repository> getRepositories();

	// TODO - Adjacency from Committer
	@Adjacency(label = HoldAdminRoleRelation.HOLD_ADMIN_ROLE, direction = Direction.IN)
	Iterable<Committer> getAdminCommitters();

	// TODO - Self Adjacency with TOP-LEVEL
	@Adjacency(label = TopLevelProjectRelation.TOP_LEVEL, direction = Direction.OUT)
	Repository getTopLevelProject();

	@Adjacency(label = TopLevelProjectRelation.TOP_LEVEL, direction = Direction.OUT)
	void setTopLevelProject(Project p);

	@Adjacency(label = TopLevelProjectRelation.TOP_LEVEL, direction = Direction.OUT)
	Repository addTopLevelProject();

	@Adjacency(label = TopLevelProjectRelation.TOP_LEVEL, direction = Direction.IN)
	Iterable<Project> getSubProjects();

}
