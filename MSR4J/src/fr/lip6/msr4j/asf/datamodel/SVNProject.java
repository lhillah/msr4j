/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.datamodel;

import java.util.HashSet;
import java.util.Set;

/**
 * Models ASF SVN Projects. It has a set of committers. This concept is
 * different from the one of ASF Project (official appellation).
 * 
 * This class represents the concept of Repository in the MSR4J Data Model.
 * 
 * @see ASFProject
 * @author lom
 * 
 */
public final class SVNProject {
	/**
	 * Name (or id) of the SVN project.
	 */
	private String name;

	private String type;

	private String uri;

	private boolean isRoot;

	/**
	 * The committers in this project.
	 */
	private Set<ASFCommitter> committers;

	public SVNProject(String name) {
		this.name = name;
		committers = new HashSet<ASFCommitter>();
	}

	public boolean addCommitter(ASFCommitter c) {
		for (ASFCommitter cm : committers) {
			cm.addColleague(c);
		}
		return this.committers.add(c);
	}

	/**
	 * Simplest degree centrality
	 * 
	 * @return
	 */
	public int degree() {
		return this.committers.size();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SVNProject other = (SVNProject) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	public Set<ASFCommitter> getCommitters() {
		return committers;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getUri() {
		return uri;
	}

	public boolean hasCommitter(ASFCommitter c) {
		return this.committers.contains(c);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	public boolean isRoot() {
		return isRoot;
	}

	public void setCommitters(Set<ASFCommitter> committers) {
		this.committers = committers;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String toString() {
		return "Project [name=" + name + ", committers=" + committers + "]";
	}

	// TODO URI
	// TODO TYPE
	// TODO isRoot
	// TODO Official project
}
