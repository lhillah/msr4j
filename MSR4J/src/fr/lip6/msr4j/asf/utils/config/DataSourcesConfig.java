/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.utils.config;

public class DataSourcesConfig {

	private static String committerIndexBase = "http://people.apache.org/";

	private static String asfMemeberIndexBase = "http://www.apache.org/foundation/";

	private static String projectsIndexBase = "http://projects.apache.org/";

	private static String committerIndexRemote = "http://people.apache.org/committer-index.html";
	private static String committerIndexLocal = "dataSources/ASF-SVN-Committer-index.html";
	private static String committerBySVNProjectLocal = "dataSources/ASF-Committers-by-SVNProject.html";
	private static String committerListLocal = "dataSources/ListofASFCommitters.html";

	private static String projectIndexAlphaLocal = "dataSources/ASFProjects-Index_Alphabetical.html";
	private static String projectIndexListLocal = "dataSources/ASFProjects-Index_Listing.html";
	private static String projectIndexPmcLocal = "dataSources/Index_Project_Management_Committee.html";
	private static String asfFoundationProjectLocal = "dataSources/FoundationProject.html";

	private static String asfMemberProjectLocal = "dataSources/ASFMembersNProjects.html";

	private static String svnIndexBase = "http://svn.apache.org/repos/asf/";
	private static String svnIndexLocal = "dataSources/asf-SVNrepositories.html";
	private static String svnProperties = "resources/svnurls.properties";
	private static String svnBasePath = "resources/repositories/";

	private static String trSelector = "tbody";

	public static final String UTF8 = "UTF8";

	/**
	 * Constructor.
	 */
	private DataSourcesConfig() {
		super();
	}

	/**
	 * @return the committerIndexBase
	 */
	public static String getCommitterIndexBase() {
		return committerIndexBase;
	}

	/**
	 * @param committerIndexBase
	 *            the committerIndexBase to set
	 */
	public static void setCommitterIndexBase(String committerIndexBase) {
		DataSourcesConfig.committerIndexBase = committerIndexBase;
	}

	/**
	 * @return the asfMemeberIndexBase
	 */
	public static String getAsfMemeberIndexBase() {
		return asfMemeberIndexBase;
	}

	/**
	 * @param asfMemeberIndexBase
	 *            the asfMemeberIndexBase to set
	 */
	public static void setAsfMemeberIndexBase(String asfMemeberIndexBase) {
		DataSourcesConfig.asfMemeberIndexBase = asfMemeberIndexBase;
	}

	/**
	 * @return the projectsIndexBase
	 */
	public static String getProjectsIndexBase() {
		return projectsIndexBase;
	}

	/**
	 * @param projectsIndexBase
	 *            the projectsIndexBase to set
	 */
	public static void setProjectsIndexBase(String projectsIndexBase) {
		DataSourcesConfig.projectsIndexBase = projectsIndexBase;
	}

	/**
	 * @return the committerIndexRemote
	 */
	public static String getCommitterIndexRemote() {
		return committerIndexRemote;
	}

	/**
	 * @param committerIndexRemote
	 *            the committerIndexRemote to set
	 */
	public static void setCommitterIndexRemote(String committerIndexRemote) {
		DataSourcesConfig.committerIndexRemote = committerIndexRemote;
	}

	/**
	 * @return the committerIndexLocal
	 */
	public static String getCommitterIndexLocal() {
		return committerIndexLocal;
	}

	/**
	 * @param committerIndexLocal
	 *            the committerIndexLocal to set
	 */
	public static void setCommitterIndexLocal(String committerIndexLocal) {
		DataSourcesConfig.committerIndexLocal = committerIndexLocal;
	}

	/**
	 * @return the committerBySVNProjectLocal
	 */
	public static String getCommitterBySVNProjectLocal() {
		return committerBySVNProjectLocal;
	}

	/**
	 * @param committerBySVNProjectLocal
	 *            the committerBySVNProjectLocal to set
	 */
	public static void setCommitterBySVNProjectLocal(
			String committerBySVNProjectLocal) {
		DataSourcesConfig.committerBySVNProjectLocal = committerBySVNProjectLocal;
	}

	/**
	 * @return the committerListLocal
	 */
	public static String getCommitterListLocal() {
		return committerListLocal;
	}

	/**
	 * @param committerListLocal
	 *            the committerListLocal to set
	 */
	public static void setCommitterListLocal(String committerListLocal) {
		DataSourcesConfig.committerListLocal = committerListLocal;
	}

	/**
	 * @return the projectIndexAlphaLocal
	 */
	public static String getProjectIndexAlphaLocal() {
		return projectIndexAlphaLocal;
	}

	/**
	 * @param projectIndexAlphaLocal
	 *            the projectIndexAlphaLocal to set
	 */
	public static void setProjectIndexAlphaLocal(String projectIndexAlphaLocal) {
		DataSourcesConfig.projectIndexAlphaLocal = projectIndexAlphaLocal;
	}

	/**
	 * @return the projectIndexListLocal
	 */
	public static String getProjectIndexListLocal() {
		return projectIndexListLocal;
	}

	/**
	 * @param projectIndexListLocal
	 *            the projectIndexListLocal to set
	 */
	public static void setProjectIndexListLocal(String projectIndexListLocal) {
		DataSourcesConfig.projectIndexListLocal = projectIndexListLocal;
	}

	/**
	 * @return the projectIndexPmcLocal
	 */
	public static String getProjectIndexPmcLocal() {
		return projectIndexPmcLocal;
	}

	/**
	 * @param projectIndexPmcLocal
	 *            the projectIndexPmcLocal to set
	 */
	public static void setProjectIndexPmcLocal(String projectIndexPmcLocal) {
		DataSourcesConfig.projectIndexPmcLocal = projectIndexPmcLocal;
	}

	/**
	 * @return the asfMemberProjectLocal
	 */
	public static String getAsfMemberProjectLocal() {
		return asfMemberProjectLocal;
	}

	/**
	 * @param asfMemberProjectLocal
	 *            the asfMemberProjectLocal to set
	 */
	public static void setAsfMemberProjectLocal(String asfMemberProjectLocal) {
		DataSourcesConfig.asfMemberProjectLocal = asfMemberProjectLocal;
	}

	/**
	 * @return the svnIndexBase
	 */
	public static String getSvnIndexBase() {
		return svnIndexBase;
	}

	/**
	 * @param svnIndexBase
	 *            the svnIndexBase to set
	 */
	public static void setSvnIndexBase(String svnIndexBase) {
		DataSourcesConfig.svnIndexBase = svnIndexBase;
	}

	/**
	 * @return the svnIndexLocal
	 */
	public static String getSvnIndexLocal() {
		return svnIndexLocal;
	}

	/**
	 * @param svnIndexLocal
	 *            the svnIndexLocal to set
	 */
	public static void setSvnIndexLocal(String svnIndexLocal) {
		DataSourcesConfig.svnIndexLocal = svnIndexLocal;
	}

	/**
	 * @return the svnProperties
	 */
	public static String getSvnProperties() {
		return svnProperties;
	}

	/**
	 * @param svnProperties
	 *            the svnProperties to set
	 */
	public static void setSvnProperties(String svnProperties) {
		DataSourcesConfig.svnProperties = svnProperties;
	}

	/**
	 * @return the svnBasePath
	 */
	public static String getSvnBasePath() {
		return svnBasePath;
	}

	/**
	 * @param svnBasePath
	 *            the svnBasePath to set
	 */
	public static void setSvnBasePath(String svnBasePath) {
		DataSourcesConfig.svnBasePath = svnBasePath;
	}

	/**
	 * @return the trSelector
	 */
	public static String getTrSelector() {
		return trSelector;
	}

	/**
	 * @param trSelector
	 *            the trSelector to set
	 */
	public static void setTrSelector(String trSelector) {
		DataSourcesConfig.trSelector = trSelector;
	}

	/**
	 * @return the asfFoundationProjectLocal
	 */
	public static String getAsfFoundationProjectLocal() {
		return asfFoundationProjectLocal;
	}

	/**
	 * @param asfFoundationProjectLocal
	 *            the asfFoundationProjectLocal to set
	 */
	public static void setAsfFoundationProjectLocal(
			String asfFoundationProjectLocal) {
		DataSourcesConfig.asfFoundationProjectLocal = asfFoundationProjectLocal;
	}
}
