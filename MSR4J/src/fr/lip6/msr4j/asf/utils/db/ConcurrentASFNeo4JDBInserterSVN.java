/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.utils.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import fr.lip6.msr4j.asf.datamodel.ASFCommitter;
import fr.lip6.msr4j.asf.datamodel.ASFProject;
import fr.lip6.msr4j.asf.datamodel.ASFProjectsCommitters;
import fr.lip6.msr4j.asf.datamodel.ASFTopLevelProject;
import fr.lip6.msr4j.asf.datamodel.SVNProject;
import fr.lip6.msr4j.datamodel.datatypes.RelationType;
import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.datatypes.VEProperties;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.db.ConcurrentNeo4JBatchInserterSVN;
import fr.lip6.msr4j.utils.parsers.MimeTypeDetector;
import fr.lip6.msr4j.utils.parsers.SVNLogEntryActionsParser;
import fr.lip6.msr4j.utils.parsers.SVNLogEntryParser;
import fr.lip6.msr4j.utils.parsers.SVNParser;

public class ConcurrentASFNeo4JDBInserterSVN extends ConcurrentNeo4JBatchInserterSVN<ASFProjectsCommitters> {

	private static ConcurrentMap<ASFTopLevelProject, Vertex> tlpVertex;
	private static ConcurrentMap<ASFProject, Vertex> asfpVertex;
	private static ConcurrentMap<SVNProject, Vertex> svnVertex;
	private static ConcurrentMap<ASFCommitter, Vertex> cmVertex;

	private static ConcurrentMap<String, ASFCommitter> committers;

	public ConcurrentASFNeo4JDBInserterSVN(String dbPath, Set<Entry<Object, Object>> svnProp, PropertiesManager wcProp, PropertiesManager credProp) {
		super(dbPath, svnProp, wcProp, credProp);
		tlpVertex = new ConcurrentHashMap<ASFTopLevelProject, Vertex>();
		asfpVertex = new ConcurrentHashMap<ASFProject, Vertex>();
		svnVertex = new ConcurrentHashMap<SVNProject, Vertex>();
		cmVertex = new ConcurrentHashMap<ASFCommitter, Vertex>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populateDB(ASFProjectsCommitters projects) {
		nbVertexCreated = 0L;
		long ostart, start, end, oend;
		committers = new ConcurrentHashMap<String, ASFCommitter>(projects.getCommittersMap());
		logger.info("Starting batch insertion. Inserting nodes from HTML pages.");
		ostart = start = System.nanoTime();
		insertTLPProjectsSVNProjects(projects);
		end = System.nanoTime();
		logger.info("Finished inserting nodes from HTML pages. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
		logger.info("Inserting nodes from SVN logs.");
		start = System.nanoTime();
		insertSVNSCMData(projects);
		oend = end = System.nanoTime();
		logger.info("Finished inserting nodes from SVN logs. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
		logger.info("End of batch insertion. Database must be shut down. Total time taken: {}.", (oend - ostart) / ConcurrencyConfig.NANO);
		logger.info("Number of vertices created: {}", nbVertexCreated);
	}

	/**
	 * {@inheritDoc}
	 */
	public void insertSVNSCMData(ASFProjectsCommitters projects) {
		Vertex svnV, commitV, cmV, fileV;
		@SuppressWarnings("rawtypes")
		Collection logEntries;
		SVNLogEntry logEntry;
		mtd = new MimeTypeDetector();
		logger.info("Parsing SVNs logs.");
		for (Entry<Object, Object> ent : svnProp) {

			svnparser = new SVNParser();
			svnparser.setUrl(ent.getValue().toString());
			svnparser.setRepositoryName(ent.getKey().toString());
			svnparser.setUserName(credprop.getProperty(ent.getKey().toString() + "-user"));
			svnparser.setPassword(credprop.getProperty(ent.getKey().toString() + "-passwd"));
			try {
				svnparser.openRemoteHttpRepository();
				svnparser.checkRepositoryPath();
				wcc = svnparser.getWCClient();
			} catch (SVNException ex) {
				logger.error(ex.getMessage());
				logger.error(Arrays.toString(ex.getStackTrace()));
				continue;
			}
			svnV = svnVertex.get(projects.getSVNProjectByName(ent.getKey().toString()));
			if (svnV == null) {
				logger.error("Unknow SVN Project: {} ", ent.getValue().toString());
				continue;
			} else {
				try {
					logEntries = svnparser.getLogEntries(new String[] { "" }, null, 0, -1, true, true);
				} catch (SVNException e2) {
					logger.error(e2.getMessage());
					logger.error(Arrays.toString(e2.getStackTrace()));
					continue;
				}
				logger.info("Parsing logs of SVN: {}.", ent.getValue().toString());
				// <LogEntry, <FilePath, <Committer id, Long>>>
				final List<Callable<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> lePartition = new ArrayList<Callable<Map<SVNLogEntry, Map<String, Map<String, Long>>>>>();
				// <LogEntry, <FilePath, RelationType>>
				final List<Callable<Map<SVNLogEntry, Map<String, RelationType>>>> laPartition = new ArrayList<Callable<Map<SVNLogEntry, Map<String, RelationType>>>>();
				for (@SuppressWarnings("rawtypes")
				Iterator entries = logEntries.iterator(); entries.hasNext();) {
					SVNLogEntry entry = (SVNLogEntry) entries.next();
					lePartition.add(new SVNLogEntryParser(entry));
					laPartition.add(new SVNLogEntryActionsParser(entry));
				}
				List<Future<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> leValues = null;
				List<Future<Map<SVNLogEntry, Map<String, RelationType>>>> laValues = null;
				try {
					leValues = executorPool.invokeAll(lePartition, ConcurrencyConfig.ALLOWED_EXEC_TIME100, TimeUnit.SECONDS);
					laValues = executorPool.invokeAll(laPartition, ConcurrencyConfig.ALLOWED_EXEC_TIME100, TimeUnit.SECONDS);

					if (leValues != null && laValues != null) {
						Iterator<Future<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> leIt;
						Iterator<Future<Map<SVNLogEntry, Map<String, RelationType>>>> laIt;
						Map<SVNLogEntry, Map<String, Map<String, Long>>> logEMap;
						Map<String, Map<String, Long>> filesCommittersCount = new HashMap<String, Map<String, Long>>();
						// Files touch counts by committers
						Map<String, Long> committersCount;
						Map<SVNLogEntry, Map<String, RelationType>> logATMap;
						Edge e;
						Long count;
						logger.info("Counting committers touch counts for all files.");
						for (leIt = leValues.iterator(); leIt.hasNext();) {
							logEMap = leIt.next().get();
							for (Entry<SVNLogEntry, Map<String, Map<String, Long>>> logEMapEnt : logEMap.entrySet()) {
								logEntry = logEMapEnt.getKey();
								for (Entry<String, Map<String, Long>> filesEnt : logEMapEnt.getValue().entrySet()) {

									committersCount = filesCommittersCount.get(filesEnt.getKey());
									if (committersCount == null) {
										committersCount = new HashMap<String, Long>();
										filesCommittersCount.put(filesEnt.getKey(), committersCount);
									}
									for (Entry<String, Long> cmsCount : filesEnt.getValue().entrySet()) {
										count = committersCount.get(cmsCount.getKey());
										if (count == null) {
											count = 0L;
										}
										count += cmsCount.getValue();
										committersCount.put(cmsCount.getKey(), count);
									}
								}
							}
						}
						leIt = leValues.iterator();
						laIt = laValues.iterator();
						logger.info("Creating Commit vertices, setting touch relationships between committers and files.");
						for (leIt.hasNext(); laIt.hasNext();) {
							logEMap = leIt.next().get();
							logATMap = laIt.next().get();
							for (Entry<SVNLogEntry, Map<String, Map<String, Long>>> logEMapEnt : logEMap.entrySet()) {
								logEntry = logEMapEnt.getKey();
								cmV = cmVertex.get(committers.get(logEntry.getAuthor()));
								commitV = createCommitVertex(logEntry);
								nbVertexCreated++;
								for (Entry<String, Map<String, Long>> filesEnt : logEMapEnt.getValue().entrySet()) {
									fileV = createFileVertex(filesEnt.getKey(), logEntry.getRevision());
									nbVertexCreated++;
									e = addEdge(svnV, fileV, RelationType.CONTAIN.getName());
									if (cmV != null) {
										e = addEdge(cmV, fileV, RelationType.TOUCH.getName());
										setEdgeProperty(e, VEProperties.COUNT.getName(),
												filesCommittersCount.get(filesEnt.getKey()).get(logEntry.getAuthor()));
										e = addEdge(cmV, commitV, RelationType.PROPAGATE.getName());

									} else {
										logger.warn("Committer {} of revision {} is unknown. Could not set propagate and touch relationships on"
												+ "file {}.", logEntry.getAuthor(), logEntry.getRevision(), filesEnt.getKey());
									}
									e = addEdge(commitV, fileV, logATMap.get(logEntry).get(filesEnt.getKey()).getName());
								}
								// Handles Impact relation - TODO later find
								// first
								// occurrence of branch or tag creation
								e = addEdge(commitV, svnV, RelationType.IMPACT.toString());
							}
						}
						logger.info("Setting Cop_to and Rep_By relationships.");
						for (@SuppressWarnings("rawtypes")
						Iterator entries = logEntries.iterator(); entries.hasNext();) {
							logEntry = (SVNLogEntry) entries.next();
							for (@SuppressWarnings("unchecked")
							Iterator<Entry<String, SVNLogEntryPath>> iterator = (Iterator<Entry<String, SVNLogEntryPath>>) logEntry.getChangedPaths()
									.entrySet().iterator(); iterator.hasNext();) {
								Entry<String, SVNLogEntryPath> en = iterator.next();
								establishCopyToRepByRelation(en);
							}
						}
					} else if (leValues != null) {
						logger.error("Returned global result from parsing all log actions types is null");
					} else {
						logger.error("Returned global result from parsing all log entries is null");
					}

				} catch (InterruptedException iex) {
					logger.error(iex.getMessage());
					logger.error(Arrays.toString(iex.getStackTrace()));
				} catch (ExecutionException e1) {
					logger.error(e1.getMessage());
					logger.error(Arrays.toString(e1.getStackTrace()));
				}
				logger.info("Finished processing all logs of SVN {}. How many processed: {}.", ent.getValue().toString(), leValues.size());
			}
		}
		logger.info("Finished processing all SVNs logs. How many processed: {}.", svnProp.size());
	}

	/**
	 * Inserts TLPs, projects and SVN projects
	 * 
	 * @param projects
	 */
	private void insertTLPProjectsSVNProjects(final ASFProjectsCommitters projects) {
		Collection<ASFTopLevelProject> prs = projects.getTopLevelProjects();
		Vertex tlp, p, svnp, cm;
		Edge e;
		Set<ASFProject> asfPjs;
		Set<SVNProject> svnPjs;
		Set<ASFCommitter> cms;

		logger.info("Populating the DB with ASF top-level projects.");
		for (final ASFTopLevelProject aTlp : prs) {
			tlp = createASFTLPVertex(aTlp);
			nbVertexCreated++;
			// Set Projects and TOP_LEVEL relationship between them and this TLP
			asfPjs = aTlp.getProjects();
			logger.info("Populating the DB with ASF projects of TLP {}.", aTlp.getName());
			for (final ASFProject asfp : asfPjs) {
				p = createASFProjectVertex(asfp);
				nbVertexCreated++;
				e = addEdge(p, tlp, RelationType.TOP_LEVEL.getName());
				// Set SVN Projects and relationship between them and this
				// project
				svnPjs = asfp.getSvnprojects();
				logger.info("Populating the DB with ASF SVN projects of Project {}.", asfp.getName());
				for (final SVNProject svnpr : svnPjs) {
					svnp = createSCMProjectVertex(svnpr);
					nbVertexCreated++;
					svnVertex.put(svnpr, svnp);
					e = addEdge(svnp, p, RelationType.ATTACHED_TO.getName());
					// Set Committers and relationship between them and this
					// Repository
					cms = svnpr.getCommitters();
					logger.info("Populating the DB with ASF committers of SVN Project {}.", svnpr.getName());
					for (final ASFCommitter asfcm : cms) {
						cm = createCommitterVertex(asfcm);
						nbVertexCreated++;
						logger.info("Added Committer {} --> {}.", asfcm.getSvnId(), asfcm.getName());
						e = addEdge(cm, svnp, RelationType.REGISTERED_IN.getName());
						// Consequently, also set the involve relationship
						// between committer and project this
						// Repository is attached to
						e = addEdge(p, cm, RelationType.INVOLVE.getName());
					}
				}
			}
		}

		// Recycle those Projects that were not included previously
		logger.info("Recycling Projects that were not handled previously.");
		for (ASFProject pr : projects.getASFProjects()) {
			if (!asfpVertex.containsKey(pr)) {
				logger.info("Recycling Project {}.", pr.getName());
				p = createASFProjectVertex(pr);
			}
		}

		// Recycle those SVN Projects that were not included previously
		logger.info("Recycling SVN Projects that were not handled previously.");
		for (SVNProject sp : projects.getSVNProjects()) {
			if (!svnVertex.containsKey(sp)) {
				logger.info("Recycling SVN Project {}.", sp.getName());
				p = createSCMProjectVertex(sp);
			}
		}

		// Recycle those Committers that were not included previously
		logger.info("Recycling Committers that were not handled previously.");
		for (ASFCommitter c : projects.getCommitters()) {
			if (!cmVertex.containsKey(c)) {
				logger.info("Recycling Committer {} --> {}.", c.getSvnId(), c.getName());
				p = createCommitterVertex(c);
				for (SVNProject sp : c.getProjects()) {
					e = addEdge(p, svnVertex.get(sp), RelationType.REGISTERED_IN.getName());
				}
			}
		}
		// Handle Same_Project_As relationship
		logger.info("Handling same_project_as relationship...");
		for (Entry<ASFCommitter, Vertex> c : cmVertex.entrySet()) {
			for (ASFCommitter col : c.getKey().getColleagues()) {
				e = addEdge(c.getValue(), cmVertex.get(col), RelationType.SAME_PROJECT_AS.getName());
			}
		}

		// Handle hold-admin-role
		logger.info("Handling admin roles...");
		for (final Entry<ASFCommitter, Vertex> ent : cmVertex.entrySet()) {
			ASFTopLevelProject tlpr = ent.getKey().getChairedPmc();
			if (tlpr != null) {
				tlp = tlpVertex.get(tlpr);
				if (tlp != null) {
					e = addEdge(ent.getValue(), tlp, RelationType.HOLD_ADMIN_ROLE.getName());
					setEdgeProperty(e, VEProperties.ROLE_NAME.getName(), ent.getKey().getAdminRole());
					logger.info("Admin role for member {}  --> {}", ent.getKey().getName(), tlp.getProperty(VEProperties.NAME.getName()));
				}
			}
		}
	}

	private Vertex createASFTLPVertex(ASFTopLevelProject tlp) {
		Vertex v = graph.addVertex(null);
		setVertexProperty(v, VEProperties.NAME.getName(), tlp.getName());
		setVertexProperty(v, VEProperties.DECRIPTION.getName(), tlp.getTitle());
		setVertexProperty(v, VEProperties.WEB_SITE.getName(), tlp.getUrl());
		tlpVertex.put(tlp, v);
		return v;
	}

	private Vertex createASFProjectVertex(ASFProject pr) {
		Vertex v = graph.addVertex(null);
		setVertexProperty(v, VEProperties.NAME.getName(), pr.getName());
		setVertexProperty(v, VEProperties.DECRIPTION.getName(), pr.getDescription());
		asfpVertex.put(pr, v);
		return v;
	}

	@Override
	public Vertex createSCMProjectVertex(Object o) {
		SVNProject svnp = (SVNProject)o;
		Vertex v = graph.addVertex(null);
		setVertexProperty(v, VEProperties.NAME.getName(), svnp.getName());
		setVertexProperty(v, VEProperties.TYPE.getName(), RepositoryType.SVN.getName());
		setVertexProperty(v, VEProperties.URI.getName(), svnp.getUri());
		setVertexProperty(v, VEProperties.IS_ROOT.getName(), svnp.isRoot());
		// TODO : check URI, and isRoot from repository mining
		return v;
	}

	@Override
	public Vertex createCommitterVertex(Object o) {
		ASFCommitter asfcm  = (ASFCommitter)o;
		Vertex v = graph.addVertex(null);
		setVertexProperty(v, VEProperties.SVN_ID.getName(), asfcm.getSvnId());
		setVertexProperty(v, VEProperties.NAME.getName(), asfcm.getName());
		setVertexProperty(v, VEProperties.IS_ASF_MEMBER.getName(), asfcm.isApacheMember());
		setVertexProperty(v, VEProperties.IS_ASF_EMERITUS_MEMBER.getName(), asfcm.isEmeritusMember());
		setVertexProperty(v, VEProperties.EMAIL.getName(), asfcm.getEmail());
		setVertexProperty(v, VEProperties.PERSO_WEB_PAGE.getName(), asfcm.getWebSite());
		cmVertex.put(asfcm, v);
		committers.put(asfcm.getSvnId(), asfcm);
		return v;
	}
}
