/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;

public interface IVertexEdgeFactory {
	
	Vertex createSCMProjectVertex(Object svnp);
	
	Vertex createSCMProjectVertex(String name, RepositoryType type, String uri, boolean isRoot);
	
	Vertex createCommitterVertex(Object cm);
	
	Vertex createCommitterVertex(String svnID, String name, String email, String webpage);
	
	Vertex createCommitVertex(Object logEntry); 
	
	Vertex createCommitVertex(long revision, long date, String message, String actualContributor); 
	
	Vertex createFileVertex(String en, long revision);
	
	void establishCopyToRepByRelation(Object en);
	
	Edge addEdge(Vertex from, Vertex to, String relation);
	
	void setVertexProperty(Vertex v, String propName, Object propValue);
	
	void setEdgeProperty(Edge e, String propName, Object propValue);
}
