/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.slf4j.Logger;

import cern.colt.Arrays;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

/**
 * Root class for handling basic DB services.
 * 
 * @author lom
 */
public abstract class DBHandler<T> implements DBServices {
	protected final Logger logger = MSR4JLogger.getLogger(this.getClass()
			.getCanonicalName());
	protected String dbPath;

	public DBHandler(String dbPath) {
		this.dbPath = dbPath;
	}

	@Override
	public void setDBPath(String dbPath) {
		this.dbPath = dbPath;
	}

	@Override
	public String getDBPath() {
		return this.dbPath;
	}

	@Override
	public void removeDb() {
		logger.info("Removing the Graph DB directory...");
		try {
			FileUtils.deleteDirectory(new File(dbPath));
		} catch (IOException e) {
			logger.error(e.getMessage());
			logger.error(Arrays.toString(e.getStackTrace()));
		}
		logger.info("Graph DB directory removed.");
	}

	public void registerShutdownHook(final GraphDatabaseService gdb) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				gdb.shutdown();
				logger.info("Registered shutdown hook.");
			}
		});
	}

	public abstract void populateDB(T data);

}
