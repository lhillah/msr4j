/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;

import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.frames.FramedGraph;
import com.tinkerpop.frames.FramedGraphFactory;
import com.tinkerpop.frames.modules.gremlingroovy.GremlinGroovyModule;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.datatypes.VEProperties;
import fr.lip6.msr4j.datamodel.nodes.Repository;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

public final class SequentialNeo4JDBReader {
	private final Logger logger = MSR4JLogger.getLogger(SequentialNeo4JDBReader.class.getCanonicalName());
	private String dbpath;
	private FramedGraphFactory factory;
	private FramedGraph<Neo4jGraph> graph;

	public SequentialNeo4JDBReader(String path) {
		this.dbpath = path;
	}

	/**
	 * Returns all occurrences of Repository nodes having the specified name.
	 * @param name the name of the repository nodes that are being looked up
	 * @return the set of all repository nodes having the specified name
	 * @see #getRepository(String)
	 */
	public Set<Repository> getAllRepositories(String name) {
		Set<Repository> repos = new HashSet<Repository>();
		Iterable<Repository> rps = graph.getVertices(VEProperties.NAME.getName(), name, Repository.class);
		if (rps != null) {	
			for (Repository r : rps) {
				repos.add(r);
			}
		}
		return repos;
	}

	/**
	 * Returns the set of all occurrences of Repository nodes having the names in the argument (array)
	 * @param names the array of repository names that are being looked up
	 * @return the set of all occurrence of Repository node for each name in the argument.
	 * @see #getRepositories(String[])
	 */
	public Set<Repository> getAllRepositories(String[] names) {
		Set<Repository> repos = new HashSet<Repository>();
		for (String s : names) {
			repos.addAll(getAllRepositories(s)); 
		}
		return repos;
	}
	
	/**
	 * Returns the set of first occurrences of Repository node having the names in the argument (array).
	 * @param names the array of repository names that are being looked up
	 * @return the set of first occurrence of Repository node for each name in the argument.
	 * @see #getAllRepositories(String[])
	 */
	public Set<Repository> getRepositories(String[] names) {
		Set<Repository> repos = new HashSet<Repository>();
		Repository r;
		for (String s : names) {
			if ((r=getRepository(s)) != null) {
				repos.add(r); 
			}
		}
		return repos;
	}

	/**
	 * Returns the first occurrence of Repository node having the specified name.
	 * It is advised that the name be unique (thus used as id at the domain-specific level).
	 * 
	 * Node id is a long and automatically created by the underlying framework, so do not
	 * expect equals and hashcode to work on your domain-specific attributes.
	 *  
	 * @param name the name of the repository which is being looked up
	 * @return a reference to the repository node, null otherwise
	 * @see #getAllRepositories(String)
	 */
	public Repository getRepository(String name) {
		Repository result = null;
		Iterable<Repository> rps = graph.getVertices(VEProperties.NAME.getName(), name, Repository.class);
		if (rps != null) {
			for (Repository r : rps) {
				if (r.getName().equalsIgnoreCase(name)) {
					result = r;
					logger.info("Found looked-for Repository: {}", r.getName());
					break;
				}
			}
			if(result==null) {
				logger.info("Could not find Repository: {} ", name);
			}
		}
		return result;
	}

	public void openDB() {
		factory = new FramedGraphFactory(new GremlinGroovyModule());
		graph = factory.create(new Neo4jGraph(this.dbpath));
		logger.info("Graph DB opened.");
	}
	
	public void shutDownDb() {
		graph.shutdown();
		logger.info("Graph DB shut down.");
	}

	/**
	 * Returns all Repository nodes in the db back end.
	 * @return the set of all Repository nodes
	 * @see SequentialNeo4JDBReader#getRepositoriesOfType(RepositoryType)
	 */
	public Set<Repository> getAllRepositories() {
		Set<Repository> repos = new HashSet<Repository>();
		for (RepositoryType t : RepositoryType.values()) {
			repos.addAll(getRepositoriesOfType(t));
		}
		return repos;
	}
	
	/**
	 * Returns all Repository nodes of the repository types specified as argument.
	 * @param rt the repository type of the nodes to be retrieved
	 * @return the set of all Repository nodes of the given type
	 * @see #getAllRepositories()
	 */
	public Set<Repository> getRepositoriesOfType(RepositoryType rt){
		Set<Repository> repos = new HashSet<Repository>();
		Iterable<Repository> rps = graph.getVertices(VEProperties.TYPE.getName(), rt.getName(), Repository.class);
		for (Repository r : rps) {
			repos.add(r);
		}
		return repos;
	}
}
