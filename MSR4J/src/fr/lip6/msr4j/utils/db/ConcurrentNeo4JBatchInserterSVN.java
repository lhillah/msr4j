/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.tika.mime.MimeTypeException;
import org.neo4j.cypher.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.logging.BufferingLogger;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.wc.SVNInfo;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCClient;

import cern.colt.Arrays;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.neo4j.batch.Neo4jBatchGraph;

import fr.lip6.msr4j.datamodel.datatypes.RelationType;
import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.datatypes.SVNLogActionType;
import fr.lip6.msr4j.datamodel.datatypes.VEProperties;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.parsers.MimeTypeDetector;
import fr.lip6.msr4j.utils.parsers.SVNLogEntryActionsParser;
import fr.lip6.msr4j.utils.parsers.SVNLogEntryParser;
import fr.lip6.msr4j.utils.parsers.SVNParser;
import fr.lip6.msr4j.utils.repositories.CustomSVNInfoHandler;
import fr.lip6.msr4j.utils.strings.Printer;

public class ConcurrentNeo4JBatchInserterSVN<T> extends DBHandler<T> implements IVertexEdgeFactory {
	/**
	 * SVN repos vertices associated with their names.
	 */
	protected static ConcurrentMap<String, Vertex> svnVertex;
	/**
	 * Committer vertices associated with their SVN ids.
	 */
	protected static ConcurrentMap<String, Vertex> cmVertex;
	/**
	 * Files vertices associated with their (unique) paths.
	 */
	protected static ConcurrentMap<String, Vertex> fileVertex;
	/**
	 * Files types vertices associated with their (unique) mimetype
	 */
	protected static ConcurrentMap<String, Vertex> fileTypesVertex;
	/**
	 * SVN log entries, associated to touched files' path and respective commit
	 * action.
	 */
	protected static ConcurrentMap<SVNLogEntry, Map<String, RelationType>> commitActions;
	protected static Graph graph;
	/**
	 * To count the number of vertices created.
	 */
	protected static long nbVertexCreated;
	/**
	 * To handle a pool of threads for tasks that can be partitioned.
	 */
	protected final ExecutorService executorPool;
	/**
	 * Neo4J database service handler.
	 */
	protected GraphDatabaseService graphDb;

	protected ExecutionEngine engine;

	/**
	 * Set of SVN names and their URLs <name, url>
	 */
	protected Set<Entry<Object, Object>> svnProp;
	protected SVNParser svnparser;
	protected MimeTypeDetector mtd;
	/**
	 * SVNs working copies' local paths in the file system.
	 */
	protected PropertiesManager wcprop;
	/**
	 * SVNs credentials.
	 */
	protected PropertiesManager credprop;
	/**
	 * To work with entries in the local working copies of the repositories.
	 */
	protected SVNWCClient wcc;
	/**
	 * SVN Info handler.
	 */
	protected CustomSVNInfoHandler myHandler;

	private ConcurrentNeo4JBatchInserterSVN(String dbPath) {
		super(dbPath);
		executorPool = ConcurrencyConfig.getFixedThreadPoolExecutor();
	}

	/**
	 * Constructor.
	 * 
	 * @param dbPath
	 *            (Local) Path to the Neo4J DB.
	 * @param svnProp
	 *            Set of pairs where SVN URLs are listed (name - URL) as Strings
	 * @param wcprop
	 *            Properties manager, handling local paths of the SVNs working
	 *            copies
	 * @param credprop
	 *            Properties manager, handling SVNs credentials
	 */
	public ConcurrentNeo4JBatchInserterSVN(String dbPath, Set<Entry<Object, Object>> svnProp, PropertiesManager wcprop, PropertiesManager credprop) {
		this(dbPath);
		this.svnProp = svnProp;
		this.wcprop = wcprop;
		this.credprop = credprop;
		myHandler = new CustomSVNInfoHandler();
		fileVertex = new ConcurrentHashMap<String, Vertex>();
		fileTypesVertex = new ConcurrentHashMap<String, Vertex>();
		svnVertex = new ConcurrentHashMap<String, Vertex>();
		cmVertex = new ConcurrentHashMap<String, Vertex>();
		commitActions = new ConcurrentHashMap<SVNLogEntry, Map<String, RelationType>>();
	}

	/**
	 * Cleans the Neo4J back end DB, without deleting it.
	 */
	@Override
	public void cleanDb() {
		logger.info("Cleaning Graph DB...");
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(this.dbPath);
		engine = new ExecutionEngine(graphDb, new BufferingLogger());
		registerShutdownHook(graphDb);
		if (graphDb != null) {
			engine.execute("START n = node(*), ref = node(0)  WHERE n<>ref DELETE n");
		}
		graphDb.shutdown();
		logger.info("Cleaned Graph DB.");
	}

	/**
	 * Creates a Neo4J Batch Graph back end instance for batch insertion.
	 */
	@Override
	public void createDb() {
		logger.info("Creating Graph DB...");
		graph = new Neo4jBatchGraph(dbPath);
		logger.info("Graph DB created.");
	}

	@Override
	public void shutDownDb() {
		graph.shutdown();
		logger.info("Graph DB shut down.");
	}

	@Override
	public Object beginTransaction() {
		throw new UnsupportedOperationException("This method is not supported in the core implementation");
	}

	@Override
	public void closeSuccessfullTransaction(Object o) {
		throw new UnsupportedOperationException("This method is not supported in the core implementation");
	}

	/**
	 * Entry point to start populating the DB back end. This method should be
	 * overriden if you have a very specific case study.
	 * 
	 * @param data
	 *            User-defined data container (not used in the core
	 *            implementation)
	 */
	@Override
	public void populateDB(T data) {
		nbVertexCreated = 0L;
		long ostart, start, end, oend;
		ostart = start = System.nanoTime();
		logger.info("Starting batch insertion.");
		logger.info("Start nodes and relationships insertion into DB back end (according to MSR4J core data model) from SVN logs.");
		start = System.nanoTime();
		insertSVNSCMData(data);
		oend = end = System.nanoTime();
		logger.info("Finished nodes and relationships insertion. \n\tTime taken: {}.", (end - start) / ConcurrencyConfig.NANO);
		logger.info("End of batch insertion. Database must be shut down. \n\tTotal time taken: {}.", (oend - ostart) / ConcurrencyConfig.NANO);
		logger.info("Number of vertices created: {}", nbVertexCreated);
	}

	/**
	 * Inserts data from SVN logs. This method should be overriden if you have a
	 * very specific case study.
	 * 
	 * @param data
	 *            User-defined data container (not used in the core
	 *            implementation)
	 */
	public void insertSVNSCMData(T data) {
		Vertex svnV, commitV, cmV, fileV;
		@SuppressWarnings("rawtypes")
		Collection logEntries;
		SVNLogEntry logEntry;
		mtd = new MimeTypeDetector();
		mtd.setCredProp(credprop);
		logger.info("Parsing SVNs logs.");
		for (Entry<Object, Object> ent : svnProp) {

			svnparser = new SVNParser();
			svnparser.setUrl(ent.getValue().toString());
			svnparser.setRepositoryName(ent.getKey().toString());
			svnparser.setUserName(credprop.getProperty(ent.getKey().toString() + "-user"));
			svnparser.setPassword(credprop.getProperty(ent.getKey().toString() + "-passwd"));
			mtd.setSVNParser(svnparser);
			try {
				svnparser.openRemoteHttpRepository();
				svnparser.checkRepositoryPath();
				wcc = svnparser.getWCClient();
			} catch (SVNException ex) {
				logger.error(ex.getMessage());
				logger.error(Arrays.toString(ex.getStackTrace()));
				continue;
			}
			svnV = svnVertex.get(ent.getKey().toString());
			if (svnV == null) {
				svnV = createSCMProjectVertex(ent.getKey().toString(), RepositoryType.SVN, ent.getValue().toString(), true);
				nbVertexCreated++;
				svnVertex.put(ent.getKey().toString(), svnV);

			}
			try {
				logEntries = svnparser.getLogEntries(new String[] { "" }, null, 0, -1, true, true);
			} catch (SVNException e2) {
				logger.error(e2.getMessage());
				logger.error(Arrays.toString(e2.getStackTrace()));
				continue;
			}
			logger.info("Parsing logs of SVN: {}.", ent.getValue().toString());
			// <LogEntry, <FilePath, <Committer id, Long>>>
			final List<Callable<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> lePartition = new ArrayList<Callable<Map<SVNLogEntry, Map<String, Map<String, Long>>>>>();
			// <LogEntry, <FilePath, RelationType>>
			final List<Callable<Map<SVNLogEntry, Map<String, RelationType>>>> laPartition = new ArrayList<Callable<Map<SVNLogEntry, Map<String, RelationType>>>>();
			for (@SuppressWarnings("rawtypes")
			Iterator entries = logEntries.iterator(); entries.hasNext();) {
				SVNLogEntry entry = (SVNLogEntry) entries.next();
				lePartition.add(new SVNLogEntryParser(entry));
				laPartition.add(new SVNLogEntryActionsParser(entry));
			}
			List<Future<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> leValues = null;
			List<Future<Map<SVNLogEntry, Map<String, RelationType>>>> laValues = null;
			try {
				leValues = executorPool.invokeAll(lePartition, ConcurrencyConfig.ALLOWED_EXEC_TIME100, TimeUnit.SECONDS);
				laValues = executorPool.invokeAll(laPartition, ConcurrencyConfig.ALLOWED_EXEC_TIME100, TimeUnit.SECONDS);
				executorPool.shutdown();

				if (leValues != null && laValues != null) {
					Iterator<Future<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> leIt;
					Iterator<Future<Map<SVNLogEntry, Map<String, RelationType>>>> laIt;
					Map<SVNLogEntry, Map<String, Map<String, Long>>> logEMap;
					Map<String, Map<String, Long>> filesCommittersCount = new HashMap<String, Map<String, Long>>();
					// Files touch counts by committers
					Map<String, Long> committersCount;
					Map<SVNLogEntry, Map<String, RelationType>> logATMap;
					Edge e;
					Long count;
					logger.info("Counting committers touch counts for all files.");
					for (leIt = leValues.iterator(); leIt.hasNext();) {
						logEMap = leIt.next().get();
						for (Entry<SVNLogEntry, Map<String, Map<String, Long>>> logEMapEnt : logEMap.entrySet()) {
							// logEntry = logEMapEnt.getKey();
							for (Entry<String, Map<String, Long>> filesEnt : logEMapEnt.getValue().entrySet()) {
								committersCount = filesCommittersCount.get(filesEnt.getKey());
								if (committersCount == null) {
									committersCount = new HashMap<String, Long>();
									filesCommittersCount.put(filesEnt.getKey(), committersCount);
								}
								for (Entry<String, Long> cmsCount : filesEnt.getValue().entrySet()) {
									if (cmsCount.getKey() == null) { // No
																		// Author...
										continue;
									}
									// opportunity to add a new committer node
									cmV = cmVertex.get(cmsCount.getKey());
									if (cmV == null) {
										cmV = createCommitterVertex(cmsCount.getKey());
										nbVertexCreated++;
										cmVertex.put(cmsCount.getKey(), cmV);
									}

									count = committersCount.get(cmsCount.getKey());
									if (count == null) {
										count = 0L;
									}
									count += cmsCount.getValue();
									committersCount.put(cmsCount.getKey(), count);
								}
							}
						}
					}
					leIt = leValues.iterator();
					laIt = laValues.iterator();
					logger.info("Creating Commit vertices, setting touch relationships between committers and files.");
					for (leIt.hasNext(); laIt.hasNext();) {
						logEMap = leIt.next().get();
						logATMap = laIt.next().get();
						for (Entry<SVNLogEntry, Map<String, Map<String, Long>>> logEMapEnt : logEMap.entrySet()) {
							logEntry = logEMapEnt.getKey();
							if (logEntry.getAuthor() == null) {
								continue;
							}
							cmV = cmVertex.get(logEntry.getAuthor());
							commitV = createCommitVertex(logEntry);
							nbVertexCreated++;
							for (Entry<String, Map<String, Long>> filesEnt : logEMapEnt.getValue().entrySet()) {
								fileV = createFileVertex(filesEnt.getKey(), logEntry.getRevision());
								if (fileV == null){
									continue;
								}
								nbVertexCreated++;
								e = addEdge(svnV, fileV, RelationType.CONTAIN.getName());
								if (cmV != null) {
									e = addEdge(cmV, fileV, RelationType.TOUCH.getName());
									setEdgeProperty(e, VEProperties.COUNT.getName(),
											filesCommittersCount.get(filesEnt.getKey()).get(logEntry.getAuthor()));
									e = addEdge(cmV, commitV, RelationType.PROPAGATE.getName());

								} else {
									logger.warn("Committer {} of revision {} is unknown. Could not set propagate and touch relationships on"
											+ "file {}.", logEntry.getAuthor(), logEntry.getRevision(), filesEnt.getKey());
								}
								e = addEdge(commitV, fileV, logATMap.get(logEntry).get(filesEnt.getKey()).getName());
							}
							// Handles Impact relation -
							// TODO later : find first occurrence of branch or
							// tag creation
							e = addEdge(commitV, svnV, RelationType.IMPACT.toString());
						}
					}
					logger.info("Setting Cop_to and Rep_By relationships.");
					for (@SuppressWarnings("rawtypes")
					Iterator entries = logEntries.iterator(); entries.hasNext();) {
						logEntry = (SVNLogEntry) entries.next();
						for (@SuppressWarnings("unchecked")
						Iterator<Entry<String, SVNLogEntryPath>> iterator = (Iterator<Entry<String, SVNLogEntryPath>>) logEntry.getChangedPaths()
								.entrySet().iterator(); iterator.hasNext();) {
							Entry<String, SVNLogEntryPath> en = iterator.next();
							establishCopyToRepByRelation(en);
						}
					}
				} else if (leValues != null) {
					logger.error("Returned global result from parsing all log actions types is null");
				} else {
					logger.error("Returned global result from parsing all log entries is null");
				}

			} catch (InterruptedException iex) {
				logger.error(iex.getMessage());
				logger.error(Arrays.toString(iex.getStackTrace()));
			} catch (ExecutionException e1) {
				logger.error(e1.getMessage());
				logger.error(Arrays.toString(e1.getStackTrace()));
			}
			logger.info("Finished processing all logs of SVN {}. How many processed: {}.", ent.getValue().toString(), leValues.size());

		}
		logger.info("Finished processing all SVNs logs. How many processed: {}.", svnProp.size());

	}

	@Override
	public Vertex createSCMProjectVertex(String name, RepositoryType type, String uri, boolean isRoot) {
		Vertex v = graph.addVertex(null);
		setVertexProperty(v, VEProperties.NAME.getName(), name);
		setVertexProperty(v, VEProperties.TYPE.getName(), type.getName());
		setVertexProperty(v, VEProperties.URI.getName(), uri);
		setVertexProperty(v, VEProperties.IS_ROOT.getName(), isRoot);
		// TODO : check URI, and isRoot from repository mining

		return v;
	}

	@Override
	public Vertex createCommitterVertex(Object o) {
		String cm = (String) o;
		Vertex v = graph.addVertex(null);
		setVertexProperty(v, VEProperties.SVN_ID.getName(), cm);
		return v;
	}

	@Override
	public Vertex createCommitVertex(Object o) {
		SVNLogEntry logEntry = (SVNLogEntry) o;
		Vertex v = graph.addVertex(null);
		setVertexProperty(v, VEProperties.REVISION.getName(), logEntry.getRevision());
		setVertexProperty(v, VEProperties.DATE.getName(), logEntry.getDate().getTime());
		setVertexProperty(v, VEProperties.MESSAGE.getName(), logEntry.getMessage());
		// TODO: Until actual contributor is properly parsed, we assume it is
		// the same as the committer (author)
		setVertexProperty(v, VEProperties.ACTUAL_CONTRIBUTOR.getName(), logEntry.getAuthor());
		return v;
	}

	@Override
	public Vertex createFileVertex(String en, long revision) {
		Vertex v = fileVertex.get(en);
		String mime, extension;
		SVNNodeKind nodeKind;
		if (v == null) {
			v = graph.addVertex(null);
			fileVertex.put(en, v);
			setVertexProperty(v, VEProperties.PATH.getName(), en);
			try {
				// SVNNodeKind nodeKind = svnparser.checkPath(en, revision);
				File f = new File(wcprop.getProperty(svnparser.getRepositoryName()) + en.substring(en.indexOf("/")));
				if (!f.exists()) { // remote check
					logger.warn("File {} does not exist in local working copy of repository {}. Fetching info from server.",
							f.getAbsolutePath(),
							svnparser.getRepositoryName());
					try {
						nodeKind = svnparser.checkPath(en, revision);
					} catch (SVNException ex) {
						logger.error(ex.getMessage());
						nodeKind = SVNNodeKind.UNKNOWN;
						logger.error("Cannot fetch info from server for file {}. It does not exist on remote repository either." +
								" Will set its file type to octet-stream.",
								f.getAbsolutePath());
						ex = null;
					}
					// mime = mtd.getContentTypeForRemote(svnparser.getUrl() +
					// en.substring(en.indexOf("/")));
					// Actually, file is no more served by server via http,
					// although in the commits history.
					// Pay attention to mimetype of directories!
					// (text/directory)
					if (nodeKind != SVNNodeKind.DIR && nodeKind != SVNNodeKind.UNKNOWN) {
						mime = mtd.guessContentTypeFor(en.substring(en.lastIndexOf("/") != -1 ? en.lastIndexOf("/") : 0));
					} else if (nodeKind != SVNNodeKind.UNKNOWN) {
						mime = "text/directory";
					} else {
						mime = "application/octet-stream";
					}

				} else { // local check - preferred
					try {
						wcc.doInfo(f, SVNRevision.WORKING, SVNRevision.WORKING, SVNDepth.EMPTY, null, myHandler);
					} catch (SVNException ex) {
						logger.error(ex.getMessage());
						logger.error("Cannot fetch info on local copy of the file {}" +
								Printer.NL + " Aborting vertex construction for it.",
								f.getAbsolutePath());
						ex = null;
						return null;
					}
					SVNInfo info = myHandler.getInfo();
					nodeKind = info.getKind();
					mime = mtd.getContentTypeFor(f.getAbsolutePath());
				}
				setVertexProperty(v, VEProperties.IS_DIRECTORY.getName(), (nodeKind == SVNNodeKind.DIR));
				// TODO : File Type!
				extension = en.substring(en.lastIndexOf(".") != -1 ? en.lastIndexOf(".") : 0);
				Vertex ftv = fileTypesVertex.get(mime);
				if (ftv == null) {
					ftv = graph.addVertex(null);
					fileTypesVertex.put(mime, ftv);
				}

				setVertexProperty(ftv, VEProperties.TYPE.getName(), mime);
				setVertexProperty(ftv, VEProperties.EXTENSION.getName(), extension);

				// TODO : it is relevant to handle the programming language
				// property? It is a costly operation.
				@SuppressWarnings("unused")
				Edge e = addEdge(v, ftv, RelationType.FILE_TYPE.getName());

			} catch (MimeTypeException | IOException e) {
				logger.error(e.getMessage());
				logger.error(Arrays.toString(e.getStackTrace()));
			}
		}
		return v;
	}

	@Override
	public Edge addEdge(Vertex from, Vertex to, String relation) {
		return graph.addEdge(null, from, to, relation);
	}

	@Override
	public void setVertexProperty(Vertex v, String propName, Object propValue) {
		if (propValue != null) {
			v.setProperty(propName, propValue);
		}
	}

	@Override
	public void setEdgeProperty(Edge e, String propName, Object propValue) {
		if (propValue != null) {
			e.setProperty(propName, propValue);
		}
	}

	@Override
	public void establishCopyToRepByRelation(Object o) {
		@SuppressWarnings("unchecked")
		Entry<String, SVNLogEntryPath> en = (Entry<String, SVNLogEntryPath>) o;
		Edge ed;
		if (en.getValue().getCopyPath() != null) {
			Vertex v = fileVertex.get(en.getKey());
			Vertex from = fileVertex.get(en.getValue().getCopyPath());
			if (from != null) {
				if (String.valueOf(en.getValue().getType()).equalsIgnoreCase(SVNLogActionType.ADD.getName())) {
					ed = addEdge(from, v, RelationType.COP_TO.toString());
					setEdgeProperty(ed, VEProperties.COPY_REVISION.getName(), en.getValue().getCopyRevision());
				} else { // We assume it is a replacement otherwise
					ed = addEdge(from, v, RelationType.REP_BY.toString());
					setEdgeProperty(ed, VEProperties.REPLACEMENT_REVISION.getName(), en.getValue().getCopyRevision());
				}
			} else {
				logger.warn("Could not establish copy or replacement relationship for {}, from {}.", en.getValue().getPath(), en.getValue()
						.getCopyPath());
			}
		}
	}

	@Override
	public Vertex createSCMProjectVertex(Object svnp) {
		throw new UnsupportedOperationException("This method is not supported in the core implementation");
	}

	@Override
	public Vertex createCommitterVertex(String svnID, String name, String email, String webpage) {
		throw new UnsupportedOperationException("This method is not supported in the core implementation");
	}

	@Override
	public Vertex createCommitVertex(long revision, long date, String message, String actualContributor) {
		throw new UnsupportedOperationException("This method is not supported in the core implementation");
	}
}
