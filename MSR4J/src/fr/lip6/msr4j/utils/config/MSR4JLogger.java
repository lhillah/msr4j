/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.config;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;

public final class MSR4JLogger {

	public static final String LOGGING_PATTERN = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n";
	
	private MSR4JLogger() {
		super();
	}

	public static synchronized Logger getLogger(String id) {
		return LoggerFactory.getLogger(id);
	}

	public static void shutDownLogging() {
		ILoggerFactory factory = LoggerFactory.getILoggerFactory();
		if(factory instanceof LoggerContext) {
		    LoggerContext ctx = (LoggerContext)factory;
		    ctx.stop();
		}
	}
	
}
