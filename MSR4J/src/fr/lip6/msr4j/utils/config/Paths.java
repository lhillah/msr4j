/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.config;

public final class Paths {
	/**
	 * DB PATH. Will be dynamically replaced by value in db.properties file.
	 */
	private static volatile String DbPath = "/Users/lom/Applications/neo4j-enterprise-2.0.0-M02/data/graph.db";

	private Paths() {
		super();
	}

	/**
	 * @return the dbPath
	 */
	public static String getDbPath() {
		return DbPath;
	}

	/**
	 * @param dbPath
	 *            the dbPath to set
	 */
	public static void setDbPath(String dbPath) {
		DbPath = dbPath;
	}
}
