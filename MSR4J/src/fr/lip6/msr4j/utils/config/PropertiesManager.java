/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public final class PropertiesManager {
	private String path;
	private Properties userProperties;

	public PropertiesManager(String path) {
		this.path = path;
	}

	public void loadUserProperties() {
		FileInputStream fis = null;
		try {
			userProperties = new Properties();
			fis = new FileInputStream(new File(this.path));
			userProperties.load(fis);

		} catch (IOException e) {
			MSR4JLogger.getLogger(this.getClass().getCanonicalName()).error(
					e.getMessage());
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				MSR4JLogger.getLogger(this.getClass().getCanonicalName())
						.error(e.getMessage());
			}
		}
	}

	public void setUserPropertiesPath(String path) {
		this.path = path;
	}

	public String getUserDBPath() {
		return getProperty("DB_PATH");
	}

	public String getProperty(String key) {
		return this.userProperties.getProperty(key);
	}

	public Set<Object> getPropertiesKeys() {
		return userProperties.keySet();
	}

	public Set<Entry<Object, Object>> getProperties() {
		return userProperties.entrySet();
	}
	
	public Properties getPropertiesAsMap() {
		return userProperties;
	}

	public static void showFileContent(File file) {
		if (file.exists() && file.isFile()) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(
						new FileInputStream(file), "UTF8"));
				String line;
				try {
					while ((line = br.readLine()) != null) {
						System.out.println(line);
					}
				} catch (IOException e) {
					MSR4JLogger.getLogger(
							PropertiesManager.class.getCanonicalName()).error(
							e.getMessage());
				} finally {
					if (br != null) {
						br.close();
					}
				}
			} catch (IOException e) {
				MSR4JLogger.getLogger(
						PropertiesManager.class.getCanonicalName()).error(
						e.getMessage());
			}
		}
	}
}
