package fr.lip6.msr4j.utils.config;

public class CLIMessages {

	public static String APPNAME = "MSR4J";
	public static String VERSION = "0.0.3-SNAPSHOT";
	public static final String ARGS_OK = "All expected arguments ok.";
	public static final String ARGS_KO = "Insufficient number of required arguments.";
	public static final String ARGS_UNR = "Non-recognized arguments: ";
	public static final String INVOCATION = "java -jar pathToJarFile";
}
