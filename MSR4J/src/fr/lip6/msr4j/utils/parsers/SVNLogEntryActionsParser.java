/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.parsers;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;

import fr.lip6.msr4j.datamodel.datatypes.RelationType;
import fr.lip6.msr4j.datamodel.datatypes.SVNLogActionType;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

/**
 * Gets a log entry and parses it into an associative map between that entry and
 * the map associating changed paths to the commits action types that changed
 * them (i.e., add, delete, modify and replace).
 * 
 * @author lom
 * 
 */
public final class SVNLogEntryActionsParser implements Callable<Map<SVNLogEntry, Map<String, RelationType>>> {

	private final SVNLogEntry logEntry;

	private final Logger logger = MSR4JLogger.getLogger(this.getClass().getCanonicalName());

	public SVNLogEntryActionsParser(SVNLogEntry logEntry) {
		this.logEntry = logEntry;
	}

	/**
	 * <SVNLogEntry, <ChangedPath, Commit action type>>
	 */
	@Override
	public Map<SVNLogEntry, Map<String, RelationType>> call() throws Exception {

		Map<String, RelationType> actionsType = new HashMap<String, RelationType>();
		Map<SVNLogEntry, Map<String, RelationType>> logActionsType = new HashMap<SVNLogEntry, Map<String, RelationType>>();
		String revisionType;
		if (logEntry.getChangedPaths().size() > 0) {
			@SuppressWarnings("unchecked")
			Set<Entry<String, SVNLogEntryPath>> changedPaths = (Set<Entry<String, SVNLogEntryPath>>) logEntry.getChangedPaths().entrySet();
			for (Entry<String, SVNLogEntryPath> en : changedPaths) {
				revisionType = String.valueOf(en.getValue().getType());
				if (revisionType.equalsIgnoreCase(SVNLogActionType.ADD.getName())) {
					actionsType.put(en.getKey(), RelationType.ADD);
				} else if (revisionType.equalsIgnoreCase(SVNLogActionType.DELETE.getName())) {
					actionsType.put(en.getKey(), RelationType.DELETE);
				} else if (revisionType.equalsIgnoreCase(SVNLogActionType.MODIFY.getName())) {
					actionsType.put(en.getKey(), RelationType.MODIFY);
				} else if (revisionType.equalsIgnoreCase(SVNLogActionType.REPLACE.getName())) {
					actionsType.put(en.getKey(), RelationType.REPLACE);
				}

			}
			logActionsType.put(logEntry, actionsType);
		}

		logger.info("Finished parsing action types of log entry  {}.", logEntry.getRevision());
		return logActionsType;
	}

}