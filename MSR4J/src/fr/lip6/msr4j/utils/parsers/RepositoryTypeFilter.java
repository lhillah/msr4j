package fr.lip6.msr4j.utils.parsers;

import java.io.File;
import java.io.FileFilter;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;

/**
 * This class implements a filter that retains known SCM types proprietary folder:
 * .svn, .git, .hg.
 * @author lom
 *
 */
public class RepositoryTypeFilter implements FileFilter {

	private RepositoryType type;

	public RepositoryTypeFilter() {
		// TODO Auto-generated constructor stub
	}

	public RepositoryType getRepositoryType() {
		return this.type;
	}

	public void resetRepositoryType() {
		this.type = null;
	}

	@Override
	public boolean accept(File dir) {
		boolean result = true;
		String name;
		if (dir.isDirectory() && dir.isHidden()) {
			name = dir.getName();
			if (".svn".contains(name) || "svn".contains(name)) {
				this.type = RepositoryType.SVN;
			} else if (".git".contains(name) || "git".contains(name)) {
				this.type = RepositoryType.GIT;
			} else if (".hg".contains(name) || "hg".contains(name)) {
				this.type = RepositoryType.MERCURIAL;
			} else {
				result = false;
			}
		} else {
			result = false;
		}
		return result;
	}

}
