/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.parsers;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNWCClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import fr.lip6.msr4j.utils.strings.Printer;

public final class SVNParser {

	private String path;
	private SVNURL url;
	private SVNRepository repository;
	private SVNClientManager clm;
	private ISVNOptions options;
	private String user;
	private String password;
	private long startRevision = 0;
	private long endRevision = -1; // HEAD (the latest) revision
	private String name;

	public SVNParser() {
	}

	public SVNParser(String path, String repoName) {
		this.setUrl(path);
		this.setRepositoryName(repoName);
		this.user = "anonymous";
		this.password = "anonymous";
	}

	public SVNParser(String path, String repoName, String user, String pass) {
		this(path, repoName);
		if (user != null && pass != null) {
			this.setUserName(user);
			this.setPassword(pass);
		}
	}

	/**
	 * Determines the node kind of the given path (i.e. directory or file). This
	 * methods uses an API which connects to the remote repository, so beware of
	 * too many invocations.
	 * 
	 * @param nodePath
	 * @param revision
	 * @return
	 * @throws SVNException
	 */
	public SVNNodeKind checkPath(String nodePath, long revision) throws SVNException {
		return repository.checkPath(nodePath, revision);
	}

	/**
	 * Basic sanity check if the repository location. A directory entry is
	 * expected.
	 * 
	 * @return false if there is no entry at the specified URL or a file is
	 *         encountered, instead of a directory.
	 * @throws SVNException
	 */
	public boolean checkRepositoryPath() throws SVNException {
		SVNNodeKind nodeKind = repository.checkPath("", -1);
		if (nodeKind == SVNNodeKind.NONE) {
			System.err.println("There is no entry at '" + url + "'.");
			return false;
		} else if (nodeKind == SVNNodeKind.FILE) {
			System.err.println("The entry at '" + url + "' is a file while a directory was expected.");
			return false;
		}
		return true;
	}

	@SuppressWarnings("rawtypes")
	public int countLogEntries() throws SVNException {
		Collection entries = getEntries(repository, "");
		return entries.size();
	}

	@SuppressWarnings("rawtypes")
	public void displayLogEntries(boolean changePath, boolean strictNode) throws SVNException {
		Collection logEntries = getLogEntries(new String[] { "" }, null, startRevision, endRevision, changePath, strictNode);
		Printer.println("#################### NUMBER OF LOG ENTRIES = " + logEntries.size());

		for (Iterator entries = logEntries.iterator(); entries.hasNext();) {
			SVNLogEntry logEntry = (SVNLogEntry) entries.next();
			Printer.println("---------------------------------------------");
			Printer.println("revision: " + logEntry.getRevision());
			Printer.println("author: " + logEntry.getAuthor());
			Printer.println("date: " + logEntry.getDate());
			Printer.println("log message: " + logEntry.getMessage());

			if (logEntry.getChangedPaths().size() > 0) {
				Printer.println("");
				Printer.println("changed paths:");
				Set changedPathsSet = logEntry.getChangedPaths().keySet();

				for (Iterator changedPaths = changedPathsSet.iterator(); changedPaths.hasNext();) {
					SVNLogEntryPath entryPath = (SVNLogEntryPath) logEntry.getChangedPaths().get(changedPaths.next());
					Printer.println(" "
							+ entryPath.getType()
							+ " "
							+ entryPath.getPath()
							+ ((entryPath.getCopyPath() != null) ? " (from " + entryPath.getCopyPath() + " revision " + entryPath.getCopyRevision()
									+ ")" : ""));
				}
			}
		}
	}

	public void displayRepositoryTree() throws SVNException {
		// Printer.print("Repository Root: " +
		// repository.getRepositoryRoot(true));
		// Printer.print("Repository UUID: " +
		// repository.getRepositoryUUID(true));
		listEntries(repository, "");
		Printer.println("Repository latest revision: " + repository.getLatestRevision());
	}

	public long getEndRevision() {
		return endRevision;
	}

	@SuppressWarnings("rawtypes")
	public Collection getEntries(SVNRepository repository, String path) throws SVNException {
		return repository.getDir(path, -1, null, (Collection) null);
	}

	@SuppressWarnings("rawtypes")
	public Collection getLogEntries(String[] targetPaths, Collection entries, long startRevision, long endRevision, boolean changePath,
			boolean strictNode) throws SVNException {
		return repository.log(targetPaths, entries, startRevision, endRevision, changePath, changePath);
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	public String getRepositoryName() {
		return this.name;
	}

	public long getStartRevision() {
		return startRevision;
	}

	/**
	 * @return the repository base URL
	 */
	public String getUrl() {
		return path;
	}

	/**
	 * @return the user name
	 */
	public String getUserName() {
		return user;
	}

	public SVNWCClient getWCClient() {
		return clm.getWCClient();
	}

	@SuppressWarnings("rawtypes")
	public void listEntries(SVNRepository repository, String path) throws SVNException {
		Collection entries = getEntries(repository, path);
		Iterator iterator = entries.iterator();
		while (iterator.hasNext()) {
			SVNDirEntry entry = (SVNDirEntry) iterator.next();
			Printer.println("/" + (path.equals("") ? "" : path + "/") + entry.getName() + " ( author: '" + entry.getAuthor() + "'; revision: "
					+ entry.getRevision() + "; date: " + entry.getDate() + ")");
			if (entry.getKind() == SVNNodeKind.DIR) {
				listEntries(repository, (path.equals("")) ? entry.getName() : path + "/" + entry.getName());
			}
		}
	}

	/**
	 * 
	 * @throws SVNException
	 * @Deprecated
	 */
	public void openLocalRepository() throws SVNException {
		FSRepositoryFactory.setup();
		url = SVNURL.parseURIDecoded("file://" + this.path);
		repository = FSRepositoryFactory.create(url, null);
	}

	public void openRemoteHttpRepository() throws SVNException {
		DAVRepositoryFactory.setup();
		url = SVNURL.parseURIDecoded(this.path);
		ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(user, password);
		options = SVNWCUtil.createDefaultOptions(true);
		clm = SVNClientManager.newInstance(options, authManager);
		repository = clm.createRepository(url, true);
		// DAVRepositoryFactory.create(url, null);

		// repository.setAuthenticationManager(authManager);
	}

	public void setEndRevision(long endRevision) {
		this.endRevision = endRevision;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		if (password != null) {
			this.password = password;
		}
	}

	public void setRepositoryName(String name) {
		this.name = name;

	}

	public void setStartRevision(long startRevision) {
		this.startRevision = startRevision;
	}

	/**
	 * @param path
	 *            the repository base URL to set
	 */
	public void setUrl(String path) {
		this.path = path;
	}

	/**
	 * @param name
	 *            the user name to set
	 */
	public void setUserName(String name) {
		if (name != null) {
			this.user = name;
		}
	}
}
