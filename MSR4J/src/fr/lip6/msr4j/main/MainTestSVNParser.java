/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.main;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.tmatesoft.svn.core.SVNException;

import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.parsers.SVNParser;
import fr.lip6.msr4j.utils.strings.Printer;

public final class MainTestSVNParser {
	static final Logger LOGGER = MSR4JLogger.getLogger(MainTestSVNParser.class
			.getCanonicalName());
	static PropertiesManager svnprop;

	private MainTestSVNParser() {
		super();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		svnprop = new PropertiesManager(args[0]);
		svnprop.loadUserProperties();
		SVNParser parser = new SVNParser();
		try {
			if ("all".equalsIgnoreCase(args[1])) {
				for (Entry<Object, Object> ent : svnprop.getProperties()) {
					parser.setUrl(ent.getValue().toString());
					parser.openRemoteHttpRepository();
					parser.checkRepositoryPath();
					// parser.displayRepositoryTree();
					// parser.displayLogEntries(true, true);
					Printer.println(parser.countLogEntries());
				}
			} else {
				String svnUrl = svnprop.getProperty(args[1]);
				parser.setUrl(svnUrl);
				parser.openRemoteHttpRepository();
				parser.checkRepositoryPath();
				// parser.displayRepositoryTree();
				// parser.displayLogEntries(true, true);
				Printer.println(parser.countLogEntries());
			}
		} catch (SVNException e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}

	}

}
