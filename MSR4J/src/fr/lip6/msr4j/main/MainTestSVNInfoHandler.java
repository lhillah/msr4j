/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.main;

import java.io.File;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNInfo;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCClient;

import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.parsers.SVNParser;
import fr.lip6.msr4j.utils.repositories.CustomSVNInfoHandler;
import fr.lip6.msr4j.utils.strings.Printer;

public class MainTestSVNInfoHandler {

	private static PropertiesManager wcprop;
	private static PropertiesManager svnprop;
	private static SVNParser svnparser;

	private MainTestSVNInfoHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 * @throws SVNException 
	 */
	public static void main(String[] args) throws SVNException {
		SVNWCClient wcc ;
		svnprop = new PropertiesManager(args[0]);
		svnprop.loadUserProperties();
		wcprop = new PropertiesManager(args[1]);
		wcprop.loadUserProperties();
		svnparser = new SVNParser(svnprop.getProperty(args[2]), args[2]);
		svnparser.openRemoteHttpRepository();
		CustomSVNInfoHandler myHandler = new CustomSVNInfoHandler();
		wcc = svnparser.getWCClient();
		wcc.doInfo(new File(wcprop.getProperty(args[2])
				+ "/tags/drivers/java/1.0.3/src/org/apache/cassandra/cql/jdbc/CassandraConnection.java"), SVNRevision.WORKING,
				SVNRevision.WORKING, SVNDepth.EMPTY, null, myHandler);
		SVNInfo info = myHandler.getInfo();
		Printer.println(info.getKind().toString());
		Printer.println(info.getRepositoryRootURL());
		
	}

}
