/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.main;

import java.io.IOException;

import org.apache.tika.mime.MimeTypeException;

import fr.lip6.msr4j.utils.parsers.MimeTypeDetector;
import fr.lip6.msr4j.utils.strings.Printer;

public final class MainTestMimeTypeDetector {

	public MainTestMimeTypeDetector() {
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws MimeTypeException
	 */
	public static void main(String[] args) throws IOException, MimeTypeException {
		MimeTypeDetector mtd = new MimeTypeDetector();
		String mime = mtd
				.getContentTypeFor("/Applications/Eclipses/eclipse-4.2-harmony/workspace/repositories/jackrabbit/trunk/jackrabbit-core/src/test/java/org/apache/jackrabbit/api/JackrabbitNodeTest.java");
		Printer.println(mime + " -- " + mtd.getExtensionOf(mime));

		mime = mtd.getContentTypeFor("/Users/lom/Documents/Enseignement/P10/2012-2013/S&R-L3A/SystemeReseau-L3-App/Programmes/p1tube.c");
		Printer.println(mime + " -- " + mtd.getExtensionOf(mime));

		mime = mtd.getContentTypeForRemote("http://www.ibm.com/developerworks/opensource/tutorials/os-apache-tika/section4.html");
		Printer.println(mime + " -- " + mtd.getExtensionOf(mime));
	}

}
