/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.main;

import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.DB_PATH;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.LOG_FILE;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.REPO_NAME;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.SCM_CRED;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.SCM_URL;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.SCM_WC;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.FileAppender;
import fr.lip6.msr4j.utils.config.CLIMessages;
import fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.db.ConcurrentNeo4JBatchInserterSVN;
import fr.lip6.msr4j.utils.db.DBHandler;
import fr.lip6.msr4j.utils.strings.Printer;

/**
 * MSR4J Core component to extract data from repositories.
 * Uses command-line arguments, that are available using -help option. 
 * @author lom
 *
 */
public class MainAnySCM {
	protected static Logger logger;
	protected static PropertiesManager dbprop, svnprop, wcprop, credprop;
	protected static CommandLineParser parser;
	protected static CoreCLIOptionsBuilder cop;
	protected static HelpFormatter formatter;
	protected static CommandLine line;

	protected MainAnySCM() {
		super();
	}

	/**
	 * Main program to launch MSR4J Core: scan a repository, collect data and
	 * build the corresponding model in back end DB.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			cop = CoreCLIOptionsBuilder.buildCoreOptions();
			if (parseArguments(args, MainAnySCM.class.getCanonicalName())) {
				logger.info(CLIMessages.ARGS_OK);
				loadCorePropertiesFromArgs();
				try {
					Set<Entry<Object, Object>> svnUrls = findRepositoriesToScan();
					DBHandler<Object> dbh = 
							new ConcurrentNeo4JBatchInserterSVN<Object>(dbprop.getUserDBPath(), svnUrls, wcprop, credprop);
					extractToDBFromScratch(dbh);
				} catch (Exception e) {
					logger.error(e.getMessage());
					logger.error(Arrays.toString(e.getStackTrace()));
				}
			}
		} finally {
			MSR4JLogger.shutDownLogging();
		}
	}

	/**
	 * Extract data from the pointed at repositories and call the provider db
	 * handler to populate Db from scratch, which means that the existing db
	 * back end used will be removed first, then created anew.
	 * 
	 * @param dbh the db handler to use
	 */
	public static void extractToDBFromScratch(DBHandler<Object> dbh) {
		dbh.removeDb();
		dbh.createDb();
		dbh.populateDB(null);
		dbh.shutDownDb();
		dbh = null;
	}

	/**
	 * Returns the set of pairs <SCM name, SCM URL> to scan, as specified in the
	 * CLI by the -repoName option (either 'all' or a comma-separated list of
	 * names).
	 * 
	 * @return the set of pairs <SCM name, SCM URL> to scan
	 */
	public static Set<Entry<Object, Object>> findRepositoriesToScan() {
		Set<Entry<Object, Object>> svnUrls;
		if ("all".equalsIgnoreCase(line.getOptionValue(REPO_NAME))) {
			svnUrls = svnprop.getProperties();
		} else {
			svnUrls = new HashSet<Entry<Object, Object>>();
			String[] repos = line.getOptionValues(REPO_NAME);
			Properties props = svnprop.getPropertiesAsMap();
			for (int i = 0; i < repos.length; i++) {
				if (props.containsKey(repos[i])) {
					svnUrls.add(new AbstractMap.SimpleImmutableEntry<Object, Object>(repos[i], props.get(repos[i])));
				}
			}
		}
		return svnUrls;
	}

	/**
	 * Parses command-line arguments and prints help if necessary
	 * 
	 * @param args
	 *            the command-line arguments
	 * @param classCanonicalName
	 *            the canonical name of the (main) class, which is passed to the
	 *            logging framework
	 * @return true if all expected arguments are provided
	 */
	public static boolean parseArguments(String[] args, String classCanonicalName) {
		boolean result = true;
		parser = new BasicParser();
		formatter = new HelpFormatter();
		logger = MSR4JLogger.getLogger(classCanonicalName);
		try {
			line = parser.parse(cop, args);
			if (line.hasOption(LOG_FILE)) {
				configureLogging(line.getOptionValue(LOG_FILE));
			}
			if (line.hasOption("help")) {
				Printer.println(CLIMessages.APPNAME + CLIMessages.VERSION + Printer.NL);
				formatter.printHelp(CLIMessages.INVOCATION, cop);
				result = false;
			} else if (line.hasOption("version")) {
				Printer.println(CLIMessages.APPNAME + CLIMessages.VERSION);
				result = false;
			} else {
				String[] nonRecog = line.getArgs();
				if (nonRecog != null && nonRecog.length != 0) {
					Error.fail(CLIMessages.ARGS_UNR + Arrays.toString(nonRecog), logger);
					result = false;
				}
			}
		} catch (ParseException e) {
			logger.error(e.getMessage());
			formatter.printHelp(CLIMessages.INVOCATION, cop);
			Error.fail(Arrays.toString(e.getStackTrace()), logger);
			result = false;
		} 
		return result;
	}

	/**
	 * Load the following set of properties from the command-line arguments:
	 * <ul>
	 * <li>DB PATH</li>
	 * <li>SCM URLs</li>
	 * <li>SCM Working Copies local Paths (or clones)</li>
	 * <li>SCM Credentials</li>
	 * </ul>
	 */
	public static void loadCorePropertiesFromArgs() {
		loadDBProperties();
		loadSCMURLProperties();
		loadSCMWCProperties();
		loadSCMCredentialsProperties();
	}

	public static void loadSCMCredentialsProperties() {
		credprop = new PropertiesManager(line.getOptionValue(SCM_CRED));
		credprop.loadUserProperties();
	}

	public static void loadSCMWCProperties() {
		wcprop = new PropertiesManager(line.getOptionValue(SCM_WC));
		wcprop.loadUserProperties();
	}

	public static void loadSCMURLProperties() {
		svnprop = new PropertiesManager(line.getOptionValue(SCM_URL));
		svnprop.loadUserProperties();
	}

	public static void loadDBProperties() {
		dbprop = new PropertiesManager(line.getOptionValue(DB_PATH));
		dbprop.loadUserProperties();
	}

	/**
	 * Configures log file option.
	 * 
	 * @param optionValue
	 *            the log file path (relative or absolute).
	 */
	@SuppressWarnings("unchecked")
	public static void configureLogging(String optionValue) {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		@SuppressWarnings("rawtypes")
		FileAppender fileAppender = new FileAppender();
		fileAppender.setContext(loggerContext);
		fileAppender.setName(String.valueOf(System.nanoTime()));
		// set the file name
		fileAppender.setFile(optionValue);

		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(loggerContext);
		encoder.setPattern(MSR4JLogger.LOGGING_PATTERN);
		encoder.start();

		fileAppender.setEncoder(encoder);
		fileAppender.start();
		logger = null;
		logger = loggerContext.getLogger(MainTestCLI.class.getCanonicalName());
		((ch.qos.logback.classic.Logger) logger).addAppender(fileAppender);
	}

}