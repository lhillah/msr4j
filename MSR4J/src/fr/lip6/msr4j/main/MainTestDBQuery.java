/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.main;

import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;

import fr.lip6.ms4j.utils.analysis.RepositoryAnalyser;
import fr.lip6.msr4j.datamodel.nodes.Repository;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.db.SequentialNeo4JDBReader;

/**
 * 
 * @author lom
 * @Deprecated Use MSR4J-Analysis project instead
 */
public final class MainTestDBQuery {
	static final Logger LOGGER = MSR4JLogger.getLogger(MainTestDBQuery.class.getCanonicalName());
	static PropertiesManager dbprop;

	private MainTestDBQuery() {
		super();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 4)
			Error.fail("Expected arguments: 1) db properties, 2) name of repository node to analyse,"
					+ " 3) output file path 4) list of files extensions to consider (e.g .java), or 'all'", LOGGER);
		dbprop = new PropertiesManager(args[0]);
		dbprop.loadUserProperties();

		SequentialNeo4JDBReader dbReader = new SequentialNeo4JDBReader(dbprop.getUserDBPath());
		dbReader.openDB();
		Repository r = dbReader.getRepository(args[1]);
		// 1) Count the number of files in the repository
		/*
		 * Iterable<File> it = r.getFiles(); int nb = 0;
		 * 
		 * for (File f : it) { LOGGER.info(f.getPath()); nb++; } LOGGER.info(
		 * "How many files in the repository jackrabbit (counted in java program): {}"
		 * , nb); /* LOGGER.info(
		 * "How many files in the repository jackrabbit (Gremlin query): {}",
		 * r.getNumberOfFiles());
		 */

		// 2) Count number of commits per (type of) file per committer
		LOGGER.info("Data generation: count the number of touches per file per committer in repository: {}", r.getName());
		RepositoryAnalyser ra = new RepositoryAnalyser(r);
		Set<String> extensions = new TreeSet<String>();
		for (int i = 3; i < args.length; i++) {
			extensions.add(args[i]);
		}
		@SuppressWarnings("unused")
		java.io.File f = ra.createTouchHeatMapData(args[2], extensions);
		ra = null;
		dbReader.shutDownDb();
		System.exit(0);
	}

}
