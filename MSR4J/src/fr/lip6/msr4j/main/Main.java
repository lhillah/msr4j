/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.main;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;

import fr.lip6.msr4j.asf.datamodel.ASFProjectsCommitters;
import fr.lip6.msr4j.asf.utils.db.ConcurrentASFNeo4JDBInserterSVN;
import fr.lip6.msr4j.asf.utils.parsers.ASFCommitterIndexesParser;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.db.DBHandler;

/**
 * 
 * @author lom
 * @Deprecated
 */
public final class Main {
	static final Logger LOGGER = MSR4JLogger.getLogger(Main.class.getCanonicalName());
	static PropertiesManager dbprop, svnprop, wcprop, credprop;;

	private Main() {
		super();
	}

	/**
	 * Launches the application. Expected arguments are (in this order):
	 * <ul>
	 * <li>location of the DB properties file (local file system)</li>
	 * <li>location of the repositories remote URLs properties file (local file
	 * system)</li>
	 * <li>location of the repositories local working copies local paths
	 * properties file (local file system)</li>
	 * <li>location of the properties files containing repositories credentials (anonymous
	 * is assumed if none provided for the parsed repository)</li>
	 * <li>the name(s) of the repository(ies) to process, or <em>all</em> if all
	 * repositories listed in the above files must be processed. The number of
	 * listed repositories must match in both remote URLs and local paths
	 * properties files.</li>
	 * </ul>
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// For now, we require both remote repos and local copies.
		if (args.length < 5)
			Error.fail("Expected arguments: 1) db properties, 2) repositories remote URLs properties," +
					" 3) repositories local paths properties, 4) name(s) of repository(ies) to process, or 'all'", LOGGER);
		dbprop = new PropertiesManager(args[0]);
		dbprop.loadUserProperties();
		svnprop = new PropertiesManager(args[1]);
		svnprop.loadUserProperties();
		wcprop = new PropertiesManager(args[2]);
		wcprop.loadUserProperties();
		credprop = new PropertiesManager(args[3]);
		credprop.loadUserProperties();

		try {
			ASFCommitterIndexesParser parser = new ASFCommitterIndexesParser();
			ASFProjectsCommitters projects = parser.parseIndex();
			//Printer.showTopLevelProjects(projects);
			Set<Entry<Object, Object>> svnUrls;
			if ("all".equalsIgnoreCase(args[3])) {
				svnUrls = svnprop.getProperties();
			} else {
				svnUrls = new HashSet<Entry<Object, Object>>();
				for (Entry<Object, Object> ent : svnprop.getProperties()) {
					for (int i = 3; i < args.length; i++) {
						if (args[i].equalsIgnoreCase(ent.getKey().toString()))
							svnUrls.add(ent);
					}
				}
			}

			DBHandler<ASFProjectsCommitters> dbh;
			dbh = new ConcurrentASFNeo4JDBInserterSVN(dbprop.getUserDBPath(), svnUrls, wcprop, credprop);
			dbh.removeDb();
			dbh.createDb();
			dbh.populateDB(projects);
			dbh.shutDownDb();
			dbh = null;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		System.exit(0);
	}
}
