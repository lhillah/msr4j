package fr.lip6.msr4j.main;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.EmptyTreeIterator;
import org.slf4j.Logger;

import fr.lip6.msr4j.utils.config.MSR4JLogger;

public class MainTestGit {
	private static final String localPath = "/Users/lom/Documents/Recherche/Projets/ASFDataset";
	//private static final String remoteURL = "https://lhillah@bitbucket.org/lhillah/msr4j.git";
	private  static Logger logger;
	private static File repoFolder;
	private static Repository repo;
	private static Git git;
	
	private MainTestGit() {
		super();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger = MSR4JLogger.getLogger(MainTestGit.class.getCanonicalName());
		repoFolder = new File(localPath + "/.git");
		try {
			repo = new FileRepositoryBuilder().setGitDir(repoFolder).readEnvironment().findGitDir().build();
			git = new Git(repo);
		
			
			//ObjectId from = repo.resolve(Constants.HEAD);
			//ObjectId to = repo.resolve("refs/remotes/origin/master");
			RevWalk walk = new RevWalk(repo);
			//walk.markStart(walk.parseCommit(from));
			//walk.markUninteresting(walk.parseCommit(to));
			Iterable<RevCommit> logs = git.log().call();
			RevCommit c;
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			DiffFormatter df = new DiffFormatter(out);
			df.setRepository(repo);
			df.setDetectRenames(true);
			List<DiffEntry> entries = new ArrayList<DiffEntry>();
			RevCommit parent = null;
			for (RevCommit comm : logs) {
				c = walk.parseCommit(comm);
				logger.info("Revision: {} \n\tDate: {}\n\tAuthor: {}\n\tEmail: {}\n\tMessage: {}", c.getId().name(), c.getAuthorIdent().getWhen(), c.getCommitterIdent().getName(),
						c.getAuthorIdent().getEmailAddress(), c.getFullMessage());
				logger.info("DIFFS IN THIS COMMIT (How many Parents ? --> {} )", c.getParentCount());
				entries.clear();
				if (c.getParentCount() > 0 && c.getParent(0) != null) {
					for (int i = 0; i < c.getParentCount(); i++) {
						parent = walk.parseCommit(c.getParent(i).getId());
						entries.addAll(df.scan(parent.getTree(), c.getTree()));
					}
				} else {
					entries.addAll(df.scan(new EmptyTreeIterator(), new CanonicalTreeParser(null, walk.getObjectReader(), c.getTree())));
				}
				for(DiffEntry entry : entries) { 
					df.format(entry);
					logger.info("Changed Type ---> {}", entry.getChangeType().toString());
					logger.info("Old Path ---> {}", entry.getOldPath());
					logger.info("New Path ---> {}\n", entry.getNewPath());
					out.reset();
				}
				
			}
			df.release();
			walk.release();
			walk.dispose();
			/*logger.info("COMPUTING DIFFS");
			out = new ByteArrayOutputStream();
			entries = git.diff().setOutputStream(out).setOl.call();
			for(DiffEntry entry : entries) {
				logger.info("Changed Type ---> {}", entry.getChangeType().toString());
				logger.info("Old Path ---> {}", entry.getOldPath());
				logger.info("New Path ---> {}\n", entry.getNewPath());
			}
			*/
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (NoHeadException e) {
			e.printStackTrace();
		} catch (GitAPIException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}
	
}
