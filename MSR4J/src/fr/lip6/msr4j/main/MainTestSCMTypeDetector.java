package fr.lip6.msr4j.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.parsers.SCMTypeDetector;

public class MainTestSCMTypeDetector {
	static final Logger LOGGER = MSR4JLogger.getLogger(MainTestSCMTypeDetector.class.getCanonicalName());
	static PropertiesManager scmprop;

	private MainTestSCMTypeDetector() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			Error.fail("Expected arguments: 1) scm local paths properties", LOGGER);
			System.exit(1);
		}
		scmprop = new PropertiesManager(args[0]);
		scmprop.loadUserProperties();
		Properties props = scmprop.getPropertiesAsMap();
		Map<String, String> scmpaths = new HashMap<String, String>();

		List<Callable<Map<String, RepositoryType>>> partition = new ArrayList<Callable<Map<String, RepositoryType>>>();
		for (Entry<Object, Object> e : props.entrySet()) {
			scmpaths.put((String) e.getValue(), (String) e.getKey());
			partition.add(new SCMTypeDetector((String) e.getValue()));
		}

		ExecutorService exec = ConcurrencyConfig.getFixedThreadPoolExecutor();
		try {
			List<Future<Map<String, RepositoryType>>> values = exec.invokeAll(partition, ConcurrencyConfig.ALLOWED_EXEC_TIME10, TimeUnit.SECONDS);
			Entry<String, RepositoryType> ent;
			for (Future<Map<String, RepositoryType>> v : values) {
				ent = v.get().entrySet().iterator().next();
				LOGGER.info("{} ---> {}", scmpaths.get(ent.getKey()), ent.getValue());
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			e1.printStackTrace();
		}
		System.exit(0);
	}
}
