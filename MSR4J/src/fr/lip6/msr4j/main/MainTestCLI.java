package fr.lip6.msr4j.main;

import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.LOG_FILE;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.REPO_NAME;

import java.util.Arrays;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.FileAppender;
import fr.lip6.msr4j.utils.config.CLIMessages;
import fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.strings.Printer;

public class MainTestCLI {

	private static Logger logger;

	public MainTestCLI() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * if (args.length < 5) Error.fail(
	 * "Expected arguments: 1) db properties, 2) repositories remote URLs properties,"
	 * +
	 * " 3) repositories local paths properties, 4) repositories credentials, 5) name(s) of repository(ies) to process, or 'all'"
	 * , LOGGER); /**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		CommandLineParser parser = new BasicParser();
		CoreCLIOptionsBuilder cop = CoreCLIOptionsBuilder.buildCoreOptions();
		HelpFormatter formatter = new HelpFormatter();
		logger = MSR4JLogger.getLogger(MainTestCLI.class.getCanonicalName());
		try {
			CommandLine line = parser.parse(cop, args);
			if (line.hasOption(LOG_FILE)) {
				configureLogging(line.getOptionValue(LOG_FILE));
			}
			if (line.hasOption("help")) {
				Printer.println(CLIMessages.APPNAME + CLIMessages.VERSION + Printer.NL);
				formatter.printHelp(CLIMessages.INVOCATION, cop);
			} else if (line.hasOption("version")) {
				Printer.println(CLIMessages.APPNAME + CLIMessages.VERSION);
			} else {
				String[] nonRecog = line.getArgs();
				if (nonRecog != null && nonRecog.length != 0) {
					Error.fail(CLIMessages.ARGS_UNR + Arrays.toString(nonRecog), logger);
				}

				logger.info(CLIMessages.ARGS_OK);
				String[] repos = line.getOptionValues(REPO_NAME);
				logger.info("Repositories to scan: " + Arrays.toString(repos));

			}
		} catch (ParseException e) {
			logger.error(e.getMessage());
			formatter.printHelp(CLIMessages.INVOCATION, cop);
			Error.fail(Arrays.toString(e.getStackTrace()), logger);
			
		} finally {
			MSR4JLogger.shutDownLogging();
		}
	}

	@SuppressWarnings("unchecked")
	private static void configureLogging(String optionValue) {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

		@SuppressWarnings("rawtypes")
		FileAppender fileAppender = new FileAppender();
		fileAppender.setContext(loggerContext);
		fileAppender.setName(String.valueOf(System.nanoTime()));
		// set the file name
		fileAppender.setFile(optionValue);

		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(loggerContext);
		encoder.setPattern(MSR4JLogger.LOGGING_PATTERN);
		encoder.start();

		fileAppender.setEncoder(encoder);
		fileAppender.start();
		logger = null;
		logger = loggerContext.getLogger(MainTestCLI.class.getCanonicalName());
		((ch.qos.logback.classic.Logger) logger).addAppender(fileAppender);
	}

}
