# find all committers who participate in project velocity, by their svnId. 
start n=node:node_auto_index(name="velocity") match n<-[:PARTICIPATES]-m return m.svnId;

# find all committers who participate in project velocity, by their svnId; order by svnId
start n=node:node_auto_index(name="velocity") match n<-[:PARTICIPATES]-m return m.svnId order by m.svnId;

# find all projects in which committer wglass participates; order by name
start n=node:node_auto_index(svnId="wglass") match n-[:PARTICIPATES]->m return m.name order by m.name;

# find committer wglass node record (svnId, name, weight, isApacgeMember)
start n=node:node_auto_index(svnId="wglass") return n;

#who are the colleagues of wglass (identified by "workswith" relationship)?
start n=node:node_auto_index(svnId="wglass") match n<-[:WORKSWITH]-m return m.name order by m.name;

#who are the colleagues of wglass, navigating along the projects he participates in?
start n=node:node_auto_index(svnId="wglass") match n-[:PARTICIPATES]->m<-[:PARTICIPATES]-r return r.name, collect(distinct r.name) order by r.name;

# who are the colleagues of wglass, he works with the most? (weight on relationship "workswith", order by weight desc)
start n=node:node_auto_index(svnId="wglass") match n-[r:WORKSWITH]->m return r,m.name order by r.weight desc;

MATCH (thing:Red)

WHERE thing.uid = "TK-421" 

RETURN labels(thing);