/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.utils.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.google.common.collect.Sets;

import fr.lip6.msr4j.asf.datamodel.ASFCommitter;
import fr.lip6.msr4j.asf.datamodel.ASFProjectsCommitters;
import fr.lip6.msr4j.asf.datamodel.ASFRelTypes;
import fr.lip6.msr4j.asf.datamodel.SVNProject;
import fr.lip6.msr4j.asf.utils.config.ASFProperties;
import fr.lip6.msr4j.datamodel.nodes.NodeLabel;
import fr.lip6.msr4j.utils.db.Neo4JDBHandler;

/**
 * Handles the population of ASF projects and committers in the DB. No batch
 * insertion, therefore chunks of data are inserted within many successive
 * transactions.
 * 
 * @author lom
 * 
 */
@SuppressWarnings("unchecked")
@Deprecated
public class SequentialASFCommittersDBHandler extends Neo4JDBHandler {

	private final Map<SVNProject, Node> projectNodes;
	private final Map<ASFCommitter, Node> committerNodes;

	public SequentialASFCommittersDBHandler(String dbPath) {
		super(dbPath);
		projectNodes = new HashMap<SVNProject, Node>();
		committerNodes = new HashMap<ASFCommitter, Node>();
	}

	@Override
	public void populateDB(ASFProjectsCommitters projects) {
		String[] propToIndex = { ASFProperties.SVN_ID, ASFProperties.NAME,
				ASFProperties.IS_APACHE_MEMBER, ASFProperties.WEIGHT };
		setPropertiesToIndex(propToIndex);
		final long start = System.nanoTime();
		Transaction tx = beginTransaction();
		try {
			logger.info("Beginning insertions...");
			// Updating operations go here
			insertProjects(projects);
			closeSuccessfullTransaction(tx);
			logger.info("Successfully inserted all projects.");
			tx = beginTransaction();
			insertCommitters(projects);
			closeSuccessfullTransaction(tx);
			logger.info("Successfully inserted all committers.");
			tx = beginTransaction();
			linkCommitters(projects);
			tx.success();
			logger.info("Successfully inserted all projects, committers and their relationships");
		} finally {
			tx.finish();
		}
		final long end = System.nanoTime();
		logger.info("End of transaction. All resources shut down."
				+ "Time taken: " + (end - start) / NANO);
	}

	private void linkCommitters(final ASFProjectsCommitters projects) {
		Set<ASFCommitter> colleagues;
		Set<SVNProject> mine, theirs, common;
		Node colNode, miNode;
		for (final ASFCommitter comm : projects.getCommitters()) {
			colleagues = comm.getColleagues();
			for (ASFCommitter c : colleagues) {
				theirs = c.getProjects();
				mine = comm.getProjects();
				if (theirs.size() < mine.size()) {
					common = Sets.intersection(theirs, mine);
				} else {
					common = Sets.intersection(mine, theirs);
				}
				colNode = committerNodes.get(c);
				miNode = committerNodes.get(comm);
				Relationship r = miNode.createRelationshipTo(colNode,
						ASFRelTypes.WORKSWITH);
				// Relationship weigth = number of common projects
				r.setProperty(ASFProperties.WEIGHT, common.size());
			}
		}
	}

	private void insertCommitters(final ASFProjectsCommitters projects) {
		final Set<ASFCommitter> cms = projects.getCommitters();
		Set<SVNProject> pr;
		Node prNode;
		double weight;
		for (final ASFCommitter comm : cms) {
			Node aComm = createNode(NodeLabel.COMMITTER);
			aComm.setProperty(ASFProperties.SVN_ID, comm.getSvnId());
			aComm.setProperty(ASFProperties.NAME, comm.getName());
			aComm.setProperty(ASFProperties.IS_APACHE_MEMBER, true);
			aComm.setProperty(ASFProperties.WEIGHT, comm.weight());
			committerNodes.put(comm, aComm);
			// Link committer to all projects he is involved in
			pr = comm.getProjects();
			for (SVNProject p : pr) {
				prNode = projectNodes.get(p);
				Relationship r1 = aComm.createRelationshipTo(prNode,
						ASFRelTypes.PARTICIPATES);
				Relationship r2 = prNode.createRelationshipTo(aComm,
						ASFRelTypes.COMMITTER);
				// Relationship weight = half-weight if Apache Member, 1
				// otherwise
				if (comm.isApacheMember()) {
					weight = comm.weight() * HALF;
				} else {
					weight = 1;
				}
				r1.setProperty(ASFProperties.WEIGHT, weight);
				r2.setProperty(ASFProperties.WEIGHT, weight);
			}
		}
	}

	private void insertProjects(final ASFProjectsCommitters projects) {
		Set<SVNProject> prs = projects.getSVNProjects();
		Node aPr;
		for (final SVNProject p : prs) {
			aPr = createNode(NodeLabel.PROJECT);
			aPr.setProperty(ASFProperties.NAME, p.getName());
			aPr.setProperty(ASFProperties.WEIGHT, p.degree());
			projectNodes.put(p, aPr);
		}
	}
}
