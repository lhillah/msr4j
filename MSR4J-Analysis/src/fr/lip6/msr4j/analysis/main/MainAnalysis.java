/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.main;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;

import fr.lip6.msr4j.analysis.DataFormat;
import fr.lip6.msr4j.analysis.RepoAnalyserRegistry;
import fr.lip6.msr4j.analysis.RepositoryAnalyser;
import fr.lip6.msr4j.datamodel.nodes.Repository;
import fr.lip6.msr4j.main.Error;
import fr.lip6.msr4j.main.MainAnySCM;
import fr.lip6.msr4j.utils.config.CLIMessages;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.db.SequentialNeo4JDBReader;
import fr.lip6.msr4j.utils.strings.Printer;

@SuppressWarnings("static-access")
public final class MainAnalysis extends MainAnySCM {
	private static final String EXTENSIONS = "extensions";
	private static final String OUTPUTS = "outputs";
	private static final String DESTINATION = "destination";
	private static String DataPath = "analysis";
	private static Option outputformats;
	private static Option extensions;
	private static Option destination;
	private static SequentialNeo4JDBReader dbReader;

	static {
		outputformats = OptionBuilder
				.isRequired()
				.withArgName("output-formats")
				.hasArgs()
				.withValueSeparator(',')
				.withDescription(
						"Required: comma-separated list of output format(s) for each examined repository (e.g. csv,datatable). We infer paths from repos names into destination folder.")
				.create(OUTPUTS);

		extensions = OptionBuilder.isRequired().withArgName("file-extensions").hasArgs().withValueSeparator(',')
				.withDescription("Required: comma-separated list of file extensions to examine (e.g: .java or .java,.c,.rb or 'all')")
				.create(EXTENSIONS);
		destination = OptionBuilder
				.withArgName("destination-path")
				.hasArg()
				.withDescription(
						"Destination folder, without trailing slash (folder is created if it doesn't exist). If not specified, it will be 'analysis' by default (relative path).")
				.create(DESTINATION);

	}

	private MainAnalysis() {
		super();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		CLIMessages.APPNAME = "MSR4J - Repository Analysis Application";
		CLIMessages.VERSION = "0.0.2-SNAPSHOT";
		Long start, end;
		start = System.nanoTime();
		setCoreCLIOptionsBuilder(CoreCLIOptionsBuilder.buildHelpOption().buildVersionOption().buildDBPathOption().buildRepoNameOption()
				.buildLogfileOption().addCustomOption(outputformats).addCustomOption(extensions).addCustomOption(destination));
		try {
			if (parseArguments(args, MainAnalysis.class.getCanonicalName())) {
				getLogger().info(CLIMessages.ARGS_OK);

				loadDBProperties();
				try {
					dbReader = new SequentialNeo4JDBReader(getDbProp().getUserDBPath());
					dbReader.openDB();

					// Repository r = dbReader.getRepository(args[1]);
					Set<Repository> reps;
					String[] repos = getCommandLine().getOptionValues(CoreCLIOptionsBuilder.REPO_NAME);
					if (repos.length == 1 && "all".equalsIgnoreCase(repos[0])) {
						reps = dbReader.getAllRepositories();
					} else {
						reps = dbReader.getRepositories(repos);
					}

					if (reps.size() == 0) {
						Error.fail("No repository of the specified name(s) found.", getLogger());
					} else {
						Set<String> fextensions = new TreeSet<String>(Arrays.asList(getCommandLine().getOptionValues(EXTENSIONS)));
						Map<String, String> opf = inferOutputFiles(reps, DataFormat.CSV);
						Map<String, String> opf2 = inferOutputFiles(reps, DataFormat.DATATABLE);
						if (opf != null) {
							RepoAnalyserRegistry.addOutPutFiles(opf, DataFormat.CSV);
						}
						if (opf2 != null) {
							RepoAnalyserRegistry.addOutPutFiles(opf2, DataFormat.DATATABLE);
						}
						// 2) Count number of commits per (type of) file per
						// committer
						RepositoryAnalyser ra = new RepositoryAnalyser().setCreateFileTouchBarChartData(true).setCreateFileTypePieChartData(true);
						ra.setFileExtensions(fextensions);
						List<Set<java.io.File>> values = RepoAnalyserRegistry.launchAnalysers(reps);
						if (values.isEmpty()) {
							getLogger().warn("No output from the analysers.");
						} else {
							getLogger().info("Output files are:");
							for (Set<java.io.File> ff : values) {
								for (java.io.File f : ff) {
									Printer.println(f.getAbsolutePath());
								}
							}
						}
					}
				} finally {
					dbReader.shutDownDb();
				}
			}
			end = System.nanoTime();
			getLogger().info("Total time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
		} finally {
			MSR4JLogger.shutDownLogging();
		}
		System.exit(0);
	}

	/**
	 * Infers outputs files names paths and path from the repositories' names,
	 * destintion path (specified on command line) and data format.
	 * 
	 * @param reps
	 * @param dataformat
	 * @return
	 */
	private static Map<String, String> inferOutputFiles(Set<Repository> reps, DataFormat dataformat) {
		Map<String, String> reposPaths = null;
		Set<String> formats = new HashSet<String>(Arrays.asList(getCommandLine().getOptionValues(OUTPUTS)));
		String dest = (getCommandLine().hasOption(DESTINATION) ? getCommandLine().getOptionValue(DESTINATION) : DataPath)
				+ System.getProperty("file.separator");

		File f = new File(dest);
		if (!f.exists()) {
			f.mkdirs();
			getLogger().warn("Created destination folder: {}", f.getAbsolutePath());
		}

		if (formats.contains("all") || formats.contains(dataformat.getName())) {
			reposPaths = new HashMap<String, String>();
			for (Repository r : reps) {
				reposPaths.put(r.getName(), dest + r.getName() + "." + dataformat.getName());
			}
		}
		return reposPaths;
	}

	public static SequentialNeo4JDBReader getDbReader() {
		return dbReader;
	}

}
