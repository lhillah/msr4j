package fr.lip6.msr4j.analysis.spi;

import java.util.List;
import java.util.Map;

import fr.lip6.msr4j.analysis.DataFormat;
import fr.lip6.msr4j.analysis.IRepositoryAnalyser;

/**
 * This interface must be implemented by repository analysis service providers to
 * advertise the analysis services they propose, in addition to {@link IRepositoryAnalyser}.
 * These services will then be discovered automatically.
 * @author lom
 * @see IRepositoryAnalyser
 */
public interface Analysis {
	
	/**
	 * Returns a human-readable name of the service provider.
	 * @return
	 */
	String getProviderHumanReadableName();
	
	/**
	 * Returns a human-readable name of the service provider description.
	 * @return
	 */
	String getProviderHumanReadableDescription();
	
	/**
	 * Returns the associations between each provided service (unique) name
	 * and the map of its human-readable name and description.
	 * @return
	 */
	Map<String, Map<String, String>> getServicesHumanReadableNameNDescription();
	
	/**
	 * <p>Returns the associations between each provided service (unique) name
	 * and the map of its formats setter method and list of supported formats.</p>
	 * <p>The formats setter method must have a parameter of type {@link java.util.List<DataFormat>}</p>
	 * <p><strong>Note:</strong> the information provided by the returned map provides the
	 * knowledge about which formats each service supports, whereas the {@link IRepositoryAnalyser#addDataFormat(DataFormat)}
	 * method informs the provider about all asked-for output formats, regardless of the invoked service.</p>
	 * @return
	 */
	Map<String, Map<String, List<DataFormat>>> getServicesFormatsSetterMethodNFormats();
	
	/**
	 * <p>Returns the associations between each provided service (unique) name and its
	 * activation method. The activation method must have a parameter of type {@link boolean}.</p>
	 * 
	 * <p>The activation method must implement the Builder design pattern, so that a chain of 
	 * activations can be invoked, like so: <code>providerObject.setService1(true).setService2(false).setService3(true);</code>
	 * </p>
	 * <p>Each service activation method must therefore return the reference to the service provider object.</p>
	 *
	 * @return
	 */
	Map<String, String> getServicesActivationMethod();
	
}
