/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.utils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;

import org.slf4j.Logger;

import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

public final class IOUtils {
	private final Logger logger = MSR4JLogger.getLogger(this.getClass().getCanonicalName());

	public IOUtils() {
		super();
	}

	/**
	 * Write specified content into destination file. Assumes file exists, so
	 * does not create any parent folder on the path.
	 * 
	 * @param f
	 *            the destination file
	 * @param content
	 *            the content to write into the destination file
	 * @return reference to the destination file
	 * @throws IOException
	 *             something wrong's happened
	 */
	public java.io.File writeDataIntoDestination(File f, String content) throws IOException {
		Long start, end;
		final byte[] buffer = content.getBytes();
		logger.info("Start writing data into destination file: {}.", f.getAbsolutePath());
		start = System.nanoTime();

		final RandomAccessFile raf = new RandomAccessFile(f, "rw");
		final FileChannel rwChannel = raf.getChannel();
		final ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, buffer.length);
		wrBuf.put(buffer);
		rwChannel.close();
		raf.close();
		end = System.nanoTime();
		logger.info("Finished writing data into destination file. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);

		return f;
	}

	/**
	 * Write specified content into destination file. Assumes file exists, so
	 * does not create any parent folder on the path.
	 * 
	 * @param f
	 *            the destination file
	 * @param content
	 *            the content to write into the destination file
	 * @return reference to the destination file
	 * @throws IOException
	 *             something wrong's happened
	 */
	public java.io.File writeDataIntoDestination(File f, StringBuilder content) throws IOException {
		return writeDataIntoDestination(f, content.toString());
	}

	/**
	 * Write specified content into destination. Creates folders along the path
	 * if necessary.
	 * 
	 * @param path
	 *            file system path to the destination
	 * @param content
	 *            the content to write into the destination
	 * @return the destination file
	 */
	public java.io.File writeDataIntoDestination(String path, String content) {
		Long start, end;
		final byte[] buffer = content.getBytes();
		logger.info("Start writing data into destination file: {}.", path);
		start = System.nanoTime();
		final java.io.File f = new java.io.File(path);
		try {
			if (!f.exists()) {
				java.io.File p = f.getParentFile();
				if (p != null && !p.exists()) {
					p.mkdirs();
				}
				f.createNewFile();
			}
			final RandomAccessFile raf = new RandomAccessFile(f, "rw");
			final FileChannel rwChannel = raf.getChannel();
			final ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, buffer.length);
			wrBuf.put(buffer);
			rwChannel.close();
			raf.close();
			end = System.nanoTime();
			logger.info("Finished writing data into destination file. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
		} catch (IOException e1) {
			logger.error(e1.getMessage());
			logger.error(Arrays.toString(e1.getStackTrace()));
		}
		return f;
	}

	/**
	 * Write specified content into destination. Creates folders along the path
	 * if necessary.
	 * 
	 * @param path
	 *            file system path to the destination
	 * @param content
	 *            the content to write into the destination
	 * @return the destination file
	 */
	public java.io.File writeDataIntoDestination(String path, StringBuilder content) {
		return writeDataIntoDestination(path, content.toString());
	}

	/**
	 * Reads and return the contents of a file as a String.
	 * 
	 * @param path
	 *            the file path
	 * @return the contents of the file
	 */
	public String readFileContent(String path) {
		StringBuilder result = new StringBuilder();
		RandomAccessFile f;
		try {
			f = new RandomAccessFile(path, "r");
			FileChannel inChannel = f.getChannel();
			MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
			buffer.load();
			for (int i = 0; i < buffer.limit(); i++) {
				result.append((char) buffer.get());
			}
			buffer.clear();
			inChannel.close();
			f.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
			logger.error(Arrays.toString(e.getStackTrace()));
		}
		return result.toString();
	}
}
