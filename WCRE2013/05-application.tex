\section{Application to the Apache Software Foundation}
\label{sec:application}
Open Source Software development organisations like the Apache Software Foundation
have blended the necessary skills and endeavours to produce industrial-strength,
high-quality and popular software we use everyday. 
In this capacity they have been able to successfully set up and maintain 
a network of talented individuals, an infrastructure, committees and working rules
to help them tackle the challenges they face in carrying out their tasks.

In order to understand the evolution of Open Source projects through various
aspects (social network, roles, bug tracking and resolution, etc.), 
organisations like the ASF and the collaborative behaviour among its members
have been subject to detailed investigations and analyses~\cite{maclean13,squire13,perez2013}.
Most studies build datasets from those projects' software repositories, 
from which metrics are computed and organised in visual charts or any other
means useful for analysis. Each investigation roughly goes through the
following successive steps~\cite{robles2011:floss_datamining}: 
source identification, retrieval, data extraction (and cleansing),
storage and analysis.

According to~\cite{shang10}, 8 types of analysis can be performed in
mining software repositories: metadata analysis, static source code
analysis, source code differencing, software metrics, visualisation,
code clone detection, data mining and social network analysis.
There are many tools that can help researchers perform these types of analysis.
For instance SonarQube, a popular open software quality inspection platform,
can perform static source code analysis, software metrics, code clone detection
and many more. Nemo\footnote{\url{http://nemo.sonarsource.org/dashboard/index/176190}},
the online instance of SonarQube dedicated to open source projects, 
already features the analysis of many projects from the Apache Forge.

In this application example, we focus on retrieving data about committers
and their respective projects, along with the commit history from the repositories
they are registered in. All ASF projects' public repository currently rely on 
SVN technology and are located at \url{http://svn.apache.org/repos/asf/}.
Although there is a dump of the SVN repository at \url{http://svn.us.apache},
it is not as complete and up-to-date as the actual running repository.
However, since the repository is very large, a rough initial checkout is unwise
and will put a considerable strain on bandwidth. We therefore start by selectively 
working on some projects. All projects should be progressively checked out over a longer
period of time, then updates will be easier to run and have less adverse effect on bandwidth.

\begin{figure}[!t]
\centering
\includegraphics[scale=0.8]{asfDataModel}
\caption{ASF Data Model for Data Collection}
\label{fig:asfdm}
\end{figure}

\subsection{Data Model for Data Collection}
\label{sec:asfdatamod}
For the purpose of and to ease initial data collection, we designed a lightweight 
domain specific data model for the ASF committers and projects, depicted
in \figurename~\ref{fig:asfdm}. 

The \textit{SVNProject} node is the counterpart of the \textit{Repository} node
in \figurename~\ref{fig:cm}. This node represents SVN trees into which 
the committers send their contributions.

An SVN project is always part of a \textit{Project}, for instance the
Apache Axis 2 project has a root SVN tree which is\\\url{http://svn.apache.org/repos/asf/axis/}.
In this SVN tree there is the trunk repository of the core component
of Axis 2, which is located at  \url{http://svn.apache.org/repos/asf/axis/axis2/java/core/trunk}.
In the Axis 2 SVN tree, there are Java and C subtrees. The hierarchical
relationship between SVN trees is preserved in MSR4J's data model through
the \textit{PARENT} relationship in \figurename~\ref{fig:cm}.
This \textit{Project} node is the direct counterpart of the \textit{Project} node
in  \figurename~\ref{fig:cm}.

A \emph{Top-Level Project} represents the structure holding the project management
committee (PMC) of a project. For instance, the PMC of Apache Axis 2 is Apache Axis.
For instance, Apache Commons is the PMC of the projects Apache Commons Attributes, 
BCEL, BSF, BeanUtils, Betwixt CLI, Chain, Codec, Compress, etc. 
(there are nearly 40 Apache Commons projects). This node is the counterpart of the
\textit{Project} node in \figurename~\ref{fig:cm}. The relationship
\textit{IS\_PART\_OF} between a project and its top-level project is preserved
in MSR4J's data model through the \textit{TOP\_LEVEL} relationship 
in \figurename~\ref{fig:cm}.

The co-commit relationship between committers in ASF SVN trees
in represented by the \textit{WORKS\_WITH} relationship in \figurename~\ref{fig:asfdm}.
In this application example, only this relationship prevails, so we have not considered
the computation of the \textit{SAME\_PROJECT\_AS} relationship in \figurename~\ref{fig:cm}.
%having no compelling reason to do so.

\subsection{Data Collection}
\label{sec:datacol}

For collecting data about the committers and the projects we identified
9 sources, including those identified in~\cite{squire13}, but without 
the ones related to minutes of the board of directors (i.e. project management committees),
from which data were manually extracted. In this approach, we focus on automatic extraction.
The 9 sources are public web pages on ASF's site, from which we extracted data
in the following order:
\begin{enumerate}
	\item Listing of \emph{ASF committers by login id}~\cite{asfCommByID2013}.
	A committer has signed a Contributor Licence Agreement (CLA) with 
	Apache and has an SVN access to commit contributions. The id is thus
	also an SVN id. The SVN projects each committer is registered in are 
	listed. In this page are also listed people who have signed the CLA but
	are not committers. We did not take them into account when extracting the data.
	3458 unique ids and 347 SVN projects were collected from this source.
	\item Listing of \emph{ASF committers by (SVN) project}~\cite{afsCommSVNProj2013}.
	Note that even project management committees (PMC) do have an SVN repository and
	therefore are also listed. We collected 352 SVN projects from this source, among which
	5 were new compared to the previous listing of committers by login id. These
	newly discovered projects are mostly related to perl (3). 33 projects that were
	in the previous listing (committers by login id) are tagged as \emph{incubating}
	in this listing. We had to identify them explicitly to avoid duplicates.
	9532 (non unique) ids were recorded, but none is new compared to the previous listing.
	%one is about a (probably maven-generated) site and another one about a pmc. 
	\item \emph{Committers list}~\cite{asfCommList2013}, which is not a comprehensive
	list according to the warning issued on that page, since the information is voluntarily
	provided by the committers themselves. 733 committers records were collected,
	among which 1 is new compared to the set collected so far. We also checked that
	the new committer's name is not listed in the CLAs who are not committers (committers
	by login id listing).
	\item Listing of \emph{members and emeritus members} of the ASF~\cite{asfMembers2013},
	where each member is associated with his id and the projects he is involved in.
	419 members records were collected, among which 3 are new compared to the set of
	committers consolidated so far. 
	53 emeritus members records were collected, among which 6 are new compared to 
	the previous consolidated set of committers. The id of one emeritus member was missing.
	\item Listing of \emph{top-level projects} from the same source (above) as for members,
	located at the bottom of the page. Each top-level project is run by a project management 
	committee (PMC). We collected 130 projects from this source.
	There are two special types of top-level projects:
	the Incubator where new projects undergo incubation prior to graduation, 
	and the Attic where projects are archived when they are stopped.
	\item \emph{Who runs the ASF?}: 
	listing~\cite{asfBoard2013} of the ASF board members (corporate officers) and the PMC chairs 
	(all having a Vice President position in the ASF board).
	16 board members records were collected, among which 5 are new compared to the consolidated
	set of committers, so far. 129 PMC chairs records were collected. 31 chairs are new compared
	to the consolidated set of committers, while 5 PMCs are new compared to the above set
	of TLPs. We couldn't associate any ID to these new chairs, since this is the last processed
	listing containing AFS members' names.
	Corresponding TLPs were created for the new PMCs, but with finally no project associated
	since that information is not present in the next listings below.
	\item \emph{Projects alphabetical index}~\cite{asfProjAlpha2013}, which is an alphabetical
	 list of all projects currently indexed. Each record contains the project name,
	a short description, the categories it belongs to, programming languages, and its PMC name.
	Using this source, we start relating \emph{projects} to their \emph{top-level projects}.
	198 projects records were collected.
	\item \emph{Project listing}~\cite{asfProjListing2013}, which is an alphabetically sorted
	list of all projects. The description is the same as above and the exact same number
	of projects records were collected (198).
	\item \emph{Project management committees} listing~\cite{asfPMC2013}, where 
	we collected 106 PMC records, and 196 associated projects.
	 To each PMC is associated the list of managed projects.
\end{enumerate}

\begin{figure}[!t]
\centering
\includegraphics[scale=0.7]{asfPackage}
\caption{Architecture of the ASF Case Study Application}
\label{fig:asfarchi}
\end{figure}

\subsection{Architecture}
\label{sec:asfarchi}
The architecture of this application example is depicted in \figurename~\ref{fig:asfarchi}.
The \textit{datamodel} package holds the implementing classes of the concepts
exposed in  \figurename~\ref{fig:asfdm}. The \textit{config} package contains
data sources configuration classes, while the \textit{db} package contains 
classes we need to import the data into the storage back end. In this example, we use
the Neo4J implementation of Blueprints. Finally, the \textit{parsers} package 
contains classes we need to parse the data sources, whether web pages using the
Jsoup library\footnote{\url{http://jsoup.org}} or SVN trees using SVNKit.

\subsection{Data Storage}
\label{sec:asfstore}
The harvested data from the above-mentioned 9 web page sources was stored
in a Neo4J back end of Blueprints, using the batch insertion feature 
(single-threaded and non-transactional). 
The part of MSR4J's data model that was built out these sources comprises:
the Project (top-level projects and associated projects), Repository (SVN Project)
and Committer nodes; the \emph{TOP\_LEVEL, HOLD\_ADMIN\_ROLE,
REGISTERED\_IN} and \emph{SAME\_PROJECT\_AS} relationships.
Since there was no clear association between repositories and their corresponding projects,
the relationships \emph{ATTACHED\_TO} and \emph{INVOLVE} could not be set up
during this step. This process yields 4194 nodes, 8386 properties and 
2 073 239 relationship each time it is run. It takes nearly 3.1 seconds
and occupies 66 MB of storage space. 

We then extracted data from Apache Jackrabbit SVN, using the logs, that
contain nearly 13 824 events (by the extraction time).
The previous built model was extended with new types of node and relationship:
File, Commit, \emph{REP\_BY, COP\_TO, ADD, MODIFY, DELETE, REPLACE, PROPAGATE, TOUCH}
and \emph{CONTAIN}. The FileType node part of the model was not processed.
This sequential extraction step and import into the database took nearly 8h40mn.
The database now contains 139 234 nodes, 285 782 properties, and
2 467 843 relationships. It occupies 108 MB of storage space.


\subsection{Data Analysis}
\label{sec:asfanaly}

The focus of this paper being primarily targeted on the design of MSR4J,
data analysis is not part of it. By extending our experiment to this 
step in a near future, we will ponder the pertinence and opportunity of 
including bridges to data analysis tools that MSR4J's users would provide,
including ourselves.

\subsection{Run configuration}
\label{sec:asfrun}
This experiment was run on a Mac OS 10.8.4, 2.7 Ghz i7, with 16 GB DDR3 of memory.
The application itself run in an Eclipse which has 1 GB of memory (argument -Xmx to virtual machine).
Data extraction for 8 out of the 9 web data sources took less than 1 second.
This step was multithreaded (thread pool size is 8).
Data extraction for source 1) took nearly 4 seconds, the overhead being due
to a sequentially computed consolidation step of the extracted records, basically data cleansing.
This was less needed for subsequent extractions.


\subsection{Limitations}
\label{sec:asflimit}
\paragraph{\textbf{\textsc{Roles in the ASF}}}\footnote{\url{http://www.apache.org/foundation/how-it-works.html#pmc-members}}
As the basic assumption for the purpose of this application example is
that all committers, members, emeritus members, board members and PMC chairs
are expected to be committers, they all have been included in the one set of 
committers we considered.
So, it is unclear for instance whether a board member previously unknown
in the set should actually also be considered as a committer or not.

Even if we collected PMC chairs, we have not fully considered
the PMC member role in the ASF. A PMC member is a developer or a committer 
who was elected due to merit and demonstration of commitment in a project.
A candidate listing located at \url{http://projects.apache.org/pmcs.html}
appears very incomplete to us. Some PMCs just name their chair, while 
others also list their members and a third set does not list anything at all.
We believe a more comprehensive list exists in their private repository.
However in~\cite{squire13} the board of directors meetings minutes
was manually processed and the data donated to FLOSSMole. So this dataset
could be used as a basis to complete the PMC data.

The user role was not considered, as it is not relevant in this example,
but could be interesting for analysis in a further study.
According to the ASF, a user is someone who uses their software, provide
feedback to developers (bug report, feature suggestions) and contributes to
discussions on mailing lists and provide active support to other users on forums.

The developer role was not investigated since we consider it should
be part of a wider study, where the related data is extracted from
SVN log messages, mailing lists, etc. According to the ASF, a developer
is a user who contributes code or documentation to a project but does not
have access to the code repository. Extracting developers data 
is a more challenging task which is less prone to automation and
requires dedicated investigations and manual checks.

\paragraph{\textbf{\textsc{Data Sources}}}
Some data sources were not processed in this application, for instance another list
of projects, located at \url{http://people.apache.org/projects.html}.
That list is created from voluntarily given information as the warning says,
and appears to be mixing projects appellations and levels.

Also, from the above-mentioned public web page sources we could harvest from the ASF site,
there are no clear listings associating SVN Projects to their corresponding projects.

About the repositories, the single entry point for each of them at 
\url{http://svn.apache.org/repos/asf/} does not reveal clearly the classification
that was harvested from the sources 1) and 2) above. For instance, the JackRabbit 
project has \emph{jackrabbit, jackrabbit-pmc} and \emph{jackrabbit-emeritus} listed
in sources 1) and 2), but has a single entry \emph{jackrabbit} at the 
URL mentioned earlier. While we believe the \emph{jackrabbit-pmc} and \emph{jackrabbit-emeritus}
repositories are private, the current subtree of the \emph{jackrabbit} repository
does not truly enable a further subdivision into sub repositories without prior 
knowledge about the meaning of the structure. The same observation applies to 
the other repositories. In this setting, the \emph{PARENT} relationship
between repositories cannot be meaningfully established in this application.

\paragraph{\textbf{\textsc{Database Back End}}}
This case study has used the Neo4J back end so far. Other back ends will be experimented
in the near future, to assess their good integration with MSR4J's infrastructure.
The batch insertion is assumed to perform best for initial import of data
but being single-threaded and non-transactional, it takes quite a long time.
Parallelising this step by potentially other techniques is part of future work on this
application, looking into recent experiments like~\cite{shang10}.

%Also, for the reason related to bandwidth stated earlier, we have not processed (yet)
%all SVN trees in the ASF repository.

