/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.datamodel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Models ASF projects according to their official appellations (different from
 * ASF SVN projects). Each project is related to its Top Level Project (as
 * indicated by its Project Management Committee - PMC). This class represents
 * the concept of Project in the MSR4J Data Model.
 * 
 * @see SVNProject
 * @see ASFTopLevelProject
 * @author lom
 * 
 */
public final class ASFProject {
	/**
	 * Name of the ASF project (official appellation).
	 */
	private String name;
	/**
	 * Project description.
	 */
	private String description;
	/**
	 * The project home page URL
	 */
	private String url;
	/**
	 * The programming languages of this project.
	 */
	private String[] languages;
	/**
	 * The categories of this project
	 */
	private String[] categories;
	/**
	 * SVN Projects operated under this project.
	 */
	private Set<SVNProject> svnprojects;
	/**
	 * Top level project (as indicated by its PMC) this project is attached to.
	 */
	private ASFTopLevelProject tlp;
	/**
	 * This project's TLP name should be contained in this attribute.
	 */
	private String pmc;

	public ASFProject(String name, String url) {
		this();
		this.name = name;
		this.url = url;
	}

	public ASFProject(String name, String url, String desc) {
		this(name, url);
		this.description = desc;
	}

	public ASFProject(String name) {
		this(name, null);
	}

	public ASFProject() {
		this.svnprojects = new HashSet<SVNProject>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Set<SVNProject> getSvnprojects() {
		return svnprojects;
	}

	public void setSvnprojects(Set<SVNProject> svnprojects) {
		this.svnprojects = svnprojects;
	}

	public void addSVNProject(SVNProject p) {
		this.svnprojects.add(p);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ASFTopLevelProject getTopLevelProject() {
		return tlp;
	}

	public void setTopLevelProject(ASFTopLevelProject tlp) {
		this.tlp = tlp;
		this.tlp.addProject(this);
	}

	public String[] getLanguages() {
		return languages.clone();
	}

	public void setLanguages(String[] languages) {
		this.languages = languages.clone();
	}

	public String[] getCategories() {
		return categories.clone();
	}

	public void setCategories(String[] categories) {
		this.categories = categories.clone();
	}

	/**
	 * The PMC name and the associated TLP's name should be the same. At least,
	 * the associated TLP's name should be contained in the PMC String.
	 * 
	 * @return this project's PMC name (name of the associated TLP)
	 */
	public String getPmc() {
		return pmc;
	}

	public void setPmc(String pmc) {
		this.pmc = pmc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ASFProject other = (ASFProject) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ASFProject [name=" + name + ", url=" + url + ", languages="
				+ Arrays.toString(languages) + ", categories="
				+ Arrays.toString(categories) + ", pmc=" + pmc + "]";
	}

}
