/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.datamodel;

import java.util.HashSet;
import java.util.Set;

/**
 * Models Top Level Projects of the ASF. Each of them has a Project Management
 * Committee (PMC). The list of top level projects can be primarily in the
 * footer of the page http://www.apache.org/foundation/members.html This class
 * also represents the concept of Project in the MSR4J Data Model.
 * 
 * @see ASFProject
 * @author lom
 * 
 */
public final class ASFTopLevelProject {
	private final String name;
	private String url;
	private ASFCommitter chair;
	/**
	 * Short description of this TLP.
	 */
	private String title;

	private Set<ASFProject> projects;

	public ASFTopLevelProject(String name) {
		this.name = name;
		this.projects = new HashSet<ASFProject>();
	}

	public ASFTopLevelProject(String name, String url) {
		this(name);
		this.url = url;
	}

	public ASFTopLevelProject(String name, String url, String title) {
		this(name, url);
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ASFTopLevelProject other = (ASFTopLevelProject) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (url == null) {
			if (other.url != null) {
				return false;
			}
		} else if (!url.equals(other.url)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ASFTopLevelProject [name=" + name + ", url=" + url + ", title="
				+ title + "]";
	}

	public Set<ASFProject> getProjects() {
		return projects;
	}

	public void setProjects(Set<ASFProject> projects) {
		for (ASFProject p : projects) {
			addProject(p);
		}
	}

	public void addProject(ASFProject p) {
		this.projects.add(p);
	}

	/**
	 * @return the chair
	 */
	public ASFCommitter getChair() {
		return chair;
	}

	/**
	 * Sets the committer who chairs this TLP (PMC member).
	 * 
	 * @param chair
	 *            the chair to set
	 */
	public void setChair(ASFCommitter chair) {
		this.chair = chair;
		chair.setChairedPmc(this);
	}
}
