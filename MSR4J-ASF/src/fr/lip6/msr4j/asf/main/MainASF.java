/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.main;

import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Set;

import fr.lip6.msr4j.asf.datamodel.ASFProjectsCommitters;
import fr.lip6.msr4j.asf.utils.config.Printer;
import fr.lip6.msr4j.asf.utils.parsers.ASFCommitterIndexesParser;
import fr.lip6.msr4j.main.MainAnySCM;
import fr.lip6.msr4j.utils.config.CLIMessages;
import fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.db.ConcurrentNeo4JBatchInserterSVN;
import fr.lip6.msr4j.utils.db.DBHandler;

public final class MainASF extends MainAnySCM {
	
	private MainASF() {
		super();
	}

	/**
	 * Launches the application. Expected arguments are (in this order):
	 * <ul>
	 * <li>location of the DB properties file (local file system)</li>
	 * <li>location of the repositories remote URLs properties file (local file
	 * system)</li>
	 * <li>location of the repositories local working copies local paths
	 * properties file (local file system)</li>
	 * <li>location of the properties files containing repositories credentials (anonymous
	 * is assumed if none provided for the parsed repository)</li>
	 * <li>the name(s) of the repository(ies) to process, or <em>all</em> if all
	 * repositories listed in the above files must be processed. The number of
	 * listed repositories must match in both remote URLs and local paths
	 * properties files.</li>
	 * </ul>
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			CLIMessages.APPNAME = "MSR4J - ASF Case Study Application";
			CLIMessages.VERSION = "0.0.1-SNAPSHOT";
			setCoreCLIOptionsBuilder(CoreCLIOptionsBuilder.buildCoreOptions());
			if (parseArguments(args, MainASF.class.getCanonicalName())) {
				getLogger().info(CLIMessages.ARGS_OK);
				loadCorePropertiesFromArgs();
				try {
					ASFCommitterIndexesParser parser = new ASFCommitterIndexesParser();
					ASFProjectsCommitters projects = parser.parseIndex();
					Printer.showTopLevelProjects(projects);
					Set<Entry<Object, Object>> svnUrls = findRepositoriesToScan();
					DBHandler<Object> dbh = new ConcurrentNeo4JBatchInserterSVN<Object>(getDbProp().getUserDBPath(), svnUrls, getWorkingCopiesProp(), getCredentialsProp());
					extractToDBFromScratch(dbh);
				} catch (Exception e) {
					getLogger().error(e.getMessage());
					getLogger().error(Arrays.toString(e.getStackTrace()));
				}
			}
		} finally {
			MSR4JLogger.shutDownLogging();
		}
	}
}
