/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.utils.config;

import java.util.Iterator;
import java.util.Set;

import fr.lip6.msr4j.asf.datamodel.ASFCommitter;
import fr.lip6.msr4j.asf.datamodel.ASFProject;
import fr.lip6.msr4j.asf.datamodel.ASFProjectsCommitters;
import fr.lip6.msr4j.asf.datamodel.ASFTopLevelProject;
import fr.lip6.msr4j.asf.datamodel.SVNProject;


/**
 * Single place to output information on the console (apart from normal logging)
 * 
 * @author lom
 * 
 */
public final class Printer extends fr.lip6.msr4j.utils.strings.Printer {

	private Printer() {}

	/**
	 * Shows projects in the console.
	 */
	public static void showSVNProjects(ASFProjectsCommitters asfPr) {
		// Show everything on the console (for now).
		Printer.println("\n*************************************************\n");
		Set<SVNProject> all = asfPr.getSVNProjects();
		for (SVNProject p : all) {
			Printer.println(p);
		}
		Printer.println("\n*************************************************\n");
		Printer.println("Number of projects : " + all.size());
		Printer.println("Number of committers : " + asfPr.getCommitters().size());
	}

	public static void showCommitters(ASFProjectsCommitters asfPr) {
		Printer.println("\n***************** COMMITTERS ********************\n");
		for (ASFCommitter c : asfPr.getCommitters()) {
			Printer.println(c);
		}
	}

	public static void showTopLevelProjects(ASFProjectsCommitters asfPr) {
		Printer.println("\n***************** TOP LEVEL PROJECTS ********************\n");
		Iterator<ASFTopLevelProject> it = asfPr.getTopLevelProjects()
				.iterator();
		while (it.hasNext()) {
			ASFTopLevelProject tlp = it.next();
			Printer.println(tlp);
			for (ASFProject p : tlp.getProjects()) {
				Printer.println(p);
			}
		}
	}

	public static void showASFProjects(ASFProjectsCommitters asfPr) {
		Printer.println("\n***************** ASF PROJECTS ********************\n");
		Iterator<ASFProject> it = asfPr.getASFProjects().iterator();
		while (it.hasNext()) {
			Printer.println(it.next());
		}
	}

}
