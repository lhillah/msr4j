/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.asf.utils.db;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.neo4j.cypher.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.logging.BufferingLogger;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.neo4j.batch.Neo4jBatchGraph;


import fr.lip6.msr4j.asf.datamodel.ASFCommitter;
import fr.lip6.msr4j.asf.datamodel.ASFProject;
import fr.lip6.msr4j.asf.datamodel.ASFProjectsCommitters;
import fr.lip6.msr4j.asf.datamodel.ASFTopLevelProject;
import fr.lip6.msr4j.asf.datamodel.SVNProject;
import fr.lip6.msr4j.datamodel.datatypes.RelationType;
import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.datatypes.SVNLogActionType;
import fr.lip6.msr4j.datamodel.datatypes.VEProperties;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.db.DBHandler;
import fr.lip6.msr4j.utils.parsers.SVNParser;

/**
 * Sequentially (only one thread) batch inserts data from ASF projects ans repository into
 * a Neo4J DB back end.
 * @author lom
 *
 */
public final class SequentialASFNeo4JDBInserterSVN extends
		DBHandler<ASFProjectsCommitters> {

	private final Map<ASFTopLevelProject, Vertex> tlpVertex;
	private final Map<ASFProject, Vertex> asfpVertex;
	private final Map<SVNProject, Vertex> svnVertex;
	private final Map<ASFCommitter, Vertex> cmVertex;
	private final Map<String, Vertex> fileVertex;

	private GraphDatabaseService graphDb;
	private Graph graph;
	private ExecutionEngine engine;
	private long autoId;
	private Set<Entry<Object, Object>> svnProp;
	private SVNParser svnparser;

	public SequentialASFNeo4JDBInserterSVN(String dbPath) {
		super(dbPath);
		tlpVertex = new HashMap<ASFTopLevelProject, Vertex>();
		asfpVertex = new HashMap<ASFProject, Vertex>();
		svnVertex = new HashMap<SVNProject, Vertex>();
		cmVertex = new HashMap<ASFCommitter, Vertex>();
		fileVertex = new HashMap<String, Vertex>();
	}

	public SequentialASFNeo4JDBInserterSVN(String dbPath,
			Set<Entry<Object, Object>> svnProp) {
		this(dbPath);
		this.svnProp = svnProp;
	}

	@Override
	public void populateDB(ASFProjectsCommitters projects) {
		autoId = 0L;
		final long start = System.nanoTime();
		logger.info("Starting batch insertion...");
		insertTLPProjectsSVNProjects(projects);
		insertSVNSCMData(projects);
		final long end = System.nanoTime();
		logger.info(
				"End of batch insertion. Database must be shut down. Time taken: {}.",
				(end - start) / ConcurrencyConfig.NANO);
		logger.info("Number of objects created (Vertices + edges): {}", autoId);
	}

	@Override
	public void cleanDb() {
		logger.info("Cleaning Graph DB...");
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(this.getDbPath());
		engine = new ExecutionEngine(graphDb, new BufferingLogger());
		registerShutdownHook(graphDb);
		if (graphDb != null) {
			engine.execute("START n = node(*), ref = node(0)  WHERE n<>ref DELETE n");
		}
		graphDb.shutdown();
		logger.info("Cleaned Graph DB.");
	}

	@Override
	public void createBatchDb() {
		logger.info("Creating Graph DB...");
		graph = new Neo4jBatchGraph(getDbPath());
		logger.info("Graph DB created.");
	}

	@Override
	public void shutDownDb() {
		graph.shutdown();
		logger.info("Graph DB shut down.");
	}

	@Override
	public Object beginTransaction() {
		throw new UnsupportedOperationException("This method is not supported in this application.");
	}

	@Override
	public void closeSuccessfullTransaction(Object o) {
		throw new UnsupportedOperationException("This method is not supported in in this application.");
	}

	/**
	 * Inserts data from SVN logs.
	 * 
	 * @param all
	 */
	@SuppressWarnings("rawtypes")
	private void insertSVNSCMData(ASFProjectsCommitters projects) {
		Vertex commitV, svnV, cmV, fileV;
		Edge e;
		SVNProject repo;
		Collection logEntries;
		SVNLogEntry logEntry;
		Long counts;
		svnparser = new SVNParser();
		Map<Vertex, Map<Vertex, Long>> fileTouchesCount = new HashMap<Vertex, Map<Vertex, Long>>();
		Map<Vertex, Long> cmVTouchesCount;
		try {
			logger.info("Parsing SVNs logs.");
			for (Entry<Object, Object> ent : svnProp) {
				svnparser.setUrl(ent.getValue().toString());
				svnparser.openRemoteHttpRepository();
				svnparser.checkRepositoryPath();
				// parser.displayRepositoryTree();
				// parser.displayLogEntries(true, true);

				repo = projects.getSVNProjectByName(ent.getKey().toString());
				svnV = svnVertex.get(repo);
				if (svnV == null) {
					logger.warn("Unknow SVN Project: {} ", ent.getKey()
							.toString());
					continue;
				}
				logEntries = svnparser.getLogEntries(new String[] { "" }, null,
						0, -1, true, true);
				logger.info("Parsing logs of SVN: {}.", ent.getKey().toString());
				for (Iterator entries = logEntries.iterator(); entries
						.hasNext();) {
					logEntry = (SVNLogEntry) entries.next();
					commitV = createCommitVertex(logEntry);
					cmV = cmVertex.get(projects.getCommitterById(logEntry
							.getAuthor()));
					if (cmV == null) {
						logger.warn(
								"Committer {} of revision {} is unknown!!!",
								logEntry.getAuthor(), logEntry.getRevision());
					} else {
						e = graph.addEdge(null, cmV, commitV,
								RelationType.PROPAGATE.getName());
					}

					if (logEntry.getChangedPaths().size() > 0) {
						@SuppressWarnings("unchecked")
						Set<Entry<String, SVNLogEntryPath>> changedPaths = (Set<Entry<String, SVNLogEntryPath>>) logEntry
								.getChangedPaths().entrySet();
						for (Entry<String, SVNLogEntryPath> en : changedPaths) {
							fileV = createFileVertex(en, logEntry.getRevision());
							e = graph.addEdge(null, svnV, fileV,
									RelationType.CONTAIN.getName());
							String revisionType = String.valueOf(en.getValue()
									.getType());
							if (revisionType
									.equalsIgnoreCase(SVNLogActionType.ADD
											.getName())) {
								e = graph.addEdge(null, commitV, fileV,
										RelationType.ADD.toString());
							} else if (revisionType
									.equalsIgnoreCase(SVNLogActionType.DELETE
											.getName())) {
								e = graph.addEdge(null, commitV, fileV,
										RelationType.DELETE.toString());
							} else if (revisionType
									.equalsIgnoreCase(SVNLogActionType.MODIFY
											.getName())) {
								e = graph.addEdge(null, commitV, fileV,
										RelationType.MODIFY.toString());
							} else if (revisionType
									.equalsIgnoreCase(SVNLogActionType.REPLACE
											.getName())) {
								e = graph.addEdge(null, commitV, fileV,
										RelationType.REPLACE.toString());
							}
							if (cmV != null) {
								cmVTouchesCount = fileTouchesCount.get(fileV);
								if (cmVTouchesCount == null) {
									cmVTouchesCount = new HashMap<Vertex, Long>();
									fileTouchesCount
											.put(fileV, cmVTouchesCount);
								}
								counts = cmVTouchesCount.get(cmV);
								if (counts == null) {
									counts = 0L;
								}
								counts++;
								cmVTouchesCount.put(cmV, counts);
							}
							logger.info("Processed entry: {} ", en.getValue()
									.getPath());
						}

					}
					// Handles Impact relation - TODO later find first
					// occurrence of branch or tag creation
					e = graph.addEdge(null, commitV, svnV,
							RelationType.IMPACT.toString());
				}
				logger.info(
						"Setting Touch relationships between Committers and files in SVN: {}.",
						ent.getKey().toString());
				for (Entry<Vertex, Map<Vertex, Long>> entry : fileTouchesCount
						.entrySet()) {
					fileV = entry.getKey();
					for (Entry<Vertex, Long> cmVEntry : entry.getValue()
							.entrySet()) {
						e = graph.addEdge(null, cmVEntry.getKey(), fileV,
								RelationType.TOUCH.getName());
						setEdgeProperty(e, VEProperties.COUNT.getName(),
								cmVEntry.getValue());
					}
				}
				logger.info("Finished parsing SVN: {}.", ent.getKey()
						.toString());
			}

		} catch (SVNException ex) {
			ex.printStackTrace();
			logger.error(ex.getMessage());
		}

	}

	/**
	 * Inserts TLPs, projects and SVN projects
	 * 
	 * @param projects
	 */
	private void insertTLPProjectsSVNProjects(
			final ASFProjectsCommitters projects) {
		Collection<ASFTopLevelProject> prs = projects.getTopLevelProjects();
		Vertex tlp, p, svnp, cm;
		Edge e;
		Set<ASFProject> asfPjs;
		Set<SVNProject> svnPjs;
		Set<ASFCommitter> cms;

		logger.info("Populating the DB with ASF top-level projects.");
		for (final ASFTopLevelProject aTlp : prs) {
			tlp = createASFTLPVertex(aTlp);
			// Set Projects and TOP_LEVEL relationship between them and this TLP
			asfPjs = aTlp.getProjects();
			logger.info("Populating the DB with ASF projects of TLP {}.",
					aTlp.getName());
			for (final ASFProject asfp : asfPjs) {
				p = createASFProjectVertex(asfp);
				e = graph.addEdge(null, p, tlp,
						RelationType.TOP_LEVEL.getName());
				autoId++;
				// Set SVN Projects and relationship between them and this
				// project
				svnPjs = asfp.getSvnprojects();
				logger.info(
						"Populating the DB with ASF SVN projects of Project {}.",
						asfp.getName());
				for (final SVNProject svnpr : svnPjs) {
					svnp = createSVNProjectVertex(svnpr);
					e = graph.addEdge(null, svnp, p,
							RelationType.ATTACHED_TO.getName());
					autoId++;
					// Set Committers and relationship between them and this
					// Repository
					cms = svnpr.getCommitters();
					logger.info(
							"Populating the DB with ASF committers of SVN Project {}.",
							svnpr.getName());
					for (final ASFCommitter asfcm : cms) {
						cm = createCommitterVertex(asfcm);
						logger.info("Added Committer {} --> {}.",
								asfcm.getSvnId(), asfcm.getName());
						e = graph.addEdge(null, cm, svnp,
								RelationType.REGISTERED_IN.getName());
						autoId++;
						// Consequently, also set the involve relationship
						// between committer and project this
						// Repository is attached to
						e = graph.addEdge(null, p, cm,
								RelationType.INVOLVE.getName());
						autoId++;
					}
				}
			}
		}

		// Recycle those Projects that were not included previously
		logger.info("Recycling Projects that were not handled previously.");
		for (ASFProject pr : projects.getASFProjects()) {
			if (!asfpVertex.containsKey(pr)) {
				logger.info("Recycling Project {}.", pr.getName());
				p = createASFProjectVertex(pr);
			}
		}

		// Recycle those SVN Projects that were not included previously
		logger.info("Recycling SVN Projects that were not handled previously.");
		for (SVNProject sp : projects.getSVNProjects()) {
			if (!svnVertex.containsKey(sp)) {
				logger.info("Recycling SVN Project {}.", sp.getName());
				p = createSVNProjectVertex(sp);
			}
		}

		// Recycle those Committers that were not included previously
		logger.info("Recycling Committers that were not handled previously.");
		for (ASFCommitter c : projects.getCommitters()) {
			if (!cmVertex.containsKey(c)) {
				logger.info("Recycling Committer {} --> {}.", c.getSvnId(),
						c.getName());
				p = createCommitterVertex(c);
				for (SVNProject sp : c.getProjects()) {
					e = graph.addEdge(null, p, svnVertex.get(sp),
							RelationType.REGISTERED_IN.getName());
					autoId++;
				}
			}
		}
		// Handle Same_Project_As relationship
		logger.info("Handling same_project_as relationship...");
		for (Entry<ASFCommitter, Vertex> c : cmVertex.entrySet()) {
			for (ASFCommitter col : c.getKey().getColleagues()) {
				e = graph.addEdge(null, c.getValue(), cmVertex.get(col),
						RelationType.SAME_PROJECT_AS.getName());
				autoId++;
			}
		}

		// Handle hold-admin-role
		logger.info("Handling admin roles...");
		for (final Entry<ASFCommitter, Vertex> ent : cmVertex.entrySet()) {
			tlp = tlpVertex.get(ent.getKey().getChairedPmc());
			if (tlp != null) {
				e = graph.addEdge(null, ent.getValue(), tlp,
						RelationType.HOLD_ADMIN_ROLE.getName());
				autoId++;
				setEdgeProperty(e, VEProperties.ROLE_NAME.getName(), ent
						.getKey().getAdminRole());
				logger.info("Admin role for member {}  --> {}", ent.getKey()
						.getName(),
						tlp.getProperty(VEProperties.NAME.getName()));
			}
		}
	}

	private Vertex createASFTLPVertex(ASFTopLevelProject tlp) {
		Vertex v = graph.addVertex(null);
		autoId++;
		setVertexProperty(v, VEProperties.NAME.getName(), tlp.getName());
		setVertexProperty(v, VEProperties.DECRIPTION.getName(), tlp.getTitle());
		setVertexProperty(v, VEProperties.WEB_SITE.getName(), tlp.getUrl());
		tlpVertex.put(tlp, v);
		return v;
	}

	private Vertex createASFProjectVertex(ASFProject pr) {
		Vertex v = graph.addVertex(null);
		autoId++;
		setVertexProperty(v, VEProperties.NAME.getName(), pr.getName());
		setVertexProperty(v, VEProperties.DECRIPTION.getName(),
				pr.getDescription());
		asfpVertex.put(pr, v);
		return v;
	}

	private Vertex createSVNProjectVertex(SVNProject svnp) {
		Vertex v = graph.addVertex(null);
		autoId++;
		setVertexProperty(v, VEProperties.NAME.getName(), svnp.getName());
		// TODO : URI, TYPE (SVN, CVS, Git), and isRoot from
		// repository mining
		svnVertex.put(svnp, v);
		return v;
	}

	private Vertex createCommitterVertex(ASFCommitter asfcm) {
		Vertex v = graph.addVertex(null);
		autoId++;
		setVertexProperty(v, VEProperties.SCM_ID.getName(), asfcm.getSvnId());
		setVertexProperty(v, VEProperties.NAME.getName(), asfcm.getName());
		setVertexProperty(v, VEProperties.IS_ASF_MEMBER.getName(),
				asfcm.isApacheMember());
		setVertexProperty(v, VEProperties.IS_ASF_EMERITUS_MEMBER.getName(),
				asfcm.isEmeritusMember());
		setVertexProperty(v, VEProperties.EMAIL.getName(), asfcm.getEmail());
		setVertexProperty(v, VEProperties.PERSO_WEB_PAGE.getName(),
				asfcm.getWebSite());
		cmVertex.put(asfcm, v);
		return v;
	}

	private Vertex createCommitVertex(SVNLogEntry logEntry) {
		Vertex v = graph.addVertex(null);
		autoId++;
		setVertexProperty(v, VEProperties.REVISION.getName(),
				logEntry.getRevision());
		setVertexProperty(v, VEProperties.DATE.getName(), logEntry.getDate()
				.getTime());
		setVertexProperty(v, VEProperties.MESSAGE.getName(),
				logEntry.getMessage());
		// TODO: Until actual contributor is properly parsed, we assume it is
		// the same as the committer (author)
		setVertexProperty(v, VEProperties.ACTUAL_CONTRIBUTOR.getName(),
				logEntry.getAuthor());
		return v;
	}

	private Vertex createFileVertex(Entry<String, SVNLogEntryPath> en,
			long revision) {
		Edge ed;
		Vertex v = graph.addVertex(null);
		autoId++;
		setVertexProperty(v, VEProperties.PATH.getName(), en.getValue()
				.getPath());
		getASFFileVertex().put(en.getValue().getPath(), v);
		try {
			SVNNodeKind nodeKind = svnparser.checkPath(en.getValue().getPath(),
					revision);
			setVertexProperty(v, VEProperties.IS_DIRECTORY.getName(),
					(nodeKind == SVNNodeKind.DIR));
			if (en.getValue().getCopyPath() != null) {
				Vertex from = getASFFileVertex().get(en.getValue().getCopyPath());
				if (from != null) {
					if (String.valueOf(en.getValue().getType())
							.equalsIgnoreCase(SVNLogActionType.ADD.getName())) {
						ed = graph.addEdge(null, from, v,
								RelationType.COP_TO.toString());
						setEdgeProperty(ed,
								VEProperties.COPY_REVISION.getName(), en
										.getValue().getCopyRevision());
					} else { // We assume it is a replacement otherwise
						ed = graph.addEdge(null, from, v,
								RelationType.REP_BY.toString());
						setEdgeProperty(ed,
								VEProperties.REPLACEMENT_REVISION.getName(), en
										.getValue().getCopyRevision());
					}
				} else {
					logger.warn(
							"Could not establish copy or replacement relationship for {}, from {}.",
							en.getValue().getPath(), en.getValue()
									.getCopyPath());
				}
				// TODO : File Type!
			}
		} catch (SVNException e) {
			e.printStackTrace();
			logger.warn(e.getMessage());
		}
		return v;
	}

	@Override
	public Vertex createSCMProjectVertex(Object scmprop) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	@Override
	public Vertex createSCMProjectVertex(String name, RepositoryType type, String uri, boolean isRoot) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	@Override
	public Vertex createCommitterVertex(Object cm) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	@Override
	public Vertex createCommitterVertex(String scmID, String name, String email, String webpage) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	@Override
	public Vertex createCommitVertex(Object logEntry) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	@Override
	public Vertex createCommitVertex(String revision, long date, String message, String actualContributor) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	@Override
	public Vertex createFileVertex(String path, String revision) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	@Override
	public void establishCopyToRepByRelation(Object en) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	@Override
	public void incrementallyPopulate(DBHandler<Object> dbh) {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");
	}

	/**
	 * @return the fileVertex
	 */
	public Map<String, Vertex> getASFFileVertex() {
		return fileVertex;
	}

	@Override
	public void incrementalBatchLoad() {
		throw new UnsupportedOperationException("This method is not supported in the ASF implementation");	
	}
}
