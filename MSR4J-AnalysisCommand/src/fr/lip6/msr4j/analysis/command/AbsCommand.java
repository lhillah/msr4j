/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.command;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;

import org.slf4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;

import fr.lip6.msr4j.utils.config.MSR4JLogger;

public abstract class AbsCommand {

	private static final String NONE = "None";

	private final Logger logger;

	public AbsCommand() {
		super();
		logger = MSR4JLogger.getLogger(getClass().getCanonicalName());
	}

	/**
	 * Writes the root of the Json tree.
	 * 
	 * @param jg
	 * @throws IOException
	 * @throws JsonGenerationException
	 */
	public void writeJsonRoot(JsonGenerator jg) throws IOException, JsonGenerationException {

		jg.writeStartObject();// {
		jg.writeStringField("id", "Root");
		jg.writeStringField("name", "Root");
		jg.writeFieldName("parent");
		jg.writeStartArray();
		jg.writeEndArray();
		jg.writeEndObject(); // }
	}

	/**
	 * @param jg
	 * @throws IOException
	 * @throws JsonGenerationException
	 */
	public void writeGlobalArrayStart(JsonGenerator jg) throws IOException, JsonGenerationException {
		jg.writeStartArray(); // [
	}

	/**
	 * @param jg
	 * @throws IOException
	 * @throws JsonGenerationException
	 */
	public void writeGlobalArrayEnd(JsonGenerator jg) throws IOException, JsonGenerationException {
		jg.writeEndArray(); // ]
	}

	/**
	 * @param jg
	 * @throws IOException
	 * @throws JsonGenerationException
	 */
	public void writeParentRoot(JsonGenerator jg) throws IOException, JsonGenerationException {
		writeParent(jg, "Root");
	}

	/**
	 * @param jg
	 * @param pa
	 * @throws IOException
	 * @throws JsonGenerationException
	 */
	public void writeParent(JsonGenerator jg, String pa) throws IOException, JsonGenerationException {
		jg.writeFieldName("parent");
		jg.writeStartArray();
		jg.writeString(pa);
		jg.writeEndArray();
	}

	/**
	 * @param jg
	 * @param pa
	 * @throws IOException
	 * @throws JsonGenerationException
	 */
	public void writeStringField(JsonGenerator jg, String name, String value) throws IOException, JsonGenerationException {
		jg.writeStringField(name, value);
	}

	/**
	 * Just outputs None.
	 */
	public void sayNone() {
		PrintStream out = System.out;
		out.println(NONE);
	}

	/**
	 * Outputs stack trace elements.
	 * 
	 * @param trace
	 */
	public void outputTrace(StackTraceElement[] trace) {
		logger.error(Arrays.toString(trace));
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}
	
	public void shutDownLog(){
		MSR4JLogger.shutDownLogging();
	}

}