/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.command;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Set;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Property;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.ServiceProperty;
import org.apache.felix.ipojo.annotations.Validate;
import org.apache.felix.service.command.Descriptor;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;

import fr.lip6.msr4j.utils.spi.ApplicationPaths;
import fr.lip6.msr4j.utils.spi.Neo4JDBReader;

/**
 * Felix-based commands for listing repositories.
 * 
 * @author lom
 * 
 */
@Component(immediate = true)
@Instantiate
@Provides(specifications = RepositoriesCommand.class)
public class RepositoriesCommand extends AbsCommand {

	@Property
	private long rid;

	@Property
	private JsonFactory jfactory;

	@Validate
	private void start() throws Exception {
		jfactory = new JsonFactory();
		rid = 0;
		PrintStream out = System.out;
		out.println("Started Repositories Command.");
		out.flush();
	}

	@Invalidate
	private void stop() throws Exception {
		jfactory = null;
		PrintStream out = System.out;
		out.println("Stopped Repositories Command.");
		out.flush();
	}

	@Requires
	Neo4JDBReader dbReader;
	
	@Requires
	ApplicationPaths paths;

	/**
	 * Defines the command scope (ipojo).
	 */
	@ServiceProperty(name = "osgi.command.scope", value = "msr4j")
	String m_scope;

	/**
	 * Defines the functions (commands).
	 */
	@ServiceProperty(name = "osgi.command.function", value = "{}")
	String[] m_function = new String[] { "repositories", "reposNFileTypes" };

	@Descriptor("List all repositories in the current DB back end.")
	public void repositories() {
		PrintStream out = System.out;
		plistAllRepositories(out, null);
		out.println("");
		out.flush();
	}

	@Descriptor("List all repositories in the current back-end DB, and for each of them the file type they contain.")
	public void reposNFileTypes() {
		PrintStream out = System.out;
		plistAllReposNFT(out, null);
		out.println("");
		out.flush();
	}

	private void plistAllRepositories(PrintStream os, File f) {
		JsonGenerator jg;
		try {
			dbReader.setDbPath(paths.getDbPath());
			dbReader.openDB();
			Set<String> rps = dbReader.getAllRepositoriesNames();
			try {
				if (os != null) {
					jg = jfactory.createGenerator(os, JsonEncoding.UTF8);
				} else if (f != null) {
					jg = jfactory.createGenerator(f, JsonEncoding.UTF8);
				} else {
					throw new JsonGenerationException("Could not create Json Generator for output");
				}
				writeGlobalArrayStart(jg);
				if (rps != null && !rps.isEmpty()) {
					for (String r : rps) {
						jg.writeString(r);
					}
				}
				writeGlobalArrayEnd(jg);
				jg.flush();
				jg.close();
			} catch (JsonGenerationException j) {
				sayNone();
				outputTrace(j.getStackTrace());
			} catch (IOException e) {
				sayNone();
				outputTrace(e.getStackTrace());
			}
		} finally {
			dbReader.shutDownDb();
			shutDownLog();
		}
	}

	private void plistAllReposNFT(PrintStream os, File f) {
	 // TODO
	}
}
