/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.command;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Instantiate;
import org.apache.felix.ipojo.annotations.Invalidate;
import org.apache.felix.ipojo.annotations.Property;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.ServiceProperty;
import org.apache.felix.ipojo.annotations.Validate;
import org.apache.felix.service.command.Descriptor;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;

import fr.lip6.msr4j.analysis.formats.DataFormat;
import fr.lip6.msr4j.analysis.spi.Analyser;
import fr.lip6.msr4j.analysis.spi.AnalysisService;

/**
 * Felix-based commands for listing analysis services and service invocation.
 * @author lom
 *
 */
@Component(immediate = true)
@Instantiate
@Provides(specifications = ServicesCommand.class)
public class ServicesCommand extends AbsCommand {

	/**
	 * For the Json objects' id.
	 */
	@Property
	private long dfid;

	@Property
	private JsonFactory jfactory;

	@Validate
	private void start() throws Exception {
		jfactory = new JsonFactory();
		dfid = 0L;
		PrintStream out = System.out;
		StringBuffer bf = new StringBuffer("Started Analysis Service Command.");
		out.println(bf.toString());
		out.flush();
	}

	@Invalidate
	private void stop() throws Exception {
		jfactory = null;
		PrintStream out = System.out;
		out.println("Stopped Analysis Service Command.");
		out.flush();
	}

	/**
	 * Defines the command scope (ipojo).
	 */
	@ServiceProperty(name = "osgi.command.scope", value = "msr4j")
	String m_scope;

	/**
	 * Defines the functions (commands).
	 */
	@ServiceProperty(name = "osgi.command.function", value = "{}")
	String[] m_function = new String[] { "services", "services4df", "services4dfs", "servicesDump", "services4dfDump", "services4dfsDump" };

	/**
	 * Analyses services. Injected by the container.
	 * */
	@Requires(optional = true)
	private Analyser[] m_analysers;

	@Descriptor("List all available analysis services")
	public void services() {
		PrintStream out = System.out;
		plistAllServices(out, null);
		out.println("");
		out.flush();
	}

	@Descriptor("List available analysis services that support one of the provided formats (disjonction)")
	public void services4df(@Descriptor("The formats to use for filtering") Set<String> dataFormats) {
		PrintStream out = System.out;
		pServices4dfs(dataFormats, false, out, null);
		out.flush();
		return;
	}

	@Descriptor("List available analysis services that support all the provided formats (conjonction)")
	public void services4dfs(@Descriptor("The formats to use for filtering") Set<String> dataFormats) {
		PrintStream out = System.out;
		pServices4dfs(dataFormats, true, out, null);
		out.flush();
	}

	@Descriptor("List all available analysis services, write the result in the provided file path.")
	public void servicesDump(@Descriptor("The path to the file where to write the result") String path) {
		pServicesDump(path);
	}

	@Descriptor("List available analysis services supporting one of the formats, write the result in the provided file path.")
	public void services4dfDump(@Descriptor("The formats to use for filtering") Set<String> dataFormats,
			@Descriptor("The path to the file where to write the result") String path) {
		File f = new File(path);
		pServices4dfs(dataFormats, false, null, f);
	}

	@Descriptor("List available analysis services supporting all the formats, write the result in the provided file path.")
	public void services4dfsDump(@Descriptor("The formats to use for filtering") Set<String> dataFormats,
			@Descriptor("The path to the file where to write the result") String path) {
		File f = new File(path);
		pServices4dfs(dataFormats, true, null, f);
	}

	private void pServicesDump(String path) {
		File f = new File(path);
		plistAllServices(null, f);
	}

	/**
	 * Helper method for {@link #services4df(Set)} and {@ling
	 * #services4dfs(Set)} using a switch to determine conjonction (all = true)
	 * or disjonction (all=false) on the set of provided data formats with the
	 * candidate services.
	 * 
	 * @param dataFormats
	 * @param all
	 */
	private void pServices4dfs(Set<String> dataFormats, boolean all, OutputStream os, File f) {
		if (m_analysers.length > 0) {
			Set<DataFormat> formats = EnumSet.noneOf(DataFormat.class);
			for (String s : dataFormats) {
				formats.add(DataFormat.valueOf(s));
			}
			Set<Analyser> retained = new HashSet<Analyser>();
			Set<AnalysisService> services = null;
			if (all) { // conjonction
				for (int i = 0; i < m_analysers.length; i++) {
					services = m_analysers[i].getServicesSupportingAllFormats(formats);
					if (services != null && !services.isEmpty()) {
						retained.add(m_analysers[i]);
					}
				}
			} else { // disjonction
				for (int i = 0; i < m_analysers.length; i++) {
					services = m_analysers[i].getServicesSupportingFormats(formats);
					if (services != null && !services.isEmpty()) {
						retained.add(m_analysers[i]);
					}
				}
			}
			if (!retained.isEmpty()) {
				JsonGenerator jg;
				try {
					if (os != null) {
						jg = jfactory.createGenerator(os, JsonEncoding.UTF8);
					} else if (f != null) {
						jg = jfactory.createGenerator(f, JsonEncoding.UTF8);
					} else {
						throw new JsonGenerationException("Could not create Json Generator for output");
					}
					plistServices(jg, retained.toArray(new Analyser[0]));
					jg.flush();
					jg.close();
				} catch (JsonGenerationException j) {
					sayNone();
					outputTrace(j.getStackTrace());
				} catch (IOException e) {
					sayNone();
					outputTrace(e.getStackTrace());
				}
			} else {
				sayNone();
			}

		} else {
			sayNoProvider();
		}
	}

	/**
	 * Builds JSON response for services descriptions, either through
	 * given output stream or into given file.
	 * @param os the output stream into which the Json response is written
	 * @param f or the file into file the response is written
	 */
	private void plistAllServices(OutputStream os, File f) {
		if (m_analysers.length > 0) {
			JsonGenerator jg;
			try {
				if (os != null) {
					jg = jfactory.createGenerator(os, JsonEncoding.UTF8);
					jg.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
				} else if (f != null) {
					jg = jfactory.createGenerator(f, JsonEncoding.UTF8);
				} else {
					throw new JsonGenerationException("Could not create Json Generator for output");
				}
				plistServices(jg, m_analysers);
				jg.flush();
				jg.close();
				jg = null;
			} catch (JsonGenerationException j) {
				sayNone();
				outputTrace(j.getStackTrace());
			} catch (IOException e) {
				sayNone();
				outputTrace(e.getStackTrace());
			}
		} else {
			sayNoProvider();
		}
	}

	/**
	 * Lists the services of the analyser passed in argument.
	 * 
	 * @param jg
	 * @param aps
	 * @throws JsonGenerationException
	 * @throws IOException
	 */
	private void plistServices(JsonGenerator jg, Analyser[] aps) throws JsonGenerationException, IOException {
		jg.useDefaultPrettyPrinter();
		writeGlobalArrayStart(jg);
		writeJsonRoot(jg);
		Analyser pr; // a provider
		for (int i = 0; i < aps.length; i++) {
			pr = aps[i];
			writeAnalysisProvider(jg, pr);
			writeProviderServices(jg, pr);
		}
		writeGlobalArrayEnd(jg);
	}

	/**
	 * It is not normal to have a provider which doesn't provide any service...
	 * 
	 * @param jg
	 * @param pr
	 * @throws IOException
	 * @throws JsonGenerationException
	 */
	private void writeProviderServices(JsonGenerator jg, Analyser pr) throws JsonGenerationException, IOException {
		Set<AnalysisService> snd = pr.getProvidedServices();
		for (AnalysisService s : snd) {
			jg.writeStartObject();
			writeParent(jg, pr.getProviderHumanReadableName());
			writeStringField(jg, "id", s.getHumanReadableName());
			writeStringField(jg, "name", s.getHumanReadableName());
			writeStringField(jg, "description", s.getHumanReadableDescription());
			writeStringField(jg, "type", "service");
			jg.writeEndObject();
			writeServiceSupportedFormats(jg, s);
		}
	}

	/**
	 * Lists the supported formats by the analysis service passed in argument.
	 * @param jg
	 * @param s
	 * @throws IOException
	 * @throws JsonGenerationException
	 */
	private void writeServiceSupportedFormats(JsonGenerator jg, AnalysisService s) throws JsonGenerationException, IOException {
		Set<DataFormat> fs = s.getSupportedFormats();
		for (DataFormat f : fs) {
			jg.writeStartObject();
			writeParent(jg, s.getHumanReadableName());
			writeStringField(jg, "id", f.getName() + dfid++);
			writeStringField(jg, "name", f.getName());
			writeStringField(jg, "type", "format");
			jg.writeEndObject();
		}
	}

	/**
	 * List analyser details.
	 * @param jg
	 * @param pr
	 * @throws JsonGenerationException
	 * @throws IOException
	 */
	private void writeAnalysisProvider(JsonGenerator jg, Analyser pr) throws JsonGenerationException, IOException {
		jg.writeStartObject();
		writeParentRoot(jg);
		writeStringField(jg, "providerfqn", pr.getClass().getCanonicalName());
		writeStringField(jg, "id", pr.getProviderHumanReadableName());
		writeStringField(jg, "name", pr.getProviderHumanReadableName());
		writeStringField(jg, "description", pr.getProviderHumanReadableDescription());
		writeStringField(jg, "type", "provider");
		jg.writeEndObject();
	}

	/**
	 * Just outputs None, followed by (next line)
	 * "There is no analysis provider".
	 */
	private void sayNoProvider() {
		PrintStream out = System.out;
		sayNone();
		out.println("There is no analysis service provider.");
	}
}
