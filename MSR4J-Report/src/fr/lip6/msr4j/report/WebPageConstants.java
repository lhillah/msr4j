/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.report;

import java.io.File;

public final class WebPageConstants {

	public static final String SIDEMENUFRAGMENTAFTER_PHP = "sidemenufragmentafter.php";

	public static final String SIDEMENUFRAGMENTBEFORE_PHP = "sidemenufragmentbefore.php";

	public static final String REQUIRE_HEAD = "<?php require (\"layout/head.php\"); ?>";

	public static final String REQUIRE_SIDEMENU = "<?php require (\"layout/sidemenu.php\"); ?>";

	public static final String REQUIRE_FOOTER = "<?php require (\"layout/footer.php\"); ?>";

	public static final String SCRIPT_START = "<script type=\"text/javascript\">";

	public static final String SCRIPT_END = "</script>";

	public static final String MAIN_HEAD = "<!-- MAIN -->\n <div id=\"main\">\n <!-- HEADER -->\n <div id=\"header\">\n <div id=\"page-title\">";

	public static final String DIV_END = "</div>";

	public static final String MAIN_BREAD = "<!-- Breadcrumb-->\n<div id=\"breadcrumbs\">\nYou are here:\n<a title=\"Home\" href=\"index.php\">Home</a> &raquo;\n<a title=\"Repositories\" href=\"repositories.php\">Repositories</a> &raquo;";

	public static final String MAIN_CONTENT_START = "<!-- ENDS HEADER -->\n<!-- CONTENT -->\n<div id=\"content\">\n<!-- PAGE CONTENT -->\n<div id=\"page-content\">";

	public static final String H4_START = "<h4 class=\"header-line\">";

	public static final String H4_END = "</h4>";

	public static final String CHART_DIV_STYLE = "style=\"width: 600px; height: 400px;\"";
	
	public static final String CHART_GETIMAGE_START = "<div class=\"get-chart-image\"><input type='button' onclick=\"getImgData('";
	
	public static final String CHART_GETIMAGE_END = "')\" value='Get Image' />";
	
	public static final String REQUIRE_GETIMAGE = "<?php require (\"layout/getimage.php\"); ?>";

	public static final String F_SEP = System.getProperty("file.separator");

	public static final String NL = "\n";

	public static final String PHP_EXT = ".php";

	public static final String SIDEMENUFILE = "sidemenu.php";

	public static final String SIDEMENUPATH = "layout";

	/**
	 * Extract the type of analysis performed, from the name of the analysis
	 * data file.
	 * 
	 * @param f
	 * @return the type of analysis, or (unknown type of analysis) if it cannot
	 *         be figured out
	 */
	public static String extractAnalysisType(File f) {
		String temp = null;
		StringBuilder result = new StringBuilder();
		String[] parts = f.getName().split("-");
		if (parts.length >= 3) {
			temp = parts[1];
			parts = temp.split("_");
			if (parts.length > 0) {
				for (String s : parts) {
					result.append(s).append(" ");
				}
			} else {
				result.append(temp);
			}
		} else {
			result.append("Unknown type of analysis");
		}
		return result.toString().trim();
	}

	/**
	 * Extract the type of chart from the name of the analysis data file. By
	 * convention, the file must be named :
	 * repoName-Type_of_Analysis-ChartType.extension In case some chart types
	 * have "." in their name, this methods extracts the name up the the last
	 * index of "." (which should be followed by file extension).
	 * 
	 * @param f
	 *            the data file from the name of which extract the chart type
	 * @return the type of chart, null if it cannot be figured out
	 */
	public static String extractChartType(File f) {
		String temp = null, charttype = null;
		String[] parts = f.getName().split("-");
		if (parts.length >= 3) {
			temp = parts[2];
			charttype = temp.substring(0, temp.lastIndexOf("."));
		}
		return charttype;
	}

	/**
	 * Extracts the repository name from the analysis data file.
	 * 
	 * @param f
	 * @return the repository name from the analysis data file, or the complete
	 *         file name without extension if it cannot be figured out
	 */
	public static String extractRepoName(File f) {
		int index = f.getName().indexOf("-") != -1 ? f.getName().indexOf("-") : f.getName().lastIndexOf(".");
		String repoName = f.getName().substring(0, index);
		return repoName;
	}

	public WebPageConstants() {
	}

}
