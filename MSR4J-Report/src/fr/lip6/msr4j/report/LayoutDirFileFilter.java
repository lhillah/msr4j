/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.report;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class LayoutDirFileFilter implements FileFilter {

	public LayoutDirFileFilter() {
		super();
	}

	@Override
	public boolean accept(File f) {
		boolean result = true;
		String name;
		if (f.isDirectory()) {
			name = f.getName();
			if (!"layout".contains(name)) {
				result = false;
			} else {
				Set<String> files = new TreeSet<String>(Arrays.asList(f.list()));
				if (!files.contains("sidemenu.php")) {
					result = false;
				}
			}
		} else {
			result = false;
		}
		return result;
	}

}
