/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.report;

import static fr.lip6.msr4j.report.WebPageConstants.F_SEP;
import static fr.lip6.msr4j.report.WebPageConstants.SIDEMENUFILE;
import static fr.lip6.msr4j.report.WebPageConstants.SIDEMENUFRAGMENTAFTER_PHP;
import static fr.lip6.msr4j.report.WebPageConstants.SIDEMENUFRAGMENTBEFORE_PHP;
import static fr.lip6.msr4j.report.WebPageConstants.SIDEMENUPATH;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import fr.lip6.msr4j.analysis.DataFormat;
import fr.lip6.msr4j.analysis.utils.IOUtils;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

/**
 * Builds basic reports from analysis data in charts. This class is designed for
 * extension.
 * 
 * @author lom
 * 
 */
public class WebSiteReportBuilder {

	/**
	 * Given an HTML ul element and a set of names, removes the li elements that
	 * contain a elements, the text of which can be found in the provided set.
	 * We use this methods to clean the submenu which point to generated reports
	 * in case new reports' names are duplicates of existing ones.
	 * 
	 * We assume then that the new reports should replace the existing ones
	 * having the same name, while keeping the others that are not part of the
	 * new generation wagon.
	 * 
	 * @param repoNames
	 *            the names for which to check for duplicates
	 * @param ul
	 *            the containing HTLM ul element
	 * @return the ul element, without the duplicates
	 */
	public static Element removeDuplicateChildren(Set<String> repoNames, Element ul) {
		Element result = ul;
		Set<Element> retained = new HashSet<Element>();
		Elements lis = ul.getElementsByTag("li");
		@SuppressWarnings("unused")
		Elements lus;
		Element li, a;
		Iterator<Element> iter;

		for (iter = lis.iterator(); iter.hasNext();) {
			li = iter.next();
			a = li.select("a").first();
			if (!repoNames.contains(a.ownText())) {
				retained.add(li);
			}
		}
		lus = ul.select("li").remove();
		for (Element e : retained) {
			result.appendChild(e);
		}
		return result;
	}

	private final Logger logger = MSR4JLogger.getLogger(this.getClass().getCanonicalName());
	/**
	 * Expected web site path.
	 */
	private String websitePath;
	/**
	 * Expected analysis data files path.
	 */
	private String dataPath;
	private IOUtils io;
	/**
	 * Associates to each repository name the set of analysis data files related
	 * to it. Figuring out the files relies on a convention the user must
	 * follow. <repository name, Set<data files>>
	 */
	private Map<String, Set<File>> repoDataFiles;

	/**
	 * Assocites to each repository name the set of analysis report files
	 * related to it.
	 */
	private Map<String, File> repoReports;

	public WebSiteReportBuilder(String webSitePath, String dataPath) {
		this.setWebsitePath(webSitePath);
		this.setDataPath(dataPath);
		repoDataFiles = new HashMap<String, Set<File>>();
	}

	/**
	 * Creates built-in reports as Google Charts. The data for these charts are
	 * expected to be stored in .datatable files in the specified data path.
	 */
	public void buildGoogleCharts() {
		final File f = new File(dataPath);
		final Set<File> files = new HashSet<File>(Arrays.asList(f.listFiles()));
		String repoName, sidemenuPath;
		Long start, end;
		start = System.nanoTime();
		getLogger().info("Building charts using Google Charts API.");
		getLogger().warn("Warning: I only support files ending with {} extensions", DataFormat.DATATABLE.getName());
		for (File fi : files) {
			if (fi.getName().endsWith(DataFormat.DATATABLE.getName())) {
				repoName = WebPageConstants.extractRepoName(fi);
				insertRepoDataFileIntoMap(repoName, fi);
			}
		}
		if (!repoDataFiles.isEmpty()) {
			getLogger().info("Building report for each repository.");
			final List<Callable<Map<String, File>>> partition = new ArrayList<Callable<Map<String, File>>>();
			for (Entry<String, Set<File>> ent : repoDataFiles.entrySet()) {
				partition.add(new RepositoryReportBuilder(ent, websitePath));
			}

			final ExecutorService exec = ConcurrencyConfig.getFixedThreadPoolExecutor();
			try {
				List<Future<Map<String, File>>> values = exec.invokeAll(partition, ConcurrencyConfig.ALLOWED_EXEC_TIME100, TimeUnit.SECONDS);
				Entry<String, File> en;
				repoReports = new HashMap<String, File>();
				for (Future<Map<String, File>> value : values) {
					en = value.get().entrySet().iterator().next();
					repoReports.put(en.getKey(), en.getValue());
				}
				getLogger().info("Report builders have finished.");
				exec.shutdown();
				sidemenuPath =  websitePath + F_SEP + SIDEMENUPATH ;
				getLogger().info("Updating 'Repositories' menu in website side menu file {}/{}", sidemenuPath, SIDEMENUFILE);
				updateSideMenu(repoReports, sidemenuPath, SIDEMENUFILE);
			} catch (InterruptedException | ExecutionException e) {
				getLogger().error(e.getMessage());
				getLogger().error(Arrays.toString(e.getStackTrace()));
			}

		} else {
			getLogger().warn("There is no analysis data files in the specified directory!");
		}
		end = System.nanoTime();
		getLogger().info("End of Web Site Report Builder. Time = {} sec.", (end - start) / ConcurrencyConfig.NANO);
	}

	public String getDataPath() {
		return dataPath;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @return the websitePath
	 */
	public String getWebsitePath() {
		return websitePath;
	}

	private void insertRepoDataFileIntoMap(String repoName, File fi) {
		Set<File> files;
		if (repoDataFiles.containsKey(repoName)) {
			files = repoDataFiles.get(repoName);
			files.add(fi);
		} else {
			files = new HashSet<File>();
			files.add(fi);
			repoDataFiles.put(repoName, files);
		}
	}

	/**
	 * Set the path where to find analysis data files.
	 * 
	 * @param dataPath
	 */
	public void setDataPath(String dataPath) {
		this.dataPath = dataPath;
	}

	/**
	 * @param websitePath
	 *            the websitePath to set
	 */
	public void setWebsitePath(String websitePath) {
		this.websitePath = websitePath;
	}

	/**
	 * Updates Side menu entries with the name of each repository for which a
	 * report was built and link to the report file.
	 * 
	 * @param reports
	 * @param sidemenuPath
	 *            the path to the side menu file
	 */
	public void updateSideMenu(Map<String, File> reports, String sidemenuPath, String sidemenuFile) {
		Document doc;
		Element ul, newUl;
		File f = new File(sidemenuPath + F_SEP + sidemenuFile);
		StringBuilder content = new StringBuilder();
		try {
			// We keep what is already in the repository submenu
			doc = Jsoup.parse(f, "UTF-8");
			ul = doc.getElementById("sidebar").getElementById("nav").getElementById("reports-section");
			newUl = removeDuplicateChildren(reports.keySet(), ul);
			for (Entry<String, File> ent : reports.entrySet()) {
				newUl.append("<li><a href=\"" + ent.getValue().getName() + "\">" + ent.getKey() + "</a></li>\n");
			}
			io = new IOUtils();
			content.append(io.readFileContent(sidemenuPath + F_SEP + SIDEMENUFRAGMENTBEFORE_PHP)).append(newUl.html())
					.append(io.readFileContent(sidemenuPath + F_SEP + SIDEMENUFRAGMENTAFTER_PHP));
			io.writeDataIntoDestination(f, content.toString());
		} catch (IOException e) {
			getLogger().error(e.getMessage());
			getLogger().error(Arrays.toString(e.getStackTrace()));
		}
	}

}
