/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.report;

import static fr.lip6.msr4j.report.WebPageConstants.CHART_DIV_STYLE;
import static fr.lip6.msr4j.report.WebPageConstants.DIV_END;
import static fr.lip6.msr4j.report.WebPageConstants.F_SEP;
import static fr.lip6.msr4j.report.WebPageConstants.H4_END;
import static fr.lip6.msr4j.report.WebPageConstants.H4_START;
import static fr.lip6.msr4j.report.WebPageConstants.MAIN_BREAD;
import static fr.lip6.msr4j.report.WebPageConstants.MAIN_CONTENT_START;
import static fr.lip6.msr4j.report.WebPageConstants.MAIN_HEAD;
import static fr.lip6.msr4j.report.WebPageConstants.NL;
import static fr.lip6.msr4j.report.WebPageConstants.PHP_EXT;
import static fr.lip6.msr4j.report.WebPageConstants.REQUIRE_FOOTER;
import static fr.lip6.msr4j.report.WebPageConstants.REQUIRE_HEAD;
import static fr.lip6.msr4j.report.WebPageConstants.REQUIRE_SIDEMENU;
import static fr.lip6.msr4j.report.WebPageConstants.SCRIPT_END;
import static fr.lip6.msr4j.report.WebPageConstants.SCRIPT_START;
import static fr.lip6.msr4j.report.WebPageConstants.CHART_GETIMAGE_START;
import static fr.lip6.msr4j.report.WebPageConstants.CHART_GETIMAGE_END;
import static fr.lip6.msr4j.report.WebPageConstants.REQUIRE_GETIMAGE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

import org.slf4j.Logger;

import fr.lip6.msr4j.analysis.utils.IOUtils;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

public final class RepositoryReportBuilder implements Callable<Map<String, File>> {
	private final Logger logger = MSR4JLogger.getLogger(this.getClass().getCanonicalName());
	private Entry<String, Set<File>> repoInfo;
	private String destinationPath;

	public RepositoryReportBuilder() {
		// TODO Auto-generated constructor stub
	}

	public RepositoryReportBuilder(Entry<String, Set<File>> info, String websitePath) {
		this.setRepoInfo(info);
		this.setDestinationPath(websitePath);
	}

	@Override
	public Map<String, File> call() throws Exception {
		Map<String, File> result = new HashMap<String, File>();
		String charType, analysisType;
		Long start, end;
		start = System.nanoTime();
		StringBuilder content = new StringBuilder();
		logger.info("Building report for repository {}.", repoInfo.getKey());
		content.append(REQUIRE_HEAD).append(NL);
		for (File f : repoInfo.getValue()) {
			charType = WebPageConstants.extractChartType(f);
			analysisType = WebPageConstants.extractAnalysisType(f);
			if (charType != null) {
				content.append(buildChartScript(f, charType, analysisType));
			}
		}
		content.append(REQUIRE_SIDEMENU);
		content.append(buildMainDiv(repoInfo.getKey(), repoInfo.getValue()));
		content.append(REQUIRE_FOOTER).append(NL);
		
		File report = new File(destinationPath + F_SEP + repoInfo.getKey() + PHP_EXT);
		new IOUtils().writeDataIntoDestination(report, content.toString());
		result.put(repoInfo.getKey(), report);
		end = System.nanoTime();
		logger.info("Finished report for repository {}. Time = {} sec.", repoInfo.getKey(), (end - start) / ConcurrencyConfig.NANO);
		return result;
	}

	private String buildMainDiv(String name, Set<File> files) {
		StringBuilder content = new StringBuilder();
		
		content.append(MAIN_HEAD).append("Analysis Report for Repository ").append(name).append(DIV_END).append(NL);
		content.append(MAIN_BREAD).append("<a title=\"").append(name).append("\" href=\"#\">").append(name).append("</a>").append(NL);
		content.append(DIV_END).append(NL).append(DIV_END).append(NL);
		content.append(MAIN_CONTENT_START).append(NL);
		String analysisType;
		String formattedName;
		for (File f : files) {
			analysisType = WebPageConstants.extractAnalysisType(f);
			formattedName = analysisType.replaceAll("\\s", "");
			content.append(H4_START).append(analysisType).append(H4_END).append(NL);
			content.append("<div id=\"").append(formattedName).append("\" ").append(CHART_DIV_STYLE).append(">").append(DIV_END).append(NL);
			content.append(CHART_GETIMAGE_START).append(formattedName).append(CHART_GETIMAGE_END).append(DIV_END).append(NL);
		}
		// Require layout/getimage.php
		content.append(REQUIRE_GETIMAGE).append(NL);
		// END Page-Content - Content - Main
		content.append(DIV_END).append(NL).append(DIV_END).append(NL).append(DIV_END).append(NL);
		return content.toString();
	}

	private String buildChartScript(File f, String chartType, String analysisType) {
		StringBuilder content = new StringBuilder();
		String formattedName = analysisType.replaceAll("\\s", "");
		try {
			content.append(SCRIPT_START).append(NL);
			content.append("function ").append("draw").append(formattedName).append("() {").append(NL);
			content.append("var wrapper = new google.visualization.ChartWrapper({").append(NL);
			content.append("chartType: '").append(chartType).append("',").append(NL);
			content.append("dataTable: ");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			String line, options;
			// first line has additional options for the chart
			options = br.readLine();
			while((line = br.readLine()) != null) {
				content.append(line).append(NL);
			}
			content.append(",").append(NL).append("options: {'title': '").append(analysisType).append("', ").append(options).append("},").append(NL);
			content.append("containerId: '").append(formattedName).append("'});").append(NL);
			content.append(" wrapper.draw();").append(NL).append("}").append(NL);
			content.append("google.setOnLoadCallback(draw").append(formattedName).append(");").append(NL);
			content.append(SCRIPT_END).append(NL);
			br.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
			logger.error(Arrays.toString(e.getStackTrace()));
		}
		return content.toString();
	}

	/**
	 * @return the repoInfo
	 */
	public Entry<String, Set<File>> getRepoInfo() {
		return repoInfo;
	}

	/**
	 * @param repoInfo
	 *            the repoInfo to set
	 */
	public void setRepoInfo(Entry<String, Set<File>> repoInfo) {
		this.repoInfo = repoInfo;
	}

	/**
	 * @return the destinationPath
	 */
	public String getDestinationPath() {
		return destinationPath;
	}

	/**
	 * @param destinationPath
	 *            the destinationPath to set
	 */
	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}

}
