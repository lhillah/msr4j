/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.report.main;

import java.io.File;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;

import fr.lip6.msr4j.main.MainAnySCM;
import fr.lip6.msr4j.report.LayoutDirFileFilter;
import fr.lip6.msr4j.report.WebSiteReportBuilder;
import fr.lip6.msr4j.utils.config.CLIMessages;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

/**
 * This class is the entry point to create reports out of the analysis data
 * users should point the class to. These reports are generated into the MSR4J
 * web site source folder. Then the left menu in layout/sidemenu.php is updated
 * with entries corresponding to the path to the generated reports, keeping
 * existing ones (that are not duplicates).
 * 
 * @author lom
 * 
 */
@SuppressWarnings("static-access")
public final class MainWebSiteReport extends MainAnySCM {

	private static final String DATA_PATH = "dataPath";
	private static final String DESTINATION = "destination";
	private static String dataPath, webfolder;
	private static Option datapath;
	private static Option destination;

	static {
		datapath = OptionBuilder.isRequired().withArgName("data-path").hasArg()
				.withDescription("Required: folder were data to be displayed are stored (without trailing slash).").create(DATA_PATH);
		destination = OptionBuilder.isRequired().withArgName("destination-path").hasArg()
				.withDescription("Destination folder where the website sources are stored (without trailing slash). ").create(DESTINATION);
	}

	private MainWebSiteReport() {
		super();
	}

	/**
	 * Main class to launch reports construction. Expects somes arguments froms
	 * command line (will print help if needed).
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		CLIMessages.APPNAME = "MSR4J - Analysis Report Visualisation Application";
		CLIMessages.VERSION = "0.0.1-SNAPSHOT";
		Long start, end;
		start = System.nanoTime();
		setCoreCLIOptionsBuilder(CoreCLIOptionsBuilder.buildHelpOption().buildVersionOption().buildLogfileOption().addCustomOption(datapath)
				.addCustomOption(destination));
		try {
			if (parseArguments(args, MainWebSiteReport.class.getCanonicalName())) {
				getLogger().info(CLIMessages.ARGS_OK);
				if (checkDataPath() && checkWebSitePath()) {
					WebSiteReportBuilder wr = new WebSiteReportBuilder(webfolder, dataPath);
					wr.buildGoogleCharts();
				} else {
					fr.lip6.msr4j.main.Error.fail("Could not successfully work with either data path or web site path. Please check previous messages.", getLogger());
				}
				end = System.nanoTime();
				getLogger().info("Total time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
			}
		} finally {
			MSR4JLogger.shutDownLogging();
		}
		System.exit(0);
	}

	/**
	 * Checks that the specified web site path on command line (where to
	 * generate the reports) exists, contains the layout folder and that folder
	 * contains the sidemenu.php file.
	 * 
	 * @return true if the path exists, is a directory and contains the layout
	 *         folder which should contain the sidemenu.php file
	 * @see #checkDataPath()
	 */
	private static boolean checkWebSitePath() {
		boolean result = true;
		webfolder = getCommandLine().getOptionValue(DESTINATION);
		result = checkPathIsDirectory(webfolder, "Web Site");
		if (result) {
			File f = new File(webfolder);
			Set<File> files = new TreeSet<File>(Arrays.asList(f.listFiles(new LayoutDirFileFilter())));
			if (files.isEmpty()) {
				getLogger().warn("Web Site directory does not appear to contain layout folder or that folder does not contain sidemenu.php.");
				result = false;
			}
		}
		return result;
	}

	/**
	 * Checks that the specified data path on command line (where to find
	 * analysis data) exists and contains some files.
	 * 
	 * @return true is the path exists, is a directory and contains some files
	 * @see #checkWebSitePath()
	 */
	private static boolean checkDataPath() {
		boolean result = true;
		dataPath = getCommandLine().getOptionValue(DATA_PATH);
		result = checkPathIsDirectory(dataPath, "Data path");
		if (result) {
			File f = new File(dataPath);
			Set<String> files = new TreeSet<String>(Arrays.asList(f.list()));
			if (files.isEmpty()) {
				getLogger().warn("There is no file in the data folder.");
				result = false;
			}
		}

		return result;
	}

	/**
	 * Checks that a given path is a directory.
	 * 
	 * @param path
	 *            the path to check
	 * @param name
	 *            the last name of the path
	 * @return true if it exists and is a directory
	 */
	private static boolean checkPathIsDirectory(String path, String name) {
		boolean result = false;
		final File f = new File(path);
		if (!f.exists()) {
			getLogger().error("{} folder does not exist.", name);
		} else if (!f.isDirectory()) {
			getLogger().error("{} path is not a directory.", name);
		} else {
			result = true;
		}
		return result;
	}
}
