/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.report.main;

import static fr.lip6.msr4j.report.WebPageConstants.F_SEP;
import static fr.lip6.msr4j.report.WebPageConstants.SIDEMENUFRAGMENTAFTER_PHP;
import static fr.lip6.msr4j.report.WebPageConstants.SIDEMENUFRAGMENTBEFORE_PHP;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import fr.lip6.msr4j.analysis.utils.IOUtils;
import fr.lip6.msr4j.report.WebSiteReportBuilder;

/**
 * Dirty class to quickly prototype a use case of some features of {@link WebSiteReportBuilder} class. 
 * @author lom
 *
 */
public class MainTestHTMLModification {

	private static final String PATH = "resources/site/layout/sidemenu.php";
	
	private static final String SIDEMENU = "sidemenu.php";
 
	public MainTestHTMLModification() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Document doc;
		Element ul;
		File f = new File(PATH + F_SEP + SIDEMENU);
		try {
			System.out.println("Start loading document");
			doc = Jsoup.parse(f, "UTF-8");
			ul = doc.getElementById("sidebar").getElementById("nav").getElementsByTag("ul").get(1);
			System.out.println(ul.html());
			ul.append("<li><a href=\"lhillah.php" + "\">" + "lhillah" + "</a></li>\n");
			System.out.println("Finished modification");
			System.out.println(ul.html() + "\n\n");
			Set<String> dup = new HashSet<String> ();
			dup.add("lhillah");
			Element newUl = WebSiteReportBuilder.removeDuplicateChildren(dup, ul);
			//System.out.println(doc.html());
			IOUtils io = new IOUtils();
			//System.out.println(io.readFileContent(PATH));
			StringBuilder content = new StringBuilder();
			content.append(io.readFileContent(PATH + F_SEP + SIDEMENUFRAGMENTBEFORE_PHP)).append(newUl.html())
			.append(io.readFileContent(PATH + F_SEP + SIDEMENUFRAGMENTAFTER_PHP));
			io.writeDataIntoDestination(f, content.toString());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
