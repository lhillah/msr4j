<?php require ("layout/head.php"); ?>
<script type="text/javascript">
      function drawVisualization() {
        var wrapper = new google.visualization.ChartWrapper({
          chartType: 'PieChart',
          dataTable: [['File Mime Type', 'Number of Files'],
['text/css', 28],
['application/x-bplist', 2],
['application/javascript', 13],
['other', 378],
['application/vnd.ms-excel', 12],
['application/java-archive', 7],
['application/xslt+xml', 263],
['application/postscript', 462],
['image/tiff', 9],
['application/x-tex', 1987],
['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 16],
['application/xhtml+xml', 122],
['application/vnd.ms-powerpoint', 22],
['application/x-sh', 44],
['text/x-coldfusion', 1],
['text/x-log', 2],
['application/zip', 42],
['text/x-c++hdr', 144],
['image/png', 154],
['text/x-jsp', 4],
['text/x-java-source', 273],
['image/gif', 43],
['text/x-vbasic', 23],
['application/x-bibtex-text-file', 112],
['application/msword', 8],
['text/html', 31],
['application/x-gzip', 8],
['text/x-d', 7],
['application/vnd.oasis.opendocument.text', 4],
['application/x-tika-msoffice', 1],
['application/vnd.apple.keynote', 1],
['image/svg+xml', 40],
['text/x-chdr', 37],
['application/x-dvi', 2],
['application/vnd.apple.pages', 1],
['text/x-perl', 1],
['application/java-vm', 25],
['application/octet-stream', 350],
['text/troff', 1],
['text/directory', 605],
['application/xml', 883],
['image/jpeg', 71],
['application/vnd.oasis.opendocument.graphics', 3],
['text/plain', 1167],
['text/x-c++src', 57],
['application/xml-dtd', 6],
['application/pdf', 266],
['text/x-python', 2]
],
          options: {'title': 'Files per mime type'},
          containerId: 'visualization'
        });
        wrapper.draw();
      }
      google.setOnLoadCallback(drawVisualization);
</script>
<?php
require ("layout/sidemenu.php");
?>
	<!-- MAIN -->
	<div id="main">	
			<!-- HEADER -->
			<div id="header">
				<div id="page-title">Analysis Report for Repository lhillah</div>
				<!-- Breadcrumb-->
				<div id="breadcrumbs">
					You are here: 
					<a title="Home" href="index.php">Home</a> &raquo; 
					<a title="Repositories" href="repositories.php">Repositories</a> &raquo; 
					<a title="lhillah" href="#">lhillah</a>
				</div>
				<!-- ENDS Breadcrumb-->	
			</div>
			<!-- ENDS HEADER -->
		<!-- CONTENT -->
		<div id="content">
			<!-- PAGE CONTENT -->
			<div id="page-content">
				<h4 class="header-line">Files per Mime Type</h4>
			 	<div id="visualization" style="width: 600px; height: 400px;"></div>
			</div>
			<!-- ENDS PAGE-CONTENT -->
		</div>
		<!-- ENDS CONTENT -->
	</div>
	<!-- ENDS MAIN -->
<?php require ("layout/footer.php");?>