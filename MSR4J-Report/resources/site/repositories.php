<?php
require ("layout/head.php");
require ("layout/sidemenu.php");
?>
		<!-- MAIN -->
		<div id="main">

			<!-- HEADER -->
			<div id="header">
				<div id="page-title">The analysed repositories</div>
				<!-- Breadcrumb-->
				<div id="breadcrumbs">
					You are here: 
					<a title="Home" href="index.php">Home</a> &raquo; 
					<a title="Repositories" href="#">Repositories</a> &raquo; 
					<a title="Features" href="#">Types of analysis</a>
				</div>
				<!-- ENDS Breadcrumb-->	
			</div>
			<!-- ENDS HEADER -->
			
			<!-- CONTENT -->
			<div id="content">
						
				<!-- PAGE CONTENT -->
				<div id="page-content">
				
					<!-- one col -->
					<h4 class="header-line">Types of analysis</h4>
					<p>We briefly describe here each type of analysis currently supported by MSR4J.
						The analysis reports of the repositories you performed using MSR4J-Report tool
						are located in the sub-menus of the Repositories sidebar menu.</p>
					<div class="clear "></div>
					<!-- ENDS one col -->
					<!-- 2 cols -->
					<div class="one-half">
						<h6>Files Touch Count</h6>
						<p>For each file in the repository, we count the number of times each 
						developer has touched it in some manner, i.e. add, delete, modify, rename.
						The generated data (CSV and JSON) from this analysis by MSR4J is suitable
						 for displaying it in a heat map.</p>
						<p>For this analysis, you may specify a set of file extensions to consider
						only, e.g .tex, .java. The other files having extensions not in your specified 
						set will just be discarded.</p>
					</div>
					<div class="one-half last">
						<h6>Number of Files per Mime Type</h6>
						<p>This analysis computes the number of files per mime type.
						When the mime type of a file could not be determined during the analysis
						(or when the repository where scanned), it is classified under the 
						category <em>other</em>.</p>
						<p>The generated data (in JSON) from this analysis is suitable for 
						displaying it in a pie chart.</p>
					</div>
					<div class="clear "></div>
					<!-- ENDS 2 cols -->
					<!-- 3 cols -->
					<div class="one-third">
						<h6>One third </h6>
						<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
					</div>
					<div class="one-third">
						<h6>One third </h6>
						<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
					</div>
					<div class="one-third last">
						<h6>One third </h6>
						<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
					</div>
					<div class="clear "></div>
					<!-- ENDS 3 cols -->
					<!-- 2/3 cols -->
					<div class="one-third">
						<h6>One third </h6>
						<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
					</div>
					<div class="two-third last">
						<h6>Two thirds</h6>
						<p>Pellentesque vit habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat Aenean fermentum, it sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. vit Aenean fermentum, Aenean fermentum, Aenean fermentum, Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
					</div>
					<div class="clear "></div>
					<!-- ENDS 2/3 cols -->
				</div>
				<!-- ENDS PAGE-CONTENT -->
			
			</div>
			<!-- ENDS CONTENT -->
			
		</div>
		<!-- ENDS MAIN -->
<?php require ("layout/footer.php");?>