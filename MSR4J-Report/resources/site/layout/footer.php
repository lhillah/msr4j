	</div>
	<!-- ENDS WRAPPER -->

	<!-- FOOTER -->
	<div id="footer">
		<!-- FOOTER-WRAPPER -->
		<div id="footer-wrapper">
			<!-- footer-cols -->
			<ul id="footer-cols">
				<li class="col clear-col">
					<h6>About the theme</h6>
					Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.
				</li>
				<li class="col">
					<h6>Categories</h6>
					<ul>
						<li><a href="#">Webdesign</a></li>
						<li><a href="#/">Wordpress</a></li>
						<li><a href="#">Photo</a></li>
						<li><a href="#">Code</a></li>
						<li><a href="#">Web design</a></li>
						<li><a href="#/">Marketplace</a></li>
						<li><a href="#">Writting</a></li>
						<li><a href="#">Drawings</a></li>
					</ul>
				</li>
				<!-- Flickr -->
				<li class="col">
					<h6>Flickr stream</h6>
					<ul id="flickr-stream" class="thumbs">
					</ul>
				</li>
			<!-- ENDS Flickr -->
			</ul>
			<!-- ENDS footer-cols -->
			<!-- footer-bottom -->
			<div id="footer-bottom">
				<div id="bottom-left">
					&copy; 2011 Ansimuz. All Rights Reserved.  <a href="http://themeforest.net/user/Ansimuz?ref=ansimuz">View portfolio</a>
				</div>
				<div id="bottom-right">To top</div>
			</div>
			<!-- ENDS footer-bottom -->
		</div>
		<!-- ENDS FOOTER-WRAPPER -->
	</div>
	<!-- ENDS FOOTER -->
	
	
	</body>
</html>
