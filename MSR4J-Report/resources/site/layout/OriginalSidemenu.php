</head>
<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<div id="sidebar">
		<!-- logo -->
		<a href="index.html"><img src="img/msr4j.png" alt="Left template" id="logo" /></a>
		<!-- search -->
		<form  method="get" id="searchform" action="#">
			<div>
				<input type="text" value="Search..." name="s" id="s" onfocus="defaultInput(this)" onblur="clearInput(this)" />
				<input type="submit" id="searchsubmit" value=" " />
			</div>
		</form>
		<!-- ENDS search -->
					
		<!-- Navigation -->
		<ul id="nav" class="sf-menu sf-vertical">
			<li class="current-menu-item"><a href="index.php">HOME</a></li>
			<li><a href="repositories.php">REPOSITORIES</a>
				<ul>
					<li><a href="repositories.php">Overview</a>
					<li><a href="lhillah-template.php">lhillah</a>
					<li><a href="features-icons.php">General icons</a>
				</ul>
			</li>
			<li><a href="contact.php">CONTACT</a></li>
			<li><a href="https://bitbucket.org/lhillah/msr4j/overview">DOWNLOAD MSR4J</a></li>
		</ul>
		<!-- Navigation -->	
		<!-- latest tweets -->
		<!-- Social -->
		<ul class="social">
			<li><h6>Follow us</h6>
			<li><a href="http://www.facebook.com" class="facebook" title="Become a fan"></a></li>
			<li><a href="http://www.twitter.com" class="twitter" title="Follow our tweets"></a></li>
			<li><a href="http://www.dribbble.com" class="dribbble" title="View our work"></a></li>
			<li><a href="http://www.addthis.com" class="addthis" title="Tell everybody"></a></li>
			<li><a href="http://www.vimeo.com" class="vimeo" title="View our videos"></a></li>
			<li><a href="http://www.youtube.com" class="youtube" title="View our videos"></a></li>
		</ul>
		<!-- ENDS Social -->
	</div>
	<!-- ENDS SIDEBAR -->