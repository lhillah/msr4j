<script type="text/javascript" src="js/rgbcolor.js"></script> 
<script type="text/javascript" src="js/StackBlur.js"></script>
<script type="text/javascript" src="js/canvg.js"></script>
<script type="text/javascript">
function getImgData(name) {
var container = document.getElementById(name);
//alert(container.style.width);
var chartArea = container.getElementsByTagName('div')[1];
var svg = chartArea.innerHTML.replace(/&nbsp;/g," ");
var doc = container.ownerDocument;
var canvas = doc.createElement('canvas');
canvas.setAttribute('width', chartArea.offsetWidth);
canvas.setAttribute('height', chartArea.offsetHeight);
canvas.setAttribute('style', 'position: absolute; ' + 'top: ' + (-chartArea.offsetHeight * 6) + 'px;' + 'left: ' + (-chartArea.offsetWidth * 6) + 'px;');
doc.body.appendChild(canvas);
canvg(canvas, svg);
var imgData = canvas.toDataURL("image/png");
var uriContent = imgData.replace("image/png", "image/octet-stream");
var downloadLink = document.createElement("a");
downloadLink.href = uriContent;
downloadLink.download = name + ".png";
document.body.appendChild(downloadLink);
downloadLink.click();
document.body.removeChild(downloadLink);
}
</script>
