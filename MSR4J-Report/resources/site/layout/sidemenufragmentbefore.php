</head>
<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<div id="sidebar">
		<!-- logo -->
		<a href="index.php"><img src="img/msr4j-2.png" alt="MSR4J Logo" id="logo" /></a>
		<!-- search -->
		<form  method="get" id="searchform" action="#">
			<div>
				<input type="text" value="Search..." name="s" id="s" onfocus="defaultInput(this)" onblur="clearInput(this)" />
				<input type="submit" id="searchsubmit" value=" " />
			</div>
		</form>
		<!-- ENDS search -->
					
		<!-- Navigation -->
		<ul id="nav" class="sf-menu sf-vertical">
			<li class="current-menu-item"><a href="index.php">HOME</a></li>
			<li><a href="documentation.php">DOCUMENTATION</a>
				<ul>
					<li><a href="architecture.php">Architecture</a></li>
					<li><a href="configuration.php">Configuration</a></li>
					<li><a href="workflow.php">Workflow</a></li>
					<li><a href="README.html">Readme</a></li>
				</ul>
			</li>
			<li><a href="scan.php">DATA COLLECTION</a>
			<li><a href="analyse.php">DATA ANALYSIS</a>
			<li><a href="reports.php">BUILD REPORTS</a>
			<li><a href="repositories.php">ANALYSIS REPORTS</a>
<!--CONVENTION! DON'T CHANGE THE ID OF THE FOLLOWING LIST. DON'T REMOVE IT EITHER. -->  
				<ul id="reports-section">
