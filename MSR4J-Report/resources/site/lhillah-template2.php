<?php
require ("layout/head.php");
?>
<script type="text/javascript">
      function drawVisualization() {
		 var jsonData = $.ajax({
		          url: "getlhillahData.php",
				type: "GET",
		          dataType:"script",
		          }).responseText;
		// Create our data table out of JSON data loaded from server.
		//var data = 
        var wrapper = new google.visualization.ChartWrapper({
          chartType: 'PieChart',
          dataTable: new google.visualization.DataTable(jsonData),
          options: {'title': 'Files per mime type'},
          containerId: 'visualization'
        });
        wrapper.draw();
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
<?php
require ("layout/sidemenu.php");
?>
	<!-- MAIN -->
	<div id="main">	
			<!-- HEADER -->
			<div id="header">
				<div id="page-title">Analysis Report for Repository lhillah</div>
				<!-- Breadcrumb-->
				<div id="breadcrumbs">
					You are here: 
					<a title="Home" href="index.php">Home</a> &raquo; 
					<a title="Repositories" href="repositories.php">Repositories</a> &raquo; 
					<a title="lhillah" href="#">lhillah</a>
				</div>
				<!-- ENDS Breadcrumb-->	
			</div>
			<!-- ENDS HEADER -->
		<!-- CONTENT -->
		<div id="content">
			<!-- PAGE CONTENT -->
			<div id="page-content">
				<h4 class="header-line">Files per Mime Type</h4>
			 <div id="visualization" style="width: 600px; height: 400px;"></div>
			</div>
			<!-- ENDS PAGE-CONTENT -->
		</div>
		<!-- ENDS CONTENT -->
	</div>
	<!-- ENDS MAIN -->
<?php require ("layout/footer.php");?>