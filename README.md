# This is the README for MSR4J.

MSR4J is an OSGi-based framework which provides an extendable
data model for mining large software repositories,
whatever the back-end database. 

Current version: 0.0.5-SNAPSHOT (According to the Core component)

## COMPONENTS

MSR4J is shipped in 4 parts: 

* MSR4J-Core where the data model and some basic utilities are defined.

* MSR4J-Analysis to create custom analyses.

* MSR4-Report to create custom reports.

* MSR4J-WebApp to provide an interactive front end to the end user.


## DOWNLOAD

The Web app bundle, contains everything you need to run MSR4J.
In particular, it contains the Apache Felix Framework, which hosts
the components of MSR4J.

You can download an [alpha version](https://bitbucket.org/lhillah/msr4j/downloads/msr4j-webapp-0.0.2-SNAPSHOT-webapp-bundle.zip) from the [download section](https://bitbucket.org/lhillah/msr4j/downloads).

#### Note 1

Currenty, MSR4J has been tested with SVN repositories. Although Git support is currently implemented, it has not yet been tested.
Support of Mercurial will happen in a long-term perspective.

#### Note 2

Clone this repository if you want to have access to the source code (Eclipse projects).


## REQUIREMENTS


### Java 1.7, and Apache Httpd.

Make sure you have Java, at least version 1.7 and Apache Httpd 
on your machine, to run the web app. 

### Neo4J

* Download [Neo4J](neo4j.org) 1.9.M02 or 1.9.M04 or 1.9.2 to run the example (entreprise version of Neo4J is preferred). Currently version 1.9.2 is recommended.

* You need an Internet access to allow the application to fetch an SVN repository log.

## INSTALLATION

Simply unzip the web app archive in the folder where you know Httpd have
access to serve the PHP pages of the web app. Configure Httpd if necessary
to know the path to MSR4J web app directory. Launch Httpd and browse to 
the home page. For example: http://localhost/msr4j/

Read the **Documentation -> Readme** web page to ensure you comply with granting
the needed access rights to Httpd.


## CONFIGURATION

1. Before you start, update the following to your settings:

	* **jars/felix-framewok-4.2.1/conf/msr4j-db.properties** must point to the installation of your Neo4J database.

	* **jars/felix-framewok-4.2.1/conf/msr4j-scmrls.properties**: add the URLs to the SVN repositories you want to scan.

	* **jars/felix-framewok-4.2.1/conf/msr4j-scmlocalpaths.properties**: the local paths of the working copies of the repositories you want to scan (perform a checkout or an update before you start data extraction).


	* **jars/felix-framewok-4.2.1/conf/msr4j-scmcredential.properties**: put in this file the credentials of the repositories you want to scan. If no credentials are provided in this file, *anonymous* is assumed. This file is also important to update to your settings if your repository uses https and a self-signed certificate.

2. Download the provided DB archive from the [Downloads](https://bitbucket.org/lhillah/msr4j/downloads/20131106-cassandra-graph.db.zip) section of this repository. It contains logs from the Apache Cassandra public repository. Unzip the archive into the **data** directory of your Neo4J installation (replace the **data/graph.db** directory in your Neo4J installation with this one).

3. Launch the Felix framework. Change into the **jars/felix-framewok-4.2.1/** directory and run: *java -jar bin/felix.jar*

#### Note 3
You may have already launched the Felix framework through reading **Documentation -> Readme** web page, section *OSGi Container*.

## RUN

### Analysis Reports

Sample analysis report charts are provided in the web app. Browse the **Analysis Report** menu. You need an Internet access so that Google Charts API can be
invoked to draw the charts.

In the upcoming release, the reports builder component will be wired to the
web app, so that the **Build Reports** menu is enabled.

### Play with an Analysis Service 

Use the **Data Analysis** menu to select an analysis service from the
list of available services. Currently, MSR4J Repository Analyser proposes 2 services. In the **Repositories** field, provide *cassandra*, and provide *.html* in the **Extensions** field.

Choose only the *datatable* format to enable charts drawing.

Click on the **Send** button and watch the output from the analysis service,
displayed in the **Server Output** console.

At the end, the analysis data will be located under the **analyses*$ folder
in your web app directory. It is stored in a folder, the name of which
is the time stamp of the analysis request. That folder is located under another folder, the name of which is the id of your session cookie.

## FEEDBACK

Feel free to send any feedback or request at lom-messan.hillah at lip6.fr.

