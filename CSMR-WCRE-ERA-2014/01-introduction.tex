\section{Introduction} \label{sec:intro} %Present the domain, applications, and motivation for this
Industrial grade software development, whether proprietary or open source, is a complex
activity involving many long-run and non-trivial projects carried out by communities of skilled
professionals and individuals. To understand the organisation and interactions within such
communities, along with the evolution of the artefacts they produce, data generated from their
activities (logs, files, mails, issues, minutes, etc.) is collected and analysed. The extracted data
is often dumped into database archives like those proposed by the FLOSSMole
project~\cite{flossmole06}, or researchers' own databases, whatever the technology. After collecting
and cleansing the data, analysis and visualisation are performed using various tools. For source
code management systems (SCM), data tools like
CVSAnaly2\footnote{\url{http://github.com/MetricsGrimoire/CVSAnalY}} from the FLOSSMetrics
project\footnote{\url{http://flossmetrics.org}} are used to extract information out of the
repository logs and store it in a database.

Although the proposed database archives are full of useful information, they show some drawbacks
with respect to their structure, freshness of data and purpose of use. Most data archives are stored
in SQL databases or tied to just one technology, having predetermined schemas that are costly to
evolve. Additionally, these schemas are often tailored for a particular software repository's
structure. A prominent instance is the FLOSSMetrics schema template instantiated in many studies and
tools like CVSAnaly2. Whether directly reused or through implementing tools, researchers often end
up modifying the schema to fit their specific research goal~\cite{goeminne13}, which defeats
purpose. Another prominent example is the set of 15 heterogeneous schemas proposed by
FLOSSMole~\cite{flossmoleschema2012}, that makes difficult to conduct comprehensive studies on a
homogeneous basis.

The other main issue is related to maintaining the freshness of these data archives, due to the very
evolving nature of software development activities. Combined with the cost of storage space, it is a
real challenge that is hard to cope with, resulting in many archives being quickly outdated or
having limited time spans. The important data collection of FLOSSMole, started from 2004 and
counting more than 1 TB from many different software forges is selectively updated on a
user-donation basis. SourceForge Research Data Archive~\cite{Van-Antwerp:2008} (SRDA) is the most
maintained and up-to-date data collection available online. It follows its own process of
maintenance thanks to University of Notre Dame, on behalf of SourceForge, and has its own SQL schema
of 73 tables. Yet, the contract with SourceForge does not allow them to distribute the data, so
queries must be performed online via a form. Timeouts or unchecked errors may happen, as well as
unavailability of the archive due to disk failures.

The approach we propose in this paper is meant to provide researchers and developers with an
extensible framework, featuring a comprehensive data model that aims to encompass all the
concepts and relationships that are common in mining software repositories (MSR). MSR4J is an
OSGi-based, modular Java application integrating a set of suitable technologies and tools that
allow users to perform the most frequent activities in MSR: data model definition, data extraction,
data storage, and data analysis. Its easy-to-extend data model is designed using Eclipse Modeling
Framework~\cite{Steinberg2008cs} and its code generation infrastructure, that enable incremental
model enrichment. The data model high-level API relies on the Frames framework~\cite{frames2013}, so
that many back-end database technologies could be used. Another interesting advantage is that
whenever you are using a data model that does not perfectly match the underlying actual structure of
the extracted data, you can still use the API and just properly discard attributes you are not
interested in. As a consequence, the data model can evolve in time and space.

The remainder of this paper is structured as follows. Section~\ref{sec:datamodel} describes the
initial data model of MSR4J, then Section~\ref{sec:archi} presents its architecture.
Section~\ref{sec:application} gives an account of a preliminary experiment using MSR4J. Finally,
Section~\ref{sec:sota} discusses related work, and to conclude we sketch the perspectives.
