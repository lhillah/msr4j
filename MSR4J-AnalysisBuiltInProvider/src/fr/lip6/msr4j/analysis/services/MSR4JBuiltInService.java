/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.services;

import java.util.HashSet;
import java.util.Set;

import fr.lip6.msr4j.analysis.formats.DataFormat;
import fr.lip6.msr4j.analysis.provider.ServicesRegistry;
import fr.lip6.msr4j.analysis.spi.Analyser;

/**
 * Root class for analysis services, for sharing
 * common properties and API. Any new analysis service can
 * rely on this root class to increase reuse and avoid
 * re-implementing already existing generic implementation
 * of some service APIs.
 *
 * @author lom
 * 
 */
public abstract class MSR4JBuiltInService {
	private static ServicesRegistry registry;
	private String name;
	private String description;
	private Analyser provider;
	private Set<DataFormat> requestedFormats;
	private boolean couldChangeState;
	private boolean activation;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setInternalName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setInternalDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the provider
	 */
	public Analyser getInternalProvider() {
		return provider;
	}

	/**
	 * @param provider
	 *            the provider to set
	 */
	public void setInternalProvider(Analyser provider) {
		this.provider = provider;
	}

	/**
	 * @return the requestedFormats
	 */
	public Set<DataFormat> getRequestedFormats() {
		return requestedFormats;
	}

	/**
	 * @param requestedFormats
	 *            the requestedFormats to set
	 */
	public void setRequestedFormats(Set<DataFormat> requestedFormats) {
		this.requestedFormats = requestedFormats;
	}

	/**
	 * @return the couldChangeState
	 */
	public boolean isCouldChangeState() {
		return couldChangeState;
	}

	/**
	 * @param couldChangeState
	 *            the couldChangeState to set
	 */
	public void setCouldChangeState(boolean couldChangeState) {
		this.couldChangeState = couldChangeState;
	}

	/**
	 * @return the activation
	 */
	public boolean isActivation() {
		return activation;
	}

	/**
	 * @param activation
	 *            the activation to set
	 */
	public void setInternalActivation(boolean activation) {
		this.activation = activation;
	}

	/**
	 * Minimal swallow copy, to increase code reuse. In this case the name,
	 * description, provider and requested formats are copied.
	 * 
	 * @param copy
	 *            the copy
	 * @param model
	 *            the model to be copied from
	 * @return the copy
	 */
	public static <T extends MSR4JBuiltInService> T minimalSwallowCopy(T copy, T model) {
		copy.setInternalName(model.getName());
		copy.setInternalDescription(model.getDescription());
		copy.setInternalProvider(model.getInternalProvider());
		copy.setRequestedFormats(new HashSet<DataFormat>(model.getRequestedFormats()));
		return copy;
	}

	/**
	 * Sets the services registry. Done only once, since the registry is shared
	 * by potentially many services.
	 * 
	 * @param r
	 */
	public static void setRegistry(ServicesRegistry r) {
		if (registry != null) {
			registry = r;
		}
	}

	/**
	 * Returns a reference to the services registry.
	 * 
	 * @return
	 */
	public static ServicesRegistry getRegistry() {
		return registry;
	}
}
