/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.services;

import java.util.EnumSet;
import java.util.Set;
import java.util.TreeSet;

import fr.lip6.msr4j.analysis.formats.DataFormat;
import fr.lip6.msr4j.analysis.provider.ServicesRegistry;
import fr.lip6.msr4j.analysis.spi.Analyser;
import fr.lip6.msr4j.analysis.spi.AnalysisService;

/**
 * Service to count the number of files per mime type, and
 * generate the result in datatable format (only format supported now by this service).
 * @author lom
 *
 */
public class FilePerMimeTypeService extends MSR4JBuiltInService implements AnalysisService {

	private static Set<DataFormat> supportedFormats = EnumSet.noneOf(DataFormat.class);

	static {
		addDataFormat(DataFormat.DATATABLE);
	}

	public FilePerMimeTypeService() {
		this.setInternalName("Files per Mime Type");
		this.setDescription("Proportion of files per Mime Type in the repository, Pie Chart");
		this.setInternalProvider(null);
		this.setRequestedFormats(new TreeSet<DataFormat>());
		this.setCouldChangeState(true);
		this.setActivation(false);
	}

	/**
	 * Adds a supported format this service. Different from the requested
	 * formats. All instances (including those of subclasses) share this
	 * feature.
	 * 
	 * @param df
	 *            a supported format
	 * @see FilePerMimeTypeService#setDataFormats
	 */
	public static void addDataFormat(DataFormat df) {
		supportedFormats.add(df);
	}

	@Override
	public boolean couldChangeActivationState() {
		return this.isCouldChangeState();
	}

	@Override
	public String getHumanReadableDescription() {
		return getDescription();
	}

	@Override
	public String getHumanReadableName() {
		return getName();
	}

	@Override
	public Analyser getProvider() {
		return this.getInternalProvider();
	}

	@Override
	public Set<DataFormat> getSupportedFormats() {
		return supportedFormats;
	}

	@Override
	public boolean isActivable() {
		boolean res = false;
		if (this.isCouldChangeState()) {
			res = true;
		}
		return res;
	}

	@Override
	public boolean isActivated() {
		return this.isActivation();
	}

	@Override
	public boolean setActivation(boolean activation) {
		boolean res = false;
		if (this.isCouldChangeState()) {
			this.setInternalActivation(activation);
			res = true;
		}
		return res;
	}

	@Override
	public void setDataFormats(Set<DataFormat> df) {
		getRequestedFormats().clear();
		getRequestedFormats().addAll(df);
	}

	/**
	 * Subclasses are free to change the description.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.setInternalDescription(description);
	}

	/**
	 * Sub-classes may change the name.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.setInternalName(name);
	}

	@Override
	public void setProvider(Analyser p) {
		this.setInternalProvider(p);
	}

	@Override
	public void setRegistry(Object registry) {
		this.setActivation(false);
		this.setCouldChangeState(false);
		MSR4JBuiltInService.setRegistry((ServicesRegistry) registry);
	}

	@Override
	public AnalysisService swallowCopy() {
		FilePerMimeTypeService cp = new FilePerMimeTypeService();
		cp = minimalSwallowCopy(cp, this);
		cp.setCouldChangeState(this.isCouldChangeState());
		cp.setActivation(this.isActivation());
		return cp;
	}

	@Override
	public AnalysisService swallowCopyWithoutState() {
		FilePerMimeTypeService cp = new FilePerMimeTypeService();
		cp = minimalSwallowCopy(cp, this);
		return cp;
	}
}
