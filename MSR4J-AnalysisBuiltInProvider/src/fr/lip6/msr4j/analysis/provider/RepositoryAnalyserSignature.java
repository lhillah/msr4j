/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.provider;


/**
 * Declares name and description of RepositoryAnalyser
 * 
 * @author lom
 * 
 */
public final class RepositoryAnalyserSignature {

	public static String getProviderHumanReadableDescription() {
		return "Built-in Repository Analyser Provider";
	}

	public static String getProviderHumanReadableName() {
		return "MSR4J Repository Analyser";
	}
}
