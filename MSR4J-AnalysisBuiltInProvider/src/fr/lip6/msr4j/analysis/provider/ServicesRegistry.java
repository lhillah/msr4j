/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

import fr.lip6.msr4j.analysis.spi.Analyser;
import fr.lip6.msr4j.analysis.spi.AnalysisService;

/**
 * <p>
 * Registers available services. The registry is a singleton.
 * </p>
 * 
 * <p>
 * It is not useful to register all instances of the same service, or all clones
 * of the same service object. The registered instance is a representative,
 * unless it is the instance of a different subclass. A registered
 * representative is not meant to run any actual service invocation. It can emit
 * clones which do not retain its internal state. Therefore these clones can be
 * used to run actual service invocations.
 * </p>
 * 
 * <p>
 * For instance, if you have service A, an instance A0 and other similar
 * instances or clones A1, A2, A3 bearing the exact same service name, it
 * suffices to register A0.
 * </p>
 * <p>
 * If you have service A and Service B which is a subclass of A, you have to
 * register instances A0 and B0.
 * </p>
 * 
 * <p>
 * The registry will no register any service representative whose internal state
 * could be changed afterwards.
 * </p>
 * 
 * @see AnalysisService#setRegistry(Object)
 * 
 *      <p>
 *      A registered service can be unregistered at runtime. You can also use
 *      the services declaration file to unregister a service before building a
 *      new bundle (jar) of your provider.
 *      </p>
 * 
 * @author lom
 * 
 */
public final class ServicesRegistry {

	private static final ServicesRegistry INSTANCE = new ServicesRegistry();
	private static final Map<String, AnalysisService> SERVICES = new ConcurrentHashMap<String, AnalysisService>();
	private static final Map<Analyser, Set<AnalysisService>> PROVIDERS = new ConcurrentHashMap<Analyser, Set<AnalysisService>>();
	private static LogService log;

	private ServicesRegistry() {
	}

	/**
	 * Returns the registered analysis service, the name of which is given as
	 * argument, <code>null</code> if there is no such service.
	 * 
	 * @param name
	 *            the name of service to retrieve
	 * @return the reference to the registered representative, <code>null</code>
	 *         otherwise.
	 */
	public AnalysisService getRegisteredService(String name) {
		AnalysisService res = null;
		for (Entry<String, AnalysisService> ent : SERVICES.entrySet()) {
			if (ent.getKey().equalsIgnoreCase(name)) {
				res = ent.getValue();
				break;
			}
		}
		return res;
	}

	/**
	 * <p>
	 * Returns all the registered services. If there is no registered service,
	 * returns an empty set.
	 * </p>
	 * <p>
	 * The returned set is immutable since it contains references to registered
	 * services, therefore the caller cannot modify it. So this is a read-only
	 * access.
	 * </p>
	 * <p>
	 * The registered services' internal state could not (be in principle)
	 * changed, but they can emit clones whose state can be changed. So the
	 * caller is advised to clone the elements of the returned set without
	 * keeping their internal state.
	 * </p>
	 * 
	 * @return
	 * @see #getRegisteredServices(Set)
	 * @see AnalysisService#swallowCopyWithoutState()
	 */
	public Set<AnalysisService> getRegisteredServices() {
		Set<AnalysisService> res = Collections.unmodifiableSet(new HashSet<AnalysisService>(SERVICES.values()));
		return res;
	}
	/**
	 * Returns the registered services for this provider. If there is no service for 
	 * this provider, returns an empty set. The rest of the behavior is the same as
	 * described for {@link #getRegisteredServices()}
	 * @param provider
	 * @return
	 * @see #getRegisteredServices();
	 */
	public Set<AnalysisService> getRegisteredServices(Analyser provider) {
		Set<AnalysisService> res = Collections.unmodifiableSet(new HashSet<AnalysisService>(PROVIDERS.get(provider)));
		return res;
	}

	/**
	 * <p>
	 * Returns registered analysis services using the provided filter in
	 * argument.
	 * </p>
	 * <p>
	 * The returned set is immutable since it contains references to registered
	 * services, therefore the caller cannot modify it. So this is a read-only
	 * access.
	 * </p>
	 * <p>
	 * The registered services' internal state could not (be in principle)
	 * changed, but they can emit clones whose state can be changed.
	 * </p>
	 * 
	 * @param names
	 * @return an empty set if no registered service belonging to the filter
	 *         could not be found
	 * @see #getRegisteredServices()
	 * @see AnalysisService#swallowCopyWithoutState()
	 */
	public Set<AnalysisService> getRegisteredServices(Set<String> names) {
		Set<AnalysisService> res = Collections.unmodifiableSet(new HashSet<AnalysisService>());
		for (Entry<String, AnalysisService> ent : SERVICES.entrySet()) {
			if (names.contains(ent.getKey())) {
				res.add(ent.getValue());
			}
		}
		return res;
	}

	/**
	 * Returns the singleton instance of this class.
	 * 
	 * @return
	 */
	public static ServicesRegistry getInstance() {
		return INSTANCE;
	}

	/**
	 * <p>
	 * Returns true if the service in argument is already registered.
	 * </p>
	 * <p>
	 * The search is made on services' names, not using the equals method, since
	 * a service object may have been cloned, the clone's internal state might
	 * have changed (thus defeating the equals method).
	 * </p>
	 * <p>
	 * A registered service is a representative of all its subsequent clones.
	 * <p>
	 * 
	 * @param as
	 * @return
	 * @see #isRegistered(String)
	 */
	public boolean isRegistered(AnalysisService as) {
		boolean res = false;
		for (Entry<String, AnalysisService> ent : SERVICES.entrySet()) {
			if (ent.getKey().equalsIgnoreCase(as.getHumanReadableName())) {
				res = true;
				break;
			}
		}
		return res;
	}

	/**
	 * <p>
	 * Returns true if the service, the name of which is in argument, is already
	 * registered.
	 * </p>
	 * 
	 * @param serviceName
	 * @return
	 * @see #isRegistered(AnalysisService)
	 */
	public boolean isRegistered(String serviceName) {
		return SERVICES.containsKey(serviceName);
	}

	/**
	 * <p>
	 * Uses current thread's class loader to load analysis services from a
	 * provided file, the uri of which is the argument.
	 * </p>
	 * <p>
	 * Service classes FQN (one per line) must be reported in that file, the uri
	 * of which must point to an internal package of the bundle invoking the
	 * registry.
	 * </p>
	 * <p>
	 * If this method is the only one used to discover and register services, it
	 * must be invoked prior to any query on the services.
	 * </p>
	 * 
	 * @param uri bundle internal path to the services declaration file
	 * @param context bundle context, necessary to look up the declaration file
	 * @param the provider for which to load the services
	 * @return 0 if services declaration file was found and services could be
	 *         loaded; 1 if services declaration file was found but (at least
	 *         one) services could not be loaded; -1 if services declaration
	 *         file could not be found.
	 * @see #registerAnalysisService(AnalysisService)
	 * @see #getRegisteredServices(Set)
	 */
	public int loadServices(String uri, BundleContext context, Analyser provider) {
		int res = 0;
		// ClassLoader cl = Thread.currentThread().getContextClassLoader();
		// ClassLoader cl = getClass().getClassLoader();
		URL srv = context.getBundle().getEntry(uri);
		if (srv != null) {
			InputStream is = null;
			try {
				is = srv.openStream();
			} catch (IOException e1) {
				System.err.println(Arrays.toString(e1.getStackTrace()));
				res = -1;
			}
			if (is != null) {
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				Set<AnalysisService> prs = new HashSet<AnalysisService>();
				String line;
				try {
					while ((line = br.readLine()) != null) {
						try {
							// Class<?> clazz = cl.loadClass(line);
							Class<?> clazz = Class.forName(line);
							if (clazz == null) {
								log.log(LogService.LOG_ERROR, "Services registry. Cannot instantiate analysis service " + line);
								res = 1;
								break;
							} else {
								log.log(LogService.LOG_INFO, "Services registry. Instantiated service " + clazz.getCanonicalName());
							}
							AnalysisService s = AnalysisService.class.cast(clazz.newInstance());
							if (s != null) {
								this.registerAnalysisService(s);
								s.setProvider(provider);
								prs.add(s);
							} else {
								res = 1;
							}
						} catch (ClassNotFoundException e) {
							System.err.println(Arrays.toString(e.getStackTrace()));
							res = 1;
						} catch (ClassCastException e) {
							System.err.println(Arrays.toString(e.getStackTrace()));
							res = 1;
						} catch (InstantiationException e) {
							System.err.println(Arrays.toString(e.getStackTrace()));
							res = 1;
						} catch (IllegalAccessException e) {
							System.err.println(Arrays.toString(e.getStackTrace()));
							res = 1;
						}
					}
					if (!prs.isEmpty()) {
						PROVIDERS.put(provider, prs);
					}
				} catch (IOException e) {
					System.err.println(Arrays.toString(e.getStackTrace()));
					res = -1;
				} finally {
					try {
						if (is != null) {
							is.close();
						}
					} catch (IOException e) {
						System.err.println(Arrays.toString(e.getStackTrace()));
					}
				}
			} else {
				res = -1;
			}
		} else {
			log.log(LogService.LOG_ERROR, "Services registry: URL to services declaration file is null.");
			res = -1;
		}
		return res;
	}

	/**
	 * <p>
	 * Services 'human-readable' names are used a key. Won't obviously register
	 * a service which is already registered. Only the name is used as key, so
	 * any clone of the representative will do. <code>equals<code> method
	 * is not used since we do not want to enforce the developer to restrict
	 *  its semantics to the service name only.
	 * </p>
	 * <p>
	 * Won't register a service object which state change guard cannot be
	 * changed. Indeed registered service object are representatives whose state
	 * must not be changed once they are registered. Those objects are not
	 * intended for running services. They can be cloned without keeping their
	 * internal state.
	 * </p>
	 * <p>
	 * It is up to you to enforce state change guard in your implementing
	 * service class
	 * </p>
	 * 
	 * @param as
	 * @return true if the service could be registered, false otherwise
	 */
	public boolean registerAnalysisService(AnalysisService as) {
		boolean res = false;
		if (as.couldChangeActivationState()) {
			as.setRegistry(this);
			if (!as.couldChangeActivationState()) {
				if (!isRegistered(as.getHumanReadableName())) {
					SERVICES.put(as.getHumanReadableName(), as);
					res = true;
				}
			}
		}
		return res;
	}

	/**
	 * Unregisters an analysis service.
	 * 
	 * @param as
	 *            the service to be unregistered, only the name is used as key,
	 *            so any clone of the representative will do.
	 *            <code>equals<code> method
	 * is not used since we do not want to enforce the developer to restrict
	 *  its semantics to the service name only.
	 * @return true if the service could be successfully unregistered
	 * @see #unregisterAnalysisService(String)
	 */
	public boolean unregisterAnalysisService(AnalysisService as) {
		return unregisterAnalysisService(as.getHumanReadableName());
	}

	/**
	 * Unregisters an analysis service.
	 * 
	 * @param name
	 *            the service to be unregistered, only the name is used as key,
	 *            so any clone of the representative will do.
	 *            <code>equals<code> method
	 * is not used since we do not want to enforce the developer to restrict
	 *  its semantics to the service name only.
	 * @return true if the service could be successfully unregistered
	 * @see #unregisterAnalysisService(AnalysisService)
	 */
	public boolean unregisterAnalysisService(String name) {
		boolean res = false;
		AnalysisService toBeRem = SERVICES.remove(name);
		if (toBeRem != null) {
			res = true;
		}
		return res;
	}

	/**
	 * @return the log
	 */
	public static LogService getLog() {
		return log;
	}

	/**
	 * @param log the log to set
	 */
	public static void setLog(LogService log) {
		ServicesRegistry.log = log;
	}
}
