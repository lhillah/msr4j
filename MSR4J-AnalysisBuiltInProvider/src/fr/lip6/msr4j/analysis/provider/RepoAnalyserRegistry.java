/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;


import fr.lip6.msr4j.analysis.formats.DataFormat;
import fr.lip6.msr4j.analysis.spi.IRepositoryAnalyser;
import fr.lip6.msr4j.datamodel.nodes.Repository;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

/**
 * <p>Repository analysers registry, which is used to launch them all. Each repository
 * analyser must register a representative object, which will be cloned for each
 * repository to analyse.</p>
 * 
 *<p> Each repository analyser must therefore implement this cloning method declared in
 * the {@link IRepositoryAnalyser} interface.</p>
 * 
 * <p>Repository analysers are analysis service(s) providers.</p>
 * 
 * <p>This class implements a singleton.</p>
 * 
 * @author lom
 * @see IRepositoryAnalyser
 */
public final class RepoAnalyserRegistry {
	private static final Logger LOGGER = MSR4JLogger.getLogger(RepoAnalyserRegistry.class.getCanonicalName());
	private static final Set<IRepositoryAnalyser> ANALYSERS = new HashSet<IRepositoryAnalyser>();
	private static final RepoAnalyserRegistry INSTANCE = new RepoAnalyserRegistry();
	/**
	 *  Associate to each requested output format (csv, datatable,...)
	 *  a Map where each repo name is associated to its output file path.
	 */
	private static final ConcurrentMap<DataFormat, Map<String, String>> OUTPUT_BY_FORMAT = new ConcurrentHashMap<DataFormat, Map<String, String>>();

	private RepoAnalyserRegistry() {
		super();
	}
	
	public static RepoAnalyserRegistry getInstance() {
		return INSTANCE;
	}

	public static boolean registerAnalyser(IRepositoryAnalyser ra) {
		return ANALYSERS.add(ra);
	}

	public static boolean unregisterAnalyser(IRepositoryAnalyser ra) {
		return ANALYSERS.remove(ra);
	}

	/**
	 * Determines if repository analyser class already has a representative
	 * (object) registered. Compares the runtime classes.
	 * 
	 * @param ra
	 * @return true if object of the actual class of the repository analyser is
	 *         already registered
	 */
	public static boolean isRepoAnalyserTypeAlreadyRegistered(IRepositoryAnalyser ra) {
		boolean result = false;
		for (IRepositoryAnalyser ira : ANALYSERS) {
			if (ira.getClass() == ra.getClass()) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * Launches all registered analysers, by cloning their respective
	 * representative for each repository to analyse.
	 * 
	 * @param repos
	 * @return
	 */
	public static List<Set<java.io.File>> launchAnalysers(Set<Repository> repos) {
		final List<Set<java.io.File>> result = new ArrayList<Set<java.io.File>>();
		if (ANALYSERS.isEmpty()) {
			LOGGER.warn("No registered repository analyser.");
		} else {
			final List<Callable<Set<java.io.File>>> partition = new ArrayList<Callable<Set<java.io.File>>>();
			IRepositoryAnalyser clone;
			Long start, end;
			LOGGER.info("Cloning repository analysers");
			Set<Entry<DataFormat, Map<String, String>>> odf = OUTPUT_BY_FORMAT.entrySet();
			for (IRepositoryAnalyser ra : ANALYSERS) {
				for (Entry<DataFormat, Map<String, String>> e : odf) {
					ra.addDataFormat(e.getKey());
				}
				for (Repository r : repos) {
					clone = ra.cloneAnalyser();
					clone.setRepository(r);
					partition.add(clone);
				}
			}
			
			final ExecutorService executorPool = ConcurrencyConfig.getCachedThreadPoolExecutor();
			LOGGER.info("Starting repository analysers");
			start = System.nanoTime();
			try {
				final List<Future<Set<java.io.File>>> values = executorPool.invokeAll(partition, ConcurrencyConfig.ALLOWED_EXEC_TIME1000,
						TimeUnit.SECONDS);
				end = System.nanoTime();
				LOGGER.info("Repository analysers haved terminated.");
				LOGGER.info("Time taken for all analysis: {}.", (end - start) / ConcurrencyConfig.NANO);
				Set<java.io.File> value;
				for (Future<Set<java.io.File>> ff : values) {
					value = ff.get();
					if (value != null && !value.isEmpty()) { // NullPointerException may happen depending on the implementation.
						result.add(value);
					}
				}
				executorPool.shutdown();
			} catch (InterruptedException | ExecutionException e) {
				LOGGER.error(e.getMessage());
				LOGGER.error(Arrays.toString(e.getStackTrace()));
			}
		}
		return result;
	}

	public static void addOutPutFiles(Map<String, String> opf, DataFormat df) {
		OUTPUT_BY_FORMAT.put(df, opf);
	}

	public static String getDataPathForFormat(DataFormat format, Repository rep) {
		if (OUTPUT_BY_FORMAT.containsKey(format)) {
			return OUTPUT_BY_FORMAT.get(format).get(rep.getName());
		} else {
			return null;
		}
	}

}
