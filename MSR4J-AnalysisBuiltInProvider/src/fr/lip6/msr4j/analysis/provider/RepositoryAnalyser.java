/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.analysis.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.slf4j.Logger;

import fr.lip6.msr4j.analysis.formats.DataFormat;
import fr.lip6.msr4j.analysis.provider.data.FileTouchCounter;
import fr.lip6.msr4j.analysis.provider.data.FileTouchDataFormatter;
import fr.lip6.msr4j.analysis.spi.Analyser;
import fr.lip6.msr4j.analysis.spi.AnalysisService;
import fr.lip6.msr4j.analysis.spi.IRepositoryAnalyser;
import fr.lip6.msr4j.analysis.utils.IOUtils;
import fr.lip6.msr4j.datamodel.nodes.File;
import fr.lip6.msr4j.datamodel.nodes.FileType;
import fr.lip6.msr4j.datamodel.nodes.Repository;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.MSR4JLogger;

/**
 * Provides various analysis methods on a repository.
 * 
 * Provides a builder pattern mechanism for configuring the analyses to
 * run.
 * 
 * @author lom
 * 
 */
public class RepositoryAnalyser implements IRepositoryAnalyser, Analyser {
	public static final String NL = "\n";
	private static final String SERVICES_DECL = "fr/lip6/msr4j/analysis/services/services.declaration";
	private final Logger logger = MSR4JLogger.getLogger(this.getClass().getCanonicalName());
	private Repository rep;
	private Set<String> extensions;
	private boolean createFileTouchHeatMapData;
	private boolean createFileTouchBarChartData;
	private boolean createFileTouchPerCommitter;
	private boolean createFileTypePieChartData;
	private Set<DataFormat> formats;
	private Map<String, Map<String, Long>> allFiles;
	private Map<String, Integer> fTypeCount;
	private ExecutorService executorPool;
	private String fileExtensionsPrefix;
	private Set<DataFormat> fileTypePieChartFormats;
	private Set<DataFormat> fileTouchPerCommitterFormats;
	private Set<AnalysisService> services;
	private BundleContext context;
	private LogService log;
	

	/**
	 * Set of Committers found in this repository.
	 */
	private static Set<String> cms;

	public RepositoryAnalyser(BundleContext ctx) {
		this.context = ctx;
		init();
	}
	
	private void init() {
		this.createFileTouchHeatMapData = false;
		this.createFileTouchBarChartData = false;
		this.createFileTouchPerCommitter = false;
		this.createFileTypePieChartData = false;
		this.formats = new TreeSet<DataFormat>();
		this.fileTypePieChartFormats = new TreeSet<DataFormat>();
		this.fileTouchPerCommitterFormats = new TreeSet<DataFormat>();
		this.services = null;
		if (!RepoAnalyserRegistry.isRepoAnalyserTypeAlreadyRegistered(this)) {
			RepoAnalyserRegistry.registerAnalyser(this);
		}
	}

	public RepositoryAnalyser(Repository r) {
		init();
		this.setRepository(r);
	}

	public RepositoryAnalyser(Repository r, String path, Set<String> extensions) {
		this(r);
		this.setheatMapDataFilePath(path);
		this.setFileExtensions(extensions);
		this.fileExtensionsPrefix = computePrefixForExtensions(this.extensions);
	}

	@Override
	public void addDataFormat(DataFormat df) {
		this.formats.add(df);
	}
	
	@Override
	public Set<java.io.File> call() throws Exception {
		Set<java.io.File> files = new HashSet<java.io.File>();
		executorPool = ConcurrencyConfig.getFixedThreadPoolExecutor();
		if (isCreateFileTouchHeatMapData()) {
			files.addAll(createFileTouchPerCommitterData(DataFormat.CSV));
		}
		if (isCreateFileTouchBarChartData()) {
			files.addAll(createFileTouchPerCommitterData(DataFormat.DATATABLE));
		}
		if (isCreateFileTypePieChartData()) {
			files.addAll(createFileTypePieChartData());
		}
		executorPool.shutdown();
		return files;
	}
	
	@Override
	public IRepositoryAnalyser cloneAnalyser() {
		RepositoryAnalyser clone = new RepositoryAnalyser(this.context);
		clone.createFileTypePieChartData = this.createFileTypePieChartData;
		clone.createFileTouchHeatMapData = this.createFileTouchHeatMapData;
		clone.createFileTouchBarChartData = this.createFileTouchBarChartData;
		clone.createFileTouchPerCommitter = this.createFileTouchPerCommitter;
		// read-only access, these collections are never modified by the clones.
		clone.extensions = this.extensions;
		clone.formats = this.formats;
		clone.fileTypePieChartFormats = this.fileTypePieChartFormats;
		clone.fileTouchPerCommitterFormats = this.fileTouchPerCommitterFormats; 
		return clone;
	}

	private Map<String, Integer> computeFileCountPerMimeType() {
		// Mime type is used as domain-specific ID of file type node
		fTypeCount = new HashMap<String, Integer>();
		Long start, end;
		FileType ft;
		Integer nbFiles;
		logger.info("Data generation: counting the number of files per mime type in repository: {}", rep.getName());
		start = System.nanoTime();
		Iterable<fr.lip6.msr4j.datamodel.nodes.File> it = rep.getFiles();
		for (File f : it) {
			ft = f.getFileType();
			if (ft != null) {
				if (fTypeCount.containsKey(ft.getType())) {
					nbFiles = fTypeCount.get(ft.getType());
					fTypeCount.put(ft.getType(), nbFiles + 1);
				} else {
					nbFiles = new Integer(1);
					fTypeCount.put(ft.getType(), nbFiles);
				}
			} else {
				nbFiles = fTypeCount.get("other");
				if (nbFiles != null) {
					fTypeCount.put("other", nbFiles + 1);
				} else {
					nbFiles = new Integer(1);
					fTypeCount.put("other", nbFiles);
				}
			}
		}
		end = System.nanoTime();
		logger.info("Finished counting the number of files per mime type. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
		return fTypeCount;
	}

	private Map<String, Map<String, Long>> computeFileTouchCountPerCommitter() {
		Long start, end;
		int nbFiles;
		final List<Callable<Map<String, Map<String, Long>>>> partition = new ArrayList<Callable<Map<String, Map<String, Long>>>>();
		logger.info("Data generation: counting the number of touches per file per committer in repository: {}", rep.getName());
		start = System.nanoTime();
		Iterable<fr.lip6.msr4j.datamodel.nodes.File> it = rep.getFiles();
		nbFiles = 0;
		for (File f : it) {
			partition.add(new FileTouchCounter(f));
			nbFiles++;
		}
		logger.info("Number of files in the repository {} : {}", rep.getName(), nbFiles);
		logger.info("Start counting touches for each file in the repository: {}", rep.getName());
		try {
			final List<Future<Map<String, Map<String, Long>>>> values = executorPool.invokeAll(partition, ConcurrencyConfig.ALLOWED_EXEC_TIME100,
					TimeUnit.SECONDS);
			logger.info("Collecting touch counts from workers...");
			allFiles = new HashMap<String, Map<String, Long>>();
			Map<String, Map<String, Long>> aVal;
			Map<String, Long> cCount;
			cms = new ConcurrentSkipListSet<String>();
			long maxTouch = 0L;
			for (Future<Map<String, Map<String, Long>>> value : values) {
				aVal = value.get();
				for (Entry<String, Map<String, Long>> ent : aVal.entrySet()) {
					logger.info("Collecting result for {}", ent.getKey());
					if (allFiles.get(ent.getKey()) == null) {
						allFiles.put(ent.getKey(), ent.getValue());
					} else {
						cCount = allFiles.get(ent.getKey());
						for (Entry<String, Long> en : ent.getValue().entrySet()) {
							logger.info("Processing entry {}", en.getKey());
							if (cCount.get(en.getKey()) == null) {
								cCount.put(en.getKey(), en.getValue());
							} else {
								cCount.put(en.getKey(), en.getValue() + cCount.get(en.getKey()));
							}
							maxTouch = Math.max(maxTouch, cCount.get(en.getKey()));
							cms.add(en.getKey());
						}
					}
				}
			}
			end = System.nanoTime();
			logger.info("Finished counting touches for each file. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
			logger.info("Max touch count in the repository: {}", maxTouch);
		} catch (InterruptedException | ExecutionException ex) {
			logger.error(ex.getMessage());
			logger.error(Arrays.toString(ex.getStackTrace()));
		}
		return allFiles;
	}

	/**
	 * Computes a prefix from the set of file extensions given in the program arguments.
	 * For example, if we have: <code>.java</code> and <code>.python</code>, then the prefix will be:
	 * <code>java_python_</code> <br/>
	 * If we have <code>.java</code> then the prefix will be <code>java_</code>
	 * @param extensions
	 * @return a prefix build out of the set of files extensions, used to build report files names.
	 */
	public String computePrefixForExtensions(Set<String> extensions) {
		StringBuilder sb = new StringBuilder();
		for(String s : extensions) {
			sb.append(s.substring(s.indexOf(".") + 1)).append("_");
		}
		
		return sb.toString();
	}

	/**
	 * Creates data for a heatmap. For each file in the repository, we have the
	 * number of commits each committer did that touch that file. Currently the
	 * data can be formatted as CSV or data table.
	 * 
	 * Each row is separated by a blank line.
	 * 
	 * @return the reference to the created file containing the data, null if
	 *         something went wrong (see log).
	 */
	public Set<java.io.File> createFileTouchPerCommitterData(DataFormat df) {
		Set<java.io.File> result = new HashSet<java.io.File>();
		Long start, end;
		// <File path <svnid, numCommits on file>>
		// I only support json format for this analysis
		if (df.equals(DataFormat.CSV) || df.equals(DataFormat.DATATABLE)) {
			try {
				computeFileTouchCountPerCommitter();
				logger.info("Start building plot data.");
				start = System.nanoTime();
				Map<String, Map<String, Long>> aVal;
				final List<Callable<String>> spartition = new ArrayList<Callable<String>>();
				for (Entry<String, Map<String, Long>> value : allFiles.entrySet()) {
					aVal = new HashMap<String, Map<String, Long>>();
					aVal.put(value.getKey(), value.getValue());
					spartition.add(new FileTouchDataFormatter(aVal, cms, extensions, df));
				}
				
				final List<Future<String>> dataValues = executorPool.invokeAll(spartition, ConcurrencyConfig.ALLOWED_EXEC_TIME100, TimeUnit.SECONDS);
				final StringBuilder content = new StringBuilder();
				if (df.equals(DataFormat.DATATABLE)) {
					// Insert options
					content.append("'vAxis': {title: 'Files'}, 'isStacked': 'true'").append(NL);
					// Header
					content.append("[['Name' ");
					for (String s : cms) {
						content.append("," + "'" + s + "'");
					}
					content.append("]");
					// Data
					for (Future<String> d : dataValues) {
						if (!d.get().isEmpty()){
							content.append(",").append(NL).append("[").append(d.get()).append("]");
						}
					}
					content.append(NL).append("]");
				} else { // CSV formatting
					content.append("Name ");
					for (String s : cms) {
						content.append("," + s);
					}
					content.append(NL);
					// Append data
					for (Future<String> d : dataValues) {
						if (!d.get().isEmpty())
							content.append(d.get() + NL);
					}
				}
				// content.append("e"); This is for gnuplot
				end = System.nanoTime();
				logger.info("Finished building plot data. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
				String path = RepoAnalyserRegistry.getDataPathForFormat(df, rep);
				if (df.equals(DataFormat.DATATABLE)) {
					path = path.replaceFirst(rep.getName(), rep.getName() + "-" + getPrefixForExtensions() + "File_Touch_per_Committer-BarChart");
				} else {
					path = path.replaceFirst(rep.getName(), rep.getName() + "-" + getPrefixForExtensions() + "File_Touch_per_Committer-HeatMap");
				}
				result.add(new IOUtils().writeDataIntoDestination(path, content));
				end = System.nanoTime();
				logger.info("End of heat map data generation. Total time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
			} catch (InterruptedException | ExecutionException ex) {
				logger.error(ex.getMessage());
				logger.error(Arrays.toString(ex.getStackTrace()));
			}
		} else {
			logger.warn("File Touch per Committer Data Generator: I only support {} and {} for this analysis. Abort.", DataFormat.CSV,
					DataFormat.DATATABLE);
		}
		return result;
	}

	/**
	 * Create Pie Chart data in data tables for display using Google Charts API.
	 * Each analysis method must dynamically rename the output file by replacing
	 * the repository name with the following pattern:
	 * repositoryName-Type_Of_Analysis-TypeOfChart.dataformat Ex:
	 * aRepo-File_per_Mime_Type-PieChart.datatable
	 * 
	 * @return
	 * @see DataFormat
	 */
	public Set<java.io.File> createFileTypePieChartData() {
		Set<java.io.File> result = new HashSet<java.io.File>();
		Long start, end;
		// I only support JS Datatable format for this analysis
		if (this.formats.contains(DataFormat.DATATABLE)) {
			computeFileCountPerMimeType();
			logger.info("Start building plot data.");
			start = System.nanoTime();
			final StringBuilder content = new StringBuilder();
			// Insert options
			content.append("'is3D': 'true'").append(NL);
			// Insert header
			content.append("[['File Mime Type', 'Number of Files']");
			for (Entry<String, Integer> e : fTypeCount.entrySet()) {
				content.append(",").append(NL).append("['").append(e.getKey()).append("', ").append(e.getValue().intValue()).append("]");
			}
			content.append(NL).append("]");
			end = System.nanoTime();
			logger.info("Finished building plot data. Time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
			String path = RepoAnalyserRegistry.getDataPathForFormat(DataFormat.DATATABLE, rep);
			path = path.replaceFirst(rep.getName(), rep.getName() + "-" + "Files_per_Mime_Type-PieChart");
			result.add(new IOUtils().writeDataIntoDestination(path, content));
			end = System.nanoTime();
			logger.info("End of pie chart data generation. Total time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
		} else {
			logger.warn("File Type Pie Chart Data Generator: I only support {} for this analysis. Abort.", DataFormat.DATATABLE);
		}
		return result;
	}

	/**
	 * Returns the count of files per mime type in the repository. To each mime
	 * type is associated the number of files.
	 * 
	 * If the data was computed before, this method just returns it again.
	 * 
	 * @return
	 * @see #updateAndGetFileCountPerMimeType
	 */
	public Map<String, Integer> getFileCountPerMimeType() {
		if (fTypeCount == null) {
			computeFileCountPerMimeType();
		}
		return fTypeCount;
	}

	/**
	 * Returns the file touch count per committer in the repository. To each
	 * file is associated a map in which there are committers and for each of
	 * them the number of times they have touched the file in any manner (add,
	 * delete, modifiy, replace).
	 * 
	 * If the data was computed before, this method just returns it again.
	 * 
	 * @return
	 * @see #updateAndGetFileTouchCountPerCommitter
	 */
	public Map<String, Map<String, Long>> getFileTouchCountPerCommitter() {
		if (allFiles == null) {
			computeFileTouchCountPerCommitter();
		}
		return allFiles;
	}

	public String getPrefixForExtensions() {
		if (this.fileExtensionsPrefix == null) {
			this.fileExtensionsPrefix = computePrefixForExtensions(extensions);
		}
		return this.fileExtensionsPrefix;
	}

	@Override
	public String getProviderHumanReadableDescription() {
		return RepositoryAnalyserSignature.getProviderHumanReadableDescription();
	}
	
	@Override
	public String getProviderHumanReadableName() {
		return RepositoryAnalyserSignature.getProviderHumanReadableName();
	}

	/**
	 * @return the rep
	 */
	public Repository getRepository() {
		return rep;
	}

	public boolean isCreateFileTouchBarChartData() {
		return this.createFileTouchBarChartData;
	}
	public boolean isCreateFileTouchHeatMapData() {
		return this.createFileTouchHeatMapData;
	}

	public boolean isCreateFileTouchPerCommitter() {
		return createFileTouchPerCommitter;
	}

	public boolean isCreateFileTypePieChartData() {
		return this.createFileTypePieChartData;
	}

	public RepositoryAnalyser setCreateFileTouchBarChartData(boolean createTouchBarChartData) {
		this.createFileTouchBarChartData = createTouchBarChartData;
		return this;
	}

	public RepositoryAnalyser setCreateFileTouchHeatMapData(boolean createTouchHeatMapData) {
		this.createFileTouchHeatMapData = createTouchHeatMapData;
		return this;
	}

	public RepositoryAnalyser setCreateFileTouchPerCommitter(boolean createFileTouchPerCommitter) {
		this.createFileTouchPerCommitter = createFileTouchPerCommitter;
		this.setCreateFileTouchHeatMapData(createFileTouchPerCommitter);
		this.setCreateFileTouchBarChartData(createFileTouchPerCommitter);
		return this;
	}

	public RepositoryAnalyser setCreateFileTypePieChartData(boolean createFileTypePieChartData) {
		this.createFileTypePieChartData = createFileTypePieChartData;
		return this;
	}

	public void setFileExtensions(Set<String> extensions) {
		this.extensions = new TreeSet<String>(extensions);
	}

	public void setFileTouchPerCommitterDataFormats(Set<DataFormat> fs) {
		this.fileTouchPerCommitterFormats.addAll(fs);
	}

	public void setFileTypePieChartDataFormats(Set<DataFormat> fs) {
		this.fileTypePieChartFormats.addAll(fs);
	}

	public void setheatMapDataFilePath(String path) {
	}

	/**
	 * @param rep
	 *            the repo to set
	 */
	@Override
	public void setRepository(Repository rep) {
		this.rep = rep;
	}

	/**
	 * Returns the same data as {@link #getFileCountPerMimeType} but with prior
	 * re-computation of the data.
	 * 
	 * @return
	 * @see #getFileCountPerMimeType
	 */
	public Map<String, Integer> updateAndGetFileCountPerMimeType() {
		computeFileCountPerMimeType();
		return fTypeCount;
	}

	/**
	 * Returns the same data as {@link #getFileTouchCountPerCommitter} but with
	 * prior re-computation of the data.
	 * 
	 * @return
	 * @see #getFileTouchCountPerCommitter
	 */
	public Map<String, Map<String, Long>> updateAndGetFileTouchCountPerCommitter() {
		computeFileTouchCountPerCommitter();
		return allFiles;
	}

	@Override
	public Set<AnalysisService> getProvidedServices() {
		Set<AnalysisService> res = null;
		if (this.services != null && !this.services.isEmpty()) {
			res = new HashSet<AnalysisService>();
			for (AnalysisService s : this.services) {
				res.add(s.swallowCopyWithoutState());
			}
		} else { 
			res = getServices();
			if (res != null && res.isEmpty()) {
				res = null;
			}
		}
		return res;
	}
	
	private Set<AnalysisService> getServices() {
		if (this.services == null) { // initialize
			ServicesRegistry.setLog(getLog());
			int ret = ServicesRegistry.getInstance().loadServices(SERVICES_DECL, context, this);
			if (ret != 0) {
				getLog().log(LogService.LOG_ERROR, "Services loader returned code: " + ret);
			} else {
				this.services = ServicesRegistry.getInstance().getRegisteredServices(this); // possibly empty
			}
		}
		return this.services;
	}

	@Override
	public AnalysisService getService(String name) {
		AnalysisService res = null;
		if (getServices() != null) {
			res = ServicesRegistry.getInstance().getRegisteredService(name);
		}
		return res;
	}

	@Override
	public boolean activateServicesByName(Set<String> name) {
		// TODO Auto-generated method stub
		return false;
	}

	

	@Override
	public boolean activateAllServices() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean activatePossibleServices(Set<AnalysisService> services) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean activatePossibleServicesByName(Set<String> services) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Set<AnalysisService> getServicesSupportingFormats(Set<DataFormat> formats) {
		Set<AnalysisService> res = null;
		if (getServices() != null) {
			if (this.services.isEmpty()) {
				res = Collections.emptySet();
			} else {
				res = new HashSet<AnalysisService>();
				for (AnalysisService s : this.services) {
					if(!Collections.disjoint(formats, s.getSupportedFormats())) {
						res.add(s);
					}
				}
			}
		}
		return res;
	}

	@Override
	public Set<AnalysisService> getServicesSupportingAllFormats(Set<DataFormat> formats) {
		Set<AnalysisService> res = null;
		if (getServices() != null) {
			if (this.services.isEmpty()) {
				res = Collections.emptySet();
			} else {
				res = new HashSet<AnalysisService>();
				for (AnalysisService s : this.services) {
					if(s.getSupportedFormats().containsAll(formats)) {
						res.add(s);
					}
				}
			}
		}
		return res;
	}

	@Override
	public int hashCode() {
		return RepositoryAnalyserSignature.getProviderHumanReadableName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass() && !(obj instanceof Analyser)) {
			return false;
		}
		Analyser other = (Analyser) obj;
		return RepositoryAnalyserSignature.getProviderHumanReadableName().equals(other.getProviderHumanReadableName());
	}

	/**
	 * @return the log
	 */
	public LogService getLog() {
		return log;
	}

	/**
	 * @param log the log to set
	 */
	public void setLog(LogService log) {
		this.log = log;
	}

}
