/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.nodes;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Adjacency;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;

import fr.lip6.msr4j.datamodel.relations.AttachedToRelation;
import fr.lip6.msr4j.datamodel.relations.ContainFileTypeRelation;
import fr.lip6.msr4j.datamodel.relations.ContainRelation;
import fr.lip6.msr4j.datamodel.relations.ForkRelation;
import fr.lip6.msr4j.datamodel.relations.ForkedIntoRelation;
import fr.lip6.msr4j.datamodel.relations.ImpactRelation;
import fr.lip6.msr4j.datamodel.relations.ParentRelation;
import fr.lip6.msr4j.datamodel.relations.RegisteredInRelation;

public interface Repository extends VertexFrame {
	@Property("name")
	void setName(String name);

	@Property("name")
	String getName();

	@Property("uri")
	void setUrj(String uri);

	@Property("uri")
	String getUri();

	@Property("type")
	void setType(String type);

	@Property("type")
	String getType();

	@Property("isRoot")
	void setRootRepository(boolean r);

	@Property("isRoot")
	boolean isRootRepository();

	// TODO - Adjacency to Project
	@Adjacency(label = AttachedToRelation.ATTACHED_TO, direction = Direction.OUT)
	Project getProject();

	@Adjacency(label = AttachedToRelation.ATTACHED_TO, direction = Direction.OUT)
	void setProject(Project p);

	@Adjacency(label = AttachedToRelation.ATTACHED_TO, direction = Direction.OUT)
	Project addProject();

	// TODO - Self Adjacency with Forked into
	@Adjacency(label = ForkedIntoRelation.FORKED_INTO, direction = Direction.OUT)
	Repository getForkedIntoRepository();

	@Adjacency(label = ForkedIntoRelation.FORKED_INTO, direction = Direction.OUT)
	void setForkedIntoRepository(Repository p);

	@Adjacency(label = ForkedIntoRelation.FORKED_INTO, direction = Direction.OUT)
	Repository addForkedIntoRepository();

	@Adjacency(label = ForkedIntoRelation.FORKED_INTO, direction = Direction.IN)
	Repository getForkedRepository();

	// TODO - Self Adjacency with Parent
	@Adjacency(label = ParentRelation.PARENT, direction = Direction.OUT)
	Repository getParentRepository();

	@Adjacency(label = ParentRelation.PARENT, direction = Direction.OUT)
	void setParentRepository(Repository p);

	@Adjacency(label = ParentRelation.PARENT, direction = Direction.OUT)
	Repository addParentRepository();

	@Adjacency(label = ParentRelation.PARENT, direction = Direction.IN)
	Iterable<Repository> getChildrenRepositories();

	// TODO - Adjacency to File
	@Adjacency(label = ContainRelation.CONTAIN, direction = Direction.OUT)
	Iterable<File> getFiles();

	@Adjacency(label = ContainRelation.CONTAIN, direction = Direction.OUT)
	void addFile(File f);

	@Adjacency(label = ContainRelation.CONTAIN, direction = Direction.OUT)
	void addFiles(Iterable<File> files);

	// TODO - Adjacency to File Type
	@Adjacency(label = ContainFileTypeRelation.CONTAIN_FILE_TYPE, direction = Direction.OUT)
	Iterable<FileType> getFileTypes();

	@Adjacency(label = ContainFileTypeRelation.CONTAIN_FILE_TYPE, direction = Direction.OUT)
	void addFile(FileType ft);

	@Adjacency(label = ContainFileTypeRelation.CONTAIN_FILE_TYPE, direction = Direction.OUT)
	void addFileType(Iterable<FileType> fts);

	// TODO - Adjacency from Commit
	@Adjacency(label = ImpactRelation.IMPACT, direction = Direction.IN)
	Iterable<Commit> getImpactingCommits();

	// TODO - Adjacency from Commiter (FORK)
	@Adjacency(label = ForkRelation.FORK, direction = Direction.IN)
	Iterable<Committer> getForkingCommitters();

	// TODO - Adjacency from Commiter (REGISTERED_IN)
	@Adjacency(label = RegisteredInRelation.REGISTERED_IN, direction = Direction.IN)
	Iterable<Committer> getRegisteredCommitters();

	/**
	 * Number of Files.
	 * 
	 * @return the number of files in this repository.
	 * @GremlinGroovy(value = "it.outE.has('label', '" + ContainRelation.CONTAIN
	 *                      + "').inV.gather{it.size()}", frame = false) long
	 *                      getNumberOfFiles();
	 */
}
