/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.nodes;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Adjacency;
import com.tinkerpop.frames.Incidence;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;

import fr.lip6.msr4j.datamodel.relations.ContainFileTypeRelation;
import fr.lip6.msr4j.datamodel.relations.FileTypeRelation;

public interface FileType extends VertexFrame {
	/**
	 * Relies on mimetypes from {@link http
	 * ://svn.apache.org/repos/asf/tika/trunk
	 * /tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml}.
	 * 
	 * @param type
	 */
	@Property("type")
	void setType(String type);

	/**
	 * Relies on mimetypes from {@link http
	 * ://svn.apache.org/repos/asf/tika/trunk
	 * /tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml}.
	 * 
	 * @return
	 */
	@Property("type")
	String getType();

	@Property("extension")
	void setExtension(String extension);

	@Property("extension")
	String getExtension();

	@Property("lang")
	void setProgrammingLanguage(String lang);

	@Property("lang")
	String getProgrammingLanguage();

	@Adjacency(label = FileTypeRelation.FILE_TYPE, direction = Direction.IN)
	Iterable<File> getFiles();

	@Incidence(label = FileTypeRelation.FILE_TYPE, direction = Direction.IN)
	Iterable<FileTypeRelation> getFileTypeRelations();
	
	@Adjacency(label = ContainFileTypeRelation.CONTAIN_FILE_TYPE, direction = Direction.IN)
	Iterable<Repository> getRepositories();

	@Incidence(label = ContainFileTypeRelation.CONTAIN_FILE_TYPE, direction = Direction.IN)
	Iterable<ContainFileTypeRelation> getContainFileTypeRelations();
}
