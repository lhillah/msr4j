/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.nodes;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Adjacency;
import com.tinkerpop.frames.Incidence;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.VertexFrame;

import fr.lip6.msr4j.datamodel.relations.AddRelation;
import fr.lip6.msr4j.datamodel.relations.ContainRelation;
import fr.lip6.msr4j.datamodel.relations.CopToRelation;
import fr.lip6.msr4j.datamodel.relations.CopyRelation;
import fr.lip6.msr4j.datamodel.relations.DeleteRelation;
import fr.lip6.msr4j.datamodel.relations.FileTypeRelation;
import fr.lip6.msr4j.datamodel.relations.ModifyRelation;
import fr.lip6.msr4j.datamodel.relations.RenToRelation;
import fr.lip6.msr4j.datamodel.relations.RenameRelation;
import fr.lip6.msr4j.datamodel.relations.RepByRelation;
import fr.lip6.msr4j.datamodel.relations.ReplaceRelation;
import fr.lip6.msr4j.datamodel.relations.TouchRelation;

public interface File extends VertexFrame {
	@Property("path")
	void setPath(String path);

	@Property("path")
	String getPath();

	@Property("isDirectory")
	void setDirectory(boolean d);

	@Property("isDirectory")
	boolean isDirectory();

	// TODO - Self Adjacency with REP_BY
	@Adjacency(label = RepByRelation.REP_BY, direction = Direction.OUT)
	File getReplacedBy();

	@Adjacency(label = RepByRelation.REP_BY, direction = Direction.OUT)
	void setReplacedBy(File f);

	@Adjacency(label = RepByRelation.REP_BY, direction = Direction.OUT)
	File addReplacedBy();

	@Adjacency(label = RepByRelation.REP_BY, direction = Direction.IN)
	File getReplacedFile();

	// TODO -Self Adjacency with COP_TO
	@Adjacency(label = CopToRelation.COP_TO, direction = Direction.OUT)
	File getCopiedTo();

	@Adjacency(label = CopToRelation.COP_TO, direction = Direction.OUT)
	void setCopiedTo(File f);

	@Adjacency(label = CopToRelation.COP_TO, direction = Direction.OUT)
	File addCopiedTo();

	@Adjacency(label = CopToRelation.COP_TO, direction = Direction.IN)
	File getCopiedFile();

	// TODO - Self Adjacency with REN_TO
	@Adjacency(label = RenToRelation.REN_TO, direction = Direction.OUT)
	File getRenamedInto();

	@Adjacency(label = RenToRelation.REN_TO, direction = Direction.OUT)
	void setRenamedInto(File f);

	@Adjacency(label = RenToRelation.REN_TO, direction = Direction.OUT)
	File addRenamedInto();

	@Adjacency(label = RenToRelation.REN_TO, direction = Direction.IN)
	File getRenamedFile();

	// TODO - Adjacency to File Type
	@Adjacency(label = FileTypeRelation.FILE_TYPE, direction = Direction.OUT)
	FileType getFileType();

	@Adjacency(label = FileTypeRelation.FILE_TYPE, direction = Direction.OUT)
	void setFileType(File f);

	@Adjacency(label = FileTypeRelation.FILE_TYPE, direction = Direction.OUT)
	FileType addFileType();

	// TODO - Adjacency from Committer
	@Adjacency(label = TouchRelation.TOUCH, direction = Direction.IN)
	Iterable<Committer> getCommitters();
	
	// TODO - Incidence from Touch
	@Incidence(label = TouchRelation.TOUCH, direction = Direction.IN)
	Iterable<TouchRelation> getTouches();

	// TODO - Adjacency from Repository
	@Adjacency(label = ContainRelation.CONTAIN, direction = Direction.IN)
	Repository getRepository();

	// TODO - Adjacency from Commit (Is it really useful?)
	@Adjacency(label = AddRelation.ADD, direction = Direction.IN)
	Commit getAddingCommit();

	@Adjacency(label = ModifyRelation.MODIFY, direction = Direction.IN)
	Iterable<Commit> getModifyingCommits();

	@Adjacency(label = DeleteRelation.DELETE, direction = Direction.IN)
	Commit getDeletingCommit();

	@Adjacency(label = RenameRelation.RENAME, direction = Direction.IN)
	Iterable<Commit> getRenamingCommits();

	@Adjacency(label = CopyRelation.COPY, direction = Direction.IN)
	Iterable<Commit> getCopyingCommits();

	@Adjacency(label = ReplaceRelation.REPLACE, direction = Direction.IN)
	Commit getReplacingCommit();

}
