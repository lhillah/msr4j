/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.relations;

import com.tinkerpop.frames.EdgeFrame;
import com.tinkerpop.frames.InVertex;
import com.tinkerpop.frames.OutVertex;
import com.tinkerpop.frames.Property;

import fr.lip6.msr4j.datamodel.nodes.Committer;
import fr.lip6.msr4j.datamodel.nodes.Project;

/**
 * Models the HOLD_ADMIN_ROLE between a Committer and a Project.
 * 
 * @author lom
 * 
 */
public interface HoldAdminRoleRelation extends EdgeFrame {

	String HOLD_ADMIN_ROLE = "Hold_Admin_Role";

	@OutVertex
	Committer getSourceNode();

	@InVertex
	Project getTargetNode();

	@Property("role-name")
	void setRoleName(String name);

	@Property("role-name")
	String getRoleName();

	@Property("start-date")
	void setStartDate(long date);

	@Property("start-date")
	long getStartDate();

	@Property("end-date")
	void setEndDate(long date);

	@Property("end-date")
	long getEndDate();

}
