/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.relations;

import com.tinkerpop.frames.EdgeFrame;
import com.tinkerpop.frames.InVertex;
import com.tinkerpop.frames.OutVertex;
import com.tinkerpop.frames.Property;

import fr.lip6.msr4j.datamodel.nodes.Committer;
import fr.lip6.msr4j.datamodel.nodes.Repository;

/**
 * Models the REGISTERED_IN relationship between a Committer node and a
 * Repository node.
 * 
 * @author lom
 * 
 */
public interface RegisteredInRelation extends EdgeFrame {

	String REGISTERED_IN = "Registered_In";

	@OutVertex
	Committer getSourceNode();

	@InVertex
	Repository getTargetNode();

	@Property("first-commit-date")
	void setFirstCommitDate(long date);

	@Property("first-commit-date")
	long getCommitDate();

	@Property("last-commit-date")
	void setLastCommitDate(long date);

	@Property("last-commit-date")
	long getLastCommitDate();

}
