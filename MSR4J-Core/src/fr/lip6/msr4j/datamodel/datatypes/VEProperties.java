/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.datamodel.datatypes;

/**
 * Properties that nodes and edges bear.
 * 
 * @author lom
 */
public enum VEProperties {
	ACTUAL_CONTRIBUTOR("actual-contributor"), COPY_REVISION("copy-revision"), COUNT(
			"count"), DATE("date"), DECRIPTION("description"), EMAIL("email"), EXTENSION(
			"extension"), SCM_ID("scmid"), IS_ASF_EMERITUS_MEMBER(
			"isASEmeritusFMember"), IS_ASF_MEMBER("isASFMember"), IS_BRANCHING(
			"is-branching"), IS_FORKING("is-forking"), IS_DIRECTORY(
			"isDirectory"), IS_ROOT("isRoot"), IS_TAGGING("is-tagging"), LANG(
			"lang"), MESSAGE("message"), NAME("name"), PATH("path"), PERSO_WEB_PAGE(
			"perso-web-page"), REPLACEMENT_REVISION("replacement-revision"), REVISION(
			"revision"), ROLE_NAME("role-name"), TYPE("type"), URI("uri"), WEB_SITE(
			"web-site"), RENAME_REVISION("rename-revision");

	public String getName() {
		return name;
	}

	private VEProperties(String name) {
		this.name = name;
	}

	private String name;
}
