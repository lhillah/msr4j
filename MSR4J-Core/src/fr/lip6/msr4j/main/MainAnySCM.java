/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.main;

import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.DB_PATH;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.LOG_FILE;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.REPO_NAME;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.SCM_CRED;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.SCM_URL;
import static fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder.SCM_WC;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.FileAppender;
import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.utils.config.CLIMessages;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.CoreCLIOptionsBuilder;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.db.SequentialNeo4JBatchInserterGIT;
import fr.lip6.msr4j.utils.db.ConcurrentNeo4JBatchInserterSVN;
import fr.lip6.msr4j.utils.db.DBHandler;
import fr.lip6.msr4j.utils.parsers.SCMTypeDetector;
import fr.lip6.msr4j.utils.strings.Printer;

/**
 * MSR4J Core component to extract data from repositories. Uses command-line
 * arguments, that are available using -help option.
 * This class was designed for extension by subclasses.
 * @author lom
 * 
 */
public class MainAnySCM {
	private static Logger logger;
	private static PropertiesManager dbprop;
	private static PropertiesManager scmprop;
	private static PropertiesManager wcprop;
	private static PropertiesManager credprop;
	private static CommandLineParser parser;
	private static CoreCLIOptionsBuilder cop;
	private static HelpFormatter formatter;
	private static CommandLine line;
	private static Map<String, RepositoryType> scmTypes;

	protected MainAnySCM() {
		super();
	}

	/**
	 * Main program to launch MSR4J Core: scan a repository, collect data and
	 * build the corresponding model in back end DB.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			setCoreCLIOptionsBuilder(CoreCLIOptionsBuilder.buildCoreOptions());
			if (parseArguments(args, MainAnySCM.class.getCanonicalName())) {
				getLogger().info(CLIMessages.ARGS_OK);
				loadCorePropertiesFromArgs();
				try {
					Set<Entry<Object, Object>> scmUrls = findRepositoriesToScan();
					setScmTypes(detectSCMTypes(getWorkingCopiesProp().getPropertiesAsMap()));
					Set<Entry<Object, Object>> svnUrls = extractScmUrls(scmUrls, RepositoryType.SVN);
					Set<Entry<Object, Object>> gitUrls = extractScmUrls(scmUrls, RepositoryType.GIT);
					Set<Entry<Object, Object>> mercurialUrls = extractScmUrls(scmUrls, RepositoryType.MERCURIAL);
					DBHandler<Object> dbhsvn = null, dbhgit = null;
					
					if(!svnUrls.isEmpty()) {
						getLogger().info("Handling SVN repositories first...");
						dbhsvn = new ConcurrentNeo4JBatchInserterSVN<Object>(getDbProp().getUserDBPath(), svnUrls, getWorkingCopiesProp(), getCredentialsProp());
						removeCreateDB(dbhsvn);
						extractToDBFromScratch(dbhsvn);
					} 
					
					if (!gitUrls.isEmpty()) {
						getLogger().info("Handling Git repositories...");
						dbhgit = new SequentialNeo4JBatchInserterGIT<Object>(getDbProp().getUserDBPath(), gitUrls, getWorkingCopiesProp(), getCredentialsProp());
						if (dbhsvn != null) {
							incrementallyExtractToDBUsing(dbhgit, dbhsvn);
						} else {
							extractToDBFromScratch(dbhgit);
						}
					}
					
					if (!mercurialUrls.isEmpty()) {
						getLogger().warn("Sorry, Mercurial repositories not yet supported!");
					}
					
					if (dbhsvn != null) {
						dbhsvn.shutDownDb();
					} else if (dbhgit != null){
						dbhgit.shutDownDb();
					}
					dbhsvn = dbhgit = null;
				} catch (Exception e) {
					getLogger().error(e.getMessage());
					getLogger().error(Arrays.toString(e.getStackTrace()));
				}
			}
		} finally {
			MSR4JLogger.shutDownLogging();
		}
	}

	protected static void incrementallyExtractToDBUsing(DBHandler<Object> dbhgit, DBHandler<Object> dbhsvn) {
		dbhgit.incrementallyPopulate(dbhsvn);
		
	}

	protected static Set<Entry<Object, Object>> extractScmUrls(Set<Entry<Object, Object>> scmUrls, RepositoryType rt) {
		Set<Entry<Object, Object>> result = new HashSet<Entry<Object, Object>>();
		String repoPath;
		for (Entry<Object, Object> e : scmUrls) {
			repoPath = getWorkingCopiesProp().getProperty((String)e.getKey());
			if(getScmTypes().get(repoPath).equals(rt)) {
				result.add(e);
			}
		}
		return result;
	}

	/**
	 * Given a set of property entries where an entry = <scm name, scm local
	 * copy path>, returns a map containing a set of entries where entry = <scm
	 * local path, scm type>.
	 * 
	 * @param p
	 *            the set of property entries (i.e. a map) where entry = <scm
	 *            name, scm local copy path>
	 * @return a map containing associations between each scm local path and the
	 *         repository type
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	protected static Map<String, RepositoryType> detectSCMTypes(Properties props) throws InterruptedException, ExecutionException {
		final Map<String, RepositoryType> scmtypes = new HashMap<String, RepositoryType>();

		final List<Callable<Map<String, RepositoryType>>> partition = new ArrayList<Callable<Map<String, RepositoryType>>>();
		for (Entry<Object, Object> e : props.entrySet()) {
			partition.add(new SCMTypeDetector((String) e.getValue()));
		}
		final ExecutorService exec = ConcurrencyConfig.getFixedThreadPoolExecutor();
		try {
			final List<Future<Map<String, RepositoryType>>> values = exec.invokeAll(partition, ConcurrencyConfig.ALLOWED_EXEC_TIME10,
					TimeUnit.SECONDS);
			Entry<String, RepositoryType> ent;
			for (Future<Map<String, RepositoryType>> v : values) {
				ent = v.get().entrySet().iterator().next();
				scmtypes.put(ent.getKey(), ent.getValue());
			}
		} finally {
			exec.shutdown();
		}
		return scmtypes;
	}

	/**
	 * Extract data from the pointed at repositories and call the provider db
	 * handler to populate Db from scratch, which means that the existing db
	 * back end used will be removed first, then created anew.
	 * 
	 * @param dbh
	 *            the db handler to use
	 */
	protected static void extractToDBFromScratch(DBHandler<Object> dbh) {
		dbh.populateDB(null);
	}
	
	protected static void removeCreateDB(DBHandler<Object> dbh) {
		dbh.removeDb();
		dbh.createBatchDb();
	}

	/**
	 * Returns the set of pairs <SCM name, SCM URL> to scan, as specified in the
	 * CLI by the -repoName option (either 'all' or a comma-separated list of
	 * names).
	 * 
	 * @return the set of pairs <SCM name, SCM URL> to scan
	 */
	protected static Set<Entry<Object, Object>> findRepositoriesToScan() {
		Set<Entry<Object, Object>> svnUrls;
		if ("all".equalsIgnoreCase(getCommandLine().getOptionValue(REPO_NAME))) {
			svnUrls = getScmUrlProp().getProperties();
		} else {
			svnUrls = new HashSet<Entry<Object, Object>>();
			String[] repos = getCommandLine().getOptionValues(REPO_NAME);
			Properties props = getScmUrlProp().getPropertiesAsMap();
			for (int i = 0; i < repos.length; i++) {
				if (props.containsKey(repos[i])) {
					svnUrls.add(new AbstractMap.SimpleImmutableEntry<Object, Object>(repos[i], props.get(repos[i])));
				}
			}
		}
		return svnUrls;
	}

	/**
	 * Parses command-line arguments and prints help if necessary
	 * 
	 * @param args
	 *            the command-line arguments
	 * @param classCanonicalName
	 *            the canonical name of the (main) class, which is passed to the
	 *            logging framework
	 * @return true if all expected arguments are provided
	 */
	protected static boolean parseArguments(String[] args, String classCanonicalName) {
		boolean result = true;
		setCommandLineParser(new BasicParser());
		setHelpFormatter(new HelpFormatter());
		setLogger(MSR4JLogger.getLogger(classCanonicalName));
		try {
			setCommandLine(getCommandLineParser().parse(getCoreCLIOptionsBuilder(), args));
			if (getCommandLine().hasOption(LOG_FILE)) {
				configureLogging(getCommandLine().getOptionValue(LOG_FILE));
			}
			if (getCommandLine().hasOption("help")) {
				Printer.println(CLIMessages.APPNAME + CLIMessages.VERSION + Printer.NL);
				getHelpFormatter().printHelp(CLIMessages.INVOCATION, getCoreCLIOptionsBuilder());
				result = false;
			} else if (getCommandLine().hasOption("version")) {
				Printer.println(CLIMessages.APPNAME + CLIMessages.VERSION);
				result = false;
			} else {
				String[] nonRecog = getCommandLine().getArgs();
				if (nonRecog != null && nonRecog.length != 0) {
					Error.fail(CLIMessages.ARGS_UNR + Arrays.toString(nonRecog), getLogger());
					result = false;
				}
			}
		} catch (ParseException e) {
			getLogger().error(e.getMessage());
			getHelpFormatter().printHelp(CLIMessages.INVOCATION, getCoreCLIOptionsBuilder());
			Error.fail(Arrays.toString(e.getStackTrace()), getLogger());
			result = false;
		}
		return result;
	}

	/**
	 * Load the following set of properties from the command-line arguments:
	 * <ul>
	 * <li>DB PATH</li>
	 * <li>SCM URLs</li>
	 * <li>SCM Working Copies local Paths (or clones)</li>
	 * <li>SCM Credentials</li>
	 * </ul>
	 */
	protected static void loadCorePropertiesFromArgs() {
		loadDBProperties();
		loadSCMURLProperties();
		loadSCMWCProperties();
		loadSCMCredentialsProperties();
	}

	protected static void loadSCMCredentialsProperties() {
		setCredentialsProp(PropertiesManager.getPropertyManagerFor(getCommandLine().getOptionValue(SCM_CRED)));
		getCredentialsProp().loadUserProperties();
	}

	protected static void loadSCMWCProperties() {
		setWorkingCopiesProp(PropertiesManager.getPropertyManagerFor(getCommandLine().getOptionValue(SCM_WC)));
		getWorkingCopiesProp().loadUserProperties();
	}

	protected static void loadSCMURLProperties() {
		setScmUrlprop(PropertiesManager.getPropertyManagerFor(getCommandLine().getOptionValue(SCM_URL)));
		getScmUrlProp().loadUserProperties();
	}

	protected static void loadDBProperties() {
		setDbProp(PropertiesManager.getPropertyManagerFor(getCommandLine().getOptionValue(DB_PATH)));
		getDbProp().loadUserProperties();
	}

	/**
	 * Configures log file option.
	 * 
	 * @param logfile
	 *            the log file path (relative or absolute).
	 */
	@SuppressWarnings("unchecked")
	protected static void configureLogging(String logfile) {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		@SuppressWarnings("rawtypes")
		FileAppender fileAppender = new FileAppender();
		fileAppender.setContext(loggerContext);
		fileAppender.setName(String.valueOf(System.nanoTime()));
		// set the file name
		fileAppender.setFile(logfile);

		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(loggerContext);
		encoder.setPattern(MSR4JLogger.LOGGING_PATTERN);
		encoder.start();

		fileAppender.setEncoder(encoder);
		fileAppender.start();
		setLogger(null);
		setLogger(loggerContext.getLogger(MainAnySCM.class.getCanonicalName()));
		((ch.qos.logback.classic.Logger) getLogger()).addAppender(fileAppender);
	}

	/**
	 * @return the credprop
	 */
	public static PropertiesManager getCredentialsProp() {
		return credprop;
	}

	/**
	 * @param credprop the credprop to set
	 */
	public static void setCredentialsProp(PropertiesManager credprop) {
		MainAnySCM.credprop = credprop;
	}

	/**
	 * @return the scmTypes
	 */
	public static Map<String, RepositoryType> getScmTypes() {
		return scmTypes;
	}

	/**
	 * @param scmTypes the scmTypes to set
	 */
	public static void setScmTypes(Map<String, RepositoryType> scmTypes) {
		MainAnySCM.scmTypes = scmTypes;
	}

	/**
	 * @return the wcprop
	 */
	public static PropertiesManager getWorkingCopiesProp() {
		return wcprop;
	}

	/**
	 * @param wcprop the wcprop to set
	 */
	public static void setWorkingCopiesProp(PropertiesManager wcprop) {
		MainAnySCM.wcprop = wcprop;
	}

	/**
	 * @return the scmprop
	 */
	public static PropertiesManager getScmUrlProp() {
		return scmprop;
	}

	/**
	 * @param scmprop the scmprop to set
	 */
	public static void setScmUrlprop(PropertiesManager scmprop) {
		MainAnySCM.scmprop = scmprop;
	}

	/**
	 * @return the dbprop
	 */
	public static PropertiesManager getDbProp() {
		return dbprop;
	}

	/**
	 * @param dbprop the dbprop to set
	 */
	public static void setDbProp(PropertiesManager dbprop) {
		MainAnySCM.dbprop = dbprop;
	}

	/**
	 * @return the cop
	 */
	public static CoreCLIOptionsBuilder getCoreCLIOptionsBuilder() {
		return cop;
	}

	/**
	 * @param cop the cop to set
	 */
	public static void setCoreCLIOptionsBuilder(CoreCLIOptionsBuilder cop) {
		MainAnySCM.cop = cop;
	}

	/**
	 * @return the parser
	 */
	public static CommandLineParser getCommandLineParser() {
		return parser;
	}

	/**
	 * @param parser the parser to set
	 */
	public static void setCommandLineParser(CommandLineParser parser) {
		MainAnySCM.parser = parser;
	}

	/**
	 * @return the formatter
	 */
	public static HelpFormatter getHelpFormatter() {
		return formatter;
	}

	/**
	 * @param formatter the formatter to set
	 */
	public static void setHelpFormatter(HelpFormatter formatter) {
		MainAnySCM.formatter = formatter;
	}

	/**
	 * @return the line
	 */
	public static CommandLine getCommandLine() {
		return line;
	}

	/**
	 * @param line the line to set
	 */
	public static void setCommandLine(CommandLine line) {
		MainAnySCM.line = line;
	}

	/**
	 * @return the logger
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public static void setLogger(Logger logger) {
		MainAnySCM.logger = logger;
	}
}