/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.repositories;

import java.io.File;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;

public class WorkingCopyFetcher {
	private static SVNClientManager ourClientManager = SVNClientManager
			.newInstance();

	private WorkingCopyFetcher() {
		super();
	}

	/**
	 * @see http 
	 *      ://www.svnkit.com/javadoc/org/tmatesoft/svn/core/wc/SVNUpdateClient
	 *      .html
	 *      #doCheckout(org.tmatesoft.svn.core.SVNURL,%20java.io.File,%20org
	 *      .tmatesoft
	 *      .svn.core.wc.SVNRevision,%20org.tmatesoft.svn.core.wc.SVNRevision
	 *      ,%20org.tmatesoft.svn.core.SVNDepth,%20boolean)
	 * @param url
	 * @param revision
	 * @param destPath
	 * @param allowUnversionedObstructions
	 * @return
	 * @throws SVNException
	 */
	public static long checkout(SVNURL url, SVNRevision revision,
			File destPath, boolean allowUnversionedObstructions)
			throws SVNException {
		SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
		/*
		 * sets externals not to be ignored during the checkout
		 */
		updateClient.setIgnoreExternals(false);
		/*
		 * returns the number of the revision at which the working copy is
		 */
		return updateClient.doCheckout(url, destPath, SVNRevision.HEAD,
				revision, SVNDepth.INFINITY, allowUnversionedObstructions);

	}

	/**
	 * @see http 
	 *      ://www.svnkit.com/javadoc/org/tmatesoft/svn/core/wc/SVNUpdateClient
	 *      .html
	 *      #doUpdate(java.io.File,%20org.tmatesoft.svn.core.wc.SVNRevision,
	 *      %20org.tmatesoft.svn.core.SVNDepth,%20boolean,%20boolean)
	 * @param wcPath
	 * @param updateToRevision
	 * @param depth
	 * @param allowUnversionedObstructions
	 * @param depthIsSticky
	 * @return
	 * @throws SVNException
	 */
	public static long update(File wcPath, SVNRevision updateToRevision,
			SVNDepth depth, boolean allowUnversionedObstructions,
			boolean depthIsSticky) throws SVNException {

		SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
		/*
		 * sets externals not to be ignored during the update
		 */
		updateClient.setIgnoreExternals(false);
		/*
		 * returns the number of the revision wcPath was updated to
		 */
		return updateClient.doUpdate(wcPath, updateToRevision, depth,
				allowUnversionedObstructions, depthIsSticky);
	}

}
