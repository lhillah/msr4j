/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.spi;

import java.util.Map;
import java.util.Set;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.nodes.Repository;

public interface Neo4JDBReader {
	
	/**
	 * Returns all Repository nodes in the back-end db.
	 * 
	 * @return the set of all Repository nodes
	 * @see Neo4JDBReader#getRepositoriesOfType(RepositoryType)
	 */
	Set<Repository> getAllRepositories();
	
	/**
	 * Returns all Repository nodes' names in the back-end db.
	 * 
	 * @return the set of all Repository nodes
	 * @see Neo4JDBReader#getAllRepositories()
	 */
	Set<String> getAllRepositoriesNames();
	
	/**
	 * Returns the set of all occurrences of Repository nodes having the names
	 * in the argument (array)
	 * 
	 * @param names
	 *            the array of repository names that are being looked up
	 * @return the set of all occurrence of Repository node for each name in the
	 *         argument.
	 * @see #getRepositories(String[])
	 */
	Set<Repository> getAllRepositories(String[] names);
	
	/**
	 * Returns the path to the back-end DB.
	 * @return
	 */
	String getDbPath();
	
	/**
	 * Returns all file extensions available in the DB. Returns null otherwise.
	 * 
	 * @return the set of all file extensions available in the DB,
	 *         <code>null</code> if no file extension can be found.
	 */
	Set<String> getFileExtensions();
	
	/**
	 * Returns the map associating each repository with its set of file types.
	 *  If there are no repository, returns <code>null</code>.
	 *  If there are no file types for a specific repository, the associated
	 *  valyue its key (in the corresponding entry) is <code>null</code>. 
	 * @return
	 */
	Map<Repository, Set<String>> getRepositoriesAndExtensions();
	
	/**
	 * Returns all Repository nodes of the repository type specified as
	 * argument.
	 * 
	 * @param rt
	 *            the repository type of the nodes to be retrieved
	 * @return the set of all Repository nodes of the given type
	 * @see #getAllRepositories()
	 */
	Set<Repository> getRepositoriesOfType(RepositoryType rt);
	
	/**
	 * Returns the Repository node having the specified
	 * name. It is advised that the name be unique (thus used as id at the
	 * domain-specific level).
	 * 
	 * Node id is a long and automatically created by the underlying framework,
	 * so do not expect equals and hashcode to work on your domain-specific
	 * attributes.
	 * 
	 * @param name
	 *            the name of the repository which is being looked up
	 * @return a reference to the repository node, null otherwise
	 * @see #getAllRepositories(String)
	 *
	 */
	Repository getRepository(String name);
	
	/**
	 * Opens the DB.
	 */
	void openDB();
	
	/**
	 * Sets the path to the back-end DB.
	 * @param dbpath
	 */
	void setDbPath(String dbpath);
	
	/**
	 * Shuts down the DB.
	 */
	void shutDownDb();
}
