/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
/**
 * 
 */
package fr.lip6.msr4j.utils.spi;

/**
 * Interface for providing services about the paths used in MSR4J.
 * 
 * @author lom
 * 
 */
public interface ApplicationPaths {
	public static final String FSEP = System.getProperty("file.separator");
	public static final String USER_DIR = System.getProperty("user.dir");
	/**
	 * Configuration files base directory. We assume it is Felix base directory.
	 */
	public static final String CONFIG_BASEDIR = USER_DIR + FSEP + "conf";
	/**
	 * Log file base directory.
	 */
	public static final String LOG_BASEDIR = USER_DIR + FSEP + "log";
	/**
	 * Front end property name.
	 */
	public static final String FRONTEND_BASE = "msr4j.frontend.base";
	/**
	 * Back end property name
	 */
	public static final String BACKEND_DB = "msr4j.backend.db";
	/**
	 * SCM URLS property name
	 */
	public static final String BACKEND_SCMURLS = "msr4j.backend.scmurls";
	/**
	 * SCM local paths property name.
	 */
	public static final String BACKEND_SCMLOCAL = "msr4j.backend.scmlocal";
	/**
	 * SCM credentials property name.
	 */
	public static final String BACKEND_SCMCRED = "msr4j.backend.scmcred";
	/**
	 * Path to the base configuration file.
	 */
	public static final String CONFIG_PATH = CONFIG_BASEDIR + FSEP + "msr4j.properties";
	/**
	 * Path to the config file where to find the path to the DB.
	 */
	public static final String DB_CONFIG_PATH = CONFIG_BASEDIR + FSEP + "msr4j-db.properties";

	public static final String SCM_URLS_CONFIG_PATH = CONFIG_BASEDIR + FSEP + "msr4j-scmurls.properties";
	public static final String SCM_LOCAL_CONFIG_PATH = CONFIG_BASEDIR + FSEP + "msr4j-scmlocapaths.properties";
	public static final String SCM_CRED_CONFIG_PATH = CONFIG_BASEDIR + FSEP + "msr4j-scmcredential.properties";

	/**
	 * Application-wide log file.
	 */
	public static final String APP_LOG_FILE = LOG_BASEDIR + FSEP + "msr4j.log";

	/**
	 * Path to the back-end DB.
	 * 
	 * @return the dbPath
	 */
	String getDbPath();

	/**
	 * Sets the path to the back-end DB.
	 * 
	 * @param dbPath
	 *            the dbPath to set
	 */
	void setDbPath(String path);

	/**
	 * Path to msr4j base 'property file' (msr4j.properties). If this value was
	 * not overriden at runtime, then returns the default path.
	 * 
	 * @return the configPath
	 * @see #setConfigPath(String)
	 */
	String getConfigPath();

	/**
	 * Enables overring at runtime of the path to msr4j base property file's
	 * default path.
	 * 
	 * @param configPath
	 *            the configPath to set
	 * @see #getConfigPath()
	 */
	void setConfigPath(String cPath);

	/**
	 * Path to the Db 'property file', where to find the path to the back-end
	 * DB.
	 * 
	 * @return the dbconfig
	 * @see #setDbconfig(String)
	 */
	String getDbconfig();

	/**
	 * @param dbconfig
	 *            the dbconfig to set
	 * @see #getDbconfig()
	 */
	void setDbconfig(String dbconf);

	/**
	 * Path to SCM URLs property file
	 * 
	 * @return the scmurlsconfig
	 */
	String getScmurlsconfig();

	/**
	 * @param scmurlsconfig
	 *            the scmurlsconfig to set
	 */
	void setScmurlsconfig(String scmurlsconf);

	/**
	 * Path to SCM local paths property file.
	 * 
	 * @return the scmlocalconfig
	 * @see #setScmlocalconfig()
	 */
	String getScmlocalconfig();

	/**
	 * @param scmlocalconfig
	 *            the scmlocalconfig to set
	 * @see #getScmlocalconfig()
	 */
	void setScmlocalconfig(String scmlocalconf);

	/**
	 * Path to SCM credentials property file.
	 * 
	 * @return the scmcredconfig
	 * @see #setScmcredconfig(String)
	 */
	String getScmcredconfig();

	/**
	 * @param scmcredconfig
	 *            the scmcredconfig to set
	 * @see #getScmcredconfig()
	 */
	void setScmcredconfig(String scmcredconf);

	/**
	 * Path to the front-end folder of MSR4J, where the web application resides.
	 * 
	 * @return the frontendpath
	 * @see #setFrontendpath(String)
	 */
	String getFrontendpath();

	/**
	 * Sets the path to the front-end older of MSR4J, where the web application
	 * resides.
	 * 
	 * @param frontendpath
	 *            the frontendpath to set
	 * @see #getFrontendpath()
	 */
	void setFrontendpath(String frontendpath);

}
