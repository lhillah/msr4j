/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.config;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;

import fr.lip6.msr4j.utils.spi.ApplicationPaths;

/**
 * Loads the various configurations files of MSR4J, in which path to needed
 * resources are defined.
 * 
 * @author lom
 * 
 */
public final class Paths implements ApplicationPaths {

	private final Logger logger;

	// Actual locations of config files
	private String configPath;

	private String dbconfig;

	private String scmurlsconfig;

	private String scmlocalconfig;

	private String scmcredconfig;

	private String frontendpath;

	/**
	 * Path to the back-end DB. Will be set to the value in db.properties file.
	 */
	private String dbPath;

	public Paths() {
		logger = MSR4JLogger.getLogger(getClass().getCanonicalName());
		logger.info("Started Paths component instance.");
		initPaths();
	}

	/**
	 * Loads all paths from the base configuration file (msr4j.properties) and
	 * sets the path to the back-end DB.
	 */
	public void initPaths() {
		PropertiesManager pm = new PropertiesManager(CONFIG_PATH);
		pm.loadUserProperties();
		logger.info("Loaded MSR4J root configuration file.");
		setFrontendpath(pm.getProperty(FRONTEND_BASE));
		setDbconfig(checkAndFetch(pm.getProperty(BACKEND_DB), DB_CONFIG_PATH));
		setScmurlsconfig(checkAndFetch(pm.getProperty(BACKEND_SCMURLS), SCM_URLS_CONFIG_PATH));
		setScmlocalconfig(checkAndFetch(pm.getProperty(BACKEND_SCMLOCAL), SCM_LOCAL_CONFIG_PATH));
		setScmcredconfig(checkAndFetch(pm.getProperty(BACKEND_SCMCRED), SCM_CRED_CONFIG_PATH));
		// Fetch DB path
		pm.setUserPropertiesPath(getDbconfig());
		pm.loadUserProperties();
		setDbPath(pm.getUserDBPath());
		logger.info("Initialised paths for MSR4J from all configuration files.");
	}

	private String checkAndFetch(String newPath, String defaultPath) {
		String res = defaultPath;
		if (changedDefaultPath(newPath)) {
			res = newPath;
			logger.info("User-defined configuration file: " + newPath);
		}
		return res;
	}

	private boolean changedDefaultPath(String path) {
		return !path.startsWith("msr4j-");
	}

	@Override
	public String getDbPath() {
		return dbPath;
	}

	@Override
	public void setDbPath(String path) {
		dbPath = path;
	}

	/**
	 * Create a file:// system path scheme, out of given absolute path (Windows
	 * and *nix-based operating systems).
	 * 
	 * @param path
	 * @return
	 * @throws URISyntaxException
	 */
	public static URI createSystemPath(String path) throws URISyntaxException {
		URI uri = null;
		if (SystemUtils.IS_OS_WINDOWS) {
			uri = new URI("file:///" + path);
		} else {
			uri = new URI("file://" + path);
		}
		return uri;
	}

	@Override
	public String getConfigPath() {
		String res = CONFIG_PATH;
		if (configPath != null) {
			res = configPath;
		}
		return res;
	}

	@Override
	public void setConfigPath(String cPath) {
		configPath = cPath;
	}

	@Override
	public String getDbconfig() {
		return dbconfig;
	}

	@Override
	public void setDbconfig(String dbconf) {
		dbconfig = dbconf;
	}

	@Override
	public String getScmurlsconfig() {
		return scmurlsconfig;
	}

	@Override
	public void setScmurlsconfig(String scmurlsconf) {
		scmurlsconfig = scmurlsconf;
	}

	@Override
	public String getScmlocalconfig() {
		return scmlocalconfig;
	}

	@Override
	public void setScmlocalconfig(String scmlocalconf) {
		scmlocalconfig = scmlocalconf;
	}

	@Override
	public String getScmcredconfig() {
		return scmcredconfig;
	}

	@Override
	public void setScmcredconfig(String scmcredconf) {
		scmcredconfig = scmcredconf;
	}

	@Override
	public String getFrontendpath() {
		return frontendpath;
	}

	@Override
	public void setFrontendpath(String frontendpath) {
		this.frontendpath = frontendpath;
	}
}
