/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.config;

public class CLIMessages {

	public static String APPNAME = "MSR4J";
	public static String VERSION = "0.0.3-SNAPSHOT";
	public static final String ARGS_OK = "All expected arguments ok.";
	public static final String ARGS_KO = "Insufficient number of required arguments.";
	public static final String ARGS_UNR = "Non-recognized arguments: ";
	public static final String INVOCATION = "java -jar pathToJarFile";
}
