/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.config;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.FileAppender;
import fr.lip6.msr4j.utils.spi.ApplicationPaths;

public final class MSR4JLogger {

	public static final String LOGGING_PATTERN = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n";

	public static final String MSR4J_LOG_NAME = "fr.lip6.msr4j.logging";

	private static volatile Logger logger;

	private MSR4JLogger() {
		super();
	}

	/**
	 * <p>Class-level logger, but output is written in the same application-wide file log.
	 * In this case, it is assumed the Osgi (Felix) framework has been launched with:
	 * -Dlogback.configurationFile=/path/to/config.xml to specify the location of the
	 * default configuration file.</p>
	 * <p>Therefore, it wont be necessary in this case to invoke neither {@link setAndGetLogger}
	 * nor {@link getAppLogger}</p>
	 * <p>This way is recommended as it is simpler to set up (via command-line property).</p>
	 * @param id
	 * @return
	 */
	public static synchronized Logger getLogger(String id) {
		return LoggerFactory.getLogger(id);
	}

	/**
	 * The logging must be shut down from a single authoritative point in the application.
	 */
	public static void shutDownLogging() {
		ILoggerFactory factory = LoggerFactory.getILoggerFactory();
		if (factory instanceof LoggerContext) {
			LoggerContext ctx = (LoggerContext) factory;
			ctx.stop();
		}
	}
	

	/**
	 * Class-level specific logger, with a specific file for the object asking
	 * for it.
	 * 
	 * @param logfile path to the logfile
	 * @param myClass
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Logger setAndGetLogger(String logfile, @SuppressWarnings("rawtypes") Class myClass) {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		@SuppressWarnings("rawtypes")
		FileAppender fileAppender = new FileAppender();
		fileAppender.setContext(loggerContext);
		fileAppender.setName(String.valueOf(System.nanoTime()));
		// set the file name
		fileAppender.setFile(logfile);

		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(loggerContext);
		encoder.setPattern(MSR4JLogger.LOGGING_PATTERN);
		encoder.start();

		fileAppender.setEncoder(encoder);
		fileAppender.start();

		Logger logger = loggerContext.getLogger(myClass.getCanonicalName());
		((ch.qos.logback.classic.Logger) logger).addAppender(fileAppender);
		return logger;
	}

	/**
	 * Application-wide logger (same logger name), all objects can use it.
	 * 
	 * @param logfile
	 * @return Application-wide logger
	 */
	@SuppressWarnings("unchecked")
	public static Logger setAndGetAppLogger(String logfile) {
		if (logger == null) {
			LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
			@SuppressWarnings("rawtypes")
			FileAppender fileAppender = new FileAppender();
			fileAppender.setContext(loggerContext);
			fileAppender.setName(String.valueOf(System.nanoTime()));
			// set the file name
			fileAppender.setFile(logfile);

			PatternLayoutEncoder encoder = new PatternLayoutEncoder();
			encoder.setContext(loggerContext);
			encoder.setPattern(MSR4JLogger.LOGGING_PATTERN);
			encoder.start();

			fileAppender.setEncoder(encoder);
			fileAppender.start();

			logger = loggerContext.getLogger(MSR4J_LOG_NAME);
			((ch.qos.logback.classic.Logger) logger).addAppender(fileAppender);
		}
		return logger;
	}

	/**
	 * Application-wide logger (same logger name). Creates one if none already exists
	 * by invoking {@link #setAndGetAppLogger(String)}
	 * @return the application-wide logger.
	 * @see #setAndGetAppLogger(String)
	 */
	public static Logger getAppLogger() {
		if (logger == null) {
			return setAndGetAppLogger(ApplicationPaths.APP_LOG_FILE);
		} else {
			return logger;
		}
	}
}
