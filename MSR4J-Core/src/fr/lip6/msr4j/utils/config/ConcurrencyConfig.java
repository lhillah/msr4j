/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Runtime configuration for concurrency. Blocking coefficient set to 0.1
 * because the IO-intensive profile of this application.
 * 
 * @author lom
 * 
 */
public final class ConcurrencyConfig {
	protected static final double HALF = 0.5;
	
	
	public static final double NANO = 1.0e9;
	
	public static final int ALLOWED_EXEC_TIME10 = 10;
	public static final int ALLOWED_EXEC_TIME100 = 100;
	public static final int ALLOWED_EXEC_TIME1000 = 1000;
	
	private ConcurrencyConfig() {
		super();
	}

	public static final int NUM_OF_CORE = Runtime.getRuntime()
			.availableProcessors();
	public static final double BLOCKING_COEF = 0.2;
	
	public static final int POOL_SIZE = (int) (NUM_OF_CORE / (1 - BLOCKING_COEF));
	
	public static ExecutorService getFixedThreadPoolExecutor() {
		return Executors.newFixedThreadPool(ConcurrencyConfig.POOL_SIZE);
	}
	
	public static ExecutorService getCachedThreadPoolExecutor() {
		return Executors.newCachedThreadPool();
	}
	
}
