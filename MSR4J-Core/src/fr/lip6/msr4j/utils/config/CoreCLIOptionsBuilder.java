/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.config;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

/**
 * Core options builder for MSR4J Core component. It relies on Apache Commons
 * CLI. You may add the custom options of your own application which builds upon
 * MSR4J.
 * 
 * @author lom
 * 
 */
@SuppressWarnings("static-access")
public final class CoreCLIOptionsBuilder extends Options {

	public static final String SCM_URL = "scmURL";
	public static final String SCM_WC = "scmWC";
	public static final String SCM_CRED = "scmCred";
	public static final String REPO_NAME = "repoNames";
	public static final String DB_PATH = "dbPath";
	public static final String LOG_FILE = "logfile";
	private static final long serialVersionUID = -2834776987526059421L;
	private static final CoreCLIOptionsBuilder instance = new CoreCLIOptionsBuilder();
	private static Option logfile;
	private static Option scmURL;
	private static Option scmWC;
	private static Option scmCredentials;
	private static Option dbPath;
	private static Option help;
	private static Option version;
	private static Option repoName;

	static {
		logfile = OptionBuilder.withArgName("file").hasArg()
				.withDescription("use given file for log. It will be created if it doesn't exist (including parent folders, recursively)")
				.create(LOG_FILE);

		dbPath = OptionBuilder.isRequired().withArgName("file").hasArg().withDescription("Required: use given file containing the back end DB path")
				.create(DB_PATH);

		scmURL = OptionBuilder.isRequired().withArgName("file").hasArg().withDescription("Required: use given file containing the SCM URLs")
				.create(SCM_URL);

		scmWC = OptionBuilder.isRequired().withArgName("file").hasArg()
				.withDescription("Required: use given file containing the SCM working copies local paths (or clones)").create(SCM_WC);

		scmCredentials = OptionBuilder.isRequired().withArgName("file").hasArg()
				.withDescription("Required: use given file containing the SCM Credentials").create(SCM_CRED);

		repoName = OptionBuilder.isRequired().withArgName("repository-names").hasArgs().withValueSeparator(',')
				.withDescription("Required: process the repositories identified by these names (e.g: a or a,b,c or 'all')").create(REPO_NAME);
	}

	private CoreCLIOptionsBuilder() {
		help = new Option("help", "print this help message");
		version = new Option("version", "print the version information and exit");
	}

	/**
	 * Returns a singleton instance holding all core options (dbPath, scmURL,
	 * scmWC, scmCredentials, logfile, repo names). If you don't call this
	 * method, no option is registered.
	 * 
	 * @return a singleton instance which holds MSR4J's core options
	 * @see #addCustomOption(Option)()
	 */
	public static CoreCLIOptionsBuilder buildCoreOptions() {
		instance.addOption(help).addOption(version);
		instance.addOption(dbPath).addOption(scmURL).addOption(scmWC).addOption(scmCredentials).addOption(logfile).addOption(repoName);
		return instance;
	}

	public static CoreCLIOptionsBuilder buildDBPathOption() {
		return addCustomOption(dbPath);
	}

	public static CoreCLIOptionsBuilder buildSCMURLOption() {
		return addCustomOption(scmURL);
	}

	public static CoreCLIOptionsBuilder buildSCMWCOption() {
		return addCustomOption(scmWC);
	}

	public static CoreCLIOptionsBuilder buildSCMCredntialsOption() {
		return addCustomOption(scmCredentials);
	}

	public static CoreCLIOptionsBuilder buildLogfileOption() {
		return addCustomOption(logfile);
	}

	public static CoreCLIOptionsBuilder buildRepoNameOption() {
		return addCustomOption(repoName);
	}
	
	public static CoreCLIOptionsBuilder buildHelpOption() {
		return addCustomOption(help);
	}
	
	public static CoreCLIOptionsBuilder buildVersionOption() {
		return addCustomOption(version);
	}
	
	/**
	 * Adds a new user application-specific option to the singleton instance of
	 * MSR4J's core options builder. If you want to also benefit from the core
	 * options of MSR4J other than help and version, call the
	 * {@link #buildCoreOptions()} method first.
	 * 
	 * @param op
	 *            the new option to add
	 * @return the singleton instance which holds MSR4J's core options
	 * @see #buildCoreOptions()
	 */
	public static CoreCLIOptionsBuilder addCustomOption(Option op) {
		instance.addOption(op);
		return instance;
	}
}
