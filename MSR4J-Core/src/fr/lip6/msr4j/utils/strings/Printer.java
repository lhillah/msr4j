/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.strings;


/**
 * Single place to output information on the console (apart from normal logging)
 * 
 * @author lom
 * 
 */
public class Printer {

	public static final String NL = StringUtils.NL;

	public Printer() {
		super();
	}

	public static void println(Object msg) {
		System.out.println(msg);
	}
	
	public static void print(Object msg) {
		System.out.print(msg);
	}
}
