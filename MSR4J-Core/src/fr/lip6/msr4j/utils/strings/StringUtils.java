/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.strings;


public final class StringUtils {

	public static final String NL = "\n";
	
	private StringUtils() {
		super();
	}

	/**
	 * "Normalizes" 2 strings and checks for equality. Normalization here means:
	 * remove any white space (inside or trailing) and turn into lowercase.
	 * 
	 * @param s
	 * @param t
	 * @return whether the two arguments, after normalisation, are equal
	 */
	public static boolean normalizeCheckEqual(String s, String t) {
		String normS = s.trim().replaceAll("\\s", "").toLowerCase();
		String normT = t.trim().replaceAll("\\s", "").toLowerCase();
		return normS.equals(normT);
	}

	/**
	 * "Normalizes" two strings and check whether either is contained in the
	 * other. Normalization: trim, remove internal spaces and turn into lower
	 * case.
	 * 
	 * @param s
	 * @param t
	 * @param direction
	 *            in which direction to make the check. Both is check with the
	 *            boolean connective 'or', not 'and' (otherwise you would be
	 *            explicitly checking for equality, which is not the purpose of
	 *            this method).
	 * @return whether either (of t or s) is contained in the other.
	 * @see {@link #normalizeCheckEqual}
	 */
	public static boolean normalizeCheckContain(String s, String t,
			ContainmentCheckDirection direction) {
		String normS = s.trim().replaceAll("\\s", "").toLowerCase();
		String normT = t.trim().replaceAll("\\s", "").toLowerCase();
		boolean result = false;
		switch (direction) {
		case LEFT_RIGHT:
			result = normS.contains(normT);
			break;
		case RIGHT_LEFT:
			result = normT.contains(normS);
			break;
		case BOTH:
			result = normS.contains(normT) || normT.contains(normS);
			break;
		default:
			break;
		}
		return result;
	}
}
