/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.neo4j.cypher.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.logging.BufferingLogger;
import org.slf4j.Logger;

import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.frames.FramedGraph;
import com.tinkerpop.frames.FramedGraphFactory;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.datatypes.VEProperties;
import fr.lip6.msr4j.datamodel.nodes.FileType;
import fr.lip6.msr4j.datamodel.nodes.Repository;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.spi.Neo4JDBReader;

public final class SequentialNeo4JDBReader implements Neo4JDBReader {

	private final Logger logger;

	private String dbpath;
	private FramedGraphFactory factory;

	private FramedGraph<Neo4jGraph> graph;

	/**
	 * Neo4J database service handler.
	 */
	private GraphDatabaseService graphDb;

	private ExecutionEngine engine;

	private String pathToNeo4J;

	/**
	 * Given the path to Neo4J DB, extracts and returns path to Neo4J
	 * installation
	 * 
	 * @param dbpath2
	 *            is path/to/neo4j/data/graph.db
	 * @return path/to/neo4j/
	 */
	public static String extractPathToNeo4J(String dbpath) {
		int index = dbpath.lastIndexOf("data/graph.db");
		return dbpath.substring(0, index);
	}

	/**
	 * Registers shutdown hook for graph db service (neo4j).
	 * 
	 * @param gdb
	 */
	public static void registerShutdownHook(final GraphDatabaseService gdb) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				gdb.shutdown();
			}
		});
	}

	/**
	 * Default constructor, to enable dependency injection. The path to the DB
	 * must be set afterwards.
	 * @see #setDbPath(String)
	 */
	public SequentialNeo4JDBReader() {
		logger = MSR4JLogger.getLogger(SequentialNeo4JDBReader.class.getCanonicalName());
	}

	public SequentialNeo4JDBReader(String path) {
		this();
		this.dbpath = path;
		this.pathToNeo4J = SequentialNeo4JDBReader.extractPathToNeo4J(this.dbpath);
	}

	/**
	 * Returns all Repository nodes in the back end db.
	 * 
	 * @return the set of all Repository nodes
	 * @see SequentialNeo4JDBReader#getRepositoriesOfType(RepositoryType)
	 */
	@Override
	public Set<Repository> getAllRepositories() {
		Set<Repository> repos = new HashSet<Repository>();
		for (RepositoryType t : RepositoryType.values()) {
			repos.addAll(getRepositoriesOfType(t));
		}
		return repos;
	}

	@Override
	public Set<String> getAllRepositoriesNames() {
		Set<Repository> repos = getAllRepositories();
		Set<String> result = new HashSet<String>();
		for (Repository r : repos) {
			result.add(r.getName());
		}
		return result;
	}

	/**
	 * Returns all occurrences of Repository nodes having the specified name.
	 * 
	 * @param name
	 *            the name of the repository nodes that are being looked up
	 * @return the set of all repository nodes having the specified name
	 * @see #getRepository(String)
	 * @deprecated
	 */
	public Set<Repository> getAllRepositories(String name) {
		Set<Repository> repos = new HashSet<Repository>();
		Iterable<Repository> rps = graph.getVertices(VEProperties.NAME.getName(), name, Repository.class);
		if (rps != null) {
			for (Repository r : rps) {
				repos.add(r);
			}
		}
		return repos;
	}

	/**
	 * Returns the set of all occurrences of Repository nodes having the names
	 * in the argument (array)
	 * 
	 * @param names
	 *            the array of repository names that are being looked up
	 * @return the set of all occurrence of Repository node for each name in the
	 *         argument.
	 * @see #getRepositories(String[])
	 */
	@Override
	public Set<Repository> getAllRepositories(String[] names) {
		Set<Repository> repos = new HashSet<Repository>();
		for (String s : names) {
			repos.addAll(getAllRepositories(s));
		}
		return repos;
	}

	public String getDbPath() {
		return dbpath;
	}

	/**
	 * Returns all file extensions available in the DB. Returns null otherwise.
	 * 
	 * @return the set of all file extensions available in the DB,
	 *         <code>null</code> if no file extension can be found.
	 */
	@Override
	public Set<String> getFileExtensions() {
		Set<String> result = null;
		Iterable<Repository> rps = getAllRepositories();
		if (rps != null) {
			result = new TreeSet<String>();
			Iterable<FileType> fts;
			for (Repository r : rps) {
				fts = r.getFileTypes();
				for (FileType ft : fts) {
					result.add(ft.getExtension());
				}
			}
			if (result.isEmpty()) {
				result = null;
			}
		}
		return result;
	}

	/**
	 * Returns the set of first occurrences of Repository node having the names
	 * in the argument (array).
	 * 
	 * @param names
	 *            the array of repository names that are being looked up
	 * @return the set of first occurrence of Repository node for each name in
	 *         the argument.
	 * @see #getAllRepositories(String[])
	 * @deprecated
	 */
	public Set<Repository> getRepositories(String[] names) {
		Set<Repository> repos = new HashSet<Repository>();
		Repository r;
		for (String s : names) {
			if ((r = getRepository(s)) != null) {
				repos.add(r);
			}
		}
		return repos;
	}

	/**
	 * Returns the map associating each repository with its set of file types.
	 * If there are no repository, returns <code>null</code>. If there are no
	 * file types for a specific repository, the associated valyue its key (in
	 * the corresponding entry) is <code>null</code>.
	 * 
	 * @return
	 */
	@Override
	public Map<Repository, Set<String>> getRepositoriesAndExtensions() {
		Map<Repository, Set<String>> result = null;
		Set<String> extensions = null;
		Iterable<Repository> rps = getAllRepositories();
		if (rps != null) {
			result = new HashMap<Repository, Set<String>>();
			Iterable<FileType> fts;
			for (Repository r : rps) {
				extensions = new TreeSet<String>();
				fts = r.getFileTypes();
				for (FileType ft : fts) {
					extensions.add(ft.getExtension());
				}
				if (extensions.isEmpty()) {
					extensions = null;
				}
				// null values permitted
				result.put(r, extensions);
			}
		}
		return result;
	}

	/**
	 * Returns all Repository nodes of the repository type specified as
	 * argument.
	 * 
	 * @param rt
	 *            the repository type of the nodes to be retrieved
	 * @return the set of all Repository nodes of the given type
	 * @see #getAllRepositories()
	 */
	@Override
	public Set<Repository> getRepositoriesOfType(RepositoryType rt) {
		Set<Repository> repos = new HashSet<Repository>();
		Iterable<Repository> rps = graph.getVertices(VEProperties.TYPE.getName(), rt.getName(), Repository.class);
		for (Repository r : rps) {
			repos.add(r);
		}
		return repos;
	}

	/**
	 * Returns the Repository node having the specified name. It is advised that
	 * the name be unique (thus used as id at the domain-specific level).
	 * 
	 * Node id is a long and automatically created by the underlying framework,
	 * so do not expect equals and hashcode to work on your domain-specific
	 * attributes.
	 * 
	 * @param name
	 *            the name of the repository which is being looked up
	 * @return a reference to the repository node, null otherwise
	 * @see #getAllRepositories(String)
	 * 
	 */
	@Override
	public Repository getRepository(String name) {
		Repository result = null;
		Iterable<Repository> rps = graph.getVertices(VEProperties.NAME.getName(), name, Repository.class);
		if (rps != null) {
			for (Repository r : rps) {
				if (r.getName().equalsIgnoreCase(name)) {
					result = r;
					logger.info("Found looked-for Repository: {}", r.getName());
					break;
				}
			}
			if (result == null) {
				logger.info("Could not find Repository: {} ", name);
			}
		}
		return result;
	}

	@Override
	public void openDB() {
		/* factory = new FramedGraphFactory(new GremlinGroovyModule()); */
		factory = new FramedGraphFactory();
		/* graph = factory.create(new Neo4jGraph(this.dbpath)); */
		/*
		 * graphDb = new
		 * GraphDatabaseFactory().newEmbeddedDatabase(this.dbpath).
		 */
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(this.dbpath)
				.loadPropertiesFromFile(this.pathToNeo4J + "conf" + System.getProperty("file.separator") + "neo4j.properties").newGraphDatabase();
		engine = new ExecutionEngine(graphDb, new BufferingLogger());
		/*SequentialNeo4JDBReader.registerShutdownHook(graphDb);*/
		graph = factory.create(new Neo4jGraph(graphDb));
		logger.info("Graph DB opened.");
	}

	@Override
	public void setDbPath(String dbpath) {
		this.dbpath = dbpath;
		this.pathToNeo4J = SequentialNeo4JDBReader.extractPathToNeo4J(this.dbpath);
	}

	public void shutDownDb() {
		if (graph != null) {
			graph.shutdown();
		}
		logger.info("Graph DB shut down.");
	}
}
