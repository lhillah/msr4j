/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.slf4j.Logger;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.datatypes.VEProperties;
import fr.lip6.msr4j.utils.config.MSR4JLogger;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.parsers.MimeTypeDetector;

/**
 * Root class for handling basic DB services.
 * 
 * @author lom
 */
public abstract class DBHandler<T> implements DBServices, IVertexEdgeFactory {

	/**
	 * @return the graph
	 */
	public static Graph getGraph() {
		return graph;
	}

	protected final Logger logger = MSR4JLogger.getLogger(this.getClass().getCanonicalName());

	private String dbPath;

	/**
	 * Working copies local paths.
	 */
	private PropertiesManager wcprop;

	/**
	 * Credentials to connect to the SCMs.
	 */
	private PropertiesManager credprop;
	/**
	 * Determine whether populate method was called from incremental method or
	 * from scratch.
	 */
	private boolean callFromIncremental;
	/**
	 * The reference to the graph handled by the underlying framework.
	 */
	private static Graph graph;

	/**
	 * @param graph
	 *            the graph to set
	 */
	public static void setGraph(Graph g) {
		DBHandler.graph = g;
	}

	/**
	 * SVN repos vertices associated with their names.
	 */
	private ConcurrentMap<String, Vertex> scmVertex;

	/**
	 * Committer vertices associated with their SVN ids.
	 */
	private ConcurrentMap<String, Vertex> cmVertex;

	/**
	 * Files vertices associated with their (unique) paths.
	 */
	private ConcurrentMap<String, Vertex> fileVertex;

	/**
	 * Files types vertices associated with their (unique) mimetype.
	 */
	private ConcurrentMap<String, Vertex> fileTypesVertex;

	/**
	 * Mime type detector.
	 */
	private MimeTypeDetector mtd;
	
	/**
	 * File vertex - File Type vertex
	 */
	private ConcurrentHashMap<Vertex, Vertex> fileToFileTypeMap;

	public DBHandler(String dbPath) {
		this.setDbPath(dbPath);
	}

	@Override
	public Edge addEdge(Vertex from, Vertex to, String relation) {
		return getGraph().addEdge(null, from, to, relation);
	}

	@Override
	public Vertex createCommitterVertex(String scmID, String name, String email, String webpage) {
		Vertex v = getGraph().addVertex(null);
		setVertexProperty(v, VEProperties.SCM_ID.getName(), scmID);
		setVertexProperty(v, VEProperties.NAME.getName(), name);
		setVertexProperty(v, VEProperties.EMAIL.getName(), email);
		setVertexProperty(v, VEProperties.PERSO_WEB_PAGE.getName(), webpage);
		return v;
	}

	@Override
	public Vertex createFileVertex(String path, String revision) {
		Vertex v = getGraph().addVertex(null);
		setVertexProperty(v, VEProperties.PATH.getName(), path);
		return v;
	}

	@Override
	public Vertex createSCMProjectVertex(String name, RepositoryType type, String uri, boolean isRoot) {
		Vertex v = getGraph().addVertex(null);
		setVertexProperty(v, VEProperties.NAME.getName(), name);
		setVertexProperty(v, VEProperties.TYPE.getName(), type.getName());
		setVertexProperty(v, VEProperties.URI.getName(), uri);
		setVertexProperty(v, VEProperties.IS_ROOT.getName(), isRoot);
		return v;
	}

	/**
	 * @return the Committers vertices map
	 */
	public ConcurrentMap<String, Vertex> getCmVertex() {
		return cmVertex;
	}

	/**
	 * @return the the SCM Credentials properties manager
	 */
	public PropertiesManager getCredentialsProp() {
		return this.credprop;
	}

	/**
	 * @return the dbPath
	 */
	public String getDbPath() {
		return dbPath;
	}

	@Override
	public String getDBPath() {
		return this.getDbPath();
	}

	/**
	 * Mime Type - File Type vertex
	 * 
	 * @return the fileTypesVertex
	 */
	public ConcurrentMap<String, Vertex> getFileTypesVertex() {
		return fileTypesVertex;
	}

	/**
	 * File path - File vertex
	 * 
	 * @return the fileVertex
	 */
	public ConcurrentMap<String, Vertex> getFileVertex() {
		return fileVertex;
	}

	/**
	 * File vertex - File Type vertex;
	 * 
	 * @return
	 */
	public ConcurrentHashMap<Vertex, Vertex> getFileToFileTypeMap() {
		return fileToFileTypeMap;
	}

	/**
	 * @return the mtd
	 */
	public MimeTypeDetector getMtd() {
		return mtd;
	}

	/**
	 * @return the scmVertex
	 */
	public ConcurrentMap<String, Vertex> getScmVertex() {
		return scmVertex;
	}

	/**
	 * @return the the working copies local paths properties
	 */
	public PropertiesManager getWorkingCopiesProp() {
		return this.wcprop;
	}

	/**
	 * Incrementally populates opened DB (batch insertion mode), possibly using
	 * DBHandler object passed as argument. That handler may hold many resources
	 * already opened (db, maps of vertices, etc.). A cast to the runtime type
	 * may be necessary to gain access to needed underlying API of the handler's
	 * actual type.
	 * 
	 * This method is typically used in the case of DB batch insertion started
	 * with an SCM type handler (e.g. SVN), then passed onto another SCM type
	 * handler (e.g. GIT). In this case, the DB is initially empty.
	 * 
	 * Its intent is different from incremental batch load of an existing DB
	 * which is not yet opened! This would be incremental batch load.
	 * 
	 * @see #incrementalBatchLoad()
	 * @param dbh
	 *            a DBHandler to possibly use for resource sharing.
	 */
	public abstract void incrementallyPopulate(DBHandler<Object> dbh);

	/**
	 * Incremental batch load of data into DB back end, assuming that the DB is
	 * not empty. For instance, after an update of an SCM, one may want to only
	 * load new updates from the SCM, not start a batch load from scratch, as
	 * with {@link #populateDB(Object)} and
	 * {@link #incrementallyPopulate(DBHandler)}
	 */
	public abstract void incrementalBatchLoad();

	/**
	 * Inits internal maps of vertices for files, file types, SCMs
	 * (repositories) and Committers. Map for commits is not created, their
	 * number in the SCM may be huge compared to files', leading into memory
	 * issues.
	 */
	public void initVerticesInternalCache() {
		setFileVertex(new ConcurrentHashMap<String, Vertex>());
		setFileTypesVertex(new ConcurrentHashMap<String, Vertex>());
		setScmVertex(new ConcurrentHashMap<String, Vertex>());
		setCmVertex(new ConcurrentHashMap<String, Vertex>());
	}

	/**
	 * @return the callFromIncremental
	 */
	public boolean isCallFromIncremental() {
		return callFromIncremental;
	}

	/**
	 * Basic API to populate a DB back end from scratch.
	 * 
	 * @param data
	 */
	public abstract void populateDB(T data);

	public void registerShutdownHook(final GraphDatabaseService gdb) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				gdb.shutdown();
				logger.info("Registered shutdown hook.");
			}
		});
	}

	@Override
	public void removeDb() {
		logger.info("Removing the Graph DB directory...");
		try {
			FileUtils.deleteDirectory(new File(getDbPath()));
		} catch (IOException e) {
			logger.error(e.getMessage());
			logger.error(Arrays.toString(e.getStackTrace()));
		}
		logger.info("Graph DB directory removed.");
	}

	/**
	 * @param callFromIncremental
	 *            the callFromIncremental to set
	 */
	public void setCallFromIncremental(boolean callFromIncremental) {
		this.callFromIncremental = callFromIncremental;
	}

	/**
	 * @param cmVertex
	 *            the cmVertex to set
	 */
	public void setCmVertex(ConcurrentMap<String, Vertex> cmVertex) {
		this.cmVertex = cmVertex;
	}

	/**
	 * @param credprop
	 *            the SCM Credentials properties manager to set
	 */
	public void setCredentialsProp(PropertiesManager credprop) {
		this.credprop = credprop;
	}

	/**
	 * @param dbPath
	 *            the dbPath to set
	 */
	public void setDbPath(String dbPath) {
		this.dbPath = dbPath;
	}

	@Override
	public void setDBPath(String dbPath) {
		this.setDbPath(dbPath);
	}

	@Override
	public void setEdgeProperty(Edge e, String propName, Object propValue) {
		if (propValue != null) {
			e.setProperty(propName, propValue);
		}
	}

	/**
	 * @param fileTypesVertex
	 *            the fileTypesVertex to set
	 */
	public void setFileTypesVertex(ConcurrentMap<String, Vertex> fileTypesVertex) {
		this.fileTypesVertex = fileTypesVertex;
	}
	
	/**
	 * 
	 * @param map the map associating each file vertex to its corresponding file type
	 */
	public void setFileToFileTypeMap(ConcurrentHashMap<Vertex, Vertex> map) {
		this.fileToFileTypeMap = map;
	}

	/**
	 * @param fileVertex
	 *            the fileVertex to set
	 */
	public void setFileVertex(ConcurrentMap<String, Vertex> fileVertex) {
		this.fileVertex = fileVertex;
	}

	/**
	 * @param mtd
	 *            the mtd to set
	 */
	public void setMtd(MimeTypeDetector mtd) {
		this.mtd = mtd;
	}

	/**
	 * @param scmVertex
	 *            the scmVertex to set
	 */
	public void setScmVertex(ConcurrentMap<String, Vertex> scmVertex) {
		this.scmVertex = scmVertex;
	}

	@Override
	public void setVertexProperty(Vertex v, String propName, Object propValue) {
		if (propValue != null) {
			v.setProperty(propName, propValue);
		}
	}

	/**
	 * @param wcprop
	 *            the working copies local paths properties to set
	 */
	public void setWorkingCopiesProp(PropertiesManager wcprop) {
		this.wcprop = wcprop;
	}

	@Override
	public void shutDownDb() {
		getGraph().shutdown();
		logger.info("Graph DB shut down.");
	}
}
