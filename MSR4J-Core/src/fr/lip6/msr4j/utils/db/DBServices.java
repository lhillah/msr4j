/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

/**
 * Provides basic services for handling the underlying DB.
 * 
 * @author lom
 * 
 */
public interface DBServices {

	/**
	 * Switch the path of the current DB to another location.
	 * 
	 * @param dbPath
	 */
	void setDBPath(String dbPath);

	/**
	 * Returns the path of the current DB
	 * 
	 * @return
	 */
	String getDBPath();

	/**
	 * Removes the current DB.
	 */
	void removeDb();

	/**
	 * Clean the current DB.
	 */
	void cleanDb();

	/**
	 * Creates a new DB.
	 */
	void createBatchDb();

	/**
	 * Shuts down the DB.
	 */
	void shutDownDb();

	/**
	 * Starts a transaction with the DB.
	 * 
	 * @return the transaction object
	 */
	Object beginTransaction();

	/**
	 * Closes a successful transaction.
	 * 
	 * @param t
	 *            the transaction to close
	 */
	void closeSuccessfullTransaction(Object o);
}
