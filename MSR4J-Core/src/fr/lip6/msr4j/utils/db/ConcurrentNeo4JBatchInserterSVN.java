/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.tika.mime.MimeTypeException;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.wc.SVNInfo;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCClient;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import fr.lip6.msr4j.datamodel.datatypes.RelationType;
import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.datatypes.SVNLogActionType;
import fr.lip6.msr4j.datamodel.datatypes.VEProperties;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.Paths;
import fr.lip6.msr4j.utils.config.PropertiesManager;
import fr.lip6.msr4j.utils.parsers.MimeTypeDetector;
import fr.lip6.msr4j.utils.parsers.SVNLogEntryActionsParser;
import fr.lip6.msr4j.utils.parsers.SVNLogEntryParser;
import fr.lip6.msr4j.utils.parsers.SVNParser;
import fr.lip6.msr4j.utils.repositories.CustomSVNInfoHandler;
import fr.lip6.msr4j.utils.strings.Printer;

public class ConcurrentNeo4JBatchInserterSVN<T> extends Neo4JDBHandler<T> {
	
	/**
	 * To count the number of vertices created.
	 */
	private long nbVertexCreated;

	/**
	 * To handle a pool of threads for tasks that can be partitioned.
	 */
	private final ExecutorService executorPool;

	/**
	 * Set of SVN names and their URLs <name, url>
	 */
	private Set<Entry<Object, Object>> svnProp;

	/**
	 * SVN Parser.
	 */
	private SVNParser svnparser;

	/**
	 * To work with entries in the local working copies of the repositories.
	 */
	private SVNWCClient wcc;

	/**
	 * SVN Info handler.
	 */
	private CustomSVNInfoHandler myHandler;

	private ConcurrentNeo4JBatchInserterSVN(String dbPath) {
		super(dbPath);
		executorPool = ConcurrencyConfig.getFixedThreadPoolExecutor();
	}

	/**
	 * Constructor.
	 * 
	 * @param dbPath
	 *            (Local) Path to the Neo4J DB.
	 * @param svnProp
	 *            Set of pairs where SVN URLs are listed (name - URL) as Strings
	 * @param wcprop
	 *            Properties manager, handling local paths of the SVNs working
	 *            copies
	 * @param credprop
	 *            Properties manager, handling SVNs credentials
	 */
	public ConcurrentNeo4JBatchInserterSVN(String dbPath, Set<Entry<Object, Object>> svnProp, PropertiesManager wcprop, PropertiesManager credprop) {
		this(dbPath);
		this.setSvnProp(svnProp);
		this.setWorkingCopiesProp(wcprop);
		this.setCredentialsProp(credprop);
		myHandler = new CustomSVNInfoHandler();
		setFileVertex(new ConcurrentHashMap<String, Vertex>());
		setFileTypesVertex(new ConcurrentHashMap<String, Vertex>());
		setFileToFileTypeMap(new ConcurrentHashMap<Vertex, Vertex>());
		setScmVertex(new ConcurrentHashMap<String, Vertex>());
		setCmVertex(new ConcurrentHashMap<String, Vertex>());
		new ConcurrentHashMap<SVNLogEntry, Map<String, RelationType>>();
	}

	@Override
	public Object beginTransaction() {
		throw new UnsupportedOperationException("This method is not supported in the SVN implementation");
	}

	@Override
	public void closeSuccessfullTransaction(Object o) {
		throw new UnsupportedOperationException("This method is not supported in the SVN implementation");
	}
	@Override
	public Vertex createCommitterVertex(Object o) {
		String cm = (String) o;
		Vertex v = getGraph().addVertex(null);
		setVertexProperty(v, VEProperties.SCM_ID.getName(), cm);
		return v;
	}
	@Override
	public Vertex createCommitterVertex(String scmID, String name, String email, String webpage) {
		throw new UnsupportedOperationException("This method is not supported in the SVN implementation");
	}

	@Override
	public Vertex createCommitVertex(Object o) {
		SVNLogEntry logEntry = (SVNLogEntry) o;
		Vertex v = getGraph().addVertex(null);
		setVertexProperty(v, VEProperties.REVISION.getName(), logEntry.getRevision());
		setVertexProperty(v, VEProperties.DATE.getName(), logEntry.getDate().getTime());
		setVertexProperty(v, VEProperties.MESSAGE.getName(), logEntry.getMessage());
		// TODO: Until actual contributor is properly parsed, we assume it is
		// the same as the committer (author)
		setVertexProperty(v, VEProperties.ACTUAL_CONTRIBUTOR.getName(), logEntry.getAuthor());
		return v;
	}

	@Override
	public Vertex createCommitVertex(String revision, long date, String message, String actualContributor) {
		throw new UnsupportedOperationException("This method is not supported in the SVN implementation");
	}

	@Override
	public Vertex createFileVertex(String path, String revision) {
		Vertex v = getFileVertex().get(path);
		String mime, extension;
		SVNNodeKind nodeKind;
		if (v == null) {
			v = getGraph().addVertex(null);
			getFileVertex().put(path, v);
			setVertexProperty(v, VEProperties.PATH.getName(), path);
			try {
				// SVNNodeKind nodeKind = svnparser.checkPath(en, revision);
				File f = new File(Paths.createSystemPath(getWorkingCopiesProp().getProperty(getSvnparser().getRepositoryName())
						+ path.substring(path.indexOf("/"))));
				if (!f.exists()) { // remote check
					logger.warn("File {} does not exist in local working copy of repository {}. Fetching info from server.", f.getAbsolutePath(),
							getSvnparser().getRepositoryName());
					try {
						nodeKind = getSvnparser().checkPath(path, Long.valueOf(revision));
					} catch (SVNException ex) {
						logger.error(ex.getMessage() + ":" + Arrays.toString(ex.getStackTrace()));
						nodeKind = SVNNodeKind.UNKNOWN;
						logger.error("Cannot fetch info from server for file {}. It does not exist on remote repository either."
								+ " Will set its file type to octet-stream.", f.getAbsolutePath());
						//ex = null;
					}
					// mime = mtd.getContentTypeForRemote(svnparser.getUrl() +
					// en.substring(en.indexOf("/")));
					// Actually, file is no more served by server via http,
					// although in the commits history.
					// Pay attention to mimetype of directories!
					// (text/directory)
					if (nodeKind != SVNNodeKind.DIR && nodeKind != SVNNodeKind.UNKNOWN) {
						mime = getMtd().guessContentTypeFor(path.substring(path.lastIndexOf("/") != -1 ? path.lastIndexOf("/") + 1 : 0));
					} else if (nodeKind != SVNNodeKind.UNKNOWN) {
						mime = "text/directory";
					} else {
						mime = "application/octet-stream";
					}

				} else { // local check - preferred
					try {
						getWcc().doInfo(f, SVNRevision.WORKING, SVNRevision.WORKING, SVNDepth.EMPTY, null, myHandler);
					} catch (SVNException ex) {
						logger.error(ex.getMessage());
						logger.error("Cannot fetch info on local copy of the file {}" + Printer.NL + " Aborting vertex construction for it.",
								f.getAbsolutePath());
						ex = null;
						return null;
					}
					SVNInfo info = myHandler.getInfo();
					nodeKind = info.getKind();
					mime = getMtd().getContentTypeFor(f.getAbsolutePath());
				}
				setVertexProperty(v, VEProperties.IS_DIRECTORY.getName(), (nodeKind == SVNNodeKind.DIR));
				// TODO : File Type!
				if (path.lastIndexOf(".") != -1) {
					extension = path.substring(path.lastIndexOf("."));
				} else {
					extension = "none";
				}
				Vertex ftv = getFileTypesVertex().get(mime);
				if (ftv == null) {
					ftv = getGraph().addVertex(null);
					getFileTypesVertex().put(mime, ftv);
				}

				setVertexProperty(ftv, VEProperties.TYPE.getName(), mime);
				setVertexProperty(ftv, VEProperties.EXTENSION.getName(), extension);

				// TODO : it is relevant to handle the programming language
				// property? It is a costly operation.
				@SuppressWarnings("unused")
				Edge e = addEdge(v, ftv, RelationType.FILE_TYPE.getName());
				// Put the association in internal map
				getFileToFileTypeMap().put(v, ftv);

			} catch (MimeTypeException | IOException | URISyntaxException e) {
				logger.error(e.getMessage());
				logger.error(Arrays.toString(e.getStackTrace()));
			}
		}
		return v;
	}

	@Override
	public Vertex createSCMProjectVertex(Object scmprop) {
		throw new UnsupportedOperationException("This method is not supported in the SVN implementation");
	}

	@Override
	public void establishCopyToRepByRelation(Object o) {
		@SuppressWarnings("unchecked")
		Entry<String, SVNLogEntryPath> en = (Entry<String, SVNLogEntryPath>) o;
		Edge ed;
		if (en.getValue().getCopyPath() != null) {
			Vertex v = getFileVertex().get(en.getKey());
			Vertex from = getFileVertex().get(en.getValue().getCopyPath());
			if (from != null) {
				if (String.valueOf(en.getValue().getType()).equalsIgnoreCase(SVNLogActionType.ADD.getName())) {
					ed = addEdge(from, v, RelationType.COP_TO.toString());
					setEdgeProperty(ed, VEProperties.COPY_REVISION.getName(), en.getValue().getCopyRevision());
				} else { // We assume it is a replacement otherwise
					ed = addEdge(from, v, RelationType.REP_BY.toString());
					setEdgeProperty(ed, VEProperties.REPLACEMENT_REVISION.getName(), en.getValue().getCopyRevision());
				}
			} else {
				logger.warn("Could not establish copy or replacement relationship for {}, from {}.", en.getValue().getPath(), en.getValue()
						.getCopyPath());
			}
		}
	}

	/**
	 * @return the executorPool
	 */
	public ExecutorService getExecutorPool() {
		return executorPool;
	}

	/**
	 * @return the nbVertexCreated
	 */
	public long getNbVertexCreated() {
		return nbVertexCreated;
	}

	/**
	 * @return the svnparser
	 */
	public SVNParser getSvnparser() {
		return svnparser;
	}

	/**
	 * @return the svnProp
	 */
	public Set<Entry<Object, Object>> getSvnProp() {
		return svnProp;
	}

	/**
	 * @return the wcc
	 */
	public SVNWCClient getWcc() {
		return wcc;
	}


	@Override
	public void incrementalBatchLoad() {
		throw new UnsupportedOperationException("This method is not yet supported in the SVN implementation");
	}

	@Override
	public void incrementallyPopulate(DBHandler<Object> dbh) {
		setWorkingCopiesProp(dbh.getWorkingCopiesProp());
		this.setMtd(dbh.getMtd());
		this.setCredentialsProp(dbh.getCredentialsProp());
		this.setScmVertex(dbh.getScmVertex());
		this.setFileVertex(dbh.getFileVertex());
		this.setFileTypesVertex(dbh.getFileTypesVertex());
		this.setCmVertex(dbh.getCmVertex());
		this.setCallFromIncremental(true);
		populateDB(null);
	}

	/**
	 * Inserts data from SVN logs. This method should be overriden if you have a
	 * very specific case study.
	 * 
	 * @param data
	 *            User-defined data container (not used in the core
	 *            implementation)
	 */
	private void insertSVNSCMData(T data) {
		Vertex svnV, commitV, cmV, fileV;
		@SuppressWarnings("rawtypes")
		Collection logEntries;
		SVNLogEntry logEntry;
		setMtd(new MimeTypeDetector());
		getMtd().setCredProp(getCredentialsProp());
		logger.info("Parsing SVNs logs.");
		for (Entry<Object, Object> ent : getSvnProp()) {

			setSvnparser(new SVNParser());
			getSvnparser().setUrl(ent.getValue().toString());
			getSvnparser().setRepositoryName(ent.getKey().toString());
			getSvnparser().setUserName(getCredentialsProp().getProperty(ent.getKey().toString() + "-user"));
			getSvnparser().setPassword(getCredentialsProp().getProperty(ent.getKey().toString() + "-passwd"));
			getMtd().setSVNParser(getSvnparser());
			try {
				getSvnparser().openRemoteHttpRepository();
				getSvnparser().checkRepositoryPath();
				setWcc(getSvnparser().getWCClient());
			} catch (SVNException ex) {
				logger.error(ex.getMessage());
				logger.error(Arrays.toString(ex.getStackTrace()));
				continue;
			}
			svnV = getScmVertex().get(ent.getKey().toString());
			if (svnV == null) {
				svnV = createSCMProjectVertex(ent.getKey().toString(), RepositoryType.SVN, ent.getValue().toString(), true);
				setNbVertexCreated(getNbVertexCreated() + 1);
				getScmVertex().put(ent.getKey().toString(), svnV);

			}
			try {
				logEntries = getSvnparser().getLogEntries(new String[] { "" }, null, 0, -1, true, true);
			} catch (SVNException e2) {
				logger.error(e2.getMessage());
				logger.error(Arrays.toString(e2.getStackTrace()));
				continue;
			}
			logger.info("Parsing logs of SVN: {}.", ent.getValue().toString());
			// <LogEntry, <FilePath, <Committer id, Long>>>
			final List<Callable<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> lePartition = new ArrayList<Callable<Map<SVNLogEntry, Map<String, Map<String, Long>>>>>();
			// <LogEntry, <FilePath, RelationType>>
			final List<Callable<Map<SVNLogEntry, Map<String, RelationType>>>> laPartition = new ArrayList<Callable<Map<SVNLogEntry, Map<String, RelationType>>>>();
			for (@SuppressWarnings("rawtypes")
			Iterator entries = logEntries.iterator(); entries.hasNext();) {
				SVNLogEntry entry = (SVNLogEntry) entries.next();
				lePartition.add(new SVNLogEntryParser(entry));
				laPartition.add(new SVNLogEntryActionsParser(entry));
			}
			List<Future<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> leValues = null;
			List<Future<Map<SVNLogEntry, Map<String, RelationType>>>> laValues = null;
			try {
				leValues = getExecutorPool().invokeAll(lePartition, ConcurrencyConfig.ALLOWED_EXEC_TIME100, TimeUnit.SECONDS);
				laValues = getExecutorPool().invokeAll(laPartition, ConcurrencyConfig.ALLOWED_EXEC_TIME100, TimeUnit.SECONDS);
				getExecutorPool().shutdown();

				if (leValues != null && laValues != null) {
					Iterator<Future<Map<SVNLogEntry, Map<String, Map<String, Long>>>>> leIt;
					Iterator<Future<Map<SVNLogEntry, Map<String, RelationType>>>> laIt;
					Map<SVNLogEntry, Map<String, Map<String, Long>>> logEMap;
					Map<String, Map<String, Long>> filesCommittersCount = new HashMap<String, Map<String, Long>>();
					// Files touch counts by committers
					Map<String, Long> committersCount;
					Map<SVNLogEntry, Map<String, RelationType>> logATMap;
					Edge e;
					Long count;
					logger.info("Counting committers touch counts for all files.");
					for (leIt = leValues.iterator(); leIt.hasNext();) {
						logEMap = leIt.next().get();
						for (Entry<SVNLogEntry, Map<String, Map<String, Long>>> logEMapEnt : logEMap.entrySet()) {
							// logEntry = logEMapEnt.getKey();
							for (Entry<String, Map<String, Long>> filesEnt : logEMapEnt.getValue().entrySet()) {
								committersCount = filesCommittersCount.get(filesEnt.getKey());
								if (committersCount == null) {
									committersCount = new HashMap<String, Long>();
									filesCommittersCount.put(filesEnt.getKey(), committersCount);
								}
								for (Entry<String, Long> cmsCount : filesEnt.getValue().entrySet()) {
									if (cmsCount.getKey() == null) { // No
																		// Author...
										continue;
									}
									// opportunity to add a new committer node
									cmV = getCmVertex().get(cmsCount.getKey());
									if (cmV == null) {
										cmV = createCommitterVertex(cmsCount.getKey());
										setNbVertexCreated(getNbVertexCreated() + 1);
										getCmVertex().put(cmsCount.getKey(), cmV);
										e = addEdge(cmV, svnV, RelationType.REGISTERED_IN.getName());
									}

									count = committersCount.get(cmsCount.getKey());
									if (count == null) {
										count = 0L;
									}
									count += cmsCount.getValue();
									committersCount.put(cmsCount.getKey(), count);
								}
							}
						}
					}
					leIt = leValues.iterator();
					laIt = laValues.iterator();
					logger.info("Creating Commit vertices, setting touch relationships between committers and files.");
					for (leIt.hasNext(); laIt.hasNext();) {
						logEMap = leIt.next().get();
						logATMap = laIt.next().get();
						for (Entry<SVNLogEntry, Map<String, Map<String, Long>>> logEMapEnt : logEMap.entrySet()) {
							logEntry = logEMapEnt.getKey();
							if (logEntry.getAuthor() == null) {
								continue;
							}
							cmV = getCmVertex().get(logEntry.getAuthor());
							commitV = createCommitVertex(logEntry);
							setNbVertexCreated(getNbVertexCreated() + 1);
							for (Entry<String, Map<String, Long>> filesEnt : logEMapEnt.getValue().entrySet()) {
								fileV = createFileVertex(filesEnt.getKey(), String.valueOf(logEntry.getRevision()));
								if (fileV == null) {
									continue;
								}
								setNbVertexCreated(getNbVertexCreated() + 1);
								e = addEdge(svnV, fileV, RelationType.CONTAIN.getName());
								e = addEdge(svnV, getFileToFileTypeMap().get(fileV), RelationType.CONTAIN_FILE_TYPE.getName());
								
								if (cmV != null) {
									e = addEdge(cmV, fileV, RelationType.TOUCH.getName());
									setEdgeProperty(e, VEProperties.COUNT.getName(),
											filesCommittersCount.get(filesEnt.getKey()).get(logEntry.getAuthor()));
									e = addEdge(cmV, commitV, RelationType.PROPAGATE.getName());

								} else {
									logger.warn("Committer {} of revision {} is unknown. Could not set propagate and touch relationships on"
											+ "file {}.", logEntry.getAuthor(), logEntry.getRevision(), filesEnt.getKey());
								}
								e = addEdge(commitV, fileV, logATMap.get(logEntry).get(filesEnt.getKey()).getName());
							}
							// Handles Impact relation -
							// TODO later : find first occurrence of branch or
							// tag creation
							e = addEdge(commitV, svnV, RelationType.IMPACT.toString());
						}
					}
					logger.info("Setting Cop_to and Rep_By relationships.");
					Entry<String, SVNLogEntryPath> en;
					for (@SuppressWarnings("rawtypes")
					Iterator entries = logEntries.iterator(); entries.hasNext();) {
						logEntry = (SVNLogEntry) entries.next();
						for (@SuppressWarnings("unchecked")
						Iterator<Entry<String, SVNLogEntryPath>> iterator = (Iterator<Entry<String, SVNLogEntryPath>>) logEntry.getChangedPaths()
								.entrySet().iterator(); iterator.hasNext();) {
							en = iterator.next();
							establishCopyToRepByRelation(en);
						}
					}
				} else if (leValues != null) {
					logger.error("Returned global result from parsing all log actions types is null");
				} else {
					logger.error("Returned global result from parsing all log entries is null");
				}

			} catch (InterruptedException iex) {
				logger.error(iex.getMessage());
				logger.error(Arrays.toString(iex.getStackTrace()));
			} catch (ExecutionException e1) {
				logger.error(e1.getMessage());
				logger.error(Arrays.toString(e1.getStackTrace()));
			}
			logger.info("Finished processing all logs of SVN {}. How many processed: {}.", ent.getValue().toString(), leValues.size());

		}
		logger.info("Finished processing all SVNs logs. How many processed: {}.", getSvnProp().size());
	}

	/**
	 * Entry point to start populating the DB back end from scratch. This method
	 * should be overriden if you have a very specific case study.
	 * 
	 * @param data
	 *            User-defined data container (not used in the core
	 *            implementation)
	 */
	@Override
	public void populateDB(T data) {
		setNbVertexCreated(0L);
		long start, end;
		start = System.nanoTime();
		logger.info("Starting batch insertion from SVN repositories.");
		start = System.nanoTime();
		if(!this.isCallFromIncremental()) {
			initVerticesInternalCache();
		}
		insertSVNSCMData(data);
		end = System.nanoTime();
		logger.info("End of batch insertion. Database must be shut down. Total time taken: {}.", (end - start) / ConcurrencyConfig.NANO);
		logger.info("Number of vertices created: {}", getNbVertexCreated());
	}

	/**
	 * @param nbVertexCreated
	 *            the nbVertexCreated to set
	 */
	public void setNbVertexCreated(long nbVertexCreated) {
		this.nbVertexCreated = nbVertexCreated;
	}

	/**
	 * @param svnparser
	 *            the svnparser to set
	 */
	public void setSvnparser(SVNParser svnparser) {
		this.svnparser = svnparser;
	}

	/**
	 * @param svnProp
	 *            the svnProp to set
	 */
	public void setSvnProp(Set<Entry<Object, Object>> svnProp) {
		this.svnProp = svnProp;
	}

	/**
	 * @param wcc
	 *            the wcc to set
	 */
	public void setWcc(SVNWCClient wcc) {
		this.wcc = wcc;
	}

}
