/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import org.neo4j.cypher.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.logging.BufferingLogger;

import com.tinkerpop.blueprints.impls.neo4j.batch.Neo4jBatchGraph;


public abstract class Neo4JDBHandler<T> extends DBHandler<T> {
	/**
	 * Neo4J database service handler.
	 */
	private GraphDatabaseService graphDb;

	private ExecutionEngine engine;

	public Neo4JDBHandler(String dbPath) {
		super(dbPath);
	}

	/**
	 * Cleans the Neo4J back end DB, without deleting it.
	 */
	@Override
	public void cleanDb() {
		logger.info("Cleaning Neo4J Graph DB...");
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(this.getDbPath());
		engine = new ExecutionEngine(graphDb, new BufferingLogger());
		registerShutdownHook(graphDb);
		if (graphDb != null) {
			engine.execute("START n = node(*), ref = node(0)  WHERE n<>ref DELETE n");
		}
		graphDb.shutdown();
		logger.info("Cleaned Graph DB.");
	}

	/**
	 * Creates a Neo4J Batch Graph back end instance for batch insertion.
	 */
	@Override
	public void createBatchDb() {
		logger.info("Creating Neo4J Graph DB...");
		setGraph(new Neo4jBatchGraph(getDbPath()));
		logger.info("Graph DB created.");
	}

}
