/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;

public interface IVertexEdgeFactory {
	/**
	 * Creates a vertex (node) for a repository.
	 * @param scmprop possibly an domain object representing
	 * this repository, or the property manager of SCM URLs
	 * (used in the Core implementation).
	 * @return
	 */
	Vertex createSCMProjectVertex(Object scmprop);
	/**
	 * Override this method is you have specific info about isRoot.
	 * Otherwise, use the default implementation.
	 * @see {@link DBHandler#createSCMProjectVertex(String, RepositoryType, String, boolean)}
	 */
	Vertex createSCMProjectVertex(String name, RepositoryType type, String uri, boolean isRoot);
	
	Vertex createCommitterVertex(Object cm);
	
	/**
	 * Override this method is you have specific info to use.
	 * Otherwise use the default implementation
	 * @param scmID the committer's id
	 * @param name the name of the committer
	 * @param email the email of the committer
	 * @param webpage the personal web page address of the committer
	 * @return vertex representing a committer node, with given properties set
	 * @see @link DBHandler#createCommitterVertex(Object)}
	 */
	Vertex createCommitterVertex(String scmID, String name, String email, String webpage);
	
	Vertex createCommitVertex(Object logEntry); 
	
	Vertex createCommitVertex(String revision, long date, String message, String actualContributor); 
	
	/**
	 * Override this method if you want to use the revision parameter.
	 * Otherwise, use the default implementation, where only the path
	 * property is set.
	 * @param path the (unique) path of the file
	 * @param revision the revision during which it is created
	 * @return the vertex created out of the path information
	 * @see {@link DBHandler#createFileVertex(String, String)}
	 */
	Vertex createFileVertex(String path, String revision);
	
	/**
	 * Etablishes "Copy to" and "Replaced by" between Files nodes
	 * in the DB, possibly using information from the object passed
	 * as parameter.
	 * @param en
	 */
	void establishCopyToRepByRelation(Object en);
	
	Edge addEdge(Vertex from, Vertex to, String relation);
	
	void setVertexProperty(Vertex v, String propName, Object propValue);
	
	void setEdgeProperty(Edge e, String propName, Object propValue);
}
