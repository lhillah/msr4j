/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.db;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.tika.mime.MimeTypeException;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.EmptyTreeIterator;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import fr.lip6.msr4j.datamodel.datatypes.RelationType;
import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;
import fr.lip6.msr4j.datamodel.datatypes.VEProperties;
import fr.lip6.msr4j.utils.config.ConcurrencyConfig;
import fr.lip6.msr4j.utils.config.Paths;
import fr.lip6.msr4j.utils.config.PropertiesManager;

/**
 * Batch insertion of data from Git repositories into DB back end.
 * 
 * This class was designed for extension by subclasses.
 * @author lom
 *
 * @param <T>
 */
public class SequentialNeo4JBatchInserterGIT<T> extends Neo4JDBHandler<T> {
	/**
	 * To count the number of vertices created.
	 */
	private static long nbVertexCreated;

	/**
	 * @return the nbVertexCreated
	 */
	public static long getNbVertexCreated() {
		return nbVertexCreated;
	}

	public static void setNbVertexCreated(long l) {
		nbVertexCreated = l;
	}

	/**
	 * Git repos URLs.
	 */
	private Set<Entry<Object, Object>> gitUrls;
	

	private SequentialNeo4JBatchInserterGIT(String dbPath) {
		super(dbPath);
	}

	public SequentialNeo4JBatchInserterGIT(String userDBPath, Set<Entry<Object, Object>> gitUrls, PropertiesManager wcprop, PropertiesManager credprop) {
		this(userDBPath);
		this.gitUrls = gitUrls;
		this.setWorkingCopiesProp(wcprop);
		this.setCredentialsProp(credprop);
	}

	@Override
	public Object beginTransaction() {
		throw new UnsupportedOperationException("This method is not supported in the GIT implementation");
	}

	private Vertex checkCreateStoreFileVertex(String path, RevCommit c,
			Entry<Object, Object> repoEntry) throws URISyntaxException, MimeTypeException, IOException {
		String mime, extension;
		int index;
		Vertex fv = getFileVertex().get(path);
		if (fv == null) {
			fv = createFileVertex(path, c.getId().name());
			setNbVertexCreated(getNbVertexCreated() + 1);
			getFileVertex().put(path, fv);
		}
		// Determine file type
		File f = new File(Paths.createSystemPath(getWorkingCopiesProp().getProperty((String) repoEntry.getKey()) + "/" + path));
		index = path.lastIndexOf(".");
		if (!f.exists()) {
			logger.warn("File {} does not exist in local working copy of repository. Doing guess work to determine its type.", f.getAbsolutePath());
			if (index != -1) { // Guess work, relying on conventions...
				mime = getMtd().guessContentTypeFor(path.substring(path.lastIndexOf("/") != -1 ? path.lastIndexOf("/") + 1 : 0));
			} else { // Presumably it is a directory, but not for certain.
				mime = "text/directory";
			}
		} else {
			mime = getMtd().getContentTypeFor(f.getAbsolutePath());
		}
		if (index != -1) {
			extension = path.substring(index);
		} else {
			extension = "none";
		}
		setVertexProperty(fv, VEProperties.IS_DIRECTORY.getName(), ("text/directory".equalsIgnoreCase(mime)));
		Vertex ftv = getFileTypesVertex().get(mime);
		if (ftv == null) {
			ftv = getGraph().addVertex(null);
			getFileTypesVertex().put(mime, ftv);
		}
		setVertexProperty(ftv, VEProperties.TYPE.getName(), mime);
		setVertexProperty(ftv, VEProperties.EXTENSION.getName(), extension);
		@SuppressWarnings("unused")
		Edge e = addEdge(fv, ftv, RelationType.FILE_TYPE.getName());
		return fv;
	}

	@Override
	public void closeSuccessfullTransaction(Object o) {
		throw new UnsupportedOperationException("This method is not supported in the GIT implementation");
	}

	@Override
	public Vertex createCommitterVertex(Object cm) {
		throw new UnsupportedOperationException("This method is not supported in the GIT implementation");
	}


	@Override
	public Vertex createCommitVertex(Object logEntry) {
		throw new UnsupportedOperationException("This method is not supported in the GIT implementation");
	}

	@Override
	public Vertex createCommitVertex(String revision, long date, String message, String actualContributor) {
		Vertex v = getGraph().addVertex(null);
		setVertexProperty(v, VEProperties.REVISION.getName(), revision);
		setVertexProperty(v, VEProperties.DATE.getName(), date);
		setVertexProperty(v, VEProperties.MESSAGE.getName(), message);
		// TODO: Until actual contributor is properly parsed, we assume it is
		// the same as the committer (author)
		setVertexProperty(v, VEProperties.ACTUAL_CONTRIBUTOR.getName(), actualContributor);
		return v;
	}

	@Override
	public Vertex createSCMProjectVertex(Object scmprop) {
		throw new UnsupportedOperationException("This method is not supported in the GIT implementation");
	}

	@Override
	public void establishCopyToRepByRelation(Object en) {
		throw new UnsupportedOperationException("This method is not supported in the GIT implementation");
	}

	@Override
	public void incrementallyPopulate(DBHandler<Object> dbh) {
		setWorkingCopiesProp(dbh.getWorkingCopiesProp());
		this.setMtd(dbh.getMtd());
		this.setCredentialsProp(dbh.getCredentialsProp());
		this.setScmVertex(dbh.getScmVertex());
		this.setFileVertex(dbh.getFileVertex());
		this.setFileTypesVertex(dbh.getFileTypesVertex());
		this.setCmVertex(dbh.getCmVertex());
		this.setCallFromIncremental(true);
		populateDB(null);
	}

	private void insertGitSCMData() {
		String msg;
		Vertex scmV, commitV, cmV, fileV, fileCp;
		Edge edge;
		File repoDir;
		Repository repo;
		Git git;
		RevWalk walk;
		Iterable<RevCommit> logs;
		RevCommit c;
		ByteArrayOutputStream out;
		DiffFormatter df;
		PersonIdent auth, cmtr;
		List<DiffEntry> entries = new ArrayList<DiffEntry>();
		RevCommit parent = null;
		FileRepositoryBuilder fbr = new FileRepositoryBuilder();

		setNbVertexCreated(0L);
		try {
			for (Entry<Object, Object> e : gitUrls) {
				repoDir = new File(Paths.createSystemPath(getWorkingCopiesProp().getProperty(e.getKey().toString()) + "/.git"));
				scmV = getScmVertex().get(e.getKey().toString());
				if (scmV == null) {
					scmV = createSCMProjectVertex(e.getKey().toString(), RepositoryType.GIT, e.getValue().toString(), true);
					setNbVertexCreated(getNbVertexCreated() + 1);
					getScmVertex().put(e.getKey().toString(), scmV);
				}

				repo = fbr.setGitDir(repoDir).readEnvironment().findGitDir().build();
				git = new Git(repo);
				walk = new RevWalk(repo);
				logs = git.log().call();
				out = new ByteArrayOutputStream();
				df = new DiffFormatter(out);
				df.setRepository(repo);
				df.setDetectRenames(true);
				parent = null;

				for (RevCommit comm : logs) {
					c = walk.parseCommit(comm);
					auth = c.getAuthorIdent();
					cmtr = c.getCommitterIdent();
					msg = c.getFullMessage();
					logger.info("Revision: {} \n\tDate: {}\n\tCommitter: {}\n\tEmail: {}\n\tMessage: {}", c.getId().toString(), cmtr.getWhen(),
							cmtr.getName(), cmtr.getEmailAddress(), c.getFullMessage());
					commitV = createCommitVertex(c.getId().name(), cmtr.getWhen().getTime(), msg, auth.getName());
					setNbVertexCreated(getNbVertexCreated() + 1);

					cmV = createCommitterVertex(cmtr.getName(), cmtr.getName(), cmtr.getEmailAddress(), null);
					setNbVertexCreated(getNbVertexCreated() + 1);
					getCmVertex().put(cmtr.getName(), cmV);
					edge = addEdge(cmV, commitV, RelationType.PROPAGATE.getName());
					edge = addEdge(cmV, scmV, RelationType.REGISTERED_IN.getName());

					logger.info("DIFFS IN THIS COMMIT (How many Parents ? --> {} )", c.getParentCount());
					entries.clear();
					if (c.getParentCount() > 0 && c.getParent(0) != null) {
						for (int i = 0; i < c.getParentCount(); i++) {
							parent = walk.parseCommit(c.getParent(i).getId());
							entries.addAll(df.scan(parent.getTree(), c.getTree()));
						}
					} else {
						entries.addAll(df.scan(new EmptyTreeIterator(), new CanonicalTreeParser(null, walk.getObjectReader(), c.getTree())));
					}
					for (DiffEntry entry : entries) {
						df.format(entry);
						logger.info("Changed Type ---> {}", entry.getChangeType().toString());
						logger.info("Old Path ---> {}", entry.getOldPath());
						logger.info("New Path ---> {}\n", entry.getNewPath());
						switch (entry.getChangeType()) {
						case ADD:
							fileV = checkCreateStoreFileVertex(entry.getNewPath(), c, e);
							edge = addEdge(commitV, fileV, RelationType.ADD.getName());
							break;
						case DELETE:
							fileV = checkCreateStoreFileVertex(entry.getOldPath(), c, e);
							edge = addEdge(commitV, fileV, RelationType.DELETE.getName());
							break;
						case MODIFY:
							fileV = checkCreateStoreFileVertex(entry.getOldPath(), c, e);
							edge = addEdge(commitV, fileV, RelationType.MODIFY.getName());
							break;
						case COPY:
							fileV = checkCreateStoreFileVertex(entry.getOldPath(), c, e);
							fileCp = checkCreateStoreFileVertex(entry.getNewPath(), c, e);
							edge = addEdge(commitV, fileV, RelationType.COPY.getName());
							edge = addEdge(commitV, fileCp, RelationType.ADD.getName());
							edge = addEdge(fileV, fileCp, RelationType.COP_TO.getName());
							setEdgeProperty(edge, VEProperties.COPY_REVISION.getName(), c.getId().name());
							break;
						case RENAME:
							fileV = checkCreateStoreFileVertex(entry.getOldPath(), c, e);
							fileCp = checkCreateStoreFileVertex(entry.getNewPath(), c, e);
							edge = addEdge(commitV, fileV, RelationType.RENAME.getName());
							edge = addEdge(commitV, fileCp, RelationType.ADD.getName());
							edge = addEdge(fileV, fileCp, RelationType.REN_TO.getName());
							setEdgeProperty(edge, VEProperties.RENAME_REVISION.getName(), c.getId().name());
							break;
						default:
							break;
						}
						out.reset();
					}
				}
				df.release();
				walk.release();
				walk.dispose();
			}
		} catch (IOException | GitAPIException | URISyntaxException | MimeTypeException e1) {
			logger.error(e1.getMessage());
			logger.error(Arrays.toString(e1.getStackTrace()));
		}
	}

	@Override
	public void populateDB(T data) {
		long start, end;
		logger.info("Starting batch insertion from Git repositories.");
		start = System.nanoTime();
		if (!this.isCallFromIncremental()) {
			initVerticesInternalCache();
		}
		insertGitSCMData();
		end = System.nanoTime();
		logger.info("End of batch insertion from Git repositories. Database must be shut down. Total time taken: {}.", (end - start)
				/ ConcurrencyConfig.NANO);
		logger.info("Number of vertices created: {}", getNbVertexCreated());
	}

	@Override
	public void incrementalBatchLoad() {
		throw new UnsupportedOperationException("This method is not yet supported in the GIT implementation");
	}
}
