/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.parsers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;

import fr.lip6.msr4j.utils.authentication.CustomAuthenticator;
import fr.lip6.msr4j.utils.authentication.CustomizedHostnameVerifier;
import fr.lip6.msr4j.utils.config.PropertiesManager;

/**
 * This class helps finding out files mimetypes and extensions.
 * @author lom
 *
 */
public final class MimeTypeDetector {

	private final Metadata metadata ;
	private final MimeTypes mimeTypes;
	private SVNParser svnparser;
	private final Tika tika; 
	private PropertiesManager credProp;

	/**
	 * It is advised to use the {@ link #MimeTypeDetector(SVNParser)} constructor
	 * instead, but if for some reason you must use this one first, then provide
	 * the SVNParser object by calling the {@link #setSVNParser(SVNParser)} method.
	 * @see #MimeTypeDetector(SVNParser) 
	 */
	public MimeTypeDetector() {
		 metadata = new Metadata();
		 mimeTypes = TikaConfig.getDefaultConfig().getMimeRepository();
		 tika = new Tika();
		HttpsURLConnection.setDefaultHostnameVerifier(new CustomizedHostnameVerifier());
		
	}
	
	/**
	 * Recommanded constructor, in particular because SVNParser object passed 
	 * as argument contains user credentials to deal with secured SVN URLs.
	 * @param parser
	 * @see #setSVNParser(SVNParser) 
	 */
	public MimeTypeDetector(SVNParser parser) {
		this();
		this.setSVNParser(svnparser);	
	}
	
	/**
	 * Returns content type of file as mimetype, among those 
	 * registered in {@link http://svn.apache.org/repos/asf/tika/trunk/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml}.
	 * @param filePath the local path of the file in the file system.
	 * @return the mimetype of the file
	 * @throws IOException
	 * @throws MimeTypeException
	 */
	public String getContentTypeFor(String filePath) throws IOException, MimeTypeException {
		File f = new File(filePath);
		metadata.set(Metadata.RESOURCE_NAME_KEY, f.getName());
		if(f.isDirectory()){
			return "text/directory";
		}
		InputStream fis = new BufferedInputStream(new FileInputStream(f));
		return mimeTypes.detect(fis, metadata).toString();
	}
	
	/**
	 * Returns the content type of remote file as mimetype, among those registered
	 * in  {@link http://svn.apache.org/repos/asf/tika/trunk/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml}.
	 * @param fileUrl the remote file URL
	 * @return the mimetype of the remote file
	 * @throws IOException
	 * @throws MimeTypeException
	 */
	/* System.setProperty("javax.net.ssl.keyStorePassword",<password of the keystore>); */
	public String getContentTypeForRemote(String fileUrl) throws IOException, MimeTypeException {
		URL url = new URL(fileUrl);
		metadata.set(Metadata.RESOURCE_NAME_KEY, fileUrl);
		if (Boolean.valueOf(credProp.getProperty("server.uses.https"))) {
			System.setProperty("https.protocols", credProp.getProperty("https.protocols"));
			System.setProperty("force.http.jre.executor", credProp.getProperty("force.http.jre.executor"));
		}
		if (Boolean.valueOf(credProp.getProperty("server.self.signed.certificate"))) {
			System.setProperty("javax.net.ssl.keyStore", credProp.getProperty("javax.net.ssl.keyStore"));
			System.setProperty("javax.net.ssl.trustStore", credProp.getProperty("javax.net.ssl.trustStore"));
		}
		InputStream in = new BufferedInputStream(url.openStream());
		return mimeTypes.detect(in, metadata).toString();
	}
	
	/**
	 * Returns the preferred file extension of the mimetype given as parameter.
	 * @param mime the mimetype for which the file extension is asked for
	 * @return the preferred file extension, empty string if unknown
	 * @throws MimeTypeException
	 */
	public String getExtensionOf(String mime) throws MimeTypeException {
		return mimeTypes.getRegisteredMimeType(mime).getExtension();
	}
	
	/**
	 * Returns the list of known extension of the mimetype given as parameter.
	 * @param mime the mimetype for which the known file extensions ares asked for
	 * @return the list of all known file extensions of this mimetype.
	 * @throws MimeTypeException
	 */
	public List<String> getExtensionsOf(String mime) throws MimeTypeException {
		return mimeTypes.getRegisteredMimeType(mime).getExtensions();
	}

	/**
	 * If you are dealing with a secured SVN URL, you will need to call this method at some
	 * point in your code, to allow this class to pick the username and password from 
	 * the SVNParser object.
	 * @param sp
	 */
	public void setSVNParser(SVNParser sp) {
		this.svnparser = sp;
		Authenticator.setDefault(new CustomAuthenticator(svnparser.getUserName(), svnparser.getPassword()));
	}
	
	/**
	 * Guesses the content type of a file based on its name only.
	 * @param substring
	 * @return
	 */
	
	public String guessContentTypeFor(String name) {
		return tika.detect(name);
	}

	/**
	 * @return the credProp
	 */
	public PropertiesManager getCredProp() {
		return credProp;
	}

	/**
	 * If you processed a repository using SSLv3, and self-signed certificate,
	 * you must set this attribute so that this object get access to the corresponding properties.
	 *
	 * @param credProp the credProp to set. References the credentials properties map
	 */
	public void setCredProp(PropertiesManager credProp) {
		this.credProp = credProp;
	}
}
