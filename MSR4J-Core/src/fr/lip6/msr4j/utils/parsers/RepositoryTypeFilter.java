/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.parsers;

import java.io.File;
import java.io.FileFilter;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;

/**
 * This class implements a filter that retains known SCM types proprietary folder:
 * .svn, .git, .hg.
 * @author lom
 *
 */
public class RepositoryTypeFilter implements FileFilter {

	private RepositoryType type;

	public RepositoryTypeFilter() {
		super();
	}

	public RepositoryType getRepositoryType() {
		return this.type;
	}

	public void resetRepositoryType() {
		this.type = null;
	}

	@Override
	public boolean accept(File dir) {
		boolean result = true;
		String name;
		if (dir.isDirectory() && dir.isHidden()) {
			name = dir.getName();
			if (".svn".contains(name) || "svn".contains(name)) {
				this.type = RepositoryType.SVN;
			} else if (".git".contains(name) || "git".contains(name)) {
				this.type = RepositoryType.GIT;
			} else if (".hg".contains(name) || "hg".contains(name)) {
				this.type = RepositoryType.MERCURIAL;
			} else {
				result = false;
			}
		} else {
			result = false;
		}
		return result;
	}

}
