/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.parsers;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;

import fr.lip6.msr4j.utils.config.MSR4JLogger;
/**
 * Gets a log entry and parses it into an associative map
 * between that entry and the map associating changed paths to committers' ids and touches count
 * for each committer per changed path.
 * @author lom
 *
 */
public final class SVNLogEntryParser implements Callable<Map<SVNLogEntry, Map<String, Map<String, Long>>>> {

	private final SVNLogEntry logEntry;
	private final Logger logger = MSR4JLogger.getLogger(this.getClass().getCanonicalName());

	public SVNLogEntryParser(SVNLogEntry logEntry) {
		this.logEntry = logEntry;
	}

	/**
	 * <SVNLogEntry <ChangedPath, <CommitterId, TouchesCount>>>
	 */
	@Override
	public Map<SVNLogEntry, Map<String, Map<String, Long>>> call() throws Exception {

		// <Committer Vertex, Long>
		Map<String, Long> cmVTouchesCount;
		// <FilePath <Committer id, Long>>
		Map<String, Map<String, Long>> fileTouchesCount = new HashMap<String, Map<String, Long>>();
		Map<SVNLogEntry, Map<String, Map<String, Long>>> leEntries = new HashMap<SVNLogEntry, Map<String, Map<String, Long>>>();
		Long counts;

		if (logEntry.getChangedPaths().size() > 0) {
			@SuppressWarnings("unchecked")
			Set<Entry<String, SVNLogEntryPath>> changedPaths = (Set<Entry<String, SVNLogEntryPath>>) logEntry.getChangedPaths().entrySet();
			for (Entry<String, SVNLogEntryPath> en : changedPaths) {
				cmVTouchesCount = fileTouchesCount.get(en.getKey());
				if (cmVTouchesCount == null) {
					cmVTouchesCount = new HashMap<String, Long>();
					fileTouchesCount.put(en.getKey(), cmVTouchesCount);
				}
				counts = cmVTouchesCount.get(logEntry.getAuthor());
				if (counts == null) {
					counts = 0L;
				}
				counts++;
				cmVTouchesCount.put(logEntry.getAuthor(), counts);
			}
		} else {
			logger.info("No changed paths for revision {} --> Author: {}.", logEntry.getRevision(), logEntry.getAuthor());
		}
		leEntries.put(logEntry, fileTouchesCount);
		logger.info("Finished parsing log entry for revision {}.", logEntry.getRevision());
		return leEntries;
	}
}