/**
 *  Copyright 2013 University Pierre & Marie Curie - UMR CNRS 7606 (LIP6/MoVe)
 *  All rights reserved.   This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Initial contributor:
 *    Lom M. Hillah - <lom-messan.hillah@lip6.fr>
 *
 *  Mailing list:
 *    lom-messan.hillah@lip6.fr
 */
package fr.lip6.msr4j.utils.parsers;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import fr.lip6.msr4j.datamodel.datatypes.RepositoryType;

/**
 * This class detects the type of an SCM, the local copy path of which is provided.
 * If the provided path does not exist or it is a file rather than a directory, 
 * then the returned map contains <code>null</code> as the value associated to the 
 * key (value of which is the provided path), in the only entry that map contains.
 * 
 * If the SCM type is not supported then {@link RepositoryType.UNSUPPORTED} is returned.
 * @author lom
 *
 */
public class SCMTypeDetector implements Callable<Map<String, RepositoryType>>{

	private String path;
	private RepositoryTypeFilter scmTypeFilter;

	public SCMTypeDetector(String localPath) {
		this.setPath(localPath);
		scmTypeFilter = new RepositoryTypeFilter();
	}

	@Override
	public Map<String, RepositoryType> call() throws Exception {
		Map<String, RepositoryType> result = new HashMap<String, RepositoryType>();
		File dir = new File(path);
		File[] children;
		if (dir.exists() && dir.isDirectory()) {
			children = dir.listFiles(scmTypeFilter);
			if(children != null && children.length > 0){
				result.put(path, scmTypeFilter.getRepositoryType());
			} else {
				result.put(path, RepositoryType.UNSUPPORTED);
			}
			
		} else {
			// Either scm path does not exist or it is a file rather than a directory
			result.put(path, null);
		}
		return result;
	}

	/**
	 * @return the path to the local copy of the SCM that was set.
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

}
